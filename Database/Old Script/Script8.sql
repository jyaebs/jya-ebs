USE [JYA_EBS]
GO

/****** Object:  Table [dbo].[DB_VENDOR]    Script Date: 1/26/2021 10:49:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DB_VENDOR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CNIC] [nvarchar](15) NOT NULL,
	[LocationTypeId] [int] NOT NULL,
	[LocationId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[NTN] [nvarchar](50) NULL,
	[IsCredit] [bit] NOT NULL,
	[CreditDays] [int] NULL,
	[CreditAmount] [decimal](18, 0) NULL,
	[ContactPerson] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](16) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[AccountReceivable] [int] NULL,
	[AccountReceivableDescription] [nvarchar](500) NULL,
	[DownPaymentAccount] [int] NULL,
	[DownPaymentAccountDescription] [nvarchar](500) NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_VENDOR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_DEV_LOCATION_TYPE] FOREIGN KEY([LocationTypeId])
REFERENCES [dbo].[DB_DEV_LOCATION_TYPE] ([Id])
GO

ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_DEV_LOCATION_TYPE]
GO

ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO

ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_LOCATION]
GO

ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_VENDOR_TYPE] FOREIGN KEY([TypeId])
REFERENCES [dbo].[DB_VENDOR_TYPE] ([Id])
GO

ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_VENDOR_TYPE]
GO


