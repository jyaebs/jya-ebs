DROP TABLE DB_CUSTOMER
DROP TABLE DB_DEV_CUSTOMER_TYPE

/****** Object:  Table [dbo].[DB_CUSTOMER_TYPE]    Script Date: 1/18/2021 5:22:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DB_CUSTOMER_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_CUSTOMER_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[DB_CUSTOMER]    Script Date: 1/18/2021 5:22:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DB_CUSTOMER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CNIC] [nvarchar](15) NOT NULL,
	[TypeId] [int] NOT NULL,
	[IsCredit] [bit] NOT NULL,
	[CreditDays] [int] NULL,
	[CreditAmount] [decimal](18, 0) NULL,
	[ContactPersonName] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](16) NULL,
	[Email] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[AccoutnReceivable] [int] NOT NULL,
	[AccoutnReceivableDescription] [nvarchar](500) NULL,
	[DownPaymentAccount] [int] NOT NULL,
	[DownPaymentAccountDescription] [nvarchar](500) NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DB_CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_DB_CUSTOMER_DB_CUSTOMER_TYPE] FOREIGN KEY([TypeId])
REFERENCES [dbo].[DB_CUSTOMER_TYPE] ([Id])
GO

ALTER TABLE [dbo].[DB_CUSTOMER] CHECK CONSTRAINT [FK_DB_CUSTOMER_DB_CUSTOMER_TYPE]
GO
