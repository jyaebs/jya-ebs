USE [master]
GO
/****** Object:  Database [JYA_EBS]    Script Date: 3/12/2021 11:15:48 AM ******/
CREATE DATABASE [JYA_EBS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JYA_EBS', FILENAME = N'D:\Databases\JYA_EBS.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'JYA_EBS_log', FILENAME = N'D:\Databases\JYA_EBS_log.ldf' , SIZE = 204800KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JYA_EBS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JYA_EBS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JYA_EBS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JYA_EBS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JYA_EBS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JYA_EBS] SET ARITHABORT OFF 
GO
ALTER DATABASE [JYA_EBS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JYA_EBS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JYA_EBS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JYA_EBS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JYA_EBS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JYA_EBS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JYA_EBS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JYA_EBS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JYA_EBS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JYA_EBS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [JYA_EBS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JYA_EBS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JYA_EBS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JYA_EBS] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [JYA_EBS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JYA_EBS] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [JYA_EBS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JYA_EBS] SET RECOVERY FULL 
GO
ALTER DATABASE [JYA_EBS] SET  MULTI_USER 
GO
ALTER DATABASE [JYA_EBS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JYA_EBS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JYA_EBS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JYA_EBS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [JYA_EBS]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitList]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitList] (@list VARCHAR(MAX), @separator VARCHAR(MAX) = ';')
RETURNS @table TABLE (Value VARCHAR(MAX))
AS BEGIN
  DECLARE @position INT, @previous INT
  SET @list = @list + @separator
  SET @previous = 1
  SET @position = CHARINDEX(@separator, @list)
  WHILE @position > 0 
  BEGIN
    IF @position - @previous > 0
      INSERT INTO @table VALUES (SUBSTRING(@list, @previous, @position - @previous))
    IF @position >= LEN(@list) BREAK
      SET @previous = @position + 1
      SET @position = CHARINDEX(@separator, @list, @previous)
  END
  RETURN
END

GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuditLog]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLog](
	[Id] [uniqueidentifier] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[EventType] [char](1) NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[EntityId] [nvarchar](100) NOT NULL,
	[ColumnName] [nvarchar](50) NOT NULL,
	[OriginalValue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL,
	[RelationalColumnName] [nvarchar](100) NULL,
	[RelationalColumnValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_BANK]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_BANK](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_BANK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_BANK_BRANCH]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_BANK_BRANCH](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BankId] [int] NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[AccountBankBalance] [nvarchar](15) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_BANK_BRANCH] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_CHART_OF_ACCOUNT]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_CHART_OF_ACCOUNT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[ParentCode] [nvarchar](15) NULL,
	[LevelID] [int] NOT NULL,
	[AccountTypeId] [int] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_CHART_OF_ACCOUNT] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_CITY]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_CITY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[Code] [nvarchar](25) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[DialingCode] [nvarchar](5) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_CITY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_COUNTRY]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_COUNTRY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[DialingCode] [nvarchar](5) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](256) NULL,
 CONSTRAINT [PK_DB_COUNTRY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_CUSTOMER]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_CUSTOMER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[CustomerTypeId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CNIC] [nvarchar](15) NOT NULL,
	[IsCredit] [bit] NOT NULL,
	[CreditDays] [int] NULL,
	[CreditAmount] [decimal](18, 0) NULL,
	[ContactPersonName] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](16) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[NTN] [nvarchar](50) NOT NULL,
	[AccountReceivable] [nvarchar](15) NOT NULL,
	[DownPaymentAccount] [nvarchar](15) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_CUSTOMER_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_CUSTOMER_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_CUSTOMER_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_CHART_OF_ACCOUNT_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_FISCAL_YEAR]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_FISCAL_YEAR](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [char](4) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_FISCAL_YEAR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[TabName] [nvarchar](100) NOT NULL,
	[FieldName] [nvarchar](100) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_LOCATION_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_LOCATION_TYPE](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_LOCATION_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PRIVILEGES]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PRIVILEGES](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Privilege] [nvarchar](60) NOT NULL,
	[Category] [nvarchar](100) NOT NULL,
	[Portal] [nvarchar](60) NOT NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_DB_DEV_PRIVILEGES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PRODUCT_PRICE_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PRODUCT_PRICE_TYPE](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_PRODUCT_PRICE_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PRODUCT_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PRODUCT_TYPE](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_PRODUCT_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PURCHASE_ORDER_STATUS]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PURCHASE_ORDER_STATUS](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_PURCHASE_ORDER_STATUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PURCHASE_QUOTATION_STATUS]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PURCHASE_QUOTATION_STATUS](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_PURCHASE_QUOTATION_STATUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PURCHASE_REQUISITION_STATUS]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PURCHASE_REQUISITION_STATUS](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_PURCHASE_REQUISITION_STATUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_PURCHASE_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_PURCHASE_TYPE](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_PURCHASE_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_SALE_ORDER_STATUS]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_SALE_ORDER_STATUS](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_SALE_ORDER_STATUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DEV_SALE_QUOTATION_STATUS]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DEV_SALE_QUOTATION_STATUS](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
 CONSTRAINT [PK_DB_DEV_SALE_QUOTATION_STATUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_DRIVER]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_DRIVER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsAvailable] [bit] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_DRIVER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_GL_ACCOUNT_INTEGRATION]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FiscalYearId] [int] NOT NULL,
	[FieldId] [int] NOT NULL,
	[AccountCode] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_DB_GL_ACCOUNT_INTEGRATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_GST]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_GST](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [decimal](4, 1) NOT NULL,
	[AccountGST] [nvarchar](15) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_GST] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_LOCATION]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_LOCATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[CountryId] [int] NOT NULL,
	[CityId] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[IsLeaf] [bit] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[IsWarehouse] [bit] NOT NULL,
	[IsCompanyWarehouse] [bit] NOT NULL,
	[Image] [nvarchar](1000) NULL,
	[STRN] [nvarchar](50) NULL,
	[NTN] [nvarchar](50) NULL,
	[ContactNo] [nvarchar](50) NULL,
	[AlternateContactNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_LOCATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRIMARY_UOM]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRIMARY_UOM](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_PRIMARY_UOM] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRODUCT]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRODUCT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[BarCode] [varchar](50) NULL,
	[ProductTypeId] [int] NOT NULL,
	[ProductBrandId] [int] NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[ProductSubCategoryId] [int] NOT NULL,
	[PrimaryUomId] [int] NOT NULL,
	[SecondaryUomId] [int] NULL,
	[ConverstionFactor] [decimal](5, 1) NULL,
	[Image] [nvarchar](1000) NULL,
	[IsTrendyItem] [bit] NOT NULL,
	[DepreciationMethod] [char](1) NULL,
	[DepreciationType] [char](1) NULL,
	[DepreciationRate] [decimal](4, 1) NULL,
	[UsageLife] [int] NULL,
	[ScrapeValue] [decimal](7, 1) NULL,
	[InventoryAccount] [nvarchar](15) NOT NULL,
	[COGSAccount] [nvarchar](15) NOT NULL,
	[DepreciationAccount] [nvarchar](15) NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_PRODUCT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRODUCT_BRAND]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRODUCT_BRAND](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[IsTrendy] [bit] NOT NULL,
 CONSTRAINT [PK_DB_PRODUCT_BRAND] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRODUCT_CATEGORY]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRODUCT_CATEGORY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_PRODUCT_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRODUCT_PRICE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRODUCT_PRICE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductPriceTypeId] [int] NOT NULL,
	[LocationId] [int] NULL,
	[ProductId] [int] NOT NULL,
	[Price] [decimal](7, 1) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_PRODUCT_PRICE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRODUCT_PRICE_HISTORY]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductPriceTypeId] [int] NOT NULL,
	[LocationId] [int] NULL,
	[ProductId] [int] NOT NULL,
	[Price] [decimal](7, 1) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_PRODUCT_PRICE_HISTORY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PRODUCT_SUB_CATEGORY]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PRODUCT_SUB_CATEGORY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_PRODUCT_SUB_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PURCHASE_ORDER]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PURCHASE_ORDER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceNumber] [nvarchar](50) NOT NULL,
	[PurchaseQuotationId] [int] NULL,
	[LocationId] [int] NOT NULL,
	[VendorId] [int] NOT NULL,
	[VoucherNo] [nvarchar](50) NOT NULL,
	[OrderDate] [date] NOT NULL,
	[NetAmount] [decimal](8, 1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_PURCHASE_ORDER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PURCHASE_ORDER_DETAIL]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PURCHASE_ORDER_DETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseOrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PrimaryQuantity] [decimal](6, 2) NULL,
	[SecondaryQuantity] [decimal](6, 2) NULL,
	[TotalQuantity] [decimal](7, 2) NOT NULL,
	[ReceivedQuantity] [decimal](7, 2) NOT NULL,
	[UnitPrice] [decimal](7, 2) NOT NULL,
	[NetAmount] [decimal](9, 2) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_PURCHASE_ORDER_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PURCHASE_QUOTATION]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PURCHASE_QUOTATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceNumber] [nvarchar](50) NOT NULL,
	[LocationId] [int] NOT NULL,
	[PurchaseRequisitionId] [int] NOT NULL,
	[PurchaseTypeId] [int] NOT NULL,
	[PriorityLevel] [char](1) NOT NULL,
	[VendorId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[Remarks] [nvarchar](2000) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_PURCHASE_QUOTATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PURCHASE_QUOTATION_DETAIL]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseQuotationId] [int] NOT NULL,
	[PurchaseRequisitionDetailId] [int] NOT NULL,
	[VendorId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PrimaryQuantity] [decimal](6, 2) NULL,
	[SecondaryQuantity] [decimal](6, 2) NULL,
	[TotalQuantity] [decimal](7, 2) NOT NULL,
	[Price] [decimal](7, 2) NOT NULL,
	[NetAmount] [decimal](9, 1) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_PURCHASE_QUOTATION_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PURCHASE_REQUISITION]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PURCHASE_REQUISITION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceNumber] [nvarchar](50) NOT NULL,
	[LocationId] [int] NOT NULL,
	[PurchaseTypeId] [int] NOT NULL,
	[PriorityLevel] [char](1) NOT NULL,
	[Remarks] [nvarchar](2000) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_PURCHASE_REQUISITION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_PURCHASE_REQUISITION_DETAIL]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_PURCHASE_REQUISITION_DETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RequisitionId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PrimaryQuantity] [decimal](6, 2) NULL,
	[SecondaryQuantity] [decimal](6, 2) NULL,
	[TotalQuantity] [decimal](7, 2) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_PURCHASE_REQUISITION_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_ROLE_PRIVILEGES]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_ROLE_PRIVILEGES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PRIVILEGE_ID] [int] NOT NULL,
	[ROLE_ID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_DB_ROLE_PRIVILEGES] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_SALE_ORDER]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_SALE_ORDER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceNumber] [nvarchar](50) NOT NULL,
	[SaleQuotationId] [int] NULL,
	[LocationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[VoucherNo] [nvarchar](50) NOT NULL,
	[OrderDate] [date] NOT NULL,
	[GrossAmount] [decimal](8, 1) NOT NULL,
	[DiscountAmount] [decimal](8, 1) NOT NULL,
	[NetAmount] [decimal](8, 1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_SALE_ORDER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_SALE_ORDER_DETAIL]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_SALE_ORDER_DETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SaleOrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PrimaryQuantity] [decimal](6, 2) NULL,
	[SecondaryQuantity] [decimal](6, 2) NULL,
	[TotalQuantity] [decimal](7, 2) NOT NULL,
	[SalePrice] [decimal](7, 2) NOT NULL,
	[GrossAmount] [decimal](7, 2) NOT NULL,
	[DiscountPercentage] [decimal](4, 2) NOT NULL,
	[DiscountAmount] [decimal](7, 2) NOT NULL,
	[NetAmount] [decimal](9, 1) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_SALE_ORDER_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_SALE_QUOTATION]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_SALE_QUOTATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceNumber] [nvarchar](50) NOT NULL,
	[LocationId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[GrossAmount] [decimal](8, 1) NOT NULL,
	[DiscountAmount] [decimal](8, 1) NOT NULL,
	[NetAmount] [decimal](8, 1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_SALE_QUOTATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_SALE_QUOTATION_DETAIL]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_SALE_QUOTATION_DETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SaleQuotationId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PrimaryQuantity] [decimal](6, 2) NULL,
	[SecondaryQuantity] [decimal](6, 2) NULL,
	[TotalQuantity] [decimal](7, 2) NOT NULL,
	[SalePrice] [decimal](7, 2) NOT NULL,
	[GrossAmount] [decimal](7, 2) NOT NULL,
	[DiscountPercentage] [decimal](4, 2) NOT NULL,
	[DiscountAmount] [decimal](7, 2) NOT NULL,
	[NetAmount] [decimal](9, 1) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_DB_SALE_QUOTATION_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_SECONDARY_UOM]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_SECONDARY_UOM](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryUOMId] [int] NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_SECONDARY_UOM] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_SECURITY]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_SECURITY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LisenceKey] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_DB_SECURITY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_USER_PROFILE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_USER_PROFILE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[CNIC] [nvarchar](15) NOT NULL,
	[Contact] [nvarchar](16) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Designation] [nvarchar](50) NOT NULL,
	[Image] [nvarchar](1000) NULL,
	[LocationTypeId] [int] NOT NULL,
	[LocationId] [int] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_USER_PROFILE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_VEHICLE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_VEHICLE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[VehicleTypeId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsAvailable] [bit] NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_VEHICLE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_VEHICLE_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_VEHICLE_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_VEHICLE_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_VENDOR]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_VENDOR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[VendorTypeId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CNIC] [nvarchar](15) NOT NULL,
	[NTN] [nvarchar](50) NOT NULL,
	[IsCredit] [bit] NOT NULL,
	[CreditDays] [int] NULL,
	[CreditAmount] [decimal](18, 0) NULL,
	[ContactPerson] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](16) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[AccountPayable] [nvarchar](15) NOT NULL,
	[DownPaymentAccount] [nvarchar](15) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_VENDOR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_VENDOR_TYPE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_VENDOR_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_DB_VENDOR_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_WAREHOUSE]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_WAREHOUSE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_WAREHOUSE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DB_WITH_HOLDING_TAX]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_WITH_HOLDING_TAX](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [decimal](4, 1) NOT NULL,
	[AccountWithHoldingTax] [nvarchar](15) NOT NULL,
	[ValidFlag] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_DB_WITH_HOLDING_TAX] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAllChildLocations]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetAllChildLocations](@id int)
RETURNS TABLE
AS
RETURN 
(
	with results as
	(
	 select id,  Name , Code, IsLeaf, Type from DB_LOCATION where Id = @id
union all
	 select t.Id,  t.Name, t.Code, t.IsLeaf, t.Type from DB_LOCATION t
	 inner join results r on r.Id = t.ParentId
	)
Select Id,  Name , Code, IsLeaf, Type from results
);

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAllParentLocations]    Script Date: 3/12/2021 11:15:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetAllParentLocations](@id int)
RETURNS TABLE
AS
RETURN 
(
	with results as
	(
	 select id,ParentId,  Name , Code, IsLeaf, Type from DB_LOCATION where Id = @id
	union all
	 select t.Id,t.ParentId,  t.Name, t.Code, t.IsLeaf, t.Type from DB_LOCATION t
	 inner join results r on r.ParentId = t.Id
	)
Select Id,  Name , Code, IsLeaf, Type from results
);
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201901111513585_InitialCreate', N'IdentitySample.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5C5B6FE4B6157E2F90FF20E8292D9C912FDDC5D6182770C6766A747DC18E37EDDB822371C6EA4A9422518E8DA2BFAC0FF949F90B3D94A80B6FBACCC833E32040B02392DF393CFC481E1E1EFAF7FFFD36FDE1390CAC279CA47E44CEECA3C9A16D61E2469E4F5667764697DF7DB07FF8FE9B3F4D2FBDF0D9FAB9AC77C2EA414B929ED98F94C6A78E93BA8F3844E924F4DD244AA3259DB851E8202F728E0F0FFFE61C1D3918206CC0B2ACE9A78C503FC4F90FF8398B888B639AA1E026F27090F2EF5032CF51AD5B14E234462E3EB3AF3D0C6DE9CB1C857180274503DB3A0F7C04CACC71B0B42D4448441105554F3FA7784E9388ACE6317C40C1C34B8CA1DE120529E65D38ADABF7EDCDE131EB8D53372CA1DC2CA5513810F0E8849BC7919BAF6564BB321F18F0323716EB756EC4DA7E9FA2000C200B3C9D0509AB7C66DF5422CED3F816D349D97052405E2500F76B947C9D34110FACDEED0E2A3A1D4F0ED97F07D62C0B6896E03382339AA0E0C0BACF1681EFFE03BF3C445F31393B395A2C4F3EBC7B8FBC93F77FC527EF9A3D85BE423DE1037CBA4FA21827A01B5E56FDB72D476CE7C80DAB668D368555804B30336CEB063D7FC464451F61CE1C7FB0AD2BFF197BE5174EAECFC48789048D6892C1CFDB2C08D022C055B9D32A93FDBF45EAF1BBF7A348BD454FFE2A1F7A493E4C9C04E6D5271CE4A5E9A31F17D34B18EF2FBCDA551285ECB7C8AFA2F4CB3CCA1297752632567940C90A5351BBA95393B717A519D4F8B42E51F79FDA4C5395DEDAAAAC43EBCC8452C4B66743A9EFEBCAEDCDB8F33886C1CBA9C52CD24638ED7E359100800E49F46FECD2C93FF1A266CF515FF610E8D51F7931BC0C911F8CB01AF69002BEC8D24F425CF5F2C708B887C8609DEF519AC262E0FD1DA58F2DAAC33F47507D8EDD2C612CA340B3579776FF18117C9B850B46FDEDC91A6D681E7E8DAE904BA3E492B0561BE37D8CDCAF51462F89778128FE4CDD1290FD7CF0C3FE00A3A873EEBA384DAF80CCD89B45E06A9780D7849E1C0F86630BD4AEBD915980FC50EF8E484BE997B26AED92E86B286E89A19ACE356953F563B4F2493F55CBAA66558B1A9DAAF26A43556560FD34E535CD8AE6153AF52C6A8DE6ECE52334BEB797C3EEBFBBB7D9E66D5A0B1A669CC30A897FC20427B08C79F788529C907A04FAAC1BBB7016F2E163425F7D6FCA25FD8C826C6C516BCD867C11187F36E4B0FB3F1B7235E1F393EF31AFA4C719A8AC0CF0BDEAEB8F57DD734ED26CDBD341E8E6B6856F670D304D97F3348D5C3F9F059AE8178F5D88FA830F677507328ADEC8C110E81810DD675B1E7C81BED932A9EEC8050E30C5D6B95B440767287591A79A113AE40D50ACDC51358AD5411151B9BF283281E938618D103B04A530537D42D569E113D78F51D06925A965CF2D8CF5BD9221975CE0181326B0D3127D84EB63204C814A8E34285D169A3A0DC6B513D1E0B59AC6BCCB85ADC75D094D6C85931DBEB38197DC7F7B1562B65B6C0BE46C37491F058CF1BC5D10949F55FA12403EB8EC1B41A5139381A0DCA5DA0A41458BED80A0A249DE1C418B236ADFF197CEABFB464FF1A0BCFD6DBDD55C3BE0A6608F3DA366E17B421B0A2D70A2D2F362C10AF133D51CCE404F7E3E4BB9AB2B5324BF3CC0540CD9D4FEAED60F75DA416412B501D644EB00E537810A9032A1062857C6F25AB5E35EC400D832EED60ACBD77E09B6C10115BB7923DAA868BE3795C9D9EBF451F5AC628342F25E8785068E8610F2E22576BC87514C7159D5307D7CE121DE70A3637C305A0CD4E1B91A8C547666742B95D4ECB692CE211BE2926D6425C97D3258A9ECCCE856E21CED3692C62918E0166C6422710B1F69B295918E6AB7A9CAA64E912BC53F4C1D4352D5F406C5B14F568D242BFEC59A171956B3EFE6C3F38EC202C371534DFA51A56D258946095A61A9144483A6577E92D20B44D102B138CFCC0B956ADABDD5B0FC97229BDBA73A88E53E50D666FF16D776F1FE5ED86E557F84C35C412743E6D4E491740D05F4CD2D96F78602946882F7B328C84262F6B1CCAD8B2BBC66FBE28B8A307524FD151F4A3198E2E98AD6EF3536EABC186F9C2A2F66FDB13243982C5EFAA04D9B9BFC52334A19A66AA29842573B1B3B933B3374BC646771F8707522BCCEECE2192A4D00FE69204623C941016B94F54715F3509A9862497F4429D9A40929150DD0B299522228D92C580BCF60517D8DFE12D4249226BA5ADA1F59934ED284D614AF81ADD1592EEB8FAAC93869026B8AFB63D7E927F23ABAC7FB97F108B3C906561C7437DBC10C18AFB3288EB30136EEF39B408DCF03B1F88DBD02C6BFEF25A18CA7BD4D0855843836239401C3BCFE0897E1E2F2D37A836FC6146EB88525BEED86DF8C378CB6AF4A0EE5BC2757A9A457E73EE97C37E567ADEE9735CAE1ABA8625BA519617B7F49290E27ACC264FE4B300B7CCC16F3B2C20D22FE12A7B4C8EAB0E16CF8417A99B33FAF649C34F502CD59D5F454461CB32D2468912794B88F2851D32536784952832A91E86BE2E1E733FB3F79ABD33CA8C1FE957F3EB0AED3CFC4FF2583828724C3D67FD5F4CF7132EBDB4F5B7BFA0EA2BF55AFFFF5A5687A60DD2530634EAD43C996EB8CB0F83A62903645D30DB459FBCDC4DB9D50C26B042DAA3421D67F7CB0F0E9280F0F4A2DBF0DD1F39F87AAA67D5CB011A2E601C15878A398D0F440601D2CE3E3000F7ED2FC71C0B0CEEA1F0BACA39AF1A1804F8683C9CF04FA2F4365CB1D6E359A63D13696A4DCCE9D69D61BE55CEE7A6F52B2B1379AE86AC6F500B80DB2AAD760C61B4B481E6D77D4E41B8F86BD4B6ABF7A92F1BEE415D7191FBB4D27DE660671CBFDD01F2A71780F52DD34A93BBB4F0FDE36D74CA1DC3DCFB11C9604BC6764E3095DBB4FF5DD36D94C61DE3D27DBA084DE3DE3DAAEF6CF1D33ADF716BAF3F45C35D3C87025A38B0577A5DF16817338E12F222041E15116AF26F5F95E266135598C02EB2A66A1E6443359B0327114B94A8D76B1C3FACA37FCD6CEF23AED620DE9996DB2F9FADF2A9BD769976D487ADC45E2B036ED5097CCDDB18EB56543BDA54461A1271D79E95D3E6BEBFDFA5BCA0B1EC528C2EC31DC11BF9D34E0514C32E6D41990F6AB5EF7C2DED9F8738BB07FA7FEAA86607F7C916057D835AB3AD76419959BB7A45159458AD0DC608A3CD852CF13EA2F914BA198C598F367DF79DC8EDD742CB0774DEE321A6714BA8CC3452004BC9813D0263FCF6D16759EDEC5F95F3019A30BA0A6CF62F377E4C7CC0FBC4AEF2B4D4CC800C1BC0B1ED1656349596477F55221DD46A42710375FE5143DE0300E002CBD2373F484D7D10DE8F711AF90FB5247004D20DD03219A7D7AE1A35582C29463D4EDE12770D80B9FBFFF3FF465D0F975540000, N'6.1.3-40302')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'fcd918d9-789a-4328-af13-ff44d14fc5d7', N'Admin')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'fe2b5d83-09d6-4f12-96e8-09860a7f135b', N'KPO')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'a74d4685-df0f-415f-8ba1-f04c565ae898', N'Order Booker')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'57c142dd-b760-403d-b011-ba89f67a0272', N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8a13a6b9-d338-49cd-a45a-bbbb1a1c3006', N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'57c142dd-b760-403d-b011-ba89f67a0272', N'malikfarhan42034@gmail.com', 0, N'AM+MPMDhCSDStVgLy5eXVz2LDTqSz0hQ1REtx++ec+Cmpndj0oaHY1hEOJshtZuZag==', N'14f4e74b-a4ce-4c38-9f81-cdec6d29c8ec', N'03074299132', 0, 0, NULL, 1, 0, N'farhan.malik')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8a13a6b9-d338-49cd-a45a-bbbb1a1c3006', N'abc@abc.com', 0, N'AOHxhZb60iomm6msd5vRl9HQUNAR5ep2wjH+YFHJQA+2cW4CxNaTPnaJA/PKUra95A==', N'7f6547d0-29c0-4c34-ab42-c05ff62e536f', N'03217261866', 0, 0, NULL, 1, 0, N'superadmin')
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'50d6581e-9f53-48cc-805f-0014b6e4f9f9', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'15b758bd-c010-4cab-9064-001e79dbc4de', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9222', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9bd7e1ef-9fa4-4cb4-bd81-0045a304b161', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fbb69d32-86ad-49c7-a9f3-00988570d4af', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8871', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'410a2875-4a46-49ec-9e42-00f158da1f57', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ab9354eb-cb13-401f-a771-010573c22de2', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8604', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b03a9704-5d66-44fd-a9ea-0185ba42b01c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a80d88f-98d9-4d02-acc7-01fbdaa2ccee', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ad557fce-e980-4091-89ef-0223e8733106', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8713', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd0de1daa-18f2-4f25-91a1-022ea92fbe53', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a42d6afa-148a-4a13-b639-0235d804157f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9131', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'26c94820-95a8-4685-ba43-024d28c785e1', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'88e174a3-e777-4f72-916f-0254afdf405d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'03eb8d0c-801c-4502-881f-026845a1f176', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9160', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd9441596-cf89-461b-9120-02bee5b1cbae', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9058', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cc16152a-79d4-45b5-8a61-03283e02db48', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8927', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e49a4da8-b8dc-48e2-bdce-032908de4c67', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'82411450-53db-4808-b9e9-0345aebe53d4', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8661', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4d3acc7d-f7bd-462b-b616-038dc2914470', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8242b2b5-7f31-4478-aea9-03ab90ec2a15', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9025', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd82b43c5-f87a-4467-b885-04363c8ecc50', N'superadmin', CAST(N'2021-03-03T10:57:14.247' AS DateTime), N'M', N'DB_PRODUCT_SUB_CATEGORY', N'1', N'Name', N'Vital 5Kg', N'Normal Tea', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'04052ad6-af47-4365-baf1-0492a5979300', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dca614c9-2381-465e-a7b1-04b389a7e6a3', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8984', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c96a90f4-b01b-4657-942f-04e0310e46d8', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9293', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0b95478a-557c-4d59-b45f-04fdfe2d1cf3', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'18333128-fc02-43ff-81fe-0516ee5a7147', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c10f2bd5-0b3c-4f85-9986-053dbccfa3cd', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9043', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'42b7378d-de2d-410e-ad14-05442b94573b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8922', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6a1e9553-cc96-493c-b54c-05716f25e005', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f24e0b38-862b-4f2a-9021-05abde74e50c', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'547e0262-c71c-4db8-9cb3-0620613cc584', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0c829473-9eec-4a47-8caa-062fbc21422d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eebaf93c-fb05-49e8-9946-06335e98141b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0943459a-528b-48d5-a42a-063a03fc3679', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8981', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'90c9b65d-a3cb-4d5f-8d43-0650899d7c5c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8607', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7423644f-e986-4fd6-854b-068edd6750b1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4e6d2e00-3d2f-4396-b117-06911686da33', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'06cf6ec6-90e1-4fae-88c9-06c0822099e2', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ea1e01fb-5321-413c-8716-06efe779eb81', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9b4c6e14-3f5f-4fc5-87ee-07bc8367dcad', N'superadmin', CAST(N'2021-03-03T10:57:14.247' AS DateTime), N'M', N'DB_PRODUCT_SUB_CATEGORY', N'1', N'UpdatedOn', NULL, N'3/3/2021 10:57:14 AM', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'831652fc-cfa5-4100-9abd-07c7a1b9c650', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9019', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fb32013f-1c99-40d1-aa0a-07cf1b563944', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9c5a3f1b-640f-4216-a300-0835dce64f26', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c8714854-8a64-4a13-94ee-08a21ae785c6', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9269', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b6414f86-f4ea-4aed-b74e-08c7868510dd', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8643', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6115c411-2834-4d9e-aedd-08eb920e83ac', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4a098784-0cd9-4565-b872-091824dfcc64', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c31b33e1-332e-45da-be14-09b318d53103', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8912', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'92b5ceab-8024-47ed-bfb4-0a13c85b222a', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd1b1c3e3-e88e-4791-a0b3-0a2876025580', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'29977dfa-5c46-45a1-ac55-0a3da30e9753', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bb370da6-1567-4a2d-a7c1-0a3eb9cb922f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8e61b6f1-c52a-4c22-9af7-0ab98d6ad9c8', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b244ae78-a412-4834-b789-0b35c641eec4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9217', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5b96dd3a-e5ce-4bdc-834b-0b6b8b1b6c86', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8953', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'056b5103-659e-4787-ae6b-0bc607174ce4', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8b0802e4-343e-4400-9e16-0c1284f2c68c', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8825', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8cc9e825-d19c-47b0-80ef-0d5b63b09f15', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8782', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'416ce530-a24e-4f83-ae1e-0d6578070005', N'superadmin', CAST(N'2021-03-08T13:00:12.263' AS DateTime), N'A', N'DB_PRODUCT', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_PRODUCT_7B8BEEC84C23A2F93B662B9CA778EA57922DA57EF551E5C6C3C5367277998E3F', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f1131e31-f620-4f46-98d9-0e78b91de615', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9139', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'475fc021-d845-4693-ab1e-0ef040b20f96', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9274', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'089df3a5-6795-4ac3-b3f9-0efffbb27db3', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c026e162-438d-4d91-b940-0f8001a5d30b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1a0fe564-e1c5-435f-8245-0f98de502c78', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'db8b34fb-29a1-4ff0-992b-0fb748b7abf7', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8794', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1c948fa6-f7cc-4c75-bc35-1035dbf2161b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2d2e7bb6-d003-4e7c-b0f3-103e0e5c7065', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f1a2546-9cf2-4402-b245-10467e4dcfbd', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'088485ea-3c1d-41bc-bac9-10487935a03a', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'14248e22-f121-40ba-8fcb-10867eb68ca5', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9086', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e4d9af56-d214-4cc0-a788-10ca3a840f5e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f5bc15b2-1a80-47dd-bfb7-1174e59fb30c', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'738fb162-4d84-43cf-9418-118e83b966a7', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6d3af8af-6667-41d2-9953-11a6757cd830', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9170', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2de6f7bd-8221-4789-bbca-11adf0b5126e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd1b96292-f9db-4f26-be0d-11b30770074d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8767', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'96b0951f-42af-4055-ac13-12016988673a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'48416af0-88ce-4971-875c-125b25698e34', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a4fe378b-d466-44ae-9979-12999c7a9e7e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ec75c119-41e5-4cfb-abfa-131ef5ab3dc5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9258', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0906fbf3-2800-4d0b-b1ca-135a227156a5', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8835', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7e27477c-9dc8-48b8-801b-147950abc754', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'50eea040-0dec-448d-8809-1491c09ed2f1', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ec7d48f0-3b74-496f-a9fb-1492e467a48e', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9152', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aa448b49-4ec5-4bc3-a438-14da8b3fd56c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6cfce82d-8af8-4bc6-91ce-151c9247c117', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9042', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd3f2b97d-8eb2-4254-a6c4-151d8a48a7ef', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9023', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f5003883-bb18-4da5-9140-1546d1292334', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9134', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0650e59b-ba00-458c-a65d-15537a7854ad', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2d55630a-4a75-4b06-b8b6-1555f74250c2', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3ae53806-62b0-45a1-8f4d-15b38023cf5d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'29ec445f-7a28-4f84-8021-15f10226a4b5', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1f8b31ff-11b1-469e-bac4-1626e26d4b09', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8868', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f6446f4-a3b8-4b58-b8d4-163179a8fd7a', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8806', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a1a169d-1043-4057-85f5-163903953ab7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8881', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ef6d26e4-fed7-419e-8372-1657d6ca9a92', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b10aceeb-648e-46ae-ae45-16a28a422288', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8925', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'803f8baf-60b2-4abb-967b-16b2c8689f84', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8956', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b252090e-dd18-419b-a18b-17219a47f309', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'514e70ab-50b3-418a-8384-174d83c3d328', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'842d1c89-392b-4f03-a07c-17a86b2c4e36', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4b9c6ce2-e0e5-4364-bb0b-18099d2f33da', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fca44389-ea51-4166-8cfe-184a732e6a61', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7e9b1834-1c2e-4c30-99f1-185174ede6de', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bed7d30e-dc66-4dda-924f-185a9c989b3a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3a6ae3d4-7078-4b76-b20d-18a7f2189e52', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8985', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3e494432-8d0a-4f58-8919-1945a59fcd2c', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9014', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e1e8309e-61a8-4ed8-a74a-194641980422', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'614f2eba-ab7e-4f50-acdd-1954b72a1503', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9192', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b59285d9-407a-4ea2-8163-1a57cb3445a0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9015', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4385b2e7-e2a0-418b-947a-1a60da47ed9f', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'106d1d48-c5e9-4810-82ff-1aaa93579cd0', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8779', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'264d647b-d50b-419f-a5b5-1b1caae03521', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bdf8b4f9-bd3f-48f2-9d0f-1b2b32caed88', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'948294f1-f8ff-4e91-a116-1b2e8ee162c4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9073', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9816a14a-1871-4b09-b0a6-1b57fc4699ec', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8949', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'90654874-2618-4970-866a-1b81a341704c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9243', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fac332e8-c161-4c3f-b799-1b92eee10587', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9001', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a89ce6f0-f885-41b3-92b2-1bb63d307846', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'133ab737-1097-4356-b986-1bebf64717e7', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0cd6b80e-6298-4093-a2b5-1bf623e9ec9d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9113', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f240b182-75e5-4d3d-be0b-1c3c1842c3ec', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8894', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'83383f95-7dec-4d01-b844-1c759c6f4790', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8852', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7c4b0bb-63f7-4be8-b099-1c7919a9f0ed', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b4cbdb2b-417d-42e1-a5ad-1c93c085e16f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9125', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3b639f67-7737-4504-bf11-1d446afb5048', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9122', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'14df97d6-653e-4aa9-9437-1d9e11aa392c', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dd8f3a01-fad0-4acc-8831-1db08c3d72bb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8862', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'86fc8261-c712-4e8d-ac94-1dc712940be5', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8873', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'32f5c4bb-ed64-4701-8824-1df1b49bd21e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c02e77f4-6349-4f3c-a57a-1e108695f9e3', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9207', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd869a6c-58fb-4e90-9f15-1e1879bbb948', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8708', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e580d572-000c-45d1-8e84-1e495d98eaf3', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3bfb96ff-c94a-44d0-8f4c-1e67869494c9', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cc974de6-da6f-412f-b87e-1e89efa1cdee', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8880', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'273d614a-a015-4a9e-8591-1edc58983e33', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9281', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a53fd1a9-0a74-4abd-94d8-1ee4ef4e9914', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'504af58a-0586-4c3b-b03f-1f13226a53e8', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9060', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ee8f4349-3265-41c7-a040-1f32fe962493', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eb6515b4-bf7c-40f1-8a53-1f34309235aa', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9249', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cf47087b-5c45-4c1a-99ea-1fc6ead1fb11', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8961', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'faff8f9c-fbe7-4d83-a154-2033d6c18592', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8d6cd21b-1862-40a1-b6a6-2035dc46c059', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8829', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'13904526-1711-4f6a-87c5-2099adb6133e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'257550bc-bc2b-4cd9-af8f-21087a67352f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f02e5629-90a3-49f9-acb1-220a0684eaef', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8389af7a-1757-43da-9a27-223a7f0d276f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'398eacdd-8d7f-487f-a0eb-22bdba7bf4d7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5d68da84-2ed4-4975-892e-22c2d66dfe43', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8606', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e46bff6e-b06e-4a70-95f6-22f37f98f1dc', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2eb6d5f3-cb2a-4557-9bf8-22fd2f3428ca', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'96dd3a1b-0ebe-4aa8-a094-230626711d22', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8618', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c6760cce-7183-4da4-a917-235afc52c593', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8877', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0d6a4219-0f70-4cb2-b822-235bd4a55a72', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a2f131e1-fd67-4efe-8883-23fb788ba51c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'53bc5481-b462-42a2-867c-23fd5e10e19a', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8905', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'36afd2cf-5c11-4c43-a42a-2439076180ba', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f8c52da-0153-40a6-b5de-245b74ca2a76', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9191', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9170c777-3af5-4256-b7b5-2468d169518b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ca4c5bb0-33c9-4881-839f-247d8c048928', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f8f1953e-da58-4a4e-a381-24a13b5439b1', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8923', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9d1fd1b1-ed9e-48ba-9848-24dbb9056386', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9135', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'88ca9e2a-c8b2-446e-80a9-24dfa975403a', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9115', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a5f3fd7d-af8f-43a1-a104-252e241dbc3d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'045051fd-afa1-4002-8c6c-253b2b16e065', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'344d53e9-23bd-4dcf-a5b5-25a2f6b7a5fe', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8982', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0d6642d8-0807-4fcb-804e-25d10bb165f6', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2cf4cc66-1769-4b15-8c38-25d2874d2d15', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'22f8ba52-c796-4838-ac49-25e3fb4dd81c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8629', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'307c2086-d715-47ec-8d41-2674dbbea3ec', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3d7deee4-5bea-4979-b473-27086268ae2b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8992', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a8dc0197-542c-4ede-845b-27dd5937cf68', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9047', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'48b0c2a7-c4d9-4474-b433-28434ee7e875', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b37111db-c907-4c00-bb2e-2853f88b2f88', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e9919c48-6a3a-4c14-8971-2961162efd3e', N'superadmin', CAST(N'2021-03-03T10:54:15.620' AS DateTime), N'A', N'DB_VENDOR_TYPE', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_VENDOR_TYPE_7380F860FD3E1365DEF8217E56896777AA7542CE1FA8F0E988A731943E42B1C4', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f5b0be6d-0f66-4861-8265-296f3820c421', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0334f4ae-a03c-460c-adb2-29b9da6bac70', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a8bdda9e-cc4b-48d4-8dc5-2a42f0630105', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c80500ad-c0ff-4ca8-9154-2a504961e1ac', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a44845cf-ef11-4bb1-9586-2ab19c97a122', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9288', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0e2a3299-5502-4515-9271-2ac329f86fa2', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8616', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd6ab669d-eb37-40ac-accc-2ae4ddef461e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'33b09069-c120-48b1-9143-2b427159b04c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9159', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9682a7c7-a8fb-4ca4-bc23-2b7e89bb8d90', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c80fbd82-475c-465d-b276-2b8468ae6181', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1d18a0f9-4d1e-4b0d-bf46-2c5e300e9941', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aef5fc36-b0da-4065-9ac0-2c62c7a100fd', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8859', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a0fee9bc-8188-4325-a637-2c98f0ef02fb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f87bbdb7-fbbe-4bf1-8c69-2caf22300714', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bd508b0a-67d5-4455-83d4-2dbca944000b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c7cc4e67-f5c6-45d9-9c39-2dc0f7dbf217', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c57eed2a-dcc6-456e-8596-2dc9bc424c75', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8875', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f58d7bf5-c696-4f55-a408-2e269d5bbc0b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8217bd58-6246-450a-afc0-2e3a0b63f70e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'df563f36-7489-4c75-bcd7-2e3cb9c1b7b4', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8807', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e6888423-eb3f-47c1-81ee-2e4c08ff05e7', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a0ecb668-4b41-46e6-83fc-2e75a19ce7ae', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e3d2e9fb-7de3-40ed-ad92-2ee0872ed089', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'df07e4ec-6571-4f3c-b620-2f27f091de3d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0f75649f-eb3e-427b-b70f-2f3dbce0a117', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8870', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'08d5c685-27e9-4823-a911-2f4bfd9ed2c5', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dd232801-90a9-49b1-82ed-2f61de3a808b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9132', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1011f9a4-6a9c-4d21-a1ba-2f68a14954d4', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'88de4286-334f-4381-b164-2f6abd9ac07c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8658', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aee915a8-2328-4493-8460-2fb7df6badfe', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7427b7a9-2158-4b51-98eb-2fe1d5cbfefe', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ec3dc8fc-cfc6-4bf9-b9a9-30138bae3ac8', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8907', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'072a7140-bf8b-40bd-9b01-30176d8f9c51', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fc18f009-e893-4ac3-92b1-302d183c951d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5df01c95-2bc3-465b-b82b-3062c8ead9d6', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e55b3df2-2fb6-4479-bd73-306ea2976fbd', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8a7685e7-c109-4f2b-9855-3093cff4c2f5', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ddd639b7-4f56-4789-8c4b-309c6b5cd0ab', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8954', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'411126fe-b709-4899-8cc1-30d2baef6492', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9275', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'702841fc-f10f-4ccf-ab8e-31084a74384c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9155', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b43fd57c-84e1-4d8d-a1ab-314c22fea670', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9224', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b3285aca-1853-468f-b185-31887cef8e40', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'26310e66-7d2a-4483-a769-318916597185', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0e295998-5e1d-4afb-8ef9-318cbd1824eb', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8820', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'141dd29b-e195-4985-881a-31a31d5422b6', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9307', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7d129c8f-39e7-4156-8542-31bbe26da44e', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bc1e0cf5-25f0-4eb5-8548-320e62a5684d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1135e636-2e6a-4fa8-b362-321882893738', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8987', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd9aa44e2-2b6b-4fbe-aff1-323e0034f038', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e48d183c-5643-466f-8de7-32f77607c22f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6159c047-b9cb-4283-bf5d-33023617aa54', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ab86e979-c3ff-417f-babb-33deb94ea84d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3cff2c2b-2b09-45dd-99cc-342b14679c5d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8781', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'772f87aa-f818-4a39-acbc-3463977916c1', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9205', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd2a6d98-74bf-4549-85a0-350525caafdb', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9032', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'389995fa-ed0d-4d39-8a5a-355bfc5a40d4', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'034750d4-6d50-4626-9081-3565c10f14a2', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c77d702f-f0b1-4786-bba3-35927f1b4498', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bb67571f-771b-4944-a742-35ac8ebe2126', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8804', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b7a87c34-2995-4a4e-bc40-35b250df2286', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8818', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'212e99fd-36ba-4682-b6c9-35ce8a87ec04', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bd37a502-71a2-4645-9fe6-3644f3fda2d6', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9245', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7cef574e-a0d5-4cef-93d9-3656eb898cbe', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'21d92fa4-e03c-4ede-b92e-366a6554e42c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9284', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c5a438b-137f-447a-aab8-36c334e4bd4e', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8620', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'203c4292-e371-4a69-94db-36d1f72b6980', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd06dafe8-9cb9-4571-91ad-3804f17abbec', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ef87d2b0-24af-4c26-af4c-3824e76532ba', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8908', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'77007736-3b54-41b8-aada-3844e07cb1e7', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9064', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'58b3d4d6-9fbb-49b4-895a-388eb0fb600f', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3420228d-3083-49da-a261-38d3c367168b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8c1354e6-542f-4756-9e1c-38dfc190118a', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'156c96c6-ca2f-4fad-8170-3930ad76a8d4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9ef7dd1e-448a-4706-93ee-39430c8f66a2', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bbc8369f-9676-43b0-a29f-398a023df873', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ff990825-b7ab-4976-b93a-39b7e0164c1c', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8902', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4c39c033-1477-4d5e-b2b9-39ea40d9a13b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8928', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f45be8fe-e434-4c74-9f8a-3a4db6745693', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9493bba9-0868-414c-b067-3acf8a1970bb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'88b1fbf6-c6a7-4ead-b541-3b7895875f95', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9248', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ac300f6a-c09e-481a-8942-3bafdc1e01cb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bef1e0a9-0a64-42de-a66c-3bde0b37d56a', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8854', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'919991fa-b0af-4359-bfca-3c0325c9ee3f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9235', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd67e68b-dd32-4f64-942f-3c24fa8ccc23', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8878', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f06e1cae-1b2c-469e-9dbd-3c6180325315', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f0f2ef4b-482e-4cc4-9db1-3c920a0e45a0', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9225', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'97d772e0-9187-4c80-a9a7-3d1f165af23b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ef0ec9d4-4107-4d61-ba2a-3d217a9bc696', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cb5917bd-0e5a-42ba-8444-3d4e977d0640', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9036', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3bec627f-95e2-4ae8-9039-3da2b2106ec7', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9050', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f089ea0a-5fbd-4555-918b-3da31b4c592e', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9108', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1029be94-8ebe-4494-924b-3dbeb8d44ece', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9006', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aa5b85c1-fb71-414c-b727-3de86575f7d0', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9177', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'86ae5919-4e1f-4021-bb1e-3e386dc1121e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9210', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6b97368e-33a7-4bc6-8469-3e487bb73e69', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd7ee43d8-1e94-42e5-b906-3e7e361b73e3', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9091', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'892ce465-117e-448b-8a19-3ec92fcc2f8a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8986', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bc141ced-45c7-4d2d-9ffe-3eee6ea1ce11', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9101', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c4374e22-dc1c-41c2-8d58-3fa44c5e3106', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9a211a7d-fd3e-4435-a17b-3fa6a10f4e27', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e508c13d-11e1-47ed-b0a0-3fc0cff0e7b9', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'53f71440-3b0e-4676-b6ee-4029f17bd2a2', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8977', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4a17fa13-9808-4546-bc4b-403b35c7efb0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'18fc377b-fcbd-4eb4-a599-4065e3592b05', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c7757855-094b-44e9-ba77-40bccdfa04bf', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8918', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'04e3245d-d9b2-4ff6-a37a-40cc80cf9f23', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b4085078-a8f4-4c40-8edb-416508e4b175', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a30247aa-4e55-4db1-a43a-419337e15c62', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9129', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a83b45d8-796a-4c4a-9b9d-4193e7e5ddb3', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9029', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a440ecdc-208c-4584-92f5-41ac82026538', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'76e59d45-022c-438b-8070-41c349a53807', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dd6e3530-8340-40d7-b2bf-41dcdb5f2258', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'798565b9-55c4-4631-abb0-42124fad2a8d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7466c0c1-3956-4f0f-ab44-423e6e12abcb', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8952', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'67aa2cbd-5125-47dc-90ff-4244b1fb6a92', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8933', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'13225be8-baec-4676-a529-426e39fe4996', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8884', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'da828320-6d4b-428b-b3aa-42a6baffa6ed', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9119', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bf2658c8-a1b2-4483-9001-431dd48a9ad7', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dc7117eb-4c56-4c69-a05a-4357c9f9e420', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7add162c-50fc-4793-a515-439423074772', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9059', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a4d71dc1-6363-4ccd-ae77-43e1e4738b6c', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'59f65524-4175-405e-afc7-44356dcbe089', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9069', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd19d5721-d058-4378-8c9e-44b37858e4cc', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'724660ee-d3e5-41a2-a405-44bc50efb5a5', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9136', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'81848f6e-1618-4e77-a57c-4525ed58323b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f0bad2e0-b465-47d1-97fd-4546e929f020', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'36f2502e-1e80-4ac4-ba74-4556eaa25e5a', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9232', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd4eaa576-8ec0-4793-b8a5-459fddf22556', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8999', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'593f95be-1cbe-41df-9bd6-45a431f790a5', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8916', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ac9e8156-3280-49a2-850d-45c18d34aef6', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9244', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cea61dc9-8859-4671-a514-45fe4a0ab634', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9011', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4e6e2cc9-d5ef-44ac-8362-469ee8f0aba1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'80615d4e-3e62-4fe4-8630-46b10fdb4ff1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8802', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8e12519e-929a-4e26-8d19-46b91aa316c5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'209d0763-5a17-4804-939f-472a260f30d6', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8797', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'53c0e789-8c5f-4fbc-bd79-4750246360b8', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd43ccb3d-e5e1-4746-a0fb-475d0f64df00', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8837', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4167b921-2405-4a71-9537-4777e38844fa', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8926', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e3cdfa41-f5e6-4184-b92e-477db6e7e2f9', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8590c575-b3ca-414a-96f0-47c33235d81a', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a24b7549-7a3f-46d4-a3ad-48a90486aa10', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8988', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7b15636-9b52-4f31-ba76-48d23926e730', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8899', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'26701e5a-55e5-4466-b9fd-4948868eb5cb', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9084', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a9968b7d-51cf-4876-8e22-49855c49fcc7', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'680ae737-5bf6-4796-b46b-49e6b4b2e542', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8886', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3774a548-7d9e-4ab2-8110-49fd4718c17f', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9061', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b21ed5e7-7f06-4cdf-ae7c-4a2d770763c1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ad7b6b05-76a7-40ba-8a70-4a3e23c86eae', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9302', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c929aa45-fdbf-4d54-911e-4a64a0c26916', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8328ca51-710a-4adb-b2f7-4a779def1a2d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8784', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c987212-94ee-48c4-9c04-4a8944654705', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3732dd87-73eb-45e5-a83c-4a90248134f4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cbc139c5-2423-44c4-82d8-4ace1ae04393', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9260', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'af6de1f8-544f-4009-b125-4ad13c00ce15', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8809', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'540cb262-7258-4c77-89da-4af0fa10aa16', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9102', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'995bf081-1966-469b-8ade-4af9419d865d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8666', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dd61d399-8f32-408f-9f86-4b1e149ce90c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'310b49b3-0307-4b5e-b386-4b5888c41620', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9226', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2802b7d5-4c62-40c1-bf2e-4bad51a9ed5c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0bd14ec1-37db-42f7-85b3-4bd41a1f46ec', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8783', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4dcd36fd-9d2e-427e-8bfb-4be1bb8f93f5', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f898cc6-6bc2-48b3-962b-4c1984303ac0', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8822', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'61b2b181-3650-4499-9d62-4c25c1d5f23d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'91319619-9c92-4687-a53a-4c29792d1f8a', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8603', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ee6ad2cd-ec6d-4d5f-b441-4c4c8f9766d8', N'superadmin', CAST(N'2021-03-03T10:56:26.153' AS DateTime), N'A', N'DB_PRODUCT_CATEGORY', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_PRODUCT_CATEGORY_CF127D738DE090D54FF7E2731D1130D5DAA15DC22DA1A7B497D7DE061B343BBE', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ba264837-732a-4508-abda-4cf925ffab9c', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8850', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38a843f9-f1b0-4201-ba8f-4cfd91e0e228', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a15c16d1-36bf-46a2-9408-4d6b8e7c1d3a', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9252', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e0e7e75a-6805-41e8-8053-4d71b1e495af', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'77ac8299-098a-4d46-a025-4de75eee7f24', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9153', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'593d55d1-384d-4655-9591-4de764c104a9', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8955', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1a01552a-a70f-4a33-b974-4df7f5423761', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9261', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b80164b2-c8af-439f-8be1-4ee980b342dc', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'69059231-6076-45b6-8026-4f0cc324d1d7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8887', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2b92d59d-5a1e-4626-8103-4f17d795299f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8885', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'25acb593-de68-4627-9765-4f70cff7ca73', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b4e24b14-fb06-4e4d-b8d7-4f76003b2010', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ed5fb691-d649-42da-a4c3-4fa44b1e0d3d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'db362ec0-a912-4e7b-8b3f-4ff0c3b4dfc2', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8821', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'af71d538-10b7-48bc-9681-4ff78292326d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ecdc168a-1345-41e2-b022-5047e2251a66', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'72801734-7d21-4322-ad0a-504bad1f37ec', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'302a323f-ed49-4979-b3d4-50559294f62f', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0997bd28-6e7d-495c-ac53-506c4935c197', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'137c1a6b-8703-4b84-a039-51858bafcb3f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2ce16e08-8229-4410-a801-518978f3baf2', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8652', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd70c9949-546b-42db-a14d-51dc532d530c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9290', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c1c9ed7d-d289-49f1-9c39-52114be32b84', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8969', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd4f824e7-80fa-4505-97ed-5228d58f1891', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2034f2ba-20bd-4624-94b3-526ae81dad47', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'41eeee2a-48a6-4d41-8403-52e1bb341cb1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8791', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fbc626d4-6dcb-4279-9dbf-5307197a771f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'539fcd06-de98-47fe-a002-5317aa40ff18', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5d7c02e0-01bf-424b-9f76-532805606815', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0f3c9c54-d698-4320-9875-53400c2f605f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1133bf32-ec51-4408-a1bf-5343bf3a3ade', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1d42137d-a3c5-4136-a932-5394fcfdfcf5', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9d6ac4f4-c79a-4962-a92e-539f54d88149', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'48ce2c25-4592-4571-b971-53d3885f4a70', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8634', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c43cb1a-61ab-4844-9b04-53fe880dcf0b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e108ec26-ff18-4f8b-ac6d-543eea98a473', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9045', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'777fc9ab-934f-4246-9769-5464abbda621', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'28399862-2787-4d84-8624-54b568c6dba6', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9203', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0b05bb58-0028-4697-acdb-54c19e4acf45', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8901', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'85706fac-82c8-4325-bd5c-5504bcecf674', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a69940f-63d0-4562-b639-552162be7968', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'822aed97-0634-4dbd-9d0f-5531a099519a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'789333e1-ce23-4322-86f4-553d630b0bfe', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0d6002b2-6156-4c66-96b4-554039776b07', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6bb67430-9151-4f85-b631-555d3492a337', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8805', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'df9ba3e6-67c4-43bc-abe8-55cdb098e01d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9112', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fa466b1a-e123-453c-8249-55dfc1ce12cc', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3da7ba01-3438-4e6b-a554-56c0805858c0', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6245d192-2309-40b1-8044-56d54941e472', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8997', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f3e738a-67d7-471b-a1fc-570d894edbe4', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8891', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3ad70b3d-2ad9-490e-ab22-572c11c6f757', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a986fc16-8411-4376-b913-5744731de99d', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'21a47801-e732-4364-a713-57f6ed9649ae', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9038', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd61e7cc9-3b28-4710-8f99-5881e4d5567a', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8667', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f689b84-9031-4110-8a8f-58c46c31484d', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8958', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c76bc69c-3d67-447d-b202-58cd45bf5cc9', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'988a09f2-18a0-4bee-9d0f-58ef8ee8a04f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'35f424cb-d808-4943-916f-5918e91942be', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd636b5be-bd6e-498e-afd6-59a7efe4b30f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ba276ef1-6120-4e44-88c1-59ab5f63fe4d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'79da6e07-452b-4a93-a068-59afb2b8ca99', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9175', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7220edf2-f558-464b-9739-59c2aa358059', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'31982857-bb3a-4d39-ad62-59f800e4dd95', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8910', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38f269fd-93e5-44ed-bf98-5a3704620eb5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a69206a-0806-4c69-bbee-5a6c83eb3257', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'430afce4-de1a-42d6-ad00-5ac8f31d9f96', N'superadmin', CAST(N'2021-03-03T10:57:14.247' AS DateTime), N'M', N'DB_PRODUCT_SUB_CATEGORY', N'1', N'UpdatedBy', NULL, N'superadmin', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'acbbe0db-c6a6-43b7-8bc9-5ae7b04d9c7d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11a443fe-a2aa-48a5-adca-5aeab1818ced', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd074b9b8-10da-4917-b5c3-5afcdaa0572b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8401edef-f559-44bb-86de-5b3a62a82fe4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9208', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fd1a699c-2398-4629-aa20-5b76eb423c28', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f9453fc5-7cde-4cf3-9d46-5b9b714d398e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'293ba5cd-73a4-4dc0-8316-5ba893b7e433', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3b653d93-153b-48d9-81d9-5bcb495f982f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'98927691-a216-40b2-898e-5c1461722f6b', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9087', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'91b5ef69-1dec-4ad3-a970-5c3d3af02e9b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'63f35670-e8cd-418c-a910-5ca4797a6751', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'24ad8919-b5dc-4c01-ad92-5cb0727ec644', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'675c5d95-7595-48c9-83cb-5cdbdc90498c', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8811', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0f32ce1a-16a6-45fa-a6bf-5d013534ab09', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'628b77d9-1cd1-4416-b983-5d0688546f75', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2448e3dc-06cc-4a35-9c42-5d260035dbd4', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8db18185-3a52-4afa-9669-5d3bb8c83ab2', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'96104e5d-a1e7-4558-9ff9-5d7c930d3b14', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c2d79479-791c-40fb-b9b7-5d92f78ddbb9', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ac0b766b-7e71-4fe6-b7a1-5e21bdcdf7de', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bb8bb7a2-decd-413f-b700-5e3eabf979f1', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1ed557a3-465a-4356-a427-5e48c1954cae', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8860', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'92efa483-7bbe-471e-b2a2-5e8787432e2b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cb188367-1554-424f-9901-5e8f25eb3b50', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9044', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f46d3ee8-6854-435b-9b6d-5f05f9c0b69f', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9085', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'49d12f33-ad51-4650-a426-5f21c3a795ca', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'451b8cfc-5b1d-4379-b173-5f55ee7c1617', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9262', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4535ffef-68be-4ab9-89ec-5f9e67a9598b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'75e1a04f-dfd1-49a0-8085-5fa53588d9e1', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b0c466ee-0fbd-48dd-b9f1-5fdeab9ca8de', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8808', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'47f400e9-96af-4790-b5ef-601ccb9f1704', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9189', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'112e289b-4847-41eb-9204-602c218268dc', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'96cded46-bf62-4744-b889-602e3df4dccf', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e742c67b-d076-4fdb-82aa-6056e45e3310', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5255e7a0-1543-449e-8daa-60e8288f8246', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9090', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cce23a74-a208-4183-aa89-60f12e2c4168', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9276', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6da49407-ee60-41bb-97fe-61376e19ce2d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'043cb6b3-4615-4113-b53f-61a22efe30db', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9295', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9f4ea2b0-e77c-41ce-b7e0-61ae0bc83444', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9016', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'61b27c7c-093b-48f4-b513-61cfe45dab75', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ab9ed2b1-abdd-4c37-a9aa-61f7f7017453', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9259', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'307aa38c-1483-4c78-be33-62234ed43e28', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9280', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b3cce90a-eb26-4869-862a-62dac080d3a5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3738a7b2-d9d2-4d71-bce9-6336582a23cb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9183', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bc663efd-e119-4c3c-a0a3-63390d7b7905', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd6f34645-5d78-49ca-ac4c-633c0543ca67', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9186', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a5b5ea8a-334f-4f05-973f-638bfdc97958', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9282', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'51796ad6-52f7-4039-86e3-6399a3b6b853', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2b43eb0c-ac1f-4062-b670-63c4268af7f0', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8712', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c8aa96a2-42dd-4968-a9d7-640433558c3c', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dde193f5-ecfe-472f-8fd5-6483b5385b71', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cda12473-204c-4fb0-b9de-6486d0828237', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8659', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'99af9ea1-3668-4c92-b66b-64980b0bc312', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6dd77aca-64ba-4b54-a769-64cbedcde998', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8972', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5b498a6f-3a3b-4429-b24f-64ecff1bd047', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2d1f9059-a940-4d4f-9484-64f454ad25cd', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9030', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5f54b7ab-3561-40d3-8d2c-652342d6cad4', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'75fc2078-1f7f-4d5e-ab0d-653fc47db656', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ecb9ff48-6232-424a-88e7-6561da1757a5', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e422dbeb-5d9e-4497-a220-65a04fdeb450', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9227', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1046291c-2a31-40cf-b7ff-65e03c1c5c8d', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9000', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0594be17-1448-418d-b778-65ec77e3ae0f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9251', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2799872b-0793-4adb-8daa-65fa83b40ea6', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8929', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11aa806c-b4d4-4962-8b4e-6625e2f7bb2b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'65a72781-0778-49a0-8f3a-66381a009e97', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3237ce35-735d-4a39-91e2-663faee28861', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'698ec05f-0b7d-4df9-8c51-6644cb484ced', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9273', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'523a1cfd-8f6b-499f-bc4c-668d70238cbb', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9071', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eda0f8a7-42f7-451e-95b4-66af8db073a3', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6ef3fb90-70c8-437a-9072-670405e0f38a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3b8f0ea3-1d04-4986-a2bb-67209992f197', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9094', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b03834df-3fe7-47c9-9c9d-6734c1d34aab', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5961dded-a5a5-4aaa-a4e3-6786dd0bc636', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8668', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e704e089-49bb-4a67-9241-67afe36d327e', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9171', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'75608e55-691d-4f0f-87af-67e533edd370', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8919', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd2422b54-d415-4589-95ba-67e7e081609c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9306', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0cc1e965-90fa-449f-8910-6825f1a47108', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8863', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5dce0b56-5e74-4ec8-a648-68328abe1941', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ce82faf9-b06a-4222-923e-68ec6f3ca447', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9137', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f41f4e69-3068-4c80-ae05-68f002d415af', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3349fce7-6a73-4a00-86bf-6918c18ea908', N'superadmin', CAST(N'2021-03-03T10:55:41.767' AS DateTime), N'A', N'DB_VENDOR', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_VENDOR_563C70EFBAE26AD2EFA59A3329CA386FB7956869D1A83B4EDA6EC47831E25A03', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3d76f487-3333-45b7-91ab-69281c413a14', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'18a4e69c-6ff8-4f6e-acba-694166a5d7e6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9027', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f5f2c056-cb1b-489b-a686-6949954c147a', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8b5dc8e8-7c12-4089-9cbd-69d263ae1dde', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'451716e5-04c4-4019-8878-6ab2c2cfe06f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8876', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ecfe891a-d885-4373-9b62-6ad358f351bd', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9055', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'04880e77-33de-4efa-8944-6aef335400c4', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f3cefd87-1e72-4e76-b834-6b039872a26f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7cf726c1-8418-4130-b3cc-6b05081c1c1e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8845', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'960cb64a-14b3-422c-b920-6b42bf16b712', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2dc4d33f-d7f2-47f3-acdf-6b45b03ae50b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8609', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e0092d4b-571e-4129-ad92-6b4cd2f3726d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8826', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9bf4aad3-0f4c-45ef-a340-6b6b3168a481', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'287eb585-4b3d-46aa-9700-6b8b12a91fd6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'10147389-9397-4e62-bf9f-6bd4ee64a0f9', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7f8c6a05-2f5e-4c3d-8b73-6bddf7a5ec60', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aaca5344-10a2-463f-9b68-6beff90df275', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9215', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'501b572c-f4b0-4f0e-9fd5-6bfac7674a92', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'34315ed8-8b11-422e-bf0f-6c1b02d5ebe0', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8670', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'54d0d5c7-3114-4f1d-a9f2-6c25c9a00661', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd640960a-13a0-4306-922c-6c50f3da7bea', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'96dd8825-4501-4b80-81df-6c82426ff9e5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9228', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'15010ea4-5d21-43ff-8644-6c9178ac9e7d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9199', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'73cc5127-7f6c-492c-8a77-6cc4a426e3e8', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8761', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5c116c91-2e39-48c8-abc7-6cd7f2b46ed7', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9195', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'44eea051-4f18-4bf7-81da-6cf494eae094', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a354b5a2-3835-433a-a626-6d1ea1cb3b81', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8671', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'05b59287-365c-4df6-a73a-6d32054ea6ea', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd3541c8c-89fd-4ebf-9401-6d8f8ee32fc1', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'91772ba3-6137-4b1b-82db-6dcdb9722add', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'58c8c342-4c9d-493a-89c4-6df8ea04fcc1', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0de159fe-ff6b-4f53-825d-6e24049f16f6', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9165', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'88f6897e-74d5-4338-a1b9-6e28b9e46344', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'24789640-c0a5-4cee-a6fd-6e302bb34f22', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8602', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8f5faf07-4fd4-45f4-9435-6e47c0efe8f6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd4f2ca0c-cb32-4ada-964c-6ec79ac785a9', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8888', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9635511e-6464-4210-a459-6ee306a097ba', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8903', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'edecf0f3-35aa-4da9-a6f4-6f28ad984409', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9103', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd7a1a670-c9a8-491a-b7ea-6f64ea6e6d16', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7ef9f7e-ce03-4e65-9e25-6f74c8a0619b', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9066', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a65bd9e1-8dd8-4f94-aaa2-6f88ce47c6c6', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8964', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'58a512a1-5fc7-491c-bfe8-6faf50f82683', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9212', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c5a10d24-96e4-4d95-9415-6fdd2424a11c', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9082', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3a37d11a-7cfb-4474-a8b4-7060a4a746b8', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8777', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'58fac1dd-02b5-41ad-be6a-7096262888bc', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8780', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'28c371a7-d7e9-4cb2-a567-70a07b76e0db', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9286', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b684eef5-bc25-4b42-a57e-70a097426059', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8869', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8b31f0d9-a23d-4d88-bdd9-70bb70bf4ede', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8991', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'84a92ff1-86ed-4658-9a52-70d1acd7e250', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0e37944f-a399-4962-96d2-71144384426c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'60f6f21d-d703-4188-adca-712dcfc20737', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ae752bea-2c78-4c98-8cf1-7162494c4acf', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9098', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'536aeaf1-15f9-49de-bfc2-717cc1240c52', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8766', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'40439437-50f5-42c8-b7f1-7199fd8264d3', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8948', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38f6cee3-ea1a-49c2-8705-71b650a665cc', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5bcfbf84-7e59-4c0b-9ab3-71c0a56a382a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8980', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'70bae283-9d23-45c1-b2f7-71dc3d9aedd5', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8665', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2448c3d4-50c8-4600-9fc0-71f654a48878', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'61063ec7-199b-454e-8a3e-721ca288ef73', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8685', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8aa6e620-0929-41a1-b5c5-724859dae954', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8817', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b9d92974-3c8e-4d03-baee-72b68b86a977', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3af31fa4-a6d4-4b5a-bdfb-730cd8f6674d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5072adbc-ee21-4208-9de7-7350e8327548', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9a5d7a47-d2f8-4ee5-b9ae-73b7b6ed3481', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0ad19afb-c9fb-4a51-8f5a-73dd51acd1c4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9239', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e84de487-975e-4afa-86df-7437f66d7763', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9004', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a91c4651-c9ec-4431-8517-7438f0c9676b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4b50d24b-21be-4a5f-b09a-747a9ad2222c', N'superadmin', CAST(N'2021-03-03T10:53:45.697' AS DateTime), N'A', N'DB_SECONDARY_UOM', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_SECONDARY_UOM_9FBD42540143F97E2B9CC92A79A3FD38C1AC240B1B7DA228BA956281C7C5A789', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'116e7f5d-6df8-4dd7-935b-747bc7d35dbb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8914', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'15a7e0b0-40b7-4e09-b958-74f56f90f064', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9173', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1874587f-711c-4e8b-8382-756d40b31dee', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9f958399-e3c5-4df7-bcd3-758f2167fc86', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5fe25865-ba48-4211-8e72-758f6fbbefe7', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9040', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eddf66f9-028b-4514-bf1e-759a4c5245a6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8bf0d9bf-a789-41f0-90f8-7626211688e1', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11762982-da16-4315-8113-7633ad4a2d05', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9246', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd46c90ad-d0c9-4e04-acc7-765c6a79a363', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'87b3f949-c184-497c-a069-76707d08efab', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9233', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'004abe13-3a2a-4501-934f-76a25864b34d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'74f254d3-6f8a-4ba4-b2db-76b6c0e9405b', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'51561d2e-243b-4da7-99e6-76d6109fd9d5', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fb14c397-ab8c-4975-abb5-76ed731b7112', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8951', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'56048c63-4ce3-4e11-910c-77b399fdd91d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8836', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3f17e9c0-e4fc-40da-8902-77bb3fc6daa8', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9278', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8865a0aa-ebd0-4759-bc2f-780bdccd6a92', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4c016c54-d4f2-4867-b5b7-78128591287c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'25b2747e-9275-49e9-bb2d-78282dbcd073', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8d06faa1-ac8d-4385-b58e-7830dda66969', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9158', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'74e252ef-a700-4d54-83ba-788aa1c69870', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6014b4d7-a6a8-4455-b691-78b079941321', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9034', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fb7e09ab-9da0-4686-bb32-78ee49d00566', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8683', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4ca1c2e0-9b7d-41cd-a007-7906c03f7a37', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'73a5fc0d-1009-4106-b019-790b801e7c3a', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8774', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6856201d-6b75-4258-94d9-790bddf5a0a7', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b18980d6-ccbb-460e-bd53-792b1ce72b35', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8841', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c79d0947-e568-4de2-aeb1-79674fd595c0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'301c6746-df2c-4ace-a75a-79810a6bec59', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'498a75c0-ab36-4963-ac0b-7996b380445b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9f657045-d160-4c8c-a753-79f7ff7285ee', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8947', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c323286a-b399-43bf-bb1d-7a413eeb96a0', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9236', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6854dc64-8004-45e1-9acd-7a59dcf2fa5d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8890', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c037c3d4-1b25-4ce3-a54f-7a923cf76811', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c100db8e-567f-4d13-a622-7a92895d1b66', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8896', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9b000371-292c-476b-9380-7abd71f24986', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0dcd8b2a-f5c6-4bfc-93d3-7abed6983958', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'52a75557-679d-41b4-92d0-7ad6584b5a23', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ccaa8687-5dbd-4850-b5ae-7ad6dc9d5e65', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8709', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'02db2f0d-d34a-4bd4-81a3-7ad835e7f624', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e672cc74-1a2f-489c-8b89-7b6f9171ed9a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9017', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a5c8ac13-7026-4cd6-a796-7b826f36a0ce', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9257', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ae61c435-8aad-4b5f-885d-7baa0435a17b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5e4fd3e3-ebd2-4bf9-ab34-7bc43174f279', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9151', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'98ebab71-efd0-40ae-bc73-7be4efb8de86', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9009', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'90d398cd-b541-4d0f-90e8-7c4af87c3069', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9111', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a683c3df-9fa1-42f6-898f-7c75fcf74b1e', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9150', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a82f615e-1ac7-4748-9ebb-7cc08a56a216', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e84e2983-cebc-4502-81ac-7ccdc9d341e6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd5bcfde2-23fa-404f-b6c9-7cdb2fb29aaf', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9053', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3ff5f993-8432-4766-a834-7d1a0217f0e3', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8939', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd32a0dba-6c03-470a-9242-7d3eea00fa9a', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9238', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b9f5c6fd-1cb2-4b0b-a907-7da633fa9ede', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9146', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'045f9d51-fe60-4f92-8c70-7e1f7db86d25', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9193', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a4f32149-2f56-4b08-8ada-7e5bbc8bd67e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b7d2de41-acb9-4560-8dd3-7e9878a5d082', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0c7cece9-5f18-4747-b0d7-7ea3bbb72422', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4d949018-6878-4fe3-9ea8-7ea4ec9563ad', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9213', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ad0c7948-415c-4503-aad9-7ec76d7731cd', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8793', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'61813bc4-2d78-44b4-93a2-7ece52748a75', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b306369e-b5b6-4cc0-bf02-7f185cd280f9', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9056', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'40fc3d70-7156-40ce-b58b-7f5b263826e7', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a93568c4-8a79-4208-9af9-7f6df0ab620e', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9007', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c5eeb497-2fa8-40ce-8ef3-7f8798424a91', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'954fa3ab-4f0a-4008-878d-7fb1bd02e920', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6e51aa7e-2268-4537-ba8b-7fd5a97d7d27', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8970', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd79d6564-dd2a-42ac-aadb-803384c6298f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8857', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eb913f08-4f4a-4b18-baf0-805e3108f794', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'57e86ee4-5d3e-4bdd-bb4d-8065d457e2ed', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'50913bbd-0518-4c56-a250-80834f04a80a', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8613', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8fc4e7a0-e14b-4944-8413-809ef2fad71f', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8760', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'95a573c5-c902-4864-bac3-810c6e08d9b3', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9049', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aea49904-e53e-443e-b917-810d31c35381', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd23f3ee5-acbc-4088-96e0-811a0fd6e929', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8834', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'627eed7b-9268-4f95-b0ea-814960903e5d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9296', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'13f3edc7-0154-4b49-9f71-8199a3287ec1', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'39366e02-f523-4914-ae43-81bb1a453b64', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2907a75e-bf5e-4d40-9939-820b24a9bec4', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9190', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2defbdd4-c427-41f5-a694-8235a94ce89d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3ebf47ca-5ccb-404d-8547-825eeaf6aa97', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8792', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0059583d-425a-4e16-8e52-82825ffb374d', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8990', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f74ab1d1-e67d-47f7-a004-82e6c2aa4e29', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9121', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c1a75dc-dc91-485d-8daa-82f7a3c7c954', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7d4d86ef-6b5f-45f9-866c-830afb81f512', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'29752417-c3ae-444b-98cd-83107c364cab', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8649', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f7a4149f-785e-44a3-a0f9-835acf5ec050', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9301', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'542f7729-3ba8-415a-8e9c-838bd49886cd', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8644', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fb6d2b96-d2c1-4e3c-ad34-8390735257bf', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8795', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dd5c52f7-996f-436f-82bc-839339745d6e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'153117b6-d86b-4885-b668-83ff5afe9eb3', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bcd684fc-20f3-4c8a-835a-84596939d483', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f75c921-c453-4cc7-825f-8477c7f7de25', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5f29f9d1-abc0-417d-a1e9-84af2677dd18', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'427e50bf-0d6f-4eb3-9577-8501d0a00e27', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'be839562-c60f-47be-abad-8543b152c8ba', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9230', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'70af48ab-a708-4a03-8d7f-85a57677f562', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9268', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'24245fbb-6ed7-4192-bbc1-85af851c9388', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9178', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c077f714-09cd-4a2d-8b2c-868b4adb0a03', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9267', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'04d1ff6c-34b3-4273-a1c1-86cdd15625c0', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6c09de67-b9ec-4260-9646-86e30d854318', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'200acbf8-6554-41ed-bad3-86fd746ebb20', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8874', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3929f9c9-6037-4663-837f-8719652119d2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f61b0f3b-6d92-43c1-85a1-871f9a35056c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a866e079-58db-4764-95c5-87684bfb00cd', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8962', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'29a66a40-7757-4671-ada7-878833bf4e85', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'41a6122a-023f-4778-a1ca-87c14dd356e9', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'225fab4e-97f5-46cc-b50c-882571570025', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8798', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cab94fdd-018b-431d-a018-8826d82d5f4e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bc0e5354-5bb8-434a-a25e-886209b661b4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bb90805b-95f9-4014-aa02-8892225e2800', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'15c69458-63d0-4001-b49b-889434986e5e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8866', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2db14d2d-9c3b-49cc-ba04-889c54fd76a0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9b135cbb-0a26-4429-8960-88c7b216485a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9097', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f7fc5b34-09d2-4195-8843-88f0c2732e6b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7a13d61b-35f4-44f8-b60c-8900155a1d86', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5b19e14e-952b-4c18-896d-8901149f62d0', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9196', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'82f56fe5-8097-4586-ba41-8929dd5fc07c', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8946', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5735455a-0436-4c72-bbcd-895c76746449', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9130', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11884026-ca8a-4165-a9a3-8a5a860b10b4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9063', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e92c7816-7a99-43f9-aa92-8a6f50db8bfe', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'71d2a098-1bd4-46de-94af-8a77f4d87627', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7aa00328-06b8-4bd6-b1b0-8ad8f76108fc', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4828728c-9afc-45a6-8e9f-8ae6df61d754', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e198968f-d29f-41e1-b57d-8b44ea4aca58', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9298', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ee0a45e8-28ec-4cad-9f44-8b47ad7fbd1a', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c1b77147-282d-41bb-bafa-8b5c870e4188', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'00537697-8d6b-406d-96d4-8b88ebc21311', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9128', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'14c69f2e-11ad-4dbe-940c-8be9468066d3', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'77bd5de2-e2fe-4b5f-8f90-8bf79b88e513', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8786', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'07c3ee91-c95a-4c9a-a69b-8c279b6b1f96', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8983', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0250ecbe-65dd-492b-b54a-8c2b024f0619', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c17fa454-3795-4b6d-9bc5-8c56a2d5a947', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1ce068d7-1932-4ba9-96ed-8cb4712b8cfd', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8763', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'afad7658-ee53-436c-9f0e-8ccac49cf2c3', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8771', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0aab5a69-8a6c-4a2d-9641-8ccdf76eeca1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8773', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'148673dd-7c02-49ab-9bf6-8cd122efba3f', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9cbb7995-1069-4ab1-8c90-8ceacc9e7dea', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9116', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9949ae52-2379-4a04-aeaa-8d49edf140a1', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'40d1a31f-fccc-41f8-a94f-8d8369a8cd27', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9220', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fa833aa1-f9c6-4f54-b92e-8e4beb73ca6f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7450eb4-e7c7-4742-8831-8e780b30bc84', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9a64e1ae-72fd-49b2-adb0-8ebe8570a830', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'29f89fad-2384-4141-bb71-8ed91c8baba4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8453898f-7522-4bef-b6c7-8f0f6bd51576', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a68c62a5-5f1a-43a0-8eb9-8f33a8497e0d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b4116d37-e6b2-4499-87e2-8fcbf4b56d6d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'07dd2cd8-8565-49ad-babc-906255d5bdc5', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e5fb5109-0e9a-461a-8b22-90837591a9e7', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a0f12e1d-0ffe-44a5-91f1-9091fd81a876', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9b01dea1-8be2-46c6-be15-909ba042aafa', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'92dddeff-50bd-4cf9-804c-90fa2ea9414c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9163', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5e7d9da5-d542-4772-8b2a-91039c361e4a', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a3c8a6b1-787f-46b0-8c37-912c7da5443f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9187', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'689ec9ad-59f2-485a-b1e0-913c25cf6f0e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'04091e0c-7143-41c1-bdec-917686dc460c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9272', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'464bd866-9141-4873-9d2f-91aba32c1f4a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7cdfcb28-4642-4647-bab4-91cdbf244a71', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ff8f3077-d8f6-4bab-a5c8-91d90679297f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9294', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cb72a08c-fa95-4c48-be03-91f8795548e0', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'859fd73f-0628-4c5a-91e9-922b1a20b472', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9088', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'53ae388b-85df-47ac-b8f6-92767a5e6395', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9010', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9a13a50b-b258-4c5d-86ce-92c92e39aa57', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9096', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8cad9235-06ea-429d-94b0-935de580cbbc', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1c03dd79-34bb-4b96-8df2-939df0d57ae8', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4c218d8a-d0bc-425c-9122-93f54be8915d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'199ff8e6-cafd-4c6e-b377-9414944bb7ff', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'62215647-2a8b-422b-b835-94a1fcae29d4', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8824', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4f9f182b-45a7-4d10-9c64-94b42efa52d5', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'715db4c2-5002-4e85-a243-94ddec8735c2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'97138eb5-fd22-4982-9540-953b8f6c6183', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8621', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9ce4fbe3-d010-4982-b59b-9567fd5733a7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8913', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'671b588e-88c4-4465-9c2a-957aad54ffa5', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9013', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c36ec409-cbf0-4768-bf36-95e3d2fdb989', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8812', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b55747e8-81be-45db-a9e9-960b34599dd0', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'85efeef2-bfd9-4c25-b87a-960cc847e891', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8fecbc4c-dfa1-45b7-b4c1-961854c34c15', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9075', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0899d594-ad18-4677-91ec-9631f42c4bdb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8842', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'81a2327d-e5b5-469a-ac39-963eed46a986', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8684', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b621ce11-5f27-4cc3-8845-965542d6426d', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'40d5b9ee-8e6a-4572-b4ec-966dd3484607', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9292', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b234073a-bc9b-4ea8-b64f-96c0443e2ebb', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8664', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cbae4e51-5e37-43cf-b9b3-96e7fe97c177', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4158128e-0af6-4ed5-96ed-971c2c9d5bdb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9166', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b990544d-f6a9-4769-befd-973c5db56245', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9149', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1f0252a1-9e16-489c-a63c-97581bcdb756', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6c715f27-6593-4bac-94b8-97cdea563518', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8895', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'48e68b46-ff5d-4f6c-bf84-9882b9ee5916', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e26dce73-a2b6-4902-a590-98d6733b7941', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9f687762-4b1e-4d53-acd3-99365f01af94', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8601', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'572c86e6-9886-4532-bcd1-9968166d53ea', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8617', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8daa9709-021a-4a8a-9546-99ce226131bc', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'45be8814-4f3e-497c-a54f-99f6450edb8a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9052', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'28cd530b-1a19-4356-ac79-9a4a773e94c6', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9141', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9635bb67-9881-4bbf-9afc-9a5754eb93f0', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8847', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1975687d-9a5b-47b5-92db-9a906048c6cb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd16d6c57-1ade-40a1-bdc7-9aa37d18e6bc', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8893', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2f5c0f07-c775-44c2-8534-9ab1cd577c5c', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'72d13679-69f7-4e4b-a37a-9ac59b147093', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8646', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'977a1244-c01d-4c73-b150-9b1c2b784730', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a70034d9-7859-489b-b5c5-9b487182bbbf', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'62db4550-2bf6-4913-8269-9b70ad1e6381', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9140', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ea55c620-3c51-46b5-8176-9b98bdadeb55', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8823', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0c5b2665-e11f-4a3f-be53-9b9af4fa0bde', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9081', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'69348973-4c73-4d83-bfbe-9ba702b2990f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c902314e-19a9-4c60-ad12-9bcbb757c241', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd09d8ca1-00d3-48ef-b747-9bf7e5238917', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9200', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6270d61e-a221-4602-b0a1-9c1eca57c4ad', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'17ffc73c-8866-4c49-a1e3-9c2c4f24b391', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8844', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'816c5e33-37e9-4d40-b151-9c70f5011098', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3cd9fba6-201a-453d-9c95-9cc2e2363899', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9114', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f3b8dd27-bb74-4fa1-9832-9cc71cafcc54', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9147', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38797375-4741-43dd-96d6-9d0d87ce00e6', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8861', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'145923d4-b2ca-4636-8917-9d0fe0fee86e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8af3bd30-d1a8-4377-8c1f-9d2e83a3b610', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c2184650-6c26-4075-8cbe-9d94ea05fc6b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'227a553f-b0bc-43b6-86da-9db023911ef1', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9051', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'24ada9cb-af71-4d74-a7c4-9dcbe63e2106', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cbcc3362-1aef-4dbf-bcb4-9e02dd4f4c34', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8768', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd13796f8-0aec-43de-a044-9e1af28cf056', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9185', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'275e8c65-a808-4f90-92fa-9e3536800893', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8934', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5c8e2c33-6e73-4cc2-bf30-9ea7139f621e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4059a8ea-555f-4a76-8d50-9eb76f512752', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0c7950e9-d3e1-4638-a436-9eea750ef75a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9077', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5878146d-d67e-4cf2-9eea-9f03b925cb7d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1940b7d8-4aa7-4a37-ad15-9f0cc1cd3512', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a5bf06b7-be04-43ec-a057-9f129b43e23f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8851', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'93efb4f4-e52e-4fe8-98f4-9f55a13fe13d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9076', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'79939465-27f1-4351-8e99-9f6d63f6c5e7', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9254', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd099ebaa-cb99-449d-afc2-9fbe8d10cba1', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8785', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'827809ad-5ae8-4e9c-abd6-9fc113ce7542', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8974', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5209384d-2fd4-4929-a927-9ff3d66cc42f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9287', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ec93b07a-b14d-4ab7-af28-a005f3a329d4', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9109', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c1cf406-fff4-4cc0-8ed4-a03db4e05c14', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8963', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'910ae55b-b3a8-4b86-a08b-a08b1b9285c2', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8865', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'196fa9f4-409b-456a-8da9-a0af96699948', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9206', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5db59efb-dd52-4370-a32c-a0b4e8aad647', N'superadmin', CAST(N'2021-03-03T10:56:48.080' AS DateTime), N'A', N'DB_PRODUCT_SUB_CATEGORY', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_PRODUCT_SUB_CATEG_16B5533CD8917F634F0295A64568DB2C1776EE91A08A94B19E071668252058B5', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b2bc6a1e-3fe0-45b9-9dd7-a0bbf91bcb5e', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'84462757-6127-4850-a085-a0db5f248f0d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c62d4fbd-858e-4106-a538-a0f4d1da57b1', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9277', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'012da08a-231b-4ff8-a838-a0fdae08abec', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8975', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3782bb59-62ab-40b6-a7f6-a1026b66ad2d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd6b3367-ba5c-40a9-8135-a10a70343681', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11469aba-3b7f-4066-9c3f-a15758f810c2', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'06f3db45-db98-48f1-876f-a17487e0bc90', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8632', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'60eafec1-7903-456c-9c71-a17e628314f6', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'875ee5bc-8d0b-4441-a948-a18a898315c4', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8814', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'328fd698-dde9-408f-bb2c-a18c1bf22542', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e18dcc95-8e3f-471b-bb9f-a18c3ac357fb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8839', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e7900ecb-d378-40f9-9684-a18fbc934ea7', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'95ef7967-762c-4483-8c0c-a19ab31e62ee', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9048', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'72190c81-2741-4a1b-9380-a1a2d8d6676f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'67fbe333-dda7-4eb5-886a-a2054db35fb0', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2d236bd1-2c1a-4b5b-8718-a23401af53bb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38de3ba4-29bd-44e4-8b84-a23f4c10d229', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8994', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'94d82e0a-ecfa-4744-9120-a27c95586977', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8915', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4b482bcc-5fc8-4736-8dd7-a285b1592cba', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fc676b10-fac5-4cc5-90f3-a3079016f190', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e2fa3367-ac22-4020-801b-a3112d0bc79f', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c23ff655-2df7-4188-ba54-a326a02984c8', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ec10c368-41a6-456d-8940-a344d57d6bfe', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b54c69ad-e63e-486f-9ff7-a365085c97dc', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8633', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'59f9147e-ba5e-4e40-b5b9-a3cebbe3989c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ceab3263-304c-4889-a0b1-a40863f8df97', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fa979e28-5d9d-405a-8234-a4104a355f94', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9bfef1c1-6783-4eea-a80b-a457de89f01b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8846', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6db2d3da-d5ed-49df-a8f4-a4925b57a1cb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7374be9f-bece-4d13-b73f-a4a56bec8d4e', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0620a3e5-b653-47e8-8476-a4c1cda8c38e', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9142', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1b4d9621-3044-489e-8922-a4da7e4aca64', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8920', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e5283a6b-50f0-446d-84fe-a4e0cecb4368', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9123', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7da1600-dd7d-450e-aeb9-a516cb15e085', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4cd85649-3bfc-469d-9bbf-a54471edbb29', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9202', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2742c4a8-1945-43be-b77f-a5514544be16', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8600', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5c75cecd-cbc1-4124-b615-a554ff38c979', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'39030896-5b77-4cb8-b386-a562557d20a4', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8957', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4830f72b-9d81-4fad-a6a3-a5a0902e9739', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1c9006e8-7613-40e2-b6b3-a5cfc203e3be', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9144', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f154c294-3e25-4c62-98e0-a652f9bd7808', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'779eb445-d3db-4b36-96bc-a6ae5981066f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8848', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a454c2b-9a38-427f-9965-a6d0cc4617e3', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9089', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'feb98aaa-d2ab-43d3-b361-a6de7ddae54d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8682', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd3cfff93-a1aa-4a09-9418-a6f9b91784d3', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'35f44e84-b139-4add-a80d-a757e3b02c9d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4f048f3f-7c82-4a83-bc54-a7585ccc0b76', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'061cf85f-5fd3-4352-86c8-a7a6898968e4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'066c58c9-ff63-40f1-96ed-a81c700d314a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8998', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e6bd643d-58cc-4eb0-9168-a8442b444a53', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'48079190-04bc-4aea-932a-a8681e1fa31c', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aadfa6cd-fa5f-4bd0-9524-a90671fc0af5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a33c0bad-12f5-4628-a18d-a94152f22933', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5e81cd2c-cf13-4030-a668-a9794e55b632', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'42f5a70f-d03e-4704-8a15-a9cc3dc14844', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'dc32602c-4b30-4bca-b85e-aa09468816fe', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e3c6417d-afb7-414f-886d-aa89853858f9', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'03f39a47-962a-412a-8869-aabe977c4157', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ed5b0a29-2314-4777-9827-ab1efd2c3427', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1416d0bf-71d8-4b09-aad4-ab22654f3499', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ba5ff23d-c61d-45b2-bcd3-ab474fffc5df', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9028', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'420d48bf-a55f-4613-8156-abdef009e4b7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'63d97558-8868-47e1-ad30-abf034858eea', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'21507da3-7e8e-4416-847e-ac0490caef8b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e0639ac2-2064-42eb-86c6-ac0625780b5f', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'26b9be7c-a056-4bf8-97c7-ac727aaf3f2c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9127', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bdb0a7b5-0ac3-4300-b563-ac895874dc88', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9126', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8b1688ca-b1be-46c6-8f3c-ac928fa111ed', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9162', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8c027d4f-5912-478e-b69e-acb4d9639387', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'89e9b3e3-9f49-4682-9be4-ad1023a19c92', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9105', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'94ea57df-1036-485c-a037-ada792c4e939', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'47280ef6-aeb3-46e4-b1d6-adac416f9255', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8858', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7b376ced-320a-47ef-9000-adcddd536114', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8941', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd3fade25-0085-403f-9730-ade03835a4bb', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'928a810f-77d3-4e5f-a83d-ade77efca039', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9124', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7c844e00-8639-4ca8-a0ac-ae1c24e40512', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8833', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e8483eba-2164-47ef-b08f-ae66a1f4dcfd', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8662', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b2b89127-0502-4fc9-9007-ae8ee708a103', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'979d9aff-3eba-4509-b9e9-ae9f1737eb3b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8717', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b9d5e6df-1aa7-4e0f-878d-aea60e4ca970', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7bd8d3e1-78a1-4be7-b1d3-aebf64f1a64e', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fc4ca053-d15a-44e4-8946-aee0339ec5dc', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8feb9700-de68-451a-bc7c-af02ea993bd9', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9237', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'287ece45-77a1-4626-ac7d-af041b8e5bc5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'30049b12-6cdf-4472-b1cc-af6d52b4dfd2', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9148', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'acd8e38a-6dba-4d25-80f0-afa29fd0342a', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8897', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a4e0568c-2e7e-4331-a387-afc7c65326da', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5e8d44f0-2783-4a2f-8f9d-afd82bfe34e8', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8788', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a59696a2-7b1b-47df-ba52-afe35408da3d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e5fd1354-1e24-4075-a98a-b00f2e687392', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8762', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b2802cf7-b702-494d-ba2f-b00f8d658556', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8776', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'45aa567e-0711-402c-b41d-b044920a6ca7', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8995', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9a86adbd-f620-4bab-8a96-b048b5ad09d4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9264', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'da86eec8-d23a-4166-a84c-b05ffbbc01c6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ab0c6d8e-7e6f-4791-a796-b0f5e4109498', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'59d536a0-9031-47d4-b4dc-b10f913b09b8', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9172', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'abc92eed-f113-41ab-a8a6-b12e9d3b9106', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8942', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e262573b-f239-44a3-a4b7-b16de0dca630', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8803', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11a61d91-c48c-4170-93f9-b1a781a49958', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'58d99258-bea9-4a8e-86dc-b1d875eb1dcf', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8816', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'affbddd8-8d82-4ee7-b550-b24bbe4dd5a8', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9062', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'03d4a926-8f4b-4270-8f60-b2a19a79db68', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8764', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cc2d3fc1-c2fb-49d1-951a-b2ac25ddfc75', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8872', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'51fcf94e-7220-4d96-98ea-b2bea53eb5c8', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8af74247-186c-4bd8-8b4c-b2c7a7b47134', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'445449b1-f181-41ad-a226-b36700b90ada', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8be4b02e-2a30-4e08-8ca1-b367eae5f1e1', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'28294560-110a-4ec0-a865-b3776976392d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3f4ca788-962a-48a3-9f2f-b42cf344b801', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8672', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1d21316a-da76-4244-beb9-b44c8d4b6b0e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9026', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'92bd6c97-dcb2-4a02-8ebc-b4f30d4cc5ef', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9181', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'62c351a3-1a2c-48ce-8217-b5a58371b086', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9303', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eba5de9c-a0a4-4558-a544-b6206acf2205', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5b4aaae1-0873-4e42-a38c-b64d4e548a2c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9271', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6efee004-92d0-4aeb-bdbc-b64ff923c0b4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ba74edc7-4249-4ef5-8502-b6d96e08e2c7', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'560ceb30-1d17-4df5-9111-b6f9fe3de5ab', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1ac9c64a-86bd-4f98-9989-b6fcaa459c1d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8909', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e0785a41-5d36-4934-b891-b7602c7e6710', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8711', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'33b051ed-be3a-49ea-a6ba-b760caedd3cc', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9104', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1b3d2cfa-a509-45a6-848f-b77a1e77f02d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'32b8065e-72b5-4cae-9b0a-b7941e854bf2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9265', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'781563d4-3650-4b2a-bd67-b7e8d116c689', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8619', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a543ad1e-d0e3-4da3-b799-b8282950c318', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8838', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7d931a12-b210-4c5e-a20f-b88578280da6', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8622', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd9b16051-13b7-43c5-9507-b905e41b02cb', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8906', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd67320d0-1484-443b-a7aa-b93dae56996f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2bca1f76-2440-428a-a5f8-b958f5cda937', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8660', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e6bb2bd5-303e-4ca6-9094-b965d775bc56', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8650', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f3ad1973-3f33-445c-b5cf-b9e9688c4948', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8924', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'53ce60c8-35d5-4800-a7a3-ba06fe993031', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9057', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c1deb56f-3620-493e-82e7-ba3d373d26fe', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd4491847-14d4-4336-b5cd-ba92693d025d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3ca161d0-c7c0-429c-b76a-bad3d634c169', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9211', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6b54efc1-c42f-49de-a42f-bae21bc8b4f2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a3a72c0f-e2e5-427d-832b-bae33a3c7ed6', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'25ffb9d3-7e52-4002-a9fc-baf06726f434', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8932', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'73a3de20-e352-4fc5-82a4-bb76f02dc2cd', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9167', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e5dee275-f2b2-4d6e-846d-bbaade351cb3', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd1052d7-fc9a-4bd9-bcb1-bbdc076eddf5', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8663', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1471f246-2f40-4da8-86e4-bbf20e0b062e', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8615', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'759e88f1-efc0-4ec6-bb52-bbf3391301d2', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8996', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bb633bea-4ad1-4a29-8ab1-bbf5dbb56dda', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8976', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9fd35312-819b-4a0b-bbca-bc6b7aeb5085', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f457fc6b-9139-49a6-be8a-bca831829a90', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8968', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'359e8231-df34-4400-9f26-bcde8379a156', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9118', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'79cc03c9-57f7-4224-924f-bcdf22934aad', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8943', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e70923fc-e826-404c-b1a5-bd2b066380d4', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8715', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3efb04a6-5727-4ebd-bf67-bd2d6269436a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9041', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd77f2f4c-4983-4931-bee8-bd31538176ef', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9174', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'95bd7976-fc70-428e-bb3f-bdbde0124426', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8935', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c2b6f553-42d0-4475-95ad-be48c595c054', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8959', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ecd7bea3-faf5-40e0-909c-be4b977ad67d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e3847c67-68e2-4194-a4fe-be66be86def1', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a3fa3476-79c5-4ebc-a955-be9b7cce7c9f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9176', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'70814aa3-60ab-48b6-bbfe-beba2241c777', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fd226853-1c88-4824-ba33-bee8ba8d346d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6cdf0f7b-d883-4ddf-b5c0-bf0d52fc843a', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8828', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1b4e974f-0e6f-459d-b7b4-bf9ecf28f3c7', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8993', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd24f3c7c-343b-4198-bfb4-c00d806854fe', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e8c179d0-2242-4052-9b11-c00f72710ca9', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8945', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'036406bc-1880-4248-8f64-c049f983e743', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'79d38545-9f6d-4f0b-83ca-c04cab09ad9f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'85b725b8-316b-438d-b493-c0600da1f1aa', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5c2dd96e-b900-4975-b31b-c0aa5fbc7e5b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8936', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ce107d10-ffec-40ae-bacd-c0c14e83f100', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8900', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0fd71771-4028-4644-a316-c0c1f6e074c8', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9266', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'081f8e69-0b69-4b9e-9eba-c0c364de18c5', N'superadmin', CAST(N'2021-03-03T11:02:42.883' AS DateTime), N'A', N'DB_VENDOR', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_VENDOR_563C70EFBAE26AD2EFA59A3329CA386FB7956869D1A83B4EDA6EC47831E25A03', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a9ffbaaf-afa4-4479-bc0c-c0f4874828b5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1bce7831-1611-43d6-94c5-c0fba96d314e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9214', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c30ac3e3-2bab-49ed-af3c-c1094578c191', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0cd4d68e-a48a-4005-82c7-c109fee2631e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'196a246e-1b13-4cf4-a235-c1197f198426', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9110', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0505c49a-70af-4916-a2ab-c14284b1a4f9', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'de4e93ea-ff22-4703-b728-c14ca2fd09fe', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9289', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e432245d-a590-41eb-9201-c1505f2ef9a9', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8669', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'561f634d-5979-4bed-928a-c1542f6f4355', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9024', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'712abf14-3b26-4b1c-8eb7-c1671081ddd9', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c79de3d5-c1b4-4be9-804f-c1d003c02a4a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9020', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fa3b99a2-9317-4781-bdad-c1d01a3cec6b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8966', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2649b0ae-dbf7-47b1-81ed-c204ed493fba', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9168', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5b42a20e-dc7e-4d75-bf5e-c21b85fbfe31', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'764cce1b-adaf-432b-bf21-c28116d04cf0', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b06b0727-635b-463a-af42-c292fe44f7e6', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'62beb773-f0fb-47a8-bc0b-c294b1db123a', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8864', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38a5358c-718f-4760-9af1-c2c2f9f799f2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9242', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'62e1e0c1-73a1-4618-acf0-c3de1c1421d6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'081e61e1-153e-4f1b-b85d-c41ee2ef8d0b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cdffc17b-c339-460c-887a-c429d63b7eaf', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8801', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7d1773c-a556-440a-90db-c433f9abd4af', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'14d828a4-f9d1-4e10-866f-c47464ab7a73', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b91bf842-b053-420f-a259-c47771890c45', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8775', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4ecbfc6e-3fc3-45f2-8271-c48c2a2dd0e9', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'680dd99c-2844-467a-9113-c4a0c237f625', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'315719bf-75a9-4013-bbb2-c4f01c4a14d5', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5513c8c8-65f9-4163-9990-c5349db0dc29', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9297', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2356715d-5bf2-4f7b-9f7d-c54808ecb9bc', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9179', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'639311ca-35a3-4852-bd2c-c560d44c3134', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8892', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'99760ce0-ce86-48b7-9003-c576517145d6', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9022', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5b621508-6f6f-438f-8ca3-c58d454d02bd', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9003', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cf82ded4-e0ce-4e26-a72f-c5942210ad4a', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bcc859b5-0963-4a85-817d-c6326a9d63d9', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9283', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'53205a46-5a95-4bd6-be4d-c6ace236610b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9005', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'52fbd9dc-4a51-4d09-8402-c6b553d25097', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a6f2c9ce-0689-42e3-9f03-c6bf9539ae3d', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8937', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b94d1473-f3de-4bfc-b037-c737bd46b83f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8883', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4bb87daf-907b-4dcc-af7a-c757f2cd20ee', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'506d8e26-d676-482c-ba9d-c7696fc9e020', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9164', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'668b2154-ddb1-4159-a8ff-c78e91aa47d6', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'859bbfb5-b70f-4094-a6eb-c7daf98c5eab', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ea49c264-b24f-477f-be03-c7fe544ca287', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ae98250d-d350-4221-a7ed-c84afc7b589f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8979', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'32305efc-2cfa-4717-b9e0-c8f731050201', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9250', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0d0470ad-813e-4cef-b20f-c9685002b058', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c4139a2-9b27-4628-84c6-c9788f29022b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9263', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0004b59f-5a1b-4dcb-9736-c9a114396fe0', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'00a8ef7b-1a2a-496b-bf1d-c9b7867373d2', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9072', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'acea2893-2546-4aac-aaf7-c9de1ca46b22', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8965', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0e83fa6d-95bc-4982-80ac-ca014eb5985c', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8539e828-9634-402a-8d4f-ca1a1c17ef50', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'54869a9c-482b-4a5d-a4b3-ca318ea052c1', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2804cfef-0731-4a9f-b517-ca36b1c8a6b7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8911', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'788d61a3-9eca-45c3-9431-cab0bcc6f6a2', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8628', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'39dd7e77-9085-4cc6-86a0-cae7fd1d0207', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'14cd6dcc-78d9-48ce-ba8a-caf63d38bf34', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8790', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd429834c-7328-49d3-9c93-cb6dab52ba30', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8612', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'18d65f97-e6fc-47fc-809e-cb78876c4db2', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9002', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'603e0dc8-18dd-4fe8-af39-cbd490201712', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9143', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'046c746a-6730-4a0c-8a71-cbf22e527381', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1cc4e2ea-52c0-4485-a20b-cbf54046720d', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9219', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4653920c-115d-42dd-bf2a-cc00a90b5cee', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9100', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8b791f1f-b645-4c63-a50f-cc31320facdf', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8631', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ac6a10ba-748f-49e9-bebd-cc4dc402400c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8611', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'08205689-f3b8-4e2a-bc47-cc65bcdc4274', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a2878a7a-7354-448f-b6f9-cc98ec8912a3', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8710', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a123cd9a-51a3-4f28-a869-ccfc85f54eec', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'640cfbe0-1e1f-4660-ac0d-cd77fd307f6f', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8a6da55f-1fb1-43b0-8667-cdcdff182f46', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8630', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'953e594d-9762-465d-9559-cdd9ab7a20bc', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9c89a8dc-0c44-44e8-bea3-cdfbb82f1459', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8637', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f0c8dc96-125c-402c-9847-ce07f662eddf', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8967', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'db4aea85-0610-40c8-b6a7-ce6dc372e2af', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8770', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4f34cf18-55a3-4cd6-9768-ce78b8143a32', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8765', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'337c67a0-e171-4b34-bd61-cf2d6dd39d1c', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9018', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aeb9b9ab-19b1-4628-bae6-cf3b154a93cf', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9197', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2c90ec5e-540c-4406-b330-cf3f280f7237', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9256', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7d4769ab-69b9-4137-8fef-cf4118dab122', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'57f49eee-c5b1-4d58-9dd2-cf4904609e49', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9012', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'da0dbcad-d59b-49a1-a866-cf6b25c0c732', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9079', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1b9b1d0d-7432-4409-a6e3-cfa30d5c55e6', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e635c80d-70ca-4b11-892a-cfaa746062b8', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c8983de9-3c61-4fe8-958f-cfb42c90b50a', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8930', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'83e33fe0-3a77-4f58-876c-cfebd3bb8a40', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6bc2a2ad-aa75-4959-be33-cff243ec8601', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4628e616-c2ce-4a42-96be-d02d545d4d33', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9188', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aab6382f-05ad-4211-968e-d04288d1724f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8960', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7fd282cd-20f0-4a03-9f14-d0c1bb6c011a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9039', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9406e7f5-f7e1-434a-81a8-d0d22a33c8dc', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'896f34dc-df13-4a0a-9fa4-d0d2904b9291', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a8a26f0d-8fac-4125-9048-d0d4ceeef9c9', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'18433148-e425-4a86-8586-d0dca927bca3', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9184', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'78a7fa8e-57ac-41e8-a263-d0f82f7872d2', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a605885d-fa60-4206-a351-d13837fce0c7', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1e4798cb-b7f0-45a1-a305-d146355f9854', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9231', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cbb7f628-f3a0-425b-9c44-d178d097671c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8608', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11fea175-2e16-4e15-a6fc-d17fd562d539', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9ac22faf-a033-4902-9dc1-d1b2f29d1e96', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0d35120c-b71a-47e3-97c9-d1df186accb4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'75817347-16a0-4088-8703-d1e71e19d467', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ff9923fb-608c-4129-a35a-d1eaf9d6c959', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9223', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2e74fc70-85b1-4cb8-8d0a-d22d7240f69f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'220cfd1a-4f1a-42a3-917e-d251dc412964', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ca6afe6e-f514-4007-a02c-d2eee766c8f9', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9145', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'15036318-84a9-4ffb-b7bd-d35cffea588e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8827', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'97c423dc-ae70-492d-a45b-d369ac871a2d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9194', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'98ef4e81-e6e4-4e9d-89f7-d3c5d0063ce0', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'419a59a8-2ac9-4f72-9df8-d3cde8e65117', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9138', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'89fd6f68-9b24-4590-828b-d479d320e336', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9182', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9d124317-44b3-4ce6-935d-d4e0cb6c1802', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'88895f6f-2343-4089-9d70-d5072c86c664', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9279', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'93bbe2e1-cf0a-4526-b395-d59170d9632d', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9156', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'db9fab7c-f401-4d61-93fa-d5942d18f00e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9304', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6f142b72-87a9-44b4-bbfb-d5958f4d8066', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a6c0f370-1b48-4845-ac70-d59bd489e020', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9054', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3509722d-f8a3-407f-8e73-d65921a78eb0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9021', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'da573a5a-82b2-43d3-acbf-d66e06530261', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c2c133e2-8c59-4445-aedf-d672f6b1e2ac', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9285', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8934b599-4d20-41e7-b76d-d6fcc46e93c5', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1134642e-daa9-47d7-8571-d779be6a7df4', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7f9f60de-24ed-49c3-b85b-d78667c6e2f2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'76f57ee5-2d6c-4eff-a51a-d7cf8cc080cc', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'17048c59-48c9-4604-a393-d7d992e9482d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0828100b-528a-4161-a909-d812dc7de7db', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e4aecfa9-7f99-455e-acbd-d8176a9d3920', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5e262d5f-fdaf-4cd2-888f-d82f469590df', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c4885c8b-54b5-43fb-8954-d84d72cf69ed', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9157', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3dfcaff8-7f7b-450d-8df2-d88e6949ea25', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9031', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'80c3c60e-ef53-42f5-be0e-d8b6d2d09220', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9099', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6a7e8388-192a-4ad6-bead-d9078c306d65', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8ee97a8c-7eb6-4641-95c8-d90bf12d7dab', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3c6375c5-c1e6-4e15-a914-d963086d8638', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'453718d8-18af-4879-8f24-d9771ffce475', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'436cc066-8cbc-487e-ad70-d981bbe3dbd4', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'afb64e04-060a-49aa-9ac7-da3370a8e3e4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0c0a390d-c5f6-4278-adee-da69604fd6bb', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0139d99b-e035-42cd-88be-da6a37be722e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b0803b28-5fb4-4533-b717-da932fc257b4', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8917', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'17f0f7ee-d15f-47cb-bf79-dab0b07c023d', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'581d4a4a-c1e1-47d2-9ae0-dab31cfe3f9e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'adea2d20-fd9d-4859-8548-db99fa0112e4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9300', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'518ace18-7a74-4f85-9448-dc0dccc9b680', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e0a36a7f-e124-4a62-839d-dc16840846f5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'72970c48-8afd-4088-b0a2-dc9fc916b1d0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9074', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e9f34575-7b83-4959-9c74-dcc792130333', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9db3ee24-c630-44d1-b963-dcc90f873e1c', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7cfe76fb-2afc-4ec7-b42d-dcd057d9b801', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7b04819c-d045-4dca-b97e-dce9d401e861', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'09189743-f6f4-4a4a-8b70-dcf21ee7d700', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e96e7cbc-a8f1-4e20-bbdc-dd2cca6f7362', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9241', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9788941e-96fd-4772-891b-dd349f85bfd2', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ebfd545e-02c3-4cba-b4e4-dd720853e11c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6806e1c4-aba8-4b43-ad1a-dd76a5e071e6', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8971', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b9368de1-19a5-4e48-abca-dd87d9d63b27', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9008', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c05fb25c-677d-4f91-ab99-dd88ec93a153', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'55874264-f481-4eab-99b4-de2ca4f9b360', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'488cadba-01a6-40c6-8fb4-de836e6fb2df', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9078', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'690e21cc-3554-445b-830f-dea32fde91d7', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'86b87d26-709a-4c35-a663-debfbb40d0e5', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8645', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3558d32a-e388-43ff-8ddf-df783a715598', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8778', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e6341ceb-8a4b-4021-832f-df8c07d05d7a', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a6ef3994-0aa3-4861-9421-dfa0716cd47e', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8973', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e1b57e71-9b80-4a02-b2a8-dfacb104b1d3', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9209', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'52c8fc93-e7dc-4d7a-a50e-dfd5e66707fc', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9080', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8334ae1d-2a72-4751-b16e-e001ab22f4f0', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9033', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ac6f5b39-228e-4b5d-b12f-e0076988bd60', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e9e982f3-d118-4a55-9295-e0080e05efd2', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8647', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e5c6a1aa-d526-49dd-96e1-e0753e5cc6dd', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9068', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'633cd20b-e069-45ed-804b-e090c1244c8c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e688b530-7621-48c4-a9e5-e0e38f3953a9', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a8532062-e9d9-45a4-97f2-e10aa8469dce', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8944', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b8595d8d-1f1c-4183-a7f1-e121891b6fea', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9291', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'04604111-bf50-477c-8229-e13a3479899f', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'00d4392b-c453-4c01-a47e-e165dceff7f5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4d4583f6-a06a-4d07-8a2a-e184bcef81c2', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4cee4bc0-6284-47cf-b79d-e1ae38d606a8', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8681', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f382004e-8fcd-4d8f-8ee1-e1b7c7566746', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9221', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd77fdf7c-fafa-4b90-a213-e1b9f129c53c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5d82a39a-c112-454c-a1f5-e1c88d63677d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9083', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'60af8242-d7b5-42b5-8e63-e27441efccbe', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'86344028-821c-4d86-bc4b-e2f5ddbc56ae', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'de84a377-1be6-40b3-a0bd-e2f6287e5e3c', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd2537128-b0d7-405f-b92d-e315ebf4504b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8855', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7f27f7e6-6714-49cd-8675-e32d39cb68c3', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd673864c-d525-4396-a4b2-e3331bd984e7', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9663a98f-9258-4ca9-9678-e3458c2ad76f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4ed50e7f-c928-4029-9009-e3e5e20d5e67', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9253', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'00f98743-b3e1-42ad-97d9-e431843b4487', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8882', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd6c04f35-0f54-4d6c-a0c9-e43f7d3656de', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5dd0823c-12d9-4743-b341-e45070bac70e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8856', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'38cb6635-cdf6-4bff-93fa-e45e9399e640', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'19d57453-7d44-4910-882d-e46ed8d0700e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8800', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6746b078-a90e-4bb7-8bba-e4d8b37cb69f', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8648', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2b51c828-0d31-4881-88e6-e4e96a07360f', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'60ae5ab9-a170-41a2-ad1b-e4f8284a8b99', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ed3eba96-3158-499e-acaf-e506760a877f', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'eb0fd7c7-3c66-4484-bbf3-e50a69fd26e5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ab713d99-e00f-4626-a5c3-e5295f5d90a5', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8796', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b68ab032-8f96-437a-a207-e53e28cd6236', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'25d32958-3250-4d68-9f71-e54adb24e627', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8978', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'bb8bbc15-fb6f-4ca4-8638-e58567e0a8d5', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8810', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'899c2e43-6961-4bd9-8c77-e5da10bcb76e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9305', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'efbef93a-919b-46c6-8c3c-e5e1923ae81d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8921', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ccfc9f96-e6e2-4afd-b451-e5fe688b244a', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'39f0e7cf-98e6-4e45-b592-e60714da736a', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8799', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'58caecc1-63d7-4eb9-8320-e6189aff7521', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'116df257-1f78-4938-9896-e61a8c056cdf', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2532b280-3765-4314-beb1-e6376c8f3ca0', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c3f927a4-15ac-499d-a730-e6420a23cc3e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b03c35d6-d5fe-4a4b-bfcf-e663e2392c3c', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8931', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'238f64a9-0574-4265-8361-e698138e280e', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'01c4ea3b-838f-4981-a602-e6999170a38f', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8614', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f7dfd64c-ff16-4197-81c0-e6c0c18c2519', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8830', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8bcd44dd-cfe9-4931-9f49-e6c6cf157585', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9270', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7d976bae-a6d7-46c4-964b-e78258363063', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ddd38f04-55dd-4c0b-aa0d-e7c6e83fa0f7', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8853', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'83368ae9-af40-46c8-b275-e8044c7127bb', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8635', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ad99e4d5-da08-4190-8d57-e80603218447', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'91fea4a7-58b6-42c1-98d2-e8067a81fe90', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8904', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'00bd1701-442f-453d-a285-e8808e413beb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'99876457-5d29-4b83-9350-e89c875f2aa6', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8831', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c860e8fc-9d43-4912-a3dd-e89c9c7c584d', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9046', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a5bf5f76-9ac7-406b-8b65-e8a82cdc7953', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8714', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'804f7de3-7870-4efe-9a79-e8c092830ec9', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9198', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fa793da9-ad25-4634-80d4-e8ca93da7eed', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8599', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'61d899bc-e26a-4831-af18-e966f2ab0616', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8879', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'efbb2d64-d1b0-448e-99d3-e9a9f1062291', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9095', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'11d33f3a-1d70-4c89-bb54-e9c110939631', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9ab6cda0-5291-4949-9507-e9f79f9eb435', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9092', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4b6014cc-7ecf-4205-84cc-ea2df0160e45', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'70ac8553-a63a-40ad-90e4-ea80c3799f95', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'767033ce-0f81-4c7f-99e4-eadca9e6636b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5afe7068-534f-452d-ba8b-eaeaf2e38b90', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd79e932c-6c6f-4913-b457-eb37e5512771', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'44f61a2e-78b9-4be0-89d7-ebc0ac37078e', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'379705b4-dcd7-4617-8bb8-ebef0e1d152e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8832', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fb889792-0ecc-4339-887c-ec03c0b97997', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8605', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'14bde4b0-9c5d-48f6-9111-ec06c23b82e4', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9106', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3a6023a7-f1b6-4fb8-b417-ec0a600a34f6', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c33cf14a-dbb2-4ba5-a805-ec2a1c26c470', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8867', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9e5feba2-cb8d-4bf7-a581-ec73d25658d3', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'10e9365d-9b18-4f92-be34-ec7a2f82e39a', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5cd874a6-17b1-4e15-97b6-ec8698295d91', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9180', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'f7cfdf3a-5af7-4cc3-b2ae-eceac95aba68', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9255', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a30a634f-af61-49dd-ac76-ecf2e6f9c4f8', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8819', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'44521108-aa48-4b0c-88ac-ed61b7ac085c', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9093', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0a20fab3-b03f-444c-8b43-ed7005dc4128', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9204', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'247e524c-17f6-478a-8064-edbfa9dfa5a2', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'68c414f5-2524-4867-ba43-edc98bbc7d78', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9201', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0f4b1a60-45ba-408c-8c21-edef2d34fc9f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9299', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'667c3cd4-238b-461c-9b6e-ee11d3fafcc4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd7e2278-4711-4c00-862b-ee4ff8ba6ff5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'170ef4e8-6fcd-4602-9d44-ee607957e14f', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cd523831-b5b9-44bf-93ba-ee9a2d98d454', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8950', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4ce40503-a8dc-4fb9-960b-ef177b74e045', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'616cb40d-2314-458a-8c57-ef804c649013', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9a811d73-d6f4-4702-851c-ef8862b4581c', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8989', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4c8c1ddc-5d49-4041-9792-ef8c326d5085', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'230ba695-0a15-409d-8ee1-efeb6dc9d9f5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9218', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b32728ab-b8bd-45a8-84c5-f026514eeedc', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9247', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1ea5e2be-a86f-462f-b100-f0f7fb604890', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4356b9c7-aa87-4cab-92a9-f10071e84b04', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e8fcca9f-a91e-42ca-8935-f11d87cda61f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8940', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fd1687d0-56a0-491b-9ad4-f1467a0232c7', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'10cee00a-c438-4427-ad7e-f16cb9d97ae0', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9b125ab9-08b0-4601-a7f7-f22cd0260fa0', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'730c4042-3730-42ce-bf9b-f2693bb41cc8', N'superadmin', CAST(N'2021-03-03T10:55:56.217' AS DateTime), N'A', N'DB_PRODUCT_BRAND', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_PRODUCT_BRAND_34A38D71CB772D070554A76FD038778283B0C24176DD97C90639909BBEF03975', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9e1c17e8-1f56-4351-b6eb-f2c3b944005f', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8769', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3b50bc3b-0f7c-4e31-8b9d-f2e8de51b76b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8898', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b5c006e0-65dc-4977-bcbd-f2eb38d64622', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8813', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'988dd94a-0a17-42c5-8160-f2fa19a5ed65', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e2bcddfe-a6e5-4023-98fa-f33fad8a5f2d', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8843', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'16a40d8a-827c-4d34-a890-f38e807ba3b3', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'71a3bbda-949a-40d4-9aa6-f453c795c3b4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9e35f305-ed86-401e-bb7b-f4fb9954d710', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8651', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'36ba5965-8702-4d05-8009-f54c69bd4340', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'adbac5da-5d17-4506-a9f9-f5591caf1fc2', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8815', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3d878965-b44f-47cd-8525-f628edb36028', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9037', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'647d07af-31c3-457b-8e5e-f68c5321d48f', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a38ab44-a010-48b5-97b6-f690aa62d8ce', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'1443c100-567b-4472-9713-f7001bfba62e', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'91805bf2-6c12-46b4-88d3-f7011742302f', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0fc1a2ec-ae85-4543-b32e-f70b0213321d', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'68af61db-daa3-44b4-99d2-f714c6ecbdce', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c11bff64-262c-4d30-9cd3-f7ce83469e07', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'23f0e195-fac0-4956-9838-f7efd6ac77ae', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9120', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a35914ec-1af0-45dc-a61f-f82e4a351d24', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9035', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fed4039e-ee8b-4a11-a557-f87e25b09a59', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9234', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'6105cbae-7333-42f9-aa74-f8c460234ad3', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cf484ed0-5e39-42c5-a18d-f93cfd9c8717', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'34865c58-f64e-4136-8924-f94c851136b8', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8636', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'95afa493-39c9-4ed9-b4a5-f9784b02b776', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'2a23e3fd-cf83-42ef-9a6e-f9a04534fefd', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9229', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'cf53b154-a1aa-4e39-baad-f9cb19b9628b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'686dbbfe-741c-4084-b8d2-fa23a72441ba', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9154', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'9baff1d8-2e7c-45bc-99ba-fa3a33fa2079', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9133', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ef4c87f8-32aa-4ef0-8d41-fa53fbe56f4b', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0b56ae28-e10e-497b-bafe-faa06ecc9c92', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8840', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'47df7b8b-e0b9-46d8-87cd-fae3d5d33a8b', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8938', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5290e810-d266-48ac-bb3c-faee6f9a6bb1', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9169', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b38ab75b-15a3-4381-8288-fb0a4c33c12b', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a370af5c-925d-4fa3-8c0e-fb152cf3c205', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8716', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'650fac4b-df6d-4b01-bd87-fb207bef6e5e', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'ecf8a88e-8aa3-44e1-936c-fb2092962e74', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'93fa4e87-7ef1-4d23-a203-fb7981c3ad8b', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8849', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b6c4bfe2-8113-47b5-9e83-fbdedce68edb', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a5a446b9-0848-4f1c-aa83-fbfa20ed8e72', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8789', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7da8e509-115b-4bbe-831b-fc5eecb6e494', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9070', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'5340e41e-e8fa-445a-8a44-fc5f6637a18a', N'superadmin', CAST(N'2021-03-03T10:53:30.510' AS DateTime), N'A', N'DB_PRIMARY_UOM', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_PRIMARY_UOM_BA263C65D0E87CA1F2082F2F0135D28EF596CC2898F0ED085170625D86D15E58', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'8b63866e-ef6f-4f79-8eaa-fc742ea9435c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8610', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a7cd015e-cf56-487e-9c47-fc74fe45aa16', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8889', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'fbb962ca-f0bd-425a-9011-fca291ac18c1', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'0eb9fc0c-758d-4f77-9606-fcc2241e0df4', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'25051541-b12e-4dc3-be09-fccb2eea71f5', N'superadmin', CAST(N'2021-03-03T11:01:31.100' AS DateTime), N'A', N'DB_PRODUCT', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_PRODUCT_22DF922DA50E0CA37FD90AFBA8A05BF9EE20DFB43BDEEB50E7B2E2BB11EB22ED', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c1a77d77-aace-4235-b65c-fd1f454f60e4', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9216', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'71f52e40-f8c7-483a-b59e-fd3ae93d3082', N'superadmin', CAST(N'2021-02-22T10:59:18.667' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'24f0ce24-4bbd-427c-9286-fd66ecf30e0b', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9065', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'338cb0c3-4f5c-4a8c-bf5f-fdb8b28ac23e', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8787', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'63645d46-8a4f-43ff-a6ce-fdceb2a429fc', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'a92dcdf2-075e-4d18-8091-fe12f717ad95', N'superadmin', CAST(N'2021-02-22T10:37:38.283' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'8772', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'722506b7-3736-4780-a994-fe234d892f13', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'e91a6806-2637-405f-a1dc-fe338e994016', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9161', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'd3b56a25-8f10-42b0-8de8-fe34469c54c3', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9107', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'290a6229-d66f-44a2-978a-fe40070652e6', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'32ca7297-9088-4561-b352-fe571c30695c', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'4e5f0808-d6ad-4d88-ab14-fe590350d8d5', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'c2a6d10b-3dcc-4d57-b9c0-fe59b92e344b', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'7d33735f-a52e-4dac-8da6-fea155a40573', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'aa9ed3b3-a189-4172-b9a2-feecd94d9594', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'70a734d1-3117-4fdc-8d49-feed0c593fb5', N'superadmin', CAST(N'2021-02-22T11:06:01.377' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'3215a4d2-bc56-41d3-9a0b-ff88088b2e99', N'superadmin', CAST(N'2021-02-23T10:05:08.657' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9117', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'49fb5789-a925-4c9d-80d1-ffc8367cf7a5', N'superadmin', CAST(N'2021-02-23T10:33:10.643' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9240', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b2dcf07c-92d5-4c9a-9d40-ffcf809d9515', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'D', N'DB_ROLE_PRIVILEGES', N'9067', N'*ALL', N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'74a946ef-2898-4cad-88ac-ffd75f291922', N'superadmin', CAST(N'2021-02-22T11:34:17.993' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
INSERT [dbo].[AuditLog] ([Id], [CreatedBy], [CreatedOn], [EventType], [TableName], [EntityId], [ColumnName], [OriginalValue], [NewValue], [RelationalColumnName], [RelationalColumnValue]) VALUES (N'b88e1c6a-5e37-4f1d-a082-ffedecbfb395', N'superadmin', CAST(N'2021-02-19T16:47:04.107' AS DateTime), N'A', N'DB_ROLE_PRIVILEGES', N'', N'*ALL', NULL, N'System.Data.Entity.DynamicProxies.DB_ROLE_PRIVILEGES_CD9EDC26C0C40263B1C8056CE14FA6C31AE6D0F06FC2C7AFA0D3132073F9E27E', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_CHART_OF_ACCOUNT] ON 
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, N'1000000', N'Fixed Assets', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, N'1001000', N'Property, Plant & Equipment', N'1000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (3, N'1001001', N'Land', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (4, N'1001002', N'Building Office', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (5, N'1001003', N'Building Godown', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (6, N'1001004', N'Building on Leasehold Land', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (7, N'1001005', N'Plant & Machinery', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (8, N'1001006', N'Furniture & Fixtures', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (9, N'1001007', N'Equipment''s & Installations', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (10, N'1001008', N'Computers & Other Office Equipment', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (11, N'1001009', N'Weapons', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (12, N'1001010', N'Vehicles - Cars', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (13, N'1001011', N'Vehicles - Bikes', N'1001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (14, N'1002000', N'Accumulated Depreciation', N'1000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (15, N'1002001', N'Dep - Building', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (16, N'1002002', N'Dep - Plant & Machinery', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (17, N'1002003', N'Dep - Furniture & Fixtures', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (18, N'1002004', N'Dep - Equipment''s & Installations', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (19, N'1002005', N'Dep - Computers & Other Office Equipment', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (20, N'1002006', N'Dep - Weapons', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (21, N'1002007', N'Dep - Vehicles', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (22, N'1002008', N'Accumulated Deperciation on Fixed Assets', N'1002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (23, N'1003000', N'Capital Work in Progress', N'1000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (24, N'1003001', N'Capital Work in Progress', N'1003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (25, N'1500000', N'Long Term Assets', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (26, N'1501000', N'Intangible Assets', N'1500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (27, N'1501001', N' Software''s', N'1501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (28, N'1501002', N'Trade Marks & Copyrights', N'1501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (29, N'1501003', N'Other Intangible Assets', N'1501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (30, N'1502000', N'Long Term Assets', N'1500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (31, N'1502001', N'Earnest Money', N'1502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (32, N'1502002', N'Performance Security', N'1502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (33, N'1502003', N'Bank Guarantee', N'1502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (34, N'1502004', N'Other Long Term Security Deposits', N'1502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (35, N'1502005', N'Other Long Term Assets', N'1502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (36, N'1502006', N'Deferred Cost', N'1502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (37, N'1503000', N'Long Term Deposits', N'1500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (38, N'1503001', N'FDR''s', N'1503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (39, N'2000000', N'Current Assets', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (40, N'2001000', N'Stock in Trade', N'2000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (41, N'2001001', N'Stock in Trade', N'2001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (42, N'2002000', N'Advances, Deposits & Receivables', N'2000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (43, N'2002001', N'Trade Debtors (Accounts Receivable)', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (44, N'2002002', N'Accounts Receivable Services - Trade', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (45, N'2002003', N'Advances & Prepayments', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (46, N'2002004', N'Security Deposits', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (47, N'2002005', N'Advance against Expenses - Employees', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (48, N'2002006', N'Advance against Salaries - Employees', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (49, N'2002007', N'Input Sales Tax Paid', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (50, N'2002008', N'Advance Income Tax & WHIT Paid', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (51, N'2002009', N'Vendor Prepayment', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (52, N'2002010', N'Intercompany Due From', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (53, N'2002011', N'Employee Loans & Advances', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (54, N'2002012', N'Prepaid Insurance', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (55, N'2002013', N'LC Provision Margin', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (56, N'2002014', N'Withholding Tax Receivable', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (57, N'2002015', N'GST Tax Paid', N'2002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (58, N'2003000', N'Cash & Bank', N'2000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (59, N'2003001', N'Cash in Hand', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (60, N'2003002', N'Cash At Balance', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (61, N'2003003', N'Petty Cash In-Transfer', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (62, N'2003004', N'Bank Balance', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (63, N'2003005', N'Bank In-Transfer', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (64, N'2003025', N'Internal Bank/Cash Transfers', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (65, N'2003026', N'Foreign Currency Bank', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (66, N'2003027', N'Foreign Currency Bank In-Transfer', N'2003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (67, N'2004000', N'Project Account', N'2000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (68, N'2004001', N'Project asset', N'2004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (69, N'2004002', N'Project WIP', N'2004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (70, N'3000000', N'Current Liabilities', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (71, N'3001000', N'Trade Creditors', N'3000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (72, N'3001001', N'Trade Creditors - Local', N'3001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (73, N'3001002', N'Trade Creditors - International', N'3001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (74, N'3001003', N'Accounts Payable Services', N'3001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (75, N'3002000', N'Accrued Liabilities', N'3000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (76, N'3002001', N'Accrued Expenses', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (77, N'3002002', N'Accrued Rent', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (78, N'3002003', N'Accrued Utility Expenses', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (79, N'3002004', N'Accrued Mark Up', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (80, N'3002005', N'Salaries & Wages Payable', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (81, N'3002006', N'Employees Provident Fund Payable ', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (82, N'3002007', N'EOBI Payable', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (83, N'3002008', N'ESSI Payable', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (84, N'3002009', N'Employer Cont Provident Fund Payable ', N'3002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (85, N'3003000', N'Other Payables', N'3000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (86, N'3003001', N'Output Sales Tax Payable', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (87, N'3003002', N'Net Sales Tax Payable', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (88, N'3003003', N'Withholding Tax Payable (Vendor)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (89, N'3003004', N'Withholding Tax Payable (Servises)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (90, N'3003005', N'Withholding Tax Payable (Rent)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (91, N'3003006', N'Withholding Tax Payable (Employee)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (92, N'3003007', N'Import Duties Payabale (Internation Vendors)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (93, N'3003008', N'Witholding Tax Adjustment (Prior Year)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (94, N'3003009', N'Accruals and other Liabilities', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (95, N'3003010', N'Intercompany Due To', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (96, N'3003011', N'GST Tax (Payable)', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (97, N'3003012', N'Provision For Bad Debts', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (98, N'3003013', N'Ptrovision For Taxation', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (99, N'3003014', N'Provision For Share A/C', N'3003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (100, N'3500000', N'Long Term Liabilities', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (101, N'3501000', N'Sponsors'' Loan', N'3500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (102, N'3501001', N'Sponsors'' Loan', N'3501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (103, N'3501002', N'Loan Payables', N'3501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (104, N'3502000', N'Other Long Term Liabilities', N'3500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (105, N'3502001', N'Long Term Loans from Financial Intuitions', N'3502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (106, N'3502002', N'Liability against Assets Subject to Finance Lease', N'3502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (107, N'3502003', N'Other Long Term Liabilities', N'3502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (108, N'3503000', N'Deffered Liabilities', N'3500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (109, N'3503001', N'Deffered Liabilities', N'3503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (110, N'4000000', N'Equity & Capital', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (111, N'4001000', N'Capital', N'4000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (112, N'4001001', N'Owner''s Equity', N'4001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (113, N'4001010', N'Opening Balances', N'4001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (114, N'4001020', N'xxxxxx', N'4001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (115, N'4002000', N'Share Deposit Money', N'4000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (116, N'4002001', N'Share Deposit Money', N'4002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (117, N'5000000', N'Reserves', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (118, N'5001000', N'Capital Reserves', N'5000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (119, N'5001001', N'Surplus on Revaluation of Fixed Assets', N'5001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (120, N'5001002', N'Share Premium', N'5001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (121, N'5001003', N'Share Discount', N'5001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (122, N'5001004', N'Drawings', N'5001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (123, N'5002000', N'Capital Reserves', N'5000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (124, N'5002001', N'General Reserves', N'5002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (125, N'5002002', N'Others', N'5002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (126, N'5003000', N'Retained Earnings', N'5000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (127, N'5003001', N'Accumulated Profit / (Loss)', N'5003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (128, N'5003002', N'Profit/(Loss)  For The Year', N'5003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (129, N'6000000', N'Income', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (130, N'6001000', N'Sales', N'6000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (131, N'6001001', N'Sales', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (132, N'6001002', N'Counter Sale', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (133, N'6001003', N'Sale Local Market', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (134, N'6001004', N'XXXXXC', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (135, N'6001005', N'XXXXXC', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (136, N'6001006', N'Sale Return', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (137, N'6001007', N'Sales Discount', N'6001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (138, N'6002000', N'Other Income', N'6000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (139, N'6002001', N'Gain / (Loss) Sale of Assets', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (140, N'6002002', N'Other Income', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (141, N'6002003', N'Unearned revenue', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (142, N'6002004', N'Trade Discounts Allowed', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (143, N'6002005', N'Payment discount expense', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (144, N'6002006', N'Cash book receipts', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (145, N'6002007', N'Bank interest revenue', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (146, N'6002008', N'Unrealized gain', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (147, N'6002009', N'Currency Realized gain', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (148, N'6002010', N'xxxxxx', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (149, N'6002011', N'xxxxxx', N'6002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (150, N'6003000', N'Franchise Fee', N'6000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (151, N'6003001', N'Franchise Fee', N'6003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (152, N'7000000', N'Cost of Sales', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (153, N'7001000', N'Cost of Product', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (154, N'7001001', N'Procurment Cost (Product Delivered Cost)', N'7001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (155, N'7001009', N'Project Expenses', N'7001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (156, N'7002000', N'Other Costs', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (157, N'7002001', N'Logistic Expenses', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (158, N'7002002', N'Transportation Charges', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (159, N'7002003', N'Origin Handling Charges', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (160, N'7002004', N'Packing Material', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (161, N'7002005', N'Commission & Brokerage - Cost of Sales', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (162, N'7002006', N'Product Cost Adjustment', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (163, N'7002007', N'Inventory Shrinkage', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (164, N'7002008', N'Sale price Currency variance', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (165, N'7002009', N'Purchase price currency variance', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (166, N'7002010', N'Price variance Adjustment', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (167, N'7002011', N'Method Change Variance', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (168, N'7002012', N'Average Cost Variance', N'7002000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (169, N'7003000', N'Salaries & Other Benefits - Cost of Sales', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (170, N'7003001', N'Salary & Wages Management - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (171, N'7003002', N'Salary & Wages Administrative - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (172, N'7003003', N'Salary & Wages Supportive Staff - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (173, N'7003004', N'Laptop Contribution Employers - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (174, N'7003005', N'Child Education - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (175, N'7003006', N'Vehicle & Fuel Allowance - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (176, N'7003007', N'Health Insurance - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (177, N'7003008', N'Bonus - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (178, N'7003009', N'Over Time - Cost of Sales', N'7003000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (179, N'7004000', N'Transportation - Cost of Sales', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (180, N'7004001', N'Transportation Fuel Expenses - Cost of Sales', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (181, N'7004002', N'Transportation Repair & other Expenses - Cost of Sales', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (182, N'7004003', N'Import Freight Charges - Cost of Sales', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (183, N'7004004', N'Cartage, Freight & Octroi - Cost of Sales', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (184, N'7004005', N'Custom Duty', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (185, N'7004006', N'Custom Inspection', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (186, N'7004007', N'D/O Charges', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (187, N'7004008', N'Examination Charges', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (188, N'7004009', N'Excise & Taxation Officer', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (189, N'7004010', N'FBR Duties (CD,GST, IT)', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (190, N'7004011', N'Air Freight Charges', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (191, N'7004012', N'Loading & Unloading Expenses', N'7004000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (192, N'7005000', N'Rent, Rates & Taxes - Cost of Sales', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (193, N'7005001', N'Building Rent - Cost of Sales', N'7005000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (194, N'7005002', N'Rates & Taxes - Cost of Sales', N'7005000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (195, N'7005003', N'Commercialization Expenses - Cost of Sales', N'7005000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (196, N'7005004', N'Facilitation Expenses - Cost of Sales', N'7005000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (197, N'7005005', N'Others - Cost of Sales', N'7005000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (198, N'7006000', N'Utility Expenses - Cost of Sales', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (199, N'7006001', N'Electricity Bills - Cost of Sales', N'7006000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (200, N'7006002', N'Water & Sanitation Bills - Cost of Sales', N'7006000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (201, N'7006003', N'Sui Gas Bills - Cost of Sales', N'7006000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (202, N'7006004', N'Generator Expenses - Cost of Sales', N'7006000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (203, N'7007000', N'Communications Expenses - Cost of Sales', N'7000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (204, N'7007001', N'Telephone Bills - Cost of Sales', N'7007000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (205, N'7007002', N'Mobile Bills & Cards - Cost of Sales', N'7007000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (206, N'7007003', N'Internet Bills & Cards - Cost of Sales', N'7007000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (207, N'7007004', N'UAN Facility - Cost of Sales', N'7007000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (208, N'7007005', N'Postage & Couriers - Cost of Sales', N'7007000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (209, N'7007006', N'Cable Charges - Cost of Sales', N'7007000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (210, N'7500000', N'Operating Expenses', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (211, N'7501000', N'Salaries & Other Benefits - Operating Expense', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (212, N'7501001', N'Salary & Wages Management - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (213, N'7501002', N'Salary & Wages Administrative - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (214, N'7501003', N'Salary & Wages Supportive Staff - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (215, N'7501006', N'Vehicle & Fuel Allowance - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (216, N'7501007', N'Health Insurance - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (217, N'7501008', N'Bonus - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (218, N'7501009', N'Over Time - Operating Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (219, N'7501010', N'Stipend', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (220, N'7501011', N'Security Services Expense', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (221, N'7501012', N'PF Employer Contribution', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (222, N'7501013', N'Education Allowance', N'7501000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (223, N'7502000', N'Transportation - Operating Expense', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (224, N'7502001', N'Transportation Fuel Expenses - Operating Expense', N'7502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (225, N'7502002', N'Transportation Repair & other Expenses - Operating Expense', N'7502000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (226, N'7503000', N'Rent, Rates & Taxes - Operating Expense', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (227, N'7503001', N'Building Rent - Operating Expense', N'7503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (228, N'7503002', N'Parking Rent - Operating Expense', N'7503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (229, N'7503003', N'Rates & Taxes - Operating Expense', N'7503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (230, N'7503004', N'Commercialization Expenses - Operating Expense', N'7503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (231, N'7503005', N'Facilitation Expenses - Operating Expense', N'7503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (232, N'7503006', N'Brocker Commission', N'7503000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (233, N'7504000', N'Utility Expenses - Operating Expense', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (234, N'7504001', N'Electricity Bills - Operating Expense', N'7504000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (235, N'7504002', N'Water & Sanitation Bills - Operating Expense', N'7504000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (236, N'7504003', N'Sui Gas Bills - Operating Expense', N'7504000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (237, N'7504004', N'Generator Expenses - Operating Expense', N'7504000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (238, N'7505000', N'Communications Expenses - Operating Expense', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (239, N'7505001', N'Telephone Bills - Operating Expense', N'7505000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (240, N'7505002', N'Mobile Bills & Cards - Operating Expense', N'7505000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (241, N'7505003', N'Internet Bills & Cards - Operating Expense', N'7505000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (242, N'7505004', N'UAN Facility - Operating Expense', N'7505000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (243, N'7505005', N'Postage & Couriers - Operating Expense', N'7505000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (244, N'7505006', N'Cable Charges - Operating Expense', N'7505000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (245, N'7506000', N'Printing & Stationery', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (246, N'7506001', N'Consumable Stationery', N'7506000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (247, N'7506002', N'Printed Stationery', N'7506000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (248, N'7506003', N'Photo Copies', N'7506000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (249, N'7507000', N'Advertisement & Marketing Expenses', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (250, N'7507001', N'Advertisement Expenses', N'7507000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (251, N'7507002', N'Web Hosting Charges', N'7507000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (252, N'7507003', N'Employee Sales Commission', N'7507000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (253, N'7507004', N'Marketing Expense', N'7507000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (254, N'7507005', N'Seminars', N'7507000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (255, N'7508000', N'Repair & Maintenance', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (256, N'7508001', N'RM Building', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (257, N'7508002', N'RM Generator', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (258, N'7508003', N'RM Vehicles & Fuel', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (259, N'7508004', N'RM Furniture & Fixture', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (260, N'7508005', N'RM Air Conditioners', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (261, N'7508006', N'RM Computers, Printers & Related Equipment', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (262, N'7508007', N'RM Office Equipment', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (263, N'7508008', N'RM Electrical', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (264, N'7508009', N'RM Other', N'7508000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (265, N'7509000', N'Travelling & Conveyance', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (266, N'7509001', N'Travelling Local', N'7509000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (267, N'7509002', N'Travelling Outstations', N'7509000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (268, N'7509003', N'Boarding & Lodgings', N'7509000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (269, N'7509004', N'Cartage, Freight & Octroi', N'7509000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (270, N'7509005', N'Travelling International', N'7509000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (271, N'7509006', N'Boarding & Lodgings International', N'7509000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (272, N'7510000', N'Entertainment Expenses', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (273, N'7510001', N'Business Entertainment', N'7510000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (274, N'7510002', N'Staff Entertainment', N'7510000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (275, N'7511000', N'Legal & Professional Expenses', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (276, N'7511001', N'Audit Fee', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (277, N'7511002', N'Tax Consultancy Fee', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (278, N'7511003', N'Legal Consultancy Fee', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (279, N'7511004', N'Trade License Professional Tax', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (280, N'7511005', N'Stamp Papers', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (281, N'7511006', N'Other Legal Expenses', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (282, N'7511007', N'Fee & Subscription', N'7511000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (283, N'7512000', N'General Administrative Expenses', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (284, N'7512001', N'Office Supplies', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (285, N'7512002', N'Gardening Exp', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (286, N'7512003', N'News Paper & Periodicals', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (287, N'7512004', N'Fee & Subscriptions', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (288, N'7512005', N'Insurance Premium', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (289, N'7512006', N'Other General Administrative Expenses', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (290, N'7512007', N'Tender Charges', N'7512000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (291, N'7513000', N'Welfare Expenses', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (292, N'7513001', N'Staff Welfare', N'7513000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (293, N'7513002', N'Gifts & Presents', N'7513000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (294, N'7513003', N'Charity & Donations', N'7513000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (295, N'7513004', N'Sports & Recreation', N'7513000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (296, N'7513005', N'Employee Training', N'7513000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (297, N'7513006', N'Medical & First Aid', N'7513000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (298, N'7514000', N'Other Expenses', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (299, N'7514001', N'Bad Debts Write-off', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (300, N'7514002', N'Petty Cash Over/Short', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (301, N'7514003', N'Suspense balancing', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (302, N'7514004', N'Unrealized loss', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (303, N'7514005', N'Realized loss', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (304, N'7514006', N'Asset Disposal Loss', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (305, N'7514007', N'Currency balancing', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (306, N'7514008', N'xxxxxx', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (307, N'7514009', N'xxxxxx', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (308, N'7514010', N'xxxxxx', N'7514000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (309, N'7515000', N'Depreciation & Amortization', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (310, N'7515001', N'Deperciation Expense', N'7515000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (311, N'7515002', N'Amortization', N'7515000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (312, N'7516000', N'Financial Charges', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (313, N'7516001', N'Mark up on Short Term Loan', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (314, N'7516002', N'Mark up on Long Term Loan', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (315, N'7516003', N'Lease Financial Charges', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (316, N'7516004', N'Letter of Guarantee', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (317, N'7516005', N'LC Charges', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (318, N'7516006', N'Import Contract Commission', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (319, N'7516007', N'LC Acceptance Commission', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (320, N'7516008', N'Trade Finance Service Charges', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (321, N'7516009', N'LC Intrest Debit', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (322, N'7516010', N'Bank Charges', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (323, N'7516011', N'Stamp Charges', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (324, N'7516012', N'Facility Renewal Charges', N'7516000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (325, N'7517000', N'Taxation', N'7500000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (326, N'7517001', N'Tax expense', N'7517000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (327, N'9000000', N'Commitments', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (328, N'9001000', N'Commitments', N'9000000', 2, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (329, N'9001001', N'PO Commitment', N'9001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CHART_OF_ACCOUNT] ([Id], [Code], [Name], [ParentCode], [LevelID], [AccountTypeId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (330, N'9001002', N'SO Commitment', N'9001000', 3, 1, 1, N'superadmin', CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_CHART_OF_ACCOUNT] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_CITY] ON 
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (0, 3, N'LHR', N'Lahore', N'042', 1, N'superadmin', CAST(N'2020-02-21T15:48:13.423' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, 3, N'KHI', N'Karachi', N'021', 1, N'superadmin', CAST(N'2020-02-26T14:29:20.707' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, 3, N'ISL', N'Islamabad', N'051', 1, N'superadmin', CAST(N'2020-02-26T14:29:40.887' AS DateTime), N'Basim', CAST(N'2020-10-28T16:48:49.437' AS DateTime))
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (3, 3, N'FSD', N'Faisalabad', N'041', 1, N'superadmin', CAST(N'2020-02-26T14:29:56.447' AS DateTime), N'superadmin', CAST(N'2020-05-07T22:57:15.023' AS DateTime))
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (4, 3, N'GRW', N'Gujranwala', N'055', 1, N'superadmin', CAST(N'2020-10-28T15:35:16.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (5, 3, N'MTL', N'Multan', N'061', 1, N'Basim', CAST(N'2020-10-28T16:16:02.117' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (6, 3, N'QTA', N'Quetta', N'081', 1, N'Basim', CAST(N'2020-10-28T16:23:03.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (7, 3, N'SKR', N'Sukkur', N'071', 1, N'Basim', CAST(N'2020-10-28T16:25:11.993' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (8, 3, N'HYD', N'Hyderabad', N'221', 1, N'Basim', CAST(N'2020-10-28T16:25:51.913' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (9, 3, N'SKT', N'Sialkot', N'052', 1, N'Basim', CAST(N'2020-10-28T16:38:53.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (10, 3, N'GUJ', N'Gujrat', N'053', 1, N'Basim', CAST(N'2020-10-28T16:40:25.597' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (11, 3, N'PEW', N'Peshawar', N'091', 1, N'Basim', CAST(N'2020-10-28T16:49:42.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (12, 3, N'MAR', N'Mardan', N'937', 1, N'Basim', CAST(N'2020-10-28T16:50:47.457' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (13, 3, N'HPR', N'Haripur', N'595', 1, N'Basim', CAST(N'2020-10-28T16:52:42.963' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (14, 3, N'MSR', N'Mansehra', N'997', 1, N'Basim', CAST(N'2020-10-28T16:54:13.997' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (15, 3, N'ABT', N'Abbottabad', N'992', 1, N'Basim', CAST(N'2020-10-28T16:55:11.603' AS DateTime), N'Basim', CAST(N'2020-10-29T11:26:59.720' AS DateTime))
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (16, 3, N'GLT', N'Gilgit ', N'572', 1, N'Basim', CAST(N'2020-10-28T16:57:15.527' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (17, 3, N'HNZ', N'Hunza', N'581', 1, N'Basim', CAST(N'2020-10-28T16:59:53.393' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (18, 3, N'SKD', N'Skardu', N'815', 1, N'Basim', CAST(N'2020-10-28T17:01:03.507' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (19, 3, N'BWP', N'Bahawalpur', N'062', 1, N'Basim', CAST(N'2020-10-28T17:01:40.333' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (20, 3, N'RYK', N'Rahim Yar Khan', N'068', 1, N'Basim', CAST(N'2020-10-28T17:02:40.873' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (21, 3, N'SGD', N'Sargodha', N'048', 1, N'Basim', CAST(N'2020-10-28T17:03:52.733' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_CITY] ([Id], [CountryId], [Code], [Name], [DialingCode], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (22, 3, N'JHG', N'Jhang', N'477', 1, N'Basim', CAST(N'2020-10-28T17:05:20.543' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_CITY] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_COUNTRY] ON 
GO
INSERT [dbo].[DB_COUNTRY] ([Id], [Code], [Name], [DialingCode], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (3, N'PK', N'Pakistan', N'092', 1, CAST(N'2020-02-19T16:46:04.417' AS DateTime), N'superadmin', CAST(N'2020-05-07T16:30:01.997' AS DateTime), N'superadmin')
GO
INSERT [dbo].[DB_COUNTRY] ([Id], [Code], [Name], [DialingCode], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (4, N'32', N'feef', N'32', 1, CAST(N'2021-01-21T10:31:44.327' AS DateTime), N'superadmin', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_COUNTRY] OFF
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Assets', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Liabilities', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Capital & Reserves', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (4, N'04', N'Turnover', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (5, N'05', N'Cost of Goods Sold', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (6, N'06', N'Non Operating Expenses', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (7, N'07', N'Taxation and Extraordinary Items', 1)
GO
INSERT [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (8, N'08', N'Non Operating Income', 1)
GO
INSERT [dbo].[DB_DEV_FISCAL_YEAR] ([Id], [Code], [Name], [StartDate], [EndDate], [ValidFlag]) VALUES (1, N'0001', N'2019', CAST(N'2019-07-01' AS Date), CAST(N'2020-06-30' AS Date), 1)
GO
INSERT [dbo].[DB_DEV_FISCAL_YEAR] ([Id], [Code], [Name], [StartDate], [EndDate], [ValidFlag]) VALUES (2, N'0002', N'2020', CAST(N'2020-07-01' AS Date), CAST(N'2021-06-30' AS Date), 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (1, N'01', N'Purchase', N'Discount Account', 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (2, N'02', N'Purchase', N'Freight Account', 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (3, N'03', N'Inventory', N'Allocation Account', 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (4, N'04', N'Fixed Asset', N'Aquisition Clearing Account', 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (5, N'05', N'General', N'GST Account', 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (6, N'06', N'General', N'Cash In Hand Account', 1)
GO
INSERT [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id], [Code], [TabName], [FieldName], [ValidFlag]) VALUES (7, N'07', N'General', N'Cheque Received Account', 1)
GO
INSERT [dbo].[DB_DEV_LOCATION_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Company', 1)
GO
INSERT [dbo].[DB_DEV_LOCATION_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Production', 1)
GO
INSERT [dbo].[DB_DEV_LOCATION_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Sales', 1)
GO
INSERT [dbo].[DB_DEV_LOCATION_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (4, N'04', N'Factory', 1)
GO
INSERT [dbo].[DB_DEV_LOCATION_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (5, N'05', N'Zone', 1)
GO
INSERT [dbo].[DB_DEV_LOCATION_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (6, N'06', N'Area', 1)
GO
SET IDENTITY_INSERT [dbo].[DB_DEV_PRIVILEGES] ON 
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (1, N'Add Role', N'Role Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (2, N'View Role', N'Role Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (3, N'Update Role', N'Role Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (4, N'Inactive Role', N'Role Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (14, N'Add Country', N'Country Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (15, N'Update Country', N'Country Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (16, N'Inactive Country', N'Country Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (17, N'View Country', N'Country Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (19, N'Add City', N'City Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (20, N'Update City', N'City Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (21, N'Inactive City', N'City Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (22, N'View City', N'City Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (25, N'Add Location', N'Location Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (26, N'Update Location', N'Location Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (27, N'Inactive Location', N'Location Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (28, N'View Location', N'Location Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (31, N'Active Country', N'Country Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (32, N'Active City', N'City Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (33, N'Active Location', N'Location Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (67, N'Add User', N'User Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (68, N'View User', N'User Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (69, N'Update User', N'User Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (70, N'Inactive User', N'User Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (71, N'Active User', N'User Management', N'User Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (77, N'Add Primary UOM', N'Primary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (78, N'View Primary UOM', N'Primary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (79, N'Update Primary UOM', N'Primary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (80, N'Inactive Primary UOM', N'Primary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (81, N'Active Primary UOM', N'Primary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (82, N'Add Secondary UOM', N'Secondary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (83, N'View Secondary UOM', N'Secondary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (84, N'Update Secondary UOM', N'Secondary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (85, N'Inactive Secondary UOM', N'Secondary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (86, N'Active Secondary UOM', N'Secondary UOM Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (92, N'Add Product Category', N'Product Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (93, N'View Product Category', N'Product Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (94, N'Update Product Category', N'Product Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (95, N'Inactive Product Category', N'Product Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (96, N'Active Product Category', N'Product Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (97, N'Add Product Sub Category', N'Product Sub Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (98, N'View Product Sub Category', N'Product Sub Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (99, N'Update Product Sub Category', N'Product Sub Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (100, N'Inactive Product Sub Category', N'Product Sub Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (101, N'Active Product Sub Category', N'Product Sub Category Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (108, N'Add Product', N'Product Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (109, N'View Product', N'Product Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (110, N'Update Product', N'Product Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (111, N'Inactive Product', N'Product Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (112, N'Active Product', N'Product Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (135, N'Add Product Brand', N'Product Brand Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (136, N'View Product Brand', N'Product Brand Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (138, N'Update Product Brand', N'Product Brand Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (139, N'Inactive Product Brand', N'Product Brand Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (140, N'Active Product Brand', N'Product Brand Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (141, N'Add GST', N'GST Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (142, N'View GST', N'GST Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (143, N'Update GST', N'GST Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (144, N'Inactive GST', N'GST Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (145, N'Active GST', N'GST Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (146, N'Add Vehicle Type', N'Vehicle Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (147, N'View Vehicle Type', N'Vehicle Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (148, N'Update Vehicle Type', N'Vehicle Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (149, N'Inactive Vehicle Type', N'Vehicle Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (150, N'Active Vehicle Type', N'Vehicle Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (151, N'Add Vehicle', N'Vehicle Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (152, N'View Vehicle', N'Vehicle Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (153, N'Update Vehicle', N'Vehicle Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (154, N'Inactive Vehicle', N'Vehicle Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (155, N'Active Vehicle', N'Vehicle Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (156, N'Add Driver', N'Driver Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (157, N'View Driver', N'Driver Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (158, N'Update Driver', N'Driver Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (159, N'Inactive Driver', N'Driver Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (160, N'Active Driver', N'Driver Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (224, N'Add Bank', N'Bank Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (225, N'View Bank', N'Bank Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (226, N'Update Bank', N'Bank Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (227, N'Inactive Bank', N'Bank Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (228, N'Active Bank', N'Bank Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (229, N'Add WHT', N'WHT Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (230, N'View WHT', N'WHT Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (232, N'Update WHT', N'WHT Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (234, N'Inactive WHT', N'WHT Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (235, N'Active WHT', N'WHT Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (236, N'Add Customer Type', N'Customer Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (237, N'View Customer Type', N'Customer Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (238, N'Update Customer Type', N'Customer Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (239, N'Inactive Customer Type', N'Customer Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (240, N'Active Customer Type', N'Customer Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (241, N'Add Customer', N'Customer Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (242, N'View Customer', N'Customer Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (243, N'Update Customer', N'Customer Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (244, N'Inactive Customer', N'Customer Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (245, N'Active Customer', N'Customer Management', N'Sales Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (246, N'Add Vendor Type', N'Vendor Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (247, N'View Vendor Type', N'Vendor Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (248, N'Update Vendor Type', N'Vendor Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (249, N'Inactive Vendor Type', N'Vendor Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (250, N'Active Vendor Type', N'Vendor Type Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (251, N'Add Vendor', N'Vendor Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (252, N'View Vendor', N'Vendor Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (253, N'Update Vendor', N'Vendor Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (254, N'Inactive Vendor', N'Vendor Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (255, N'Active Vendor', N'Vendor Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (256, N'Add Product Price', N'Product Price Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (257, N'View Product Price', N'Product Price Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (258, N'Update Product Price', N'Product Price Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (259, N'Inactive Product Price', N'Product Price Management', N'Product Master', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (260, N'Add Warehouse', N'Warehouse Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (261, N'View Warehouse', N'Warehouse Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (262, N'Update Warehouse', N'Warehouse Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (263, N'Inactive Warehouse', N'Warehouse Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (264, N'Active Warehouse', N'Warehouse Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (265, N'Add Bank Branch', N'Bank Branch Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (266, N'View Bank Branch', N'Bank Branch Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (267, N'Update Bank Branch', N'Bank Branch Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (268, N'Inactive Bank Branch', N'Bank Branch Management', N'Setup', NULL)
GO
INSERT [dbo].[DB_DEV_PRIVILEGES] ([Id], [Privilege], [Category], [Portal], [SortOrder]) VALUES (269, N'Active Bank Branch', N'Bank Branch Management', N'Setup', NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_DEV_PRIVILEGES] OFF
GO
INSERT [dbo].[DB_DEV_PRODUCT_PRICE_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Base', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_PRICE_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Factory', 0)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Raw Material', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Packing Material', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Finished Goods', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (4, N'04', N'Work in Process', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (5, N'05', N'Fixed Asset', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (6, N'06', N'General Item', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (7, N'07', N'Waste', 1)
GO
INSERT [dbo].[DB_DEV_PRODUCT_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (8, N'08', N'Service', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Open', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Inward Gate Pass Generated', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Cancelled', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_QUOTATION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Open', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_QUOTATION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Order Created', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_QUOTATION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Cancelled', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_REQUISITION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Open', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_REQUISITION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Quotation Created', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_REQUISITION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Cancelled', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Local', 1)
GO
INSERT [dbo].[DB_DEV_PURCHASE_TYPE] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Import', 1)
GO
INSERT [dbo].[DB_DEV_SALE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Open', 1)
GO
INSERT [dbo].[DB_DEV_SALE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Fully Delivered', 1)
GO
INSERT [dbo].[DB_DEV_SALE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Partial Delivered', 1)
GO
INSERT [dbo].[DB_DEV_SALE_ORDER_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (4, N'04', N'Cancelled', 1)
GO
INSERT [dbo].[DB_DEV_SALE_QUOTATION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (1, N'01', N'Open', 1)
GO
INSERT [dbo].[DB_DEV_SALE_QUOTATION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (2, N'02', N'Order Generated', 1)
GO
INSERT [dbo].[DB_DEV_SALE_QUOTATION_STATUS] ([Id], [Code], [Name], [ValidFlag]) VALUES (3, N'03', N'Cancelled', 1)
GO
SET IDENTITY_INSERT [dbo].[DB_LOCATION] ON 
GO
INSERT [dbo].[DB_LOCATION] ([Id], [ParentId], [CountryId], [CityId], [Code], [Name], [Type], [IsLeaf], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [IsWarehouse], [IsCompanyWarehouse], [Image], [STRN], [NTN], [ContactNo], [AlternateContactNo]) VALUES (1, NULL, 3, 0, N'001', N'Shahtaj', 1, 0, 1, N'superadmin', CAST(N'2020-05-08T00:00:00.000' AS DateTime), NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DB_LOCATION] ([Id], [ParentId], [CountryId], [CityId], [Code], [Name], [Type], [IsLeaf], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [IsWarehouse], [IsCompanyWarehouse], [Image], [STRN], [NTN], [ContactNo], [AlternateContactNo]) VALUES (2, 1, 3, 0, N'0001', N'Islambad Production', 2, 0, 1, N'superadmin', CAST(N'2020-05-08T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2020-11-27T23:48:55.280' AS DateTime), 0, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DB_LOCATION] ([Id], [ParentId], [CountryId], [CityId], [Code], [Name], [Type], [IsLeaf], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [IsWarehouse], [IsCompanyWarehouse], [Image], [STRN], [NTN], [ContactNo], [AlternateContactNo]) VALUES (4, 2, 3, 0, N'LHR', N'Islamabad Factory', 4, 0, 1, N'superadmin', CAST(N'2020-05-08T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2020-11-27T23:46:47.550' AS DateTime), 0, 0, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_LOCATION] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_PRIMARY_UOM] ON 
GO
INSERT [dbo].[DB_PRIMARY_UOM] ([Id], [Code], [Name], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, N'01', N'Pcs', 1, N'superadmin', CAST(N'2021-03-03T10:53:30.730' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_PRIMARY_UOM] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT] ON 
GO
INSERT [dbo].[DB_PRODUCT] ([Id], [Code], [Name], [BarCode], [ProductTypeId], [ProductBrandId], [ProductCategoryId], [ProductSubCategoryId], [PrimaryUomId], [SecondaryUomId], [ConverstionFactor], [Image], [IsTrendyItem], [DepreciationMethod], [DepreciationType], [DepreciationRate], [UsageLife], [ScrapeValue], [InventoryAccount], [COGSAccount], [DepreciationAccount], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, N'01', N'Vital Bundle Pa', N'', 5, 1, 1, 1, 1, 1, CAST(23.0 AS Decimal(5, 1)), NULL, 0, N'S', N'Y', CAST(12.0 AS Decimal(4, 1)), 2, CAST(21.0 AS Decimal(7, 1)), N'9001002', N'9001002', N'7517001', 1, N'superadmin', CAST(N'2021-03-03T11:01:31.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[DB_PRODUCT] ([Id], [Code], [Name], [BarCode], [ProductTypeId], [ProductBrandId], [ProductCategoryId], [ProductSubCategoryId], [PrimaryUomId], [SecondaryUomId], [ConverstionFactor], [Image], [IsTrendyItem], [DepreciationMethod], [DepreciationType], [DepreciationRate], [UsageLife], [ScrapeValue], [InventoryAccount], [COGSAccount], [DepreciationAccount], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (5, N'002', N'Vital Beauty So', N'', 1, 1, 1, 1, 1, NULL, CAST(0.0 AS Decimal(5, 1)), NULL, 0, N' ', N' ', CAST(0.0 AS Decimal(4, 1)), 0, CAST(0.0 AS Decimal(7, 1)), N'9001001', N'7517001', NULL, 1, N'superadmin', CAST(N'2021-03-08T13:00:12.263' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT_BRAND] ON 
GO
INSERT [dbo].[DB_PRODUCT_BRAND] ([Id], [Code], [Name], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [IsTrendy]) VALUES (1, N'01', N'JYA', 1, N'superadmin', CAST(N'2021-03-03T10:55:56.223' AS DateTime), NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT_BRAND] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT_CATEGORY] ON 
GO
INSERT [dbo].[DB_PRODUCT_CATEGORY] ([Id], [Code], [Name], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (1, N'01', N'Tea', 1, CAST(N'2021-03-03T10:56:26.160' AS DateTime), N'superadmin', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT_CATEGORY] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT_SUB_CATEGORY] ON 
GO
INSERT [dbo].[DB_PRODUCT_SUB_CATEGORY] ([Id], [Code], [Name], [ProductCategoryId], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (1, N'01', N'Normal Tea', 1, 1, CAST(N'2021-03-03T10:56:48.090' AS DateTime), N'superadmin', CAST(N'2021-03-03T10:57:14.250' AS DateTime), N'superadmin')
GO
SET IDENTITY_INSERT [dbo].[DB_PRODUCT_SUB_CATEGORY] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_ROLE_PRIVILEGES] ON 
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6901, 151, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6902, 152, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6903, 153, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6904, 154, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6905, 155, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6906, 156, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6907, 157, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6908, 158, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6909, 159, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (6910, 160, N'fe2b5d83-09d6-4f12-96e8-09860a7f135b')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9308, 1, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9309, 2, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9310, 3, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9311, 4, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9312, 67, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9313, 68, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9314, 69, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9315, 70, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9316, 71, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9317, 14, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9318, 15, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9319, 16, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9320, 17, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9321, 31, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9322, 19, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9323, 20, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9324, 21, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9325, 22, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9326, 32, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9327, 25, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9328, 26, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9329, 27, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9330, 28, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9331, 33, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9332, 77, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9333, 78, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9334, 79, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9335, 80, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9336, 81, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9337, 82, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9338, 83, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9339, 84, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9340, 85, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9341, 86, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9342, 141, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9343, 142, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9344, 143, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9345, 144, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9346, 145, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9347, 146, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9348, 147, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9349, 148, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9350, 149, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9351, 150, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9352, 224, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9353, 225, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9354, 226, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9355, 227, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9356, 228, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9357, 229, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9358, 230, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9359, 232, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9360, 234, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9361, 235, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9362, 236, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9363, 237, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9364, 238, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9365, 239, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9366, 240, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9367, 246, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9368, 247, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9369, 248, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9370, 249, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9371, 250, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9372, 251, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9373, 252, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9374, 253, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9375, 254, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9376, 255, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9377, 260, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9378, 261, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9379, 262, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9380, 263, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9381, 264, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9382, 92, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9383, 93, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9384, 94, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9385, 95, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9386, 96, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9387, 97, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9388, 98, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9389, 99, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9390, 100, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9391, 101, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9392, 108, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9393, 109, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9394, 110, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9395, 111, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9396, 112, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9397, 135, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9398, 136, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9399, 138, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9400, 139, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9401, 140, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9402, 256, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9403, 257, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9404, 258, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9405, 259, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9406, 151, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9407, 152, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9408, 153, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9409, 154, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9410, 155, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9411, 156, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9412, 157, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9413, 158, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9414, 159, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9415, 160, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9416, 241, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9417, 242, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9418, 243, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9419, 244, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
INSERT [dbo].[DB_ROLE_PRIVILEGES] ([ID], [PRIVILEGE_ID], [ROLE_ID]) VALUES (9420, 245, N'fcd918d9-789a-4328-af13-ff44d14fc5d7')
GO
SET IDENTITY_INSERT [dbo].[DB_ROLE_PRIVILEGES] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_SECONDARY_UOM] ON 
GO
INSERT [dbo].[DB_SECONDARY_UOM] ([Id], [PrimaryUOMId], [Code], [Name], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, 1, N'01', N'Pack', 1, N'superadmin', CAST(N'2021-03-03T10:53:45.707' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_SECONDARY_UOM] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_USER_PROFILE] ON 
GO
INSERT [dbo].[DB_USER_PROFILE] ([Id], [UserId], [UserName], [FirstName], [LastName], [CNIC], [Contact], [Email], [Designation], [Image], [LocationTypeId], [LocationId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, N'8a13a6b9-d338-49cd-a45a-bbbb1a1c3006', N'superadmin', N'Super', N'Admin', N'3510343976805', N'03217261866', N'abc@abc.com', N'Admin', NULL, 1, 1, 1, N'superadmin', CAST(N'2020-05-12T12:28:23.433' AS DateTime), N'superadmin', CAST(N'2021-01-15T10:59:34.117' AS DateTime))
GO
INSERT [dbo].[DB_USER_PROFILE] ([Id], [UserId], [UserName], [FirstName], [LastName], [CNIC], [Contact], [Email], [Designation], [Image], [LocationTypeId], [LocationId], [ValidFlag], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (24, N'57c142dd-b760-403d-b011-ba89f67a0272', N'farhan.malik', N'Farhan', N'Tariq', N'35103-4397680-5', N'03074299132', N'malikfarhan42034@gmail.com', N'Developer', N'f9f0f785-b428-49d5-81b1-2fd59ca4e80d.jpg', 2, 2, 1, N'superadmin', CAST(N'2021-01-15T11:15:03.533' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_USER_PROFILE] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_VENDOR] ON 
GO
INSERT [dbo].[DB_VENDOR] ([Id], [LocationId], [VendorTypeId], [Code], [Name], [CNIC], [NTN], [IsCredit], [CreditDays], [CreditAmount], [ContactPerson], [MobileNo], [Email], [Address], [AccountPayable], [DownPaymentAccount], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (1, 4, 1, N'01', N'Test', N'35103-4397680-5', N'', 0, 0, CAST(0 AS Decimal(18, 0)), N'Farhan', N'0302323223', N'far@gmail.com', N'Lahore', N'9001002', N'9001002', 1, CAST(N'2021-03-03T10:55:42.027' AS DateTime), N'superadmin', NULL, NULL)
GO
INSERT [dbo].[DB_VENDOR] ([Id], [LocationId], [VendorTypeId], [Code], [Name], [CNIC], [NTN], [IsCredit], [CreditDays], [CreditAmount], [ContactPerson], [MobileNo], [Email], [Address], [AccountPayable], [DownPaymentAccount], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (2, 4, 1, N'02', N'Salman', N'35103-4397680-5', N'', 0, 0, CAST(0 AS Decimal(18, 0)), N'Salman', N'0323233', N'salman@gmail.com', N'Lahore', N'9001001', N'7516012', 1, CAST(N'2021-03-03T11:02:43.020' AS DateTime), N'superadmin', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_VENDOR] OFF
GO
SET IDENTITY_INSERT [dbo].[DB_VENDOR_TYPE] ON 
GO
INSERT [dbo].[DB_VENDOR_TYPE] ([Id], [Code], [Name], [ValidFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (1, N'01', N'Local', 1, CAST(N'2021-03-03T10:54:15.623' AS DateTime), N'superadmin', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[DB_VENDOR_TYPE] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 3/12/2021 11:15:50 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 3/12/2021 11:15:50 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 3/12/2021 11:15:50 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_RoleId]    Script Date: 3/12/2021 11:15:50 AM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 3/12/2021 11:15:50 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 3/12/2021 11:15:50 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_DB_CITY]    Script Date: 3/12/2021 11:15:50 AM ******/
ALTER TABLE [dbo].[DB_CITY] ADD  CONSTRAINT [UC_DB_CITY] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_DB_COUNTRY]    Script Date: 3/12/2021 11:15:50 AM ******/
ALTER TABLE [dbo].[DB_COUNTRY] ADD  CONSTRAINT [UC_DB_COUNTRY] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_DB_DEV_PRIVILEGES]    Script Date: 3/12/2021 11:15:50 AM ******/
ALTER TABLE [dbo].[DB_DEV_PRIVILEGES] ADD  CONSTRAINT [UC_DB_DEV_PRIVILEGES] UNIQUE NONCLUSTERED 
(
	[Privilege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DB_DRIVER] ADD  CONSTRAINT [DF_DB_DRIVER_IsAvailable]  DEFAULT ((1)) FOR [IsAvailable]
GO
ALTER TABLE [dbo].[DB_LOCATION] ADD  DEFAULT ((0)) FOR [IsWarehouse]
GO
ALTER TABLE [dbo].[DB_LOCATION] ADD  DEFAULT ((0)) FOR [IsCompanyWarehouse]
GO
ALTER TABLE [dbo].[DB_PRODUCT] ADD  CONSTRAINT [DF_DB_PRODUCT_ConverstionFactor]  DEFAULT ((0)) FOR [ConverstionFactor]
GO
ALTER TABLE [dbo].[DB_PRODUCT_BRAND] ADD  CONSTRAINT [DF_DB_PRODUCT_BRAND_IsTrendy]  DEFAULT ((0)) FOR [IsTrendy]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER_DETAIL] ADD  CONSTRAINT [DF_DB_PURCHASE_ORDER_DETAIL_TotalQuantity]  DEFAULT ((0)) FOR [TotalQuantity]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL] ADD  CONSTRAINT [DF_DB_PURCHASE_QUOTATION_DETAIL_TotalQuantity]  DEFAULT ((0)) FOR [TotalQuantity]
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION_DETAIL] ADD  CONSTRAINT [DF_DB_PURCHASE_REQUISITION_DETAIL_TotalQuantity]  DEFAULT ((0)) FOR [TotalQuantity]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER_DETAIL] ADD  CONSTRAINT [DF_DB_SALE_ORDER_DETAIL_TotalQuantity]  DEFAULT ((0)) FOR [TotalQuantity]
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION_DETAIL] ADD  CONSTRAINT [DF_DB_SALE_QUOTATION_DETAIL_TotalQuantity]  DEFAULT ((0)) FOR [TotalQuantity]
GO
ALTER TABLE [dbo].[DB_VEHICLE] ADD  CONSTRAINT [DF_DB_VEHICLE_IsAvailable]  DEFAULT ((1)) FOR [IsAvailable]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[DB_BANK_BRANCH]  WITH CHECK ADD  CONSTRAINT [FK_DB_BANK_BRANCH_DB_BANK] FOREIGN KEY([BankId])
REFERENCES [dbo].[DB_BANK] ([Id])
GO
ALTER TABLE [dbo].[DB_BANK_BRANCH] CHECK CONSTRAINT [FK_DB_BANK_BRANCH_DB_BANK]
GO
ALTER TABLE [dbo].[DB_BANK_BRANCH]  WITH CHECK ADD  CONSTRAINT [FK_DB_BANK_BRANCH_DB_CHART_OF_ACCOUNT] FOREIGN KEY([AccountBankBalance])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_BANK_BRANCH] CHECK CONSTRAINT [FK_DB_BANK_BRANCH_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_CHART_OF_ACCOUNT]  WITH CHECK ADD  CONSTRAINT [FK_DB_CHART_OF_ACCOUNT_DB_CHART_OF_ACCOUNT] FOREIGN KEY([ParentCode])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_CHART_OF_ACCOUNT] CHECK CONSTRAINT [FK_DB_CHART_OF_ACCOUNT_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_CHART_OF_ACCOUNT]  WITH CHECK ADD  CONSTRAINT [FK_DB_CHART_OF_ACCOUNT_DB_DEV_CHART_OF_ACCOUNT_TYPE] FOREIGN KEY([AccountTypeId])
REFERENCES [dbo].[DB_DEV_CHART_OF_ACCOUNT_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_CHART_OF_ACCOUNT] CHECK CONSTRAINT [FK_DB_CHART_OF_ACCOUNT_DB_DEV_CHART_OF_ACCOUNT_TYPE]
GO
ALTER TABLE [dbo].[DB_CITY]  WITH CHECK ADD  CONSTRAINT [FK_DB_CITY_DB_COUNTRY] FOREIGN KEY([CountryId])
REFERENCES [dbo].[DB_COUNTRY] ([Id])
GO
ALTER TABLE [dbo].[DB_CITY] CHECK CONSTRAINT [FK_DB_CITY_DB_COUNTRY]
GO
ALTER TABLE [dbo].[DB_CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_DB_CUSTOMER_DB_CHART_OF_ACCOUNT] FOREIGN KEY([AccountReceivable])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_CUSTOMER] CHECK CONSTRAINT [FK_DB_CUSTOMER_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_DB_CUSTOMER_DB_CHART_OF_ACCOUNT1] FOREIGN KEY([DownPaymentAccount])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_CUSTOMER] CHECK CONSTRAINT [FK_DB_CUSTOMER_DB_CHART_OF_ACCOUNT1]
GO
ALTER TABLE [dbo].[DB_CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_DB_CUSTOMER_DB_CUSTOMER_TYPE] FOREIGN KEY([CustomerTypeId])
REFERENCES [dbo].[DB_CUSTOMER_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_CUSTOMER] CHECK CONSTRAINT [FK_DB_CUSTOMER_DB_CUSTOMER_TYPE]
GO
ALTER TABLE [dbo].[DB_CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_DB_CUSTOMER_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_CUSTOMER] CHECK CONSTRAINT [FK_DB_CUSTOMER_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_DRIVER]  WITH CHECK ADD  CONSTRAINT [FK_DB_DRIVER_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_DRIVER] CHECK CONSTRAINT [FK_DB_DRIVER_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_GL_ACCOUNT_INTEGRATION_DB_CHART_OF_ACCOUNT] FOREIGN KEY([AccountCode])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION] CHECK CONSTRAINT [FK_DB_GL_ACCOUNT_INTEGRATION_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_GL_ACCOUNT_INTEGRATION_DB_DEV_FISCAL_YEAR] FOREIGN KEY([FiscalYearId])
REFERENCES [dbo].[DB_DEV_FISCAL_YEAR] ([Id])
GO
ALTER TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION] CHECK CONSTRAINT [FK_DB_GL_ACCOUNT_INTEGRATION_DB_DEV_FISCAL_YEAR]
GO
ALTER TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_GL_ACCOUNT_INTEGRATION_DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] FOREIGN KEY([FieldId])
REFERENCES [dbo].[DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD] ([Id])
GO
ALTER TABLE [dbo].[DB_GL_ACCOUNT_INTEGRATION] CHECK CONSTRAINT [FK_DB_GL_ACCOUNT_INTEGRATION_DB_DEV_GL_ACCOUNT_INTEGRATION_FIELD]
GO
ALTER TABLE [dbo].[DB_GST]  WITH CHECK ADD  CONSTRAINT [FK_DB_GST_DB_CHART_OF_ACCOUNT] FOREIGN KEY([AccountGST])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_GST] CHECK CONSTRAINT [FK_DB_GST_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_LOCATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_LOCATION_DB_CITY] FOREIGN KEY([CityId])
REFERENCES [dbo].[DB_CITY] ([Id])
GO
ALTER TABLE [dbo].[DB_LOCATION] CHECK CONSTRAINT [FK_DB_LOCATION_DB_CITY]
GO
ALTER TABLE [dbo].[DB_LOCATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_LOCATION_DB_COUNTRY] FOREIGN KEY([CountryId])
REFERENCES [dbo].[DB_COUNTRY] ([Id])
GO
ALTER TABLE [dbo].[DB_LOCATION] CHECK CONSTRAINT [FK_DB_LOCATION_DB_COUNTRY]
GO
ALTER TABLE [dbo].[DB_LOCATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_LOCATION_DB_DEV_LOCATION_TYPE] FOREIGN KEY([Type])
REFERENCES [dbo].[DB_DEV_LOCATION_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_LOCATION] CHECK CONSTRAINT [FK_DB_LOCATION_DB_DEV_LOCATION_TYPE]
GO
ALTER TABLE [dbo].[DB_LOCATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_LOCATION_DB_LOCATION] FOREIGN KEY([ParentId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_LOCATION] CHECK CONSTRAINT [FK_DB_LOCATION_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_CHART_OF_ACCOUNT] FOREIGN KEY([InventoryAccount])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_CHART_OF_ACCOUNT1] FOREIGN KEY([InventoryAccount])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_CHART_OF_ACCOUNT1]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_CHART_OF_ACCOUNT2] FOREIGN KEY([DepreciationAccount])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_CHART_OF_ACCOUNT2]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_DEV_PRODUCT_TYPE] FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[DB_DEV_PRODUCT_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_DEV_PRODUCT_TYPE]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_PRIMARY_UOM] FOREIGN KEY([PrimaryUomId])
REFERENCES [dbo].[DB_PRIMARY_UOM] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_PRIMARY_UOM]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_PRODUCT_BRAND] FOREIGN KEY([ProductBrandId])
REFERENCES [dbo].[DB_PRODUCT_BRAND] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_PRODUCT_BRAND]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_PRODUCT_CATEGORY] FOREIGN KEY([ProductCategoryId])
REFERENCES [dbo].[DB_PRODUCT_CATEGORY] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_PRODUCT_CATEGORY]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_PRODUCT_SUB_CATEGORY] FOREIGN KEY([ProductSubCategoryId])
REFERENCES [dbo].[DB_PRODUCT_SUB_CATEGORY] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_PRODUCT_SUB_CATEGORY]
GO
ALTER TABLE [dbo].[DB_PRODUCT]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_DB_SECONDARY_UOM] FOREIGN KEY([SecondaryUomId])
REFERENCES [dbo].[DB_SECONDARY_UOM] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT] CHECK CONSTRAINT [FK_DB_PRODUCT_DB_SECONDARY_UOM]
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_PRICE_DB_DEV_PRODUCT_PRICE_TYPE] FOREIGN KEY([ProductPriceTypeId])
REFERENCES [dbo].[DB_DEV_PRODUCT_PRICE_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE] CHECK CONSTRAINT [FK_DB_PRODUCT_PRICE_DB_DEV_PRODUCT_PRICE_TYPE]
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_PRICE_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE] CHECK CONSTRAINT [FK_DB_PRODUCT_PRICE_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_PRICE_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE] CHECK CONSTRAINT [FK_DB_PRODUCT_PRICE_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_PRICE_HISTORY_DB_DEV_PRODUCT_PRICE_TYPE] FOREIGN KEY([ProductPriceTypeId])
REFERENCES [dbo].[DB_DEV_PRODUCT_PRICE_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY] CHECK CONSTRAINT [FK_DB_PRODUCT_PRICE_HISTORY_DB_DEV_PRODUCT_PRICE_TYPE]
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_PRICE_HISTORY_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY] CHECK CONSTRAINT [FK_DB_PRODUCT_PRICE_HISTORY_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_PRICE_HISTORY_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_PRICE_HISTORY] CHECK CONSTRAINT [FK_DB_PRODUCT_PRICE_HISTORY_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_PRODUCT_SUB_CATEGORY]  WITH CHECK ADD  CONSTRAINT [FK_DB_PRODUCT_SUB_CATEGORY_DB_PRODUCT_CATEGORY] FOREIGN KEY([ProductCategoryId])
REFERENCES [dbo].[DB_PRODUCT_CATEGORY] ([Id])
GO
ALTER TABLE [dbo].[DB_PRODUCT_SUB_CATEGORY] CHECK CONSTRAINT [FK_DB_PRODUCT_SUB_CATEGORY_DB_PRODUCT_CATEGORY]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_DEV_PURCHASE_ORDER_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[DB_DEV_PURCHASE_ORDER_STATUS] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER] CHECK CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_DEV_PURCHASE_ORDER_STATUS]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER] CHECK CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_PURCHASE_QUOTATION] FOREIGN KEY([PurchaseQuotationId])
REFERENCES [dbo].[DB_PURCHASE_QUOTATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER] CHECK CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_PURCHASE_QUOTATION]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_VENDOR] FOREIGN KEY([VendorId])
REFERENCES [dbo].[DB_VENDOR] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER] CHECK CONSTRAINT [FK_DB_PURCHASE_ORDER_DB_VENDOR]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_ORDER_DETAIL_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_ORDER_DETAIL_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_ORDER_DETAIL_DB_PURCHASE_ORDER] FOREIGN KEY([PurchaseOrderId])
REFERENCES [dbo].[DB_PURCHASE_ORDER] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_ORDER_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_ORDER_DETAIL_DB_PURCHASE_ORDER]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_DEV_PURCHASE_QUOTATION_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[DB_DEV_PURCHASE_QUOTATION_STATUS] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_DEV_PURCHASE_QUOTATION_STATUS]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_DEV_PURCHASE_TYPE] FOREIGN KEY([PurchaseTypeId])
REFERENCES [dbo].[DB_DEV_PURCHASE_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_DEV_PURCHASE_TYPE]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_PURCHASE_REQUISITION] FOREIGN KEY([PurchaseRequisitionId])
REFERENCES [dbo].[DB_PURCHASE_REQUISITION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_PURCHASE_REQUISITION]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_VENDOR] FOREIGN KEY([VendorId])
REFERENCES [dbo].[DB_VENDOR] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DB_VENDOR]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_PURCHASE_QUOTATION] FOREIGN KEY([PurchaseQuotationId])
REFERENCES [dbo].[DB_PURCHASE_QUOTATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_PURCHASE_QUOTATION]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_PURCHASE_REQUISITION_DETAIL] FOREIGN KEY([PurchaseRequisitionDetailId])
REFERENCES [dbo].[DB_PURCHASE_REQUISITION_DETAIL] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_PURCHASE_REQUISITION_DETAIL]
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_VENDOR] FOREIGN KEY([VendorId])
REFERENCES [dbo].[DB_VENDOR] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_QUOTATION_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_QUOTATION_DETAIL_DB_VENDOR]
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DB_DEV_PURCHASE_REQUISITION_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[DB_DEV_PURCHASE_REQUISITION_STATUS] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION] CHECK CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DB_DEV_PURCHASE_REQUISITION_STATUS]
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DB_DEV_PURCHASE_TYPE] FOREIGN KEY([PurchaseTypeId])
REFERENCES [dbo].[DB_DEV_PURCHASE_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION] CHECK CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DB_DEV_PURCHASE_TYPE]
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION] CHECK CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DETAIL_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DETAIL_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DETAIL_DB_PURCHASE_REQUISITION] FOREIGN KEY([RequisitionId])
REFERENCES [dbo].[DB_PURCHASE_REQUISITION] ([Id])
GO
ALTER TABLE [dbo].[DB_PURCHASE_REQUISITION_DETAIL] CHECK CONSTRAINT [FK_DB_PURCHASE_REQUISITION_DETAIL_DB_PURCHASE_REQUISITION]
GO
ALTER TABLE [dbo].[DB_ROLE_PRIVILEGES]  WITH CHECK ADD  CONSTRAINT [FK_DB_ROLE_PRIVILEGES_DB_DEV_PRIVILEGES] FOREIGN KEY([PRIVILEGE_ID])
REFERENCES [dbo].[DB_DEV_PRIVILEGES] ([Id])
GO
ALTER TABLE [dbo].[DB_ROLE_PRIVILEGES] CHECK CONSTRAINT [FK_DB_ROLE_PRIVILEGES_DB_DEV_PRIVILEGES]
GO
ALTER TABLE [dbo].[DB_ROLE_PRIVILEGES]  WITH CHECK ADD  CONSTRAINT [FK_DOB_ROLE_PRIVILEGES_AspNetRoles] FOREIGN KEY([ROLE_ID])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO
ALTER TABLE [dbo].[DB_ROLE_PRIVILEGES] CHECK CONSTRAINT [FK_DOB_ROLE_PRIVILEGES_AspNetRoles]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_ORDER_DB_CUSTOMER] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[DB_CUSTOMER] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_ORDER] CHECK CONSTRAINT [FK_DB_SALE_ORDER_DB_CUSTOMER]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_ORDER_DB_DEV_SALE_ORDER_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[DB_DEV_SALE_ORDER_STATUS] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_ORDER] CHECK CONSTRAINT [FK_DB_SALE_ORDER_DB_DEV_SALE_ORDER_STATUS]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_ORDER_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_ORDER] CHECK CONSTRAINT [FK_DB_SALE_ORDER_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_ORDER_DB_SALE_QUOTATION] FOREIGN KEY([SaleQuotationId])
REFERENCES [dbo].[DB_SALE_QUOTATION] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_ORDER] CHECK CONSTRAINT [FK_DB_SALE_ORDER_DB_SALE_QUOTATION]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_ORDER_DETAIL_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_ORDER_DETAIL] CHECK CONSTRAINT [FK_DB_SALE_ORDER_DETAIL_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_SALE_ORDER_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_ORDER_DETAIL_DB_SALE_ORDER] FOREIGN KEY([SaleOrderId])
REFERENCES [dbo].[DB_SALE_ORDER] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_ORDER_DETAIL] CHECK CONSTRAINT [FK_DB_SALE_ORDER_DETAIL_DB_SALE_ORDER]
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_QUOTATION_DB_CUSTOMER] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[DB_CUSTOMER] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION] CHECK CONSTRAINT [FK_DB_SALE_QUOTATION_DB_CUSTOMER]
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_QUOTATION_DB_DEV_SALE_QUOTATION_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[DB_DEV_SALE_QUOTATION_STATUS] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION] CHECK CONSTRAINT [FK_DB_SALE_QUOTATION_DB_DEV_SALE_QUOTATION_STATUS]
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_QUOTATION_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION] CHECK CONSTRAINT [FK_DB_SALE_QUOTATION_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_QUOTATION_DETAIL_DB_PRODUCT] FOREIGN KEY([ProductId])
REFERENCES [dbo].[DB_PRODUCT] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION_DETAIL] CHECK CONSTRAINT [FK_DB_SALE_QUOTATION_DETAIL_DB_PRODUCT]
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_DB_SALE_QUOTATION_DETAIL_DB_SALE_QUOTATION] FOREIGN KEY([SaleQuotationId])
REFERENCES [dbo].[DB_SALE_QUOTATION] ([Id])
GO
ALTER TABLE [dbo].[DB_SALE_QUOTATION_DETAIL] CHECK CONSTRAINT [FK_DB_SALE_QUOTATION_DETAIL_DB_SALE_QUOTATION]
GO
ALTER TABLE [dbo].[DB_SECONDARY_UOM]  WITH CHECK ADD  CONSTRAINT [FK_DB_SECONDARY_UOM_DB_PRIMARY_UOM] FOREIGN KEY([PrimaryUOMId])
REFERENCES [dbo].[DB_PRIMARY_UOM] ([Id])
GO
ALTER TABLE [dbo].[DB_SECONDARY_UOM] CHECK CONSTRAINT [FK_DB_SECONDARY_UOM_DB_PRIMARY_UOM]
GO
ALTER TABLE [dbo].[DB_USER_PROFILE]  WITH CHECK ADD  CONSTRAINT [FK_DB_USER_PROFILE_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[DB_USER_PROFILE] CHECK CONSTRAINT [FK_DB_USER_PROFILE_AspNetUsers]
GO
ALTER TABLE [dbo].[DB_USER_PROFILE]  WITH CHECK ADD  CONSTRAINT [FK_DB_USER_PROFILE_DB_DEV_LOCATION_TYPE] FOREIGN KEY([LocationTypeId])
REFERENCES [dbo].[DB_DEV_LOCATION_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_USER_PROFILE] CHECK CONSTRAINT [FK_DB_USER_PROFILE_DB_DEV_LOCATION_TYPE]
GO
ALTER TABLE [dbo].[DB_USER_PROFILE]  WITH CHECK ADD  CONSTRAINT [FK_DB_USER_PROFILE_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_USER_PROFILE] CHECK CONSTRAINT [FK_DB_USER_PROFILE_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_VEHICLE]  WITH CHECK ADD  CONSTRAINT [FK_DB_VEHICLE_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_VEHICLE] CHECK CONSTRAINT [FK_DB_VEHICLE_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_VEHICLE]  WITH CHECK ADD  CONSTRAINT [FK_DB_VEHICLE_DB_VEHICLE_TYPE] FOREIGN KEY([VehicleTypeId])
REFERENCES [dbo].[DB_VEHICLE_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_VEHICLE] CHECK CONSTRAINT [FK_DB_VEHICLE_DB_VEHICLE_TYPE]
GO
ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_CHART_OF_ACCOUNT] FOREIGN KEY([AccountPayable])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_CHART_OF_ACCOUNT]
GO
ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_CHART_OF_ACCOUNT1] FOREIGN KEY([DownPaymentAccount])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_CHART_OF_ACCOUNT1]
GO
ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_VENDOR]  WITH CHECK ADD  CONSTRAINT [FK_DB_VENDOR_DB_VENDOR_TYPE] FOREIGN KEY([VendorTypeId])
REFERENCES [dbo].[DB_VENDOR_TYPE] ([Id])
GO
ALTER TABLE [dbo].[DB_VENDOR] CHECK CONSTRAINT [FK_DB_VENDOR_DB_VENDOR_TYPE]
GO
ALTER TABLE [dbo].[DB_WAREHOUSE]  WITH CHECK ADD  CONSTRAINT [FK_DB_WAREHOUSE_DB_LOCATION] FOREIGN KEY([LocationId])
REFERENCES [dbo].[DB_LOCATION] ([Id])
GO
ALTER TABLE [dbo].[DB_WAREHOUSE] CHECK CONSTRAINT [FK_DB_WAREHOUSE_DB_LOCATION]
GO
ALTER TABLE [dbo].[DB_WITH_HOLDING_TAX]  WITH CHECK ADD  CONSTRAINT [FK_DB_WITH_HOLDING_TAX_DB_CHART_OF_ACCOUNT] FOREIGN KEY([AccountWithHoldingTax])
REFERENCES [dbo].[DB_CHART_OF_ACCOUNT] ([Code])
GO
ALTER TABLE [dbo].[DB_WITH_HOLDING_TAX] CHECK CONSTRAINT [FK_DB_WITH_HOLDING_TAX_DB_CHART_OF_ACCOUNT]
GO
USE [master]
GO
ALTER DATABASE [JYA_EBS] SET  READ_WRITE 
GO
