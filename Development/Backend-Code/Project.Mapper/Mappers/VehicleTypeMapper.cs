﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class VehicleTypeMapper
    {
        public static DB_VEHICLE_TYPE DBMapper(this DB_VEHICLE_TYPE db_VEHICLE_TYPE, VehicleTypeViewModel value)
        {
             if (db_VEHICLE_TYPE == null)
             {
                db_VEHICLE_TYPE = new DB_VEHICLE_TYPE();
                db_VEHICLE_TYPE.ValidFlag = true;
                db_VEHICLE_TYPE.CreatedOn = value.ChangedOn;
                db_VEHICLE_TYPE.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_VEHICLE_TYPE.UpdatedBy = value.ChangedBy;
                db_VEHICLE_TYPE.UpdatedOn = value.ChangedOn;
            }

            db_VEHICLE_TYPE.Code = value.Code;
            db_VEHICLE_TYPE.Name = value.Name;

            return db_VEHICLE_TYPE;
        }
        public static VehicleTypeViewModel DBValue(DB_VEHICLE_TYPE dbValue)
        {
            VehicleTypeViewModel value = new VehicleTypeViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value;
        }
    }
}
