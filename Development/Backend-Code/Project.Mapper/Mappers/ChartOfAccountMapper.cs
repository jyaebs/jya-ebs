﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class ChartOfAccountMapper
    {
        public static DB_CHART_OF_ACCOUNT DBMapper(this DB_CHART_OF_ACCOUNT db_COA, ChartOfAccountViewModel value)
        {
            if (db_COA == null)
            {
                db_COA = new DB_CHART_OF_ACCOUNT();
                db_COA.ValidFlag = true;
                db_COA.CreatedOn = value.ChangedOn;
                db_COA.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_COA.UpdatedBy = value.ChangedBy;
                db_COA.UpdatedOn = value.ChangedOn;
            }

            db_COA.Code = value.Code;
            db_COA.Name = value.Name;
            db_COA.ParentCode = value.ParentCode;
            db_COA.LevelID = value.LevelID;
            db_COA.AccountTypeId = value.AccountTypeId;

            return db_COA;
        }



        public static ChartOfAccountViewModel DBValue(DB_CHART_OF_ACCOUNT dbValue)
        {
            ChartOfAccountViewModel value = new ChartOfAccountViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.ParentCode = dbValue.ParentCode;
            value.LevelID = dbValue.LevelID;
            value.AccountTypeId = dbValue.AccountTypeId;
            return value;
        }
    }
}
