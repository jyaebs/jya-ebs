﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class ProductSubCategoryMapper
    {
        public static DB_PRODUCT_SUB_CATEGORY DBMapper(this DB_PRODUCT_SUB_CATEGORY db_PRODUCT_SUB_CATEGORY, ProductSubCategoryViewModel value)
        {
            if (db_PRODUCT_SUB_CATEGORY == null)
            {
                db_PRODUCT_SUB_CATEGORY = new DB_PRODUCT_SUB_CATEGORY();

                db_PRODUCT_SUB_CATEGORY.ValidFlag = true;
                db_PRODUCT_SUB_CATEGORY.CreatedOn = value.ChangedOn;
                db_PRODUCT_SUB_CATEGORY.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_PRODUCT_SUB_CATEGORY.UpdatedBy = value.ChangedBy;
                db_PRODUCT_SUB_CATEGORY.UpdatedOn = value.ChangedOn;
            }
            db_PRODUCT_SUB_CATEGORY.ProductCategoryId = value.ProdcutCategoryId;
            db_PRODUCT_SUB_CATEGORY.Code = value.Code;
            db_PRODUCT_SUB_CATEGORY.Name = value.Name;

            return db_PRODUCT_SUB_CATEGORY;
        }
        public static ProductSubCategoryViewModel DBValue(DB_PRODUCT_SUB_CATEGORY dbValue)
        {
            ProductSubCategoryViewModel value = new ProductSubCategoryViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.ProdcutCategoryId = dbValue.ProductCategoryId;
            return value;
        }
    }
}

