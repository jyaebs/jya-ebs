﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Project.Mapper.Mappers
{
    public static class VendorMapper
    {
        public static DB_VENDOR DBMapper(this DB_VENDOR db_vendor, VendorViewModel value)
        {
            if (db_vendor == null)
            {
                db_vendor = new DB_VENDOR();

                db_vendor.ValidFlag = true;
                db_vendor.CreatedOn = value.ChangedOn;
                db_vendor.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_vendor.UpdatedOn = value.ChangedOn;
                db_vendor.UpdatedBy = value.ChangedBy;
            }

            db_vendor.Code = value.Code;
            db_vendor.Name = value.Name;
            db_vendor.CNIC = value.CNIC;
            db_vendor.NTN = value.NTN;
            db_vendor.VendorTypeId = value.VendorTypeId;
            db_vendor.IsCredit = value.IsCredit;
            db_vendor.CreditDays = value.CreditDays;
            db_vendor.CreditAmount = value.CreditAmount;
            db_vendor.ContactPerson = value.ContactPersonName;
            db_vendor.MobileNo = value.MobileNo;
            db_vendor.Email = value.Email;
            db_vendor.Address = value.Address;
            db_vendor.LocationId = value.LocationId;
            db_vendor.AccountPayable = value.AccountPayable;
            db_vendor.DownPaymentAccount = value.DownPaymentAccount;

            return db_vendor;
        }



        public static VendorViewModel DBValue(DB_VENDOR _dbvalue)
        {
            VendorViewModel vendor = new VendorViewModel();
            vendor.Id = _dbvalue.Id;
            vendor.Code = _dbvalue.Code;
            vendor.Name = _dbvalue.Name;
            vendor.CNIC = _dbvalue.CNIC;
            vendor.NTN = _dbvalue.NTN;
            vendor.VendorTypeId = _dbvalue.VendorTypeId;
            vendor.IsCredit = _dbvalue.IsCredit;
            vendor.CreditDays = (int)_dbvalue.CreditDays;
            vendor.CreditAmount = (decimal)_dbvalue.CreditAmount;
            vendor.ContactPersonName = _dbvalue.ContactPerson;
            vendor.MobileNo = _dbvalue.MobileNo;
            vendor.Email = _dbvalue.Email;
            vendor.Address = _dbvalue.Address;
            vendor.LocationId = _dbvalue.LocationId;
            vendor.AccountPayable = _dbvalue.AccountPayable;
            vendor.AccountPayableDescription = _dbvalue.DB_CHART_OF_ACCOUNT.Name;
            vendor.DownPaymentAccount = _dbvalue.DownPaymentAccount;
            vendor.DownPaymentAccountDescription = _dbvalue.DB_CHART_OF_ACCOUNT1.Name;
            return vendor;
        }
    }
}