﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class WHTMapper
    {
        public static DB_WITH_HOLDING_TAX DBMapper(this DB_WITH_HOLDING_TAX db_WHT, WHTViewModel value)
        {
            if (db_WHT == null)
            {
                db_WHT = new DB_WITH_HOLDING_TAX();
                db_WHT.ValidFlag = true;
                db_WHT.CreatedOn = value.ChangedOn;
                db_WHT.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_WHT.UpdatedBy = value.ChangedBy;
                db_WHT.UpdatedOn = value.ChangedOn;
            }
            db_WHT.Value = value.Value;
            db_WHT.Name = value.Name;
            db_WHT.AccountWithHoldingTax = value.AccountWHT;
            
            return db_WHT;
        }


        public static WHTViewModel DBValue(DB_WITH_HOLDING_TAX dbValue)
        {
            WHTViewModel value = new WHTViewModel();
            value.Id = dbValue.Id;
            value.Value = dbValue.Value;
            value.Name = dbValue.Name;
            value.AccountWHT = dbValue.AccountWithHoldingTax;
            value.AccountWHTDescription = dbValue.DB_CHART_OF_ACCOUNT.Name;
            return value;
        }
    }
}
