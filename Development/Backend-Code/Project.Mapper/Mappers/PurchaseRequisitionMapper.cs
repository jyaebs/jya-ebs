﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Mapper.Mappers
{
   public static class PurchaseRequisitionMapper
   {
        public static DB_PURCHASE_REQUISITION DBMapper(PurchaseRequisitionViewModel value)
        {

            DB_PURCHASE_REQUISITION db_PURCHASE_REQUISITION = new DB_PURCHASE_REQUISITION();
            
            db_PURCHASE_REQUISITION.LocationId = value.LocationId;
            db_PURCHASE_REQUISITION.PurchaseTypeId = value.PurchaseTypeId;
            db_PURCHASE_REQUISITION.Remarks = value.Remarks;
            db_PURCHASE_REQUISITION.PriorityLevel = value.PriorityLevelId;
            db_PURCHASE_REQUISITION.StatusId = (int)EnumPurchaseRequisition.Open;

            db_PURCHASE_REQUISITION.ValidFlag = true;
            db_PURCHASE_REQUISITION.CreatedOn = value.ChangedOn;
            db_PURCHASE_REQUISITION.CreatedBy = value.ChangedBy;

            foreach (var item in value.PurchaseRequisitionDetail)
            {
                DB_PURCHASE_REQUISITION_DETAIL Details = new DB_PURCHASE_REQUISITION_DETAIL();

                Details.TotalQuantity = item.TotalQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.ProductId = item.ProductId;
                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_PURCHASE_REQUISITION.DB_PURCHASE_REQUISITION_DETAIL.Add(Details);
            }
            
            return db_PURCHASE_REQUISITION;
        }

        public static PurchaseRequisitionViewModel DBValue(DB_PURCHASE_REQUISITION dbValue)
        {
            PurchaseRequisitionViewModel model = new PurchaseRequisitionViewModel();

            model.Id = dbValue.Id;
            model.ReferenceNumber = dbValue.ReferenceNumber;
            model.LocationId = dbValue.LocationId;
            model.LocationName = dbValue.DB_LOCATION.Name;
            model.PurchaseTypeId = dbValue.PurchaseTypeId;
            model.PurchaseTypeName = dbValue.DB_DEV_PURCHASE_TYPE.Name;
            model.PriorityLevelId = dbValue.PriorityLevel;
            model.PriorityLevel = dbValue.PriorityLevel == "H" ? "High" : dbValue.PriorityLevel == "M" ? "Medium" : "Low";
            model.statusId = dbValue.StatusId;
            model.Remarks = dbValue.Remarks;
            
            model.PurchaseRequisitionDetail = new List<PurchaseRequisitionDetailViewModel>();

            foreach (var item in dbValue.DB_PURCHASE_REQUISITION_DETAIL)
            {
                PurchaseRequisitionDetailViewModel Req = new PurchaseRequisitionDetailViewModel();

                Req.Id = item.Id;
                Req.RequisitionId = item.RequisitionId;
                Req.ProductId = item.ProductId;
                Req.ProductName = item.DB_PRODUCT.Name;
                Req.PrimaryQuantity = item.PrimaryQuantity == null ? 0 : (decimal)item.PrimaryQuantity;
                Req.SecondaryQuantity = item.SecondaryQuantity == null ? 0 : (decimal)item.SecondaryQuantity;
                Req.TotalQuantity = item.TotalQuantity;
                model.PurchaseRequisitionDetail.Add(Req);
            }

            return model;
        }

    }
   
}
