﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class SecondaryUOMMapper
    {
        public static DB_SECONDARY_UOM DBMapper(this DB_SECONDARY_UOM db_SECONDARY_UOM, SecondaryUOMViewModel value)
        {
            if (db_SECONDARY_UOM == null)
            {
                db_SECONDARY_UOM = new DB_SECONDARY_UOM();

                db_SECONDARY_UOM.ValidFlag = true;
                db_SECONDARY_UOM.CreatedOn = value.ChangedOn;
                db_SECONDARY_UOM.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_SECONDARY_UOM.UpdatedBy = value.ChangedBy;
                db_SECONDARY_UOM.UpdatedOn = value.ChangedOn;
            }
            db_SECONDARY_UOM.PrimaryUOMId = value.PrimaryUOMId;
            db_SECONDARY_UOM.Code = value.Code;
            db_SECONDARY_UOM.Name = value.Name;

            return db_SECONDARY_UOM;
        }
        public static SecondaryUOMViewModel DBValue(DB_SECONDARY_UOM dbValue)
        {
            SecondaryUOMViewModel value = new SecondaryUOMViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.PrimaryUOMId = dbValue.PrimaryUOMId;
            return value;
        }

    }
}
