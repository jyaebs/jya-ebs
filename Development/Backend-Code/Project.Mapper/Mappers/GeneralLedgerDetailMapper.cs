﻿using Project.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public class GeneralLedgerDetailMapper
    {
        public static DB_GENERAL_LEDGER_DETAIL DBMapper(string AccountCode, string Description, decimal Debit, decimal Credit)
        {
            DB_GENERAL_LEDGER_DETAIL _DB_GENERAL_LEDGER_DETAIL = new DB_GENERAL_LEDGER_DETAIL();
            
            _DB_GENERAL_LEDGER_DETAIL.AccountCode = AccountCode;
            _DB_GENERAL_LEDGER_DETAIL.Description = Description;
            _DB_GENERAL_LEDGER_DETAIL.Debit = Debit;
            _DB_GENERAL_LEDGER_DETAIL.Credit = Credit;
 
            return _DB_GENERAL_LEDGER_DETAIL;
        }

    }
}
