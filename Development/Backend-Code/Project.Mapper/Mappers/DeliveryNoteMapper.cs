﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class DeliveryNoteMapper
    {
        public static DB_DELIVERY_NOTE DBMapper(this DB_DELIVERY_NOTE db_DELIVERY_NOTE, DeliveryNoteViewModel value)
        {
            if (db_DELIVERY_NOTE == null)
            {
                db_DELIVERY_NOTE = new DB_DELIVERY_NOTE();
                db_DELIVERY_NOTE.ValidFlag = true;
                db_DELIVERY_NOTE.CreatedOn = value.ChangedOn;
                db_DELIVERY_NOTE.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_DELIVERY_NOTE.UpdatedBy = value.ChangedBy;
                db_DELIVERY_NOTE.UpdatedOn = value.ChangedOn;
            }

            db_DELIVERY_NOTE.LocationId = value.LocationId;
            db_DELIVERY_NOTE.CustomerId = value.CustomerId;
            if (value.SaleOrderId != 0)
                db_DELIVERY_NOTE.SaleOrderId = value.SaleOrderId;
            else
                db_DELIVERY_NOTE.SaleOrderId = null;
            db_DELIVERY_NOTE.DeliveryDate = value.DeliveryDate;


            db_DELIVERY_NOTE.StatusId = (int)EnumDeliveryNote.Open;


            foreach (var item in value.DeliveryNoteDetail)
            {
                DB_DELIVERY_NOTE_DETAIL Details = new DB_DELIVERY_NOTE_DETAIL();
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.ProductId = item.ProductId;
                Details.NetAmount = item.NetAmount;
                Details.SalePrice = item.UnitPrice;
                Details.TotalQuantity = item.TotalQuantity;
                Details.TotalQuantity = item.DeliverQuantity;
                Details.WarehouseId = item.WarehouseId;
                Details.GrossAmount = item.GrossAmount;
                Details.DiscountAmount = item.DiscountAmount;
                Details.DiscountPercentage = item.Discount;
                
                db_DELIVERY_NOTE.GrossAmount += item.GrossAmount;
                db_DELIVERY_NOTE.DiscountAmount += item.DiscountAmount;
                db_DELIVERY_NOTE.NetAmount += item.NetAmount;

                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_DELIVERY_NOTE.DB_DELIVERY_NOTE_DETAIL.Add(Details);
            }
           
                db_DELIVERY_NOTE.ReferenceNumber = "REF-0001-01";

            return db_DELIVERY_NOTE;
        }

        public static DeliveryNoteViewModel ValueMapper(DB_DELIVERY_NOTE dbValue)
        {
            DeliveryNoteViewModel value = new DeliveryNoteViewModel();

            value.DeliveryNoteDetail = new List<DeliveryNoteDetailViewModel>();

            value.LocationId = dbValue.LocationId;
            value.CustomerId = dbValue.CustomerId;
            value.CustomerName = dbValue.DB_CUSTOMER.Name;
           
            value.DeliveryDate = dbValue.DeliveryDate;
            value.TotalGrossAmount = (int)dbValue.GrossAmount;
            value.TotalDiscountAmount = (int)dbValue.DiscountAmount;
            value.TotalNetAmount = (int)dbValue.NetAmount;
            value.ReferenceNumber = dbValue.ReferenceNumber;

            


            foreach (var v in dbValue.DB_DELIVERY_NOTE_DETAIL)
            {

                DeliveryNoteDetailViewModel DeliveryNotevalue = new DeliveryNoteDetailViewModel();

                DeliveryNotevalue.PrimaryQuantity = (int)v.PrimaryQuantity;
                DeliveryNotevalue.SecondaryQuantity = (int)v.SecondaryQuantity;
                DeliveryNotevalue.ProductName = v.DB_PRODUCT.Name;
                DeliveryNotevalue.TotalQuantity = (int)v.TotalQuantity;
                DeliveryNotevalue.DeliverQuantity = (int)v.TotalQuantity;
                DeliveryNotevalue.WarehouseId = v.WarehouseId;
                DeliveryNotevalue.WarehouseName = v.DB_WAREHOUSE==null?"N/A":v.DB_WAREHOUSE.Name;
                DeliveryNotevalue.UnitPrice = (int)v.SalePrice;
                DeliveryNotevalue.Discount = (int)v.DiscountPercentage;
                DeliveryNotevalue.DiscountAmount = (int)v.DiscountAmount;
                DeliveryNotevalue.GrossAmount = (int)v.GrossAmount;
                DeliveryNotevalue.NetAmount = (int)v.NetAmount;
                DeliveryNotevalue.Id = v.Id;
                DeliveryNotevalue.ProductId = v.ProductId;
                value.DeliveryNoteDetail.Add(DeliveryNotevalue);

            }

            return value;
        }

    }
}
