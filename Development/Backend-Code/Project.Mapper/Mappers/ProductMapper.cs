﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Project.Mapper.Mappers
{
    public static class ProductMapper
    {
        public static DB_PRODUCT DBMapper(this DB_PRODUCT db_DB_PRODUCT, ProductViewModel value)
        {
            if (db_DB_PRODUCT == null)
            {
                db_DB_PRODUCT = new DB_PRODUCT();

                db_DB_PRODUCT.ValidFlag = true;
                db_DB_PRODUCT.CreatedOn = value.ChangedOn;
                db_DB_PRODUCT.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_DB_PRODUCT.UpdatedBy = value.ChangedBy;
                db_DB_PRODUCT.UpdatedOn = value.ChangedOn;
            }

            db_DB_PRODUCT.Code = value.Code;
            db_DB_PRODUCT.Name = value.Name;
            db_DB_PRODUCT.BarCode = value.Barcode;
            db_DB_PRODUCT.ProductTypeId = value.ProductTypeId;
            db_DB_PRODUCT.ProductCategoryId = value.Category;
            db_DB_PRODUCT.ProductSubCategoryId = value.SubCategory;
            db_DB_PRODUCT.PrimaryUomId = value.PrimaryUOM;
            if (value.SecondaryUOM == 0)
            {
                db_DB_PRODUCT.SecondaryUomId = null;
            }
            else
            {
                db_DB_PRODUCT.SecondaryUomId = value.SecondaryUOM;
            }
            db_DB_PRODUCT.ProductBrandId = value.ProductBrandId;
            db_DB_PRODUCT.ConverstionFactor = value.ConversionFactor;
            db_DB_PRODUCT.IsTrendyItem = value.Trendy;
            db_DB_PRODUCT.DepreciationMethod = value.DepreciationMethod;
            db_DB_PRODUCT.DepreciationRate = value.DepreciationRate;
            db_DB_PRODUCT.DepreciationType = value.DepreciationType;
            db_DB_PRODUCT.UsageLife = value.UsageLife;
            db_DB_PRODUCT.ScrapeValue = value.ScrapeValue;
            if (value.DepreciationAccount == "")
            {
                db_DB_PRODUCT.DepreciationAccount = null;
            }
            else
            {
                db_DB_PRODUCT.DepreciationAccount = value.DepreciationAccount;
            }
            db_DB_PRODUCT.COGSAccount = value.COGSAccount;
            db_DB_PRODUCT.InventoryAccount = value.InventoryAccount;

            HttpFileCollection fileCollection = HttpContext.Current.Request.Files;
            if (fileCollection.Count > 0)
            {
                HttpPostedFile file = fileCollection[0];

                if (file != null)
                {
                    Guid guid = Guid.NewGuid();

                    string extension = Path.GetExtension(file.FileName);
                    string imageName = guid + extension;
                    string ImagePath = Path.Combine(HttpContext.Current.Server.MapPath("/ImagesStorageDB"), imageName);
                    file.SaveAs(ImagePath);
                    db_DB_PRODUCT.Image = imageName;
                }
            }

            return db_DB_PRODUCT;
        }
        public static ProductViewModel DBValue(DB_PRODUCT dbValue)
        {
            ProductViewModel value = new ProductViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.Barcode = dbValue.BarCode;
            value.UsageLife = dbValue.UsageLife == null ? 0 : (int)dbValue.UsageLife;
            value.PrimaryUOM = dbValue.PrimaryUomId;
            value.SecondaryUOM = dbValue.SecondaryUomId == null ? 0 : (int)dbValue.SecondaryUomId;
            value.ProductBrandId = dbValue.ProductBrandId;
            value.ProductTypeId = dbValue.ProductTypeId;
            value.ProductTypeName = dbValue.DB_DEV_PRODUCT_TYPE.Name;
            value.Category = dbValue.ProductCategoryId;
            value.SubCategory = dbValue.ProductSubCategoryId;
            value.ConversionFactor = dbValue.ConverstionFactor == null ? 0 : (decimal)dbValue.ConverstionFactor;
            value.Trendy = dbValue.IsTrendyItem;
            value.DepreciationMethod = dbValue.DepreciationMethod;
            value.DepreciationRate = dbValue.DepreciationRate == null ? 0 : (int)dbValue.DepreciationRate;
            value.DepreciationType = dbValue.DepreciationType;
            value.DepreciationAccount = dbValue.DepreciationAccount;
            value.DepreciationAccountDescription = dbValue.DepreciationAccount == null ? "" : dbValue.DB_CHART_OF_ACCOUNT2.Name;
            value.InventoryAccount = dbValue.InventoryAccount;
            value.InventoryAccountDescription = dbValue.DB_CHART_OF_ACCOUNT.Name;
            value.COGSAccount = dbValue.COGSAccount;
            value.COGSAccountDescription = dbValue.DB_CHART_OF_ACCOUNT1.Name;
            value.ScrapeValue = dbValue.ScrapeValue == null ? 0 : (decimal)dbValue.ScrapeValue;
            value.UsageLife = dbValue.UsageLife == null ? 0 : (int)dbValue.UsageLife;
            value.Image = dbValue.Image;
            value.PrimaryUOMName = dbValue.DB_PRIMARY_UOM.Name;
            value.SecondaryUOMName = dbValue.SecondaryUomId == null ? "" : dbValue.DB_SECONDARY_UOM.Name;


            return value;
        }
    }
}
