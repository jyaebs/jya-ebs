﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class PrimaryUOMMapper
    {
        public static DB_PRIMARY_UOM DBMapper(this DB_PRIMARY_UOM db_PRIMARY_UOM, PrimaryUOMViewModel value)
        {
            if (db_PRIMARY_UOM == null)
            {
                db_PRIMARY_UOM = new DB_PRIMARY_UOM();
                db_PRIMARY_UOM.ValidFlag = true;
                db_PRIMARY_UOM.CreatedOn = value.ChangedOn;
                db_PRIMARY_UOM.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_PRIMARY_UOM.UpdatedBy = value.ChangedBy;
                db_PRIMARY_UOM.UpdatedOn = value.ChangedOn;
            }

            db_PRIMARY_UOM.Code = value.Code;
            db_PRIMARY_UOM.Name = value.Name;

            return db_PRIMARY_UOM;
        }
        public static PrimaryUOMViewModel DBValue(DB_PRIMARY_UOM dbValue)
        {
            PrimaryUOMViewModel value = new PrimaryUOMViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value;

        }
    }
}