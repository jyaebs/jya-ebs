﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class ProductPriceMapper
    {
        public static DB_PRODUCT_PRICE DBMapper(this DB_PRODUCT_PRICE db_DB_PRODUCT_PRICE, ProductPriceViewModel value)
        {
            if (db_DB_PRODUCT_PRICE == null)
            {
                db_DB_PRODUCT_PRICE = new DB_PRODUCT_PRICE();

                db_DB_PRODUCT_PRICE.ValidFlag = true;
                db_DB_PRODUCT_PRICE.CreatedOn = value.ChangedOn;
                db_DB_PRODUCT_PRICE.CreatedBy = value.ChangedBy;
            }

            else
            {
                db_DB_PRODUCT_PRICE.UpdatedBy = value.ChangedBy;
                db_DB_PRODUCT_PRICE.UpdatedOn = value.ChangedOn;
            }

            db_DB_PRODUCT_PRICE.ProductId = value.ProductId;
            db_DB_PRODUCT_PRICE.LocationId = value.LocationId;
            db_DB_PRODUCT_PRICE.ProductPriceTypeId = value.ProductPriceTypeId;
            db_DB_PRODUCT_PRICE.Price = value.Price;


            return db_DB_PRODUCT_PRICE;

        }

        public static ProductPriceViewModel DBValue(this DB_PRODUCT_PRICE dbValue)
        {
            ProductPriceViewModel value = new ProductPriceViewModel();
            value.Id = dbValue.Id;
            value.ProductId = dbValue.ProductId;
            value.Price = dbValue.Price;
            value.LocationId = dbValue.LocationId == null ? 0 : (int)dbValue.LocationId;
            value.ProductPriceTypeId = dbValue.ProductPriceTypeId;

            return value;
        }
    }
}

