﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class BankBranchMapper
    {
        public static DB_BANK_BRANCH DBMapper(this DB_BANK_BRANCH db_BANK_BRANCH, BankBranchViewModel value)
        {
            if (db_BANK_BRANCH == null)
            {
                db_BANK_BRANCH = new DB_BANK_BRANCH();
                db_BANK_BRANCH.ValidFlag = true;
                db_BANK_BRANCH.CreatedOn = value.ChangedOn;
                db_BANK_BRANCH.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_BANK_BRANCH.UpdatedBy = value.ChangedBy;
                db_BANK_BRANCH.UpdatedOn = value.ChangedOn;
            }
            db_BANK_BRANCH.BankId = value.BankId;
            db_BANK_BRANCH.AccountNumber = value.Code;
            db_BANK_BRANCH.AccountName = value.Name;
            db_BANK_BRANCH.AccountBankBalance = value.AccountBankBalance;

            return db_BANK_BRANCH;
        }


        public static BankBranchViewModel DBValue(DB_BANK_BRANCH dbValue)
        {
            BankBranchViewModel value = new BankBranchViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.AccountNumber;
            value.Name = dbValue.AccountName;
            value.BankId = dbValue.BankId;
            value.AccountBankBalance = dbValue.AccountBankBalance;
            value.AccountBankBalanceDescription = dbValue.DB_CHART_OF_ACCOUNT.Name;
            return value;
        }
    }
}
