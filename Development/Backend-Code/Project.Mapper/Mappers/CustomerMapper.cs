﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Project.Mapper.Mappers
{
    public static class CustomerMapper
    {
        public static DB_CUSTOMER DBMapper(this DB_CUSTOMER db_user, CustomerViewModel value)
        {
            if (db_user == null)
            {
                db_user = new DB_CUSTOMER();

                db_user.ValidFlag = true;
                db_user.CreatedOn = value.ChangedOn;
                db_user.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_user.UpdatedOn = value.ChangedOn;
                db_user.UpdatedBy = value.ChangedBy;
            }

            db_user.Code = value.Code;
            db_user.Name = value.Name;
            db_user.CustomerTypeId = value.CustomerTypeId;
            db_user.CNIC = value.CNIC;
            db_user.NTN = value.NTN;
            db_user.IsCredit = value.IsCredit;
            db_user.CreditDays = value.CreditDays;
            db_user.CreditAmount = value.CreditAmount;
            db_user.ContactPersonName = value.ContactPersonName;
            db_user.MobileNo = value.MobileNo;
            db_user.Email = value.Email;
            db_user.Address = value.Address;
            db_user.LocationId = value.LocationId;
            db_user.AccountReceivable = value.AccountReceivable;
            db_user.DownPaymentAccount = value.DownPaymentAccount;

            return db_user;
        }



        public static CustomerViewModel DBValue(DB_CUSTOMER _dbvalue)
        {
            CustomerViewModel customer = new CustomerViewModel();
            customer.Id = _dbvalue.Id;
            customer.Code = _dbvalue.Code;
            customer.Name = _dbvalue.Name;
            customer.CNIC = _dbvalue.CNIC;
            customer.NTN = _dbvalue.NTN;
            customer.CustomerTypeId = _dbvalue.CustomerTypeId;
            customer.IsCredit = _dbvalue.IsCredit;
            customer.CreditDays = (int)_dbvalue.CreditDays;
            customer.CreditAmount = (decimal)_dbvalue.CreditAmount;
            customer.ContactPersonName = _dbvalue.ContactPersonName;
            customer.MobileNo = _dbvalue.MobileNo;
            customer.Email = _dbvalue.Email;
            customer.Address = _dbvalue.Address;
            customer.LocationId = _dbvalue.LocationId;
            customer.AccountReceivable = _dbvalue.AccountReceivable;
            customer.AccountReceivableDescription = _dbvalue.DB_CHART_OF_ACCOUNT.Name;
            customer.DownPaymentAccount = _dbvalue.DownPaymentAccount;
            customer.DownPaymentAccountDescription = _dbvalue.DB_CHART_OF_ACCOUNT1.Name;
            return customer;
        }
    }
}