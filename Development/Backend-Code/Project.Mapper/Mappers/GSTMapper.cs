﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class GSTMapper
    {
        public static DB_GST DBMapper(this DB_GST db_GST, GSTViewModel value)
        {
            if (db_GST == null)
            {
                db_GST = new DB_GST();
                db_GST.ValidFlag = true;

                db_GST.CreatedBy = value.ChangedBy;
                db_GST.CreatedOn = value.ChangedOn;
            }
            else
            {
                db_GST.UpdatedBy = value.ChangedBy;
                db_GST.UpdatedOn = value.ChangedOn;
            }

            db_GST.Value = value.Value;
            db_GST.Name = value.Name;
            db_GST.AccountGST = value.AccountGST;
           
            return db_GST;
            
        }
        public static GSTViewModel GValue(DB_GST GValue)
        {
            GSTViewModel value = new GSTViewModel();
            value.Id = GValue.Id;
            value.Value = GValue.Value;
            value.Name = GValue.Name;
            value.AccountGST = GValue.AccountGST;
            value.AccountGSTDescription = GValue.DB_CHART_OF_ACCOUNT.Name;
            return value;
        }
   }
}

