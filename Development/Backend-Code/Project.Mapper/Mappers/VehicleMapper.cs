﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class VehicleMapper
    {
        public static DB_VEHICLE DBMapper(this DB_VEHICLE db_DB_VEHICLE, VehicleViewModel value)
        {
            if (db_DB_VEHICLE == null)
            {
                db_DB_VEHICLE = new DB_VEHICLE();

                db_DB_VEHICLE.ValidFlag = true;
                db_DB_VEHICLE.CreatedOn = value.ChangedOn;
                db_DB_VEHICLE.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_DB_VEHICLE.UpdatedBy = value.ChangedBy;
                db_DB_VEHICLE.UpdatedOn = value.ChangedOn;
            }
            db_DB_VEHICLE.LocationId = value.LocationId;
            db_DB_VEHICLE.VehicleTypeId = value.VehicleTypeId;
            db_DB_VEHICLE.Code = value.Code;
            db_DB_VEHICLE.Name = value.Name;

            return db_DB_VEHICLE;
        }
        public static VehicleViewModel DBValue(DB_VEHICLE dbValue)
        {
            VehicleViewModel value = new VehicleViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.LocationId = dbValue.LocationId;
            value.VehicleTypeId = dbValue.VehicleTypeId;
            return value;
        }
    }
}
