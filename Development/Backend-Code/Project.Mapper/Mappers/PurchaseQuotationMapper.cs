﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class PurchaseQuotationMapper
    {
        public static DB_PURCHASE_QUOTATION DBMapper(PurchaseQuotationViewModel value)
        {

            DB_PURCHASE_QUOTATION db_purchase_quotation = new DB_PURCHASE_QUOTATION();
            
            db_purchase_quotation.LocationId = value.LocationId;
            db_purchase_quotation.PurchaseRequisitionId = value.PurchaseRequisitionId;
            db_purchase_quotation.PurchaseTypeId = value.PurchaseTypeId;
            db_purchase_quotation.PriorityLevel = value.PriorityLevel;
            db_purchase_quotation.Remarks = value.Remarks;
            db_purchase_quotation.StatusId = (int)EnumPurchaseQuotation.Open;

            db_purchase_quotation.ValidFlag = true;
            db_purchase_quotation.CreatedOn = value.ChangedOn;
            db_purchase_quotation.CreatedBy = value.ChangedBy;

            foreach (var item in value.PurchaseQuotationDetail)
            {
                DB_PURCHASE_QUOTATION_DETAIL Details = new DB_PURCHASE_QUOTATION_DETAIL();

                Details.PurchaseRequisitionDetailId = item.PurchaseRequisitionDetailId;
                Details.VendorId = item.VendorId;
                Details.ProductId = item.ProductId;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.TotalQuantity = item.TotalQuantity;
                Details.Price = item.Price;
                Details.NetAmount = item.NetAmount;
                if (item.isChecked)
                {
                    Details.ValidFlag = true;
                    db_purchase_quotation.VendorId = item.VendorId;
                }
                else
                {
                    Details.ValidFlag = false;
                    db_purchase_quotation.VendorId = item.VendorId;
                }

                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_purchase_quotation.DB_PURCHASE_QUOTATION_DETAIL.Add(Details);
            }


            return db_purchase_quotation;
        }
        
        public static PurchaseQuotationViewModel ValueMapper(DB_PURCHASE_QUOTATION _dbvalue)
        {
            PurchaseQuotationViewModel purchase_quotation = new PurchaseQuotationViewModel();

            purchase_quotation.Id = _dbvalue.Id;
            purchase_quotation.PurchaseRequisitionId = _dbvalue.PurchaseRequisitionId;
            purchase_quotation.ReferenceNumber = _dbvalue.ReferenceNumber;
            purchase_quotation.PurchaseTypeName = _dbvalue.DB_DEV_PURCHASE_TYPE.Name;
            purchase_quotation.PriorityLevelName = _dbvalue.PriorityLevel == "H" ? "High" : _dbvalue.PriorityLevel == "M" ? "Medium" : "Low";
            purchase_quotation.LocationId = _dbvalue.LocationId;
            purchase_quotation.LocationName = _dbvalue.DB_LOCATION.Name;
            purchase_quotation.VendorId = _dbvalue.VendorId;
            purchase_quotation.VendorName = _dbvalue.DB_VENDOR.Name;
            purchase_quotation.Remarks = _dbvalue.Remarks;


            purchase_quotation.PurchaseQuotationDetail = new List<PurchaseQuotationDetailViewModel>();

            foreach (var detail in _dbvalue.DB_PURCHASE_QUOTATION_DETAIL)
            {
                PurchaseQuotationDetailViewModel PurchaseQuotationDetail = new PurchaseQuotationDetailViewModel();
                
                PurchaseQuotationDetail.PurchaseQuotationId = detail.PurchaseQuotationId;
                PurchaseQuotationDetail.PurchaseRequisitionDetailId = detail.PurchaseRequisitionDetailId;
                PurchaseQuotationDetail.VendorId = detail.VendorId;
                PurchaseQuotationDetail.Vendor = detail.DB_VENDOR.Name;
                PurchaseQuotationDetail.ProductId = detail.ProductId;
                PurchaseQuotationDetail.Product = detail.DB_PRODUCT.Name;
                PurchaseQuotationDetail.PrimaryQuantity = (decimal)detail.PrimaryQuantity;
                PurchaseQuotationDetail.SecondaryQuantity = (decimal)detail.SecondaryQuantity;
                PurchaseQuotationDetail.TotalQuantity = detail.TotalQuantity;
                PurchaseQuotationDetail.Price = detail.Price;
                PurchaseQuotationDetail.NetAmount = detail.NetAmount;
                PurchaseQuotationDetail.isChecked = detail.ValidFlag;

                purchase_quotation.PurchaseQuotationDetail.Add(PurchaseQuotationDetail);


            }

            return purchase_quotation;
        }
    }
}
