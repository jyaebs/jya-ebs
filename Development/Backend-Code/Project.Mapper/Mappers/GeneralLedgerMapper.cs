﻿using Project.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public class GeneralLedgerMapper
    {
        public static DB_GENERAL_LEDGER DBMapper(int LocationId, int WarehouseId, int FiscalYearId, int TypeId, int DocumentNo, Decimal TotalDebit, Decimal TotalCredit, string Remarks, int? ReverseId, int VendorId, int CustomerId, DateTime PostingDate, DateTime CreatedOn, string CreatedBy)
        {
            DB_GENERAL_LEDGER db_Ledger = new DB_GENERAL_LEDGER();
            db_Ledger.LocationId = LocationId;
            db_Ledger.FiscalYearId = FiscalYearId;
            db_Ledger.PostingDate = PostingDate;
            db_Ledger.TypeId = TypeId;
            db_Ledger.DocumentNo = DocumentNo;
            db_Ledger.TotalCredit = TotalCredit;
            db_Ledger.TotalDebit = TotalDebit;
            db_Ledger.Remarks = Remarks;
            db_Ledger.ReverseId = ReverseId;
            db_Ledger.VendorId = VendorId;
            db_Ledger.CustomerId = CustomerId;
            db_Ledger.CreatedOn = CreatedOn;
            db_Ledger.CreatedBy = CreatedBy;
            
            return db_Ledger;
        }
    }
}
