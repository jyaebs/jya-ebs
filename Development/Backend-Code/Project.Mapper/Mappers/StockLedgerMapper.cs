﻿using Project.DatabaseModels;
using System;

namespace Project.Mapper.Mappers
{
   public static class StockLedgerMapper
   {
        public static DB_STOCK_LEDGER DBMapper(int StockId,int LocationId,int WarehouseId,int ProductId, int ActivityId, int Quantity,int DeliveryNoteId,DateTime CreatedOn , string CreatedBy )
        {
            DB_STOCK_LEDGER db_stock = new DB_STOCK_LEDGER();
            db_stock.LocationId = LocationId;
            db_stock.ProductId = ProductId;
            db_stock.StockId = StockId;
            db_stock.WarehouseId = WarehouseId;
            db_stock.ActivityId = ActivityId;
            db_stock.Quantity = Quantity;
            db_stock.DeliveryNoteId = DeliveryNoteId;
            db_stock.CreatedOn = CreatedOn;
            db_stock.CreatedBy = CreatedBy;
            return db_stock;
        }
    }
}
