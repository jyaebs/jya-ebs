﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class ProductBrandMapper

    {
        public static DB_PRODUCT_BRAND DBMapper(this DB_PRODUCT_BRAND db_PRODUCT_BRAND, ProductBrandViewModel value)
        {
            if (db_PRODUCT_BRAND == null)
            {
                db_PRODUCT_BRAND = new DB_PRODUCT_BRAND();
                db_PRODUCT_BRAND.ValidFlag = true;
                db_PRODUCT_BRAND.CreatedOn = value.ChangedOn;
                db_PRODUCT_BRAND.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_PRODUCT_BRAND.UpdatedBy = value.ChangedBy;
                db_PRODUCT_BRAND.UpdatedOn = value.ChangedOn;
            }

            db_PRODUCT_BRAND.Code = value.Code;
            db_PRODUCT_BRAND.Name = value.Name;

            return db_PRODUCT_BRAND;
        }
        public static ProductBrandViewModel DBValue(DB_PRODUCT_BRAND dbValue)
        {
            ProductBrandViewModel value = new ProductBrandViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value;
        }
    }
}  

