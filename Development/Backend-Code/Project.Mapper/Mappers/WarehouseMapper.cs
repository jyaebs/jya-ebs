﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class WarehouseMapper
    {
        public static DB_WAREHOUSE DBMapper(this DB_WAREHOUSE db_DB_WAREHOUSE, WarehouseViewModel value)
        {
            if (db_DB_WAREHOUSE == null)
            {
                db_DB_WAREHOUSE = new DB_WAREHOUSE();

                db_DB_WAREHOUSE.ValidFlag = true;
                db_DB_WAREHOUSE.CreatedOn = value.ChangedOn;
                db_DB_WAREHOUSE.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_DB_WAREHOUSE.UpdatedBy = value.ChangedBy;
                db_DB_WAREHOUSE.UpdatedOn = value.ChangedOn;
            }
            db_DB_WAREHOUSE.LocationId = value.LocationId;
            db_DB_WAREHOUSE.Code = value.Code;
            db_DB_WAREHOUSE.Name = value.Name;

            return db_DB_WAREHOUSE;
        }
        public static WarehouseViewModel DBValue(DB_WAREHOUSE dbValue)
        {
            WarehouseViewModel value = new WarehouseViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.LocationId = dbValue.LocationId;
            return value;
        }
    }
}
