﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class InspectionNoteMapper
    {
        public static DB_INWARD_GATE_PASS DBMapper(this DB_INWARD_GATE_PASS db_INS, InspectionNoteViewModel value)
        {
            if (db_INS == null)
            {
                db_INS = new DB_INWARD_GATE_PASS();
                db_INS.ValidFlag = true;
                db_INS.CreatedOn = value.ChangedOn;
                db_INS.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_INS.UpdatedBy = value.ChangedBy;
                db_INS.UpdatedOn = value.ChangedOn;
            }

            db_INS.ReferenceNumber = "INP-REF0001-01";
            //  db_INS.PurchaseOrderId = value.InwardGatePassId;
            db_INS.LocationId = value.LocationId;
            db_INS.VendorId = value.VendorId;
            db_INS.VoucherNo = value.VoucherNo;
            //  db_INS.InwardDate = value.InspectionDate;
            db_INS.NetAmount = value.NetAmount;
            //  db_INS.StatusId = (int)EnumInwardGatePass.Open;
            db_INS.Remarks = value.Remarks;

            foreach (var item in value.InspectionNoteDetails)
            {
                DB_INWARD_GATE_PASS_DETAIL Details = new DB_INWARD_GATE_PASS_DETAIL();


                Details.ProductId = item.ProductId;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.TotalQuantity = item.TotalQuantity;
                // Details.ReceivedQuantity = item.ApprovedQuantity;
              //  Details.ReceivedQuantity = item.RejectQuantity;
                Details.UnitPrice = item.UnitPrice;
                Details.NetAmount = item.NetAmount;
                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_INS.DB_INWARD_GATE_PASS_DETAIL.Add(Details);
            }

            return db_INS;
        }


        public static InspectionNoteViewModel ValueMapper(DB_INWARD_GATE_PASS _dbvalue)
        {
            InspectionNoteViewModel inspection_note = new InspectionNoteViewModel();

            inspection_note.Id = _dbvalue.Id;
           // inspection_note.InwardGatePassId = _dbvalue.PurchaseOrderId;
            inspection_note.ReferenceNumber = _dbvalue.ReferenceNumber;
            inspection_note.Name = _dbvalue.DB_LOCATION.Name;
            inspection_note.VendorName = _dbvalue.DB_VENDOR.Name;
            inspection_note.VoucherNo = _dbvalue.VoucherNo;
           // inspection_note.OrderDate = _dbvalue.DB_PURCHASE_ORDER.OrderDate;
           // inspection_note.InspectionDate = _dbvalue.InwardDate;
            inspection_note.NetAmount = _dbvalue.NetAmount;
            inspection_note.Remarks = _dbvalue.Remarks;


            inspection_note.InspectionNoteDetails = new List<InspectionNoteDetailViewModel>();

            foreach (var detail in _dbvalue.DB_INWARD_GATE_PASS_DETAIL)
            {
                InspectionNoteDetailViewModel InspectionDetail = new InspectionNoteDetailViewModel();

                InspectionDetail.ProductId = detail.ProductId;
                InspectionDetail.ProductName = detail.DB_PRODUCT.Name;
                InspectionDetail.PrimaryQuantity =(decimal)detail.PrimaryQuantity;
                InspectionDetail.SecondaryQuantity = (decimal)detail.SecondaryQuantity;
                InspectionDetail.TotalQuantity = detail.TotalQuantity;
               // InspectionDetail.ApprovedQuantity = detail.ReceivedQuantity;
               // InspectionDetail.RejectQuantity = detail.ReceivedQuantity
                InspectionDetail.UnitPrice = detail.UnitPrice;
                InspectionDetail.NetAmount = detail.NetAmount;

                inspection_note.InspectionNoteDetails.Add(InspectionDetail);
            }

            return inspection_note;
        }
    }
}
