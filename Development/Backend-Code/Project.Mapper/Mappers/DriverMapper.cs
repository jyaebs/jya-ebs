﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class DriverMapper
    {
        public static DB_DRIVER DBMapper(this DB_DRIVER db_DB_DRIVER, DriverViewModel value)
        {
            if (db_DB_DRIVER == null)
            {
                db_DB_DRIVER = new DB_DRIVER();

                db_DB_DRIVER.ValidFlag = true;
                db_DB_DRIVER.CreatedOn = value.ChangedOn;
                db_DB_DRIVER.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_DB_DRIVER.UpdatedBy = value.ChangedBy;
                db_DB_DRIVER.UpdatedOn = value.ChangedOn;
            }
            db_DB_DRIVER.LocationId = value.LocationId;
            db_DB_DRIVER.Code = value.Code;
            db_DB_DRIVER.Name = value.Name;

            return db_DB_DRIVER;
        }
        public static DriverViewModel DBValue(DB_DRIVER dbValue)
        {
            DriverViewModel value = new DriverViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            value.LocationId = dbValue.LocationId;
            return value;
        }
    }
}
