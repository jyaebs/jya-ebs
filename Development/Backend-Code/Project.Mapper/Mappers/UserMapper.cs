﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Project.Mapper.Mappers
{
    public static class UserMapper
    {
        public static DB_USER_PROFILE DBMapper(this DB_USER_PROFILE db_user, UserViewModel value)
        {
            if (db_user == null)
            {
                db_user = new DB_USER_PROFILE();
                db_user.UserId = value.UserId;

                db_user.ValidFlag = true;
                db_user.CreatedOn = value.ChangedOn;
                db_user.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_user.UpdatedOn = value.ChangedOn;
                db_user.UpdatedBy = value.ChangedBy;
            }

            db_user.FirstName = value.FirstName;
            db_user.LastName = value.LastName;
            db_user.CNIC = value.CNIC;
            db_user.Designation = value.Designation;
            db_user.Contact = value.ContactNumber;
            db_user.Email = value.Email;
            db_user.UserName = value.Username;
            db_user.LocationTypeId = value.LocationTypeId;
            db_user.LocationId = value.LocationId;
            HttpFileCollection file = HttpContext.Current.Request.Files;

            if (file.Count > 0 && file!=null)
            {
                Guid guid = Guid.NewGuid();
                string extension = Path.GetExtension(file[0].FileName);
                string imageName = guid + extension;
                string imagePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("/ImagesStorageDB"), imageName);
                file[0].SaveAs(imagePath);
                db_user.Image = imageName;
            }

            

            return db_user;
        }
    }
}