﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{

    public static class ProductPriceHistroryMapper
    {
        public static DB_PRODUCT_PRICE_HISTORY DBMapper(DB_PRODUCT_PRICE db_value, ProductPriceViewModel value)
        {
            DB_PRODUCT_PRICE_HISTORY db_DB_PRODUCT_PRICE_HISTORY = new DB_PRODUCT_PRICE_HISTORY();
            db_DB_PRODUCT_PRICE_HISTORY.CreatedOn = value.ChangedOn;
            db_DB_PRODUCT_PRICE_HISTORY.CreatedBy = value.ChangedBy;
            

            db_DB_PRODUCT_PRICE_HISTORY.ProductId = db_value.ProductId;
            db_DB_PRODUCT_PRICE_HISTORY.LocationId = db_value.LocationId;
            db_DB_PRODUCT_PRICE_HISTORY.ProductPriceTypeId = db_value.ProductPriceTypeId;
            db_DB_PRODUCT_PRICE_HISTORY.Price = value.Price;


            return db_DB_PRODUCT_PRICE_HISTORY;

        }

    }
}
