﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class PurchaseOrderMapper
    {
        public static DB_PURCHASE_ORDER DBMapper(PurchaseOrderViewModel value)
        {
            DB_PURCHASE_ORDER db_PURCHASE_ORDER = new DB_PURCHASE_ORDER();
            
            if (value.PurchaseQuotationId == 0)
                db_PURCHASE_ORDER.PurchaseQuotationId = null;
            else
                db_PURCHASE_ORDER.PurchaseQuotationId = value.PurchaseQuotationId;

            db_PURCHASE_ORDER.LocationId = value.LocationId;
            db_PURCHASE_ORDER.VendorId = value.VendorId;
            db_PURCHASE_ORDER.VoucherNo = value.VoucherNo;
            db_PURCHASE_ORDER.OrderDate = value.OrderDate;
            db_PURCHASE_ORDER.NetAmount = value.TotalNetAmount;

            db_PURCHASE_ORDER.StatusId = (int)EnumPurchaseOrder.Open;
            db_PURCHASE_ORDER.Remarks = value.Remarks;
            db_PURCHASE_ORDER.ValidFlag = true;
            db_PURCHASE_ORDER.CreatedOn = value.ChangedOn;
            db_PURCHASE_ORDER.CreatedBy = value.ChangedBy;

            foreach (var item in value.PurchaseDetails)
            {
                DB_PURCHASE_ORDER_DETAIL Details = new DB_PURCHASE_ORDER_DETAIL();

                Details.TotalQuantity = item.TotalQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.ProductId = item.ProductId;
                Details.NetAmount = item.NetAmount;
                Details.UnitPrice = item.UnitPrice;

                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_PURCHASE_ORDER.DB_PURCHASE_ORDER_DETAIL.Add(Details);
            }

            return db_PURCHASE_ORDER;
        }

        public static PurchaseOrderViewModel ValueMapper(DB_PURCHASE_ORDER dbValue)
        {
            PurchaseOrderViewModel value = new PurchaseOrderViewModel();
            value.PurchaseDetails = new List<PurchaseOrderDetailViewModel>();

            value.PurchaseReferenceNumber = dbValue.ReferenceNumber;

            value.PurchaseQuotationId = dbValue.PurchaseQuotationId == null ? 0 : (int)dbValue.PurchaseQuotationId;
            value.LocationId = dbValue.LocationId;
            value.Name = dbValue.DB_LOCATION.Name;
            value.VendorId = dbValue.VendorId;
            value.VendorName = dbValue.DB_VENDOR.Name;
            value.VoucherNo = dbValue.VoucherNo;
            value.OrderDate = dbValue.OrderDate;
            value.TotalNetAmount = dbValue.NetAmount;
            value.Remarks = dbValue.Remarks;
            value.OrderStatus = dbValue.DB_DEV_PURCHASE_ORDER_STATUS.Name;




            foreach (var v in dbValue.DB_PURCHASE_ORDER_DETAIL)
            {
                PurchaseOrderDetailViewModel dvalue = new PurchaseOrderDetailViewModel();

                dvalue.PrimaryQuantity = (decimal)v.PrimaryQuantity;
                dvalue.SecondaryQuantity = (decimal)v.SecondaryQuantity;
                dvalue.ProductId = v.ProductId;
                dvalue.ProductName = v.DB_PRODUCT.Name;
                dvalue.TotalQuantity = v.TotalQuantity;
                dvalue.UnitPrice = v.UnitPrice;
                dvalue.NetAmount = v.NetAmount;
                dvalue.Id = v.Id;
                dvalue.ProductId = v.ProductId;
                value.PurchaseDetails.Add(dvalue);

            }

            return value;
        }
    }
}
