﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class ProductCategoryMapper
    {
        public static DB_PRODUCT_CATEGORY DBMapper(this DB_PRODUCT_CATEGORY db_PRODUCT_CATEGORY, ProductCategoryViewModel value)
        {
            if (db_PRODUCT_CATEGORY == null)
            {
                db_PRODUCT_CATEGORY = new DB_PRODUCT_CATEGORY();
                db_PRODUCT_CATEGORY.ValidFlag = true;
                db_PRODUCT_CATEGORY.CreatedOn = value.ChangedOn;
                db_PRODUCT_CATEGORY.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_PRODUCT_CATEGORY.UpdatedBy = value.ChangedBy;
                db_PRODUCT_CATEGORY.UpdatedOn = value.ChangedOn;
            }

            db_PRODUCT_CATEGORY.Code = value.Code;
            db_PRODUCT_CATEGORY.Name = value.Name;
            return db_PRODUCT_CATEGORY;
        }
        public static ProductCategoryViewModel DBValue(DB_PRODUCT_CATEGORY dbValue)
        {
            ProductCategoryViewModel value = new ProductCategoryViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value;
        }
    }
}

