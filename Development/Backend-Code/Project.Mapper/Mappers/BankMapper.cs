﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class BankMapper
    {
        public static DB_BANK DBMapper(this DB_BANK db_BANK, BankViewModel value)
        {
            if (db_BANK == null)
            {
                db_BANK = new DB_BANK();
                db_BANK.ValidFlag = true;
                db_BANK.CreatedOn = value.ChangedOn;
                db_BANK.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_BANK.UpdatedBy = value.ChangedBy;
                db_BANK.UpdatedOn = value.ChangedOn;
            }

            db_BANK.Code = value.Code;
            db_BANK.Name = value.Name;

            return db_BANK;
        }
        public static BankViewModel DBValue(DB_BANK dbValue)
        {
            BankViewModel value = new BankViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value;
        }
    }
}
