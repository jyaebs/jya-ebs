﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
   public static class VendorTypeMapper
    {
        public static DB_VENDOR_TYPE DBMapper(this DB_VENDOR_TYPE db_VENDOR_TYPE, VendorTypeViewModel value)
        {
            if (db_VENDOR_TYPE == null)
            {
                db_VENDOR_TYPE = new DB_VENDOR_TYPE();
                db_VENDOR_TYPE.ValidFlag = true;
                db_VENDOR_TYPE.CreatedOn = value.ChangedOn;
                db_VENDOR_TYPE.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_VENDOR_TYPE.UpdatedBy = value.ChangedBy;
                db_VENDOR_TYPE.UpdatedOn = value.ChangedOn;
            }
            db_VENDOR_TYPE.Code = value.Code;
            db_VENDOR_TYPE.Name = value.Name;

            return db_VENDOR_TYPE;
        }
        public static VendorTypeViewModel DBValue(DB_VENDOR_TYPE dbValue)
        {
            VendorTypeViewModel value = new VendorTypeViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value;
        }
    }
}

