﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class CustomerTypeMapper
    {
        public static DB_CUSTOMER_TYPE DBMapper(this DB_CUSTOMER_TYPE db_CUSTOMER_TYPE, CustomerTypeViewModel value)
        {
            if (db_CUSTOMER_TYPE == null)
            {
                db_CUSTOMER_TYPE = new DB_CUSTOMER_TYPE();
                db_CUSTOMER_TYPE.ValidFlag = true;
                db_CUSTOMER_TYPE.CreatedBy = value.ChangedBy;
                db_CUSTOMER_TYPE.CreatedOn = value.ChangedOn;

            }
            else
            {
                db_CUSTOMER_TYPE.UpdatedBy = value.ChangedBy;
                db_CUSTOMER_TYPE.UpdatedOn = value.ChangedOn;
            }
            db_CUSTOMER_TYPE.Code = value.Code;
            db_CUSTOMER_TYPE.Name = value.Name;

            return db_CUSTOMER_TYPE;
        }
        public static CustomerTypeViewModel DBValue(DB_CUSTOMER_TYPE dbValue)
        {
            CustomerTypeViewModel value = new CustomerTypeViewModel();
            value.Id = dbValue.Id;
            value.Code = dbValue.Code;
            value.Name = dbValue.Name;
            return value ;
        }
    }
}