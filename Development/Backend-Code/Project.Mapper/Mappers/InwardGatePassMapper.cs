﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class InwardGatePassMapper
    {
        public static DB_INWARD_GATE_PASS DBMapper(InwardGatePassViewModel value)
        {
            DB_INWARD_GATE_PASS db_IGP = new DB_INWARD_GATE_PASS();
            db_IGP.ValidFlag = true;
            db_IGP.CreatedOn = value.ChangedOn;
            db_IGP.CreatedBy = value.ChangedBy;
            
            db_IGP.PurchaseOrderId = value.PurchaseOrderId;
            db_IGP.LocationId = value.LocationId;
            db_IGP.VendorId = value.VendorId;
            db_IGP.VoucherNo = value.VoucherNo;
            db_IGP.InwardDate = value.InwardDate;
            db_IGP.NetAmount = value.NetAmount;
            db_IGP.StatusId = (int)EnumInwardGatePass.Open;
            db_IGP.Remarks = value.Remarks;

            foreach (var item in value.InwardGatePassDetails)
            {
                DB_INWARD_GATE_PASS_DETAIL Details = new DB_INWARD_GATE_PASS_DETAIL();


                Details.ProductId = item.ProductId;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.TotalQuantity = item.TotalQuantity;
                Details.ReceivedQuantity = item.ReceivedQuantity;
                Details.UnitPrice = item.UnitPrice;
                Details.NetAmount = item.NetAmount;
                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_IGP.DB_INWARD_GATE_PASS_DETAIL.Add(Details);
            }

            return db_IGP;
        }


        public static InwardGatePassViewModel ValueMapper(DB_INWARD_GATE_PASS _dbvalue)
        {
            InwardGatePassViewModel inward_gate_pass = new InwardGatePassViewModel();

            inward_gate_pass.Id = _dbvalue.Id;
            inward_gate_pass.PurchaseOrderId = _dbvalue.PurchaseOrderId;
            inward_gate_pass.ReferenceNumber = _dbvalue.ReferenceNumber;
            inward_gate_pass.Name = _dbvalue.DB_LOCATION.Name;
            inward_gate_pass.VendorName = _dbvalue.DB_VENDOR.Name;
            inward_gate_pass.VoucherNo = _dbvalue.VoucherNo;
            inward_gate_pass.OrderDate = _dbvalue.DB_PURCHASE_ORDER.OrderDate;
            inward_gate_pass.InwardDate = _dbvalue.InwardDate;
            inward_gate_pass.NetAmount = _dbvalue.NetAmount;
            inward_gate_pass.Remarks = _dbvalue.Remarks;


            inward_gate_pass.InwardGatePassDetails = new List<InwardGatePassDetailViewModel>();

            foreach (var detail in _dbvalue.DB_INWARD_GATE_PASS_DETAIL)
            {
                InwardGatePassDetailViewModel InwardGatePassDetail = new InwardGatePassDetailViewModel();

                InwardGatePassDetail.ProductId = detail.ProductId;
                InwardGatePassDetail.ProductName = detail.DB_PRODUCT.Name;
                InwardGatePassDetail.PrimaryQuantity =(decimal)detail.PrimaryQuantity;
                InwardGatePassDetail.SecondaryQuantity = (decimal)detail.SecondaryQuantity;
                InwardGatePassDetail.TotalQuantity = detail.TotalQuantity;
                InwardGatePassDetail.ReceivedQuantity = detail.ReceivedQuantity;
                InwardGatePassDetail.UnitPrice = detail.UnitPrice;
                InwardGatePassDetail.NetAmount = detail.NetAmount;

                inward_gate_pass.InwardGatePassDetails.Add(InwardGatePassDetail);
            }

            return inward_gate_pass;
        }
    }
}
