﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class SaleQuotationMapper
    {
        public static DB_SALE_QUOTATION DBMapper(this DB_SALE_QUOTATION db_SALE_QUOTATION, SaleQuotationViewModel value)
        {
            if (db_SALE_QUOTATION == null)
            {
                db_SALE_QUOTATION = new DB_SALE_QUOTATION();
                db_SALE_QUOTATION.ValidFlag = true;
                db_SALE_QUOTATION.CreatedOn = value.ChangedOn;
                db_SALE_QUOTATION.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_SALE_QUOTATION.UpdatedBy = value.ChangedBy;
                db_SALE_QUOTATION.UpdatedOn = value.ChangedOn;
            }


            db_SALE_QUOTATION.LocationId = value.LocationId;
            db_SALE_QUOTATION.CustomerId = value.CustomerId;



            db_SALE_QUOTATION.StatusId = (int)EnumSaleQuotation.Open;


            foreach (var item in value.SaleQuotationDetail)
            {
                DB_SALE_QUOTATION_DETAIL Details = new DB_SALE_QUOTATION_DETAIL();

                Details.TotalQuantity = item.TotalQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.ProductId = item.ProductId;
                Details.NetAmount = item.NetAmount;
                Details.SalePrice = item.UnitPrice;
                Details.GrossAmount = item.GrossAmount;
                Details.DiscountAmount = item.DiscountAmount;
                Details.DiscountPercentage = item.Discount;


                db_SALE_QUOTATION.GrossAmount += item.GrossAmount;
                db_SALE_QUOTATION.DiscountAmount += item.DiscountAmount;
                db_SALE_QUOTATION.NetAmount += item.NetAmount;

                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_SALE_QUOTATION.DB_SALE_QUOTATION_DETAIL.Add(Details);
            }

            db_SALE_QUOTATION.ReferenceNumber = "REF-0001-01";

            return db_SALE_QUOTATION;
        }

        public static SaleQuotationViewModel ValueMapper(DB_SALE_QUOTATION dbValue)
        {
            SaleQuotationViewModel value = new SaleQuotationViewModel();

            value.SaleQuotationDetail = new List<SaleQuotationDetailViewModel>();

            value.LocationId = dbValue.LocationId;
            value.Name = dbValue.DB_LOCATION.Name;
            value.CustomerId = dbValue.CustomerId;
            value.CustomerName = dbValue.DB_CUSTOMER.Name;
            value.TotalGrossAmount = (int)dbValue.GrossAmount;
            value.TotalDiscountAmount = (int)dbValue.DiscountAmount;
            value.TotalNetAmount = (int)dbValue.NetAmount;
            value.ReferenceNumber = dbValue.ReferenceNumber;
           

            foreach (var v in dbValue.DB_SALE_QUOTATION_DETAIL)
            {

                SaleQuotationDetailViewModel SaleQuotationvalue = new SaleQuotationDetailViewModel();

                SaleQuotationvalue.PrimaryQuantity = (int)v.PrimaryQuantity;
                SaleQuotationvalue.SecondaryQuantity = (int)v.SecondaryQuantity;
                SaleQuotationvalue.ProductName = v.DB_PRODUCT.Name;
                SaleQuotationvalue.TotalQuantity = (int)v.TotalQuantity;
                SaleQuotationvalue.UnitPrice = (int)v.SalePrice;
                SaleQuotationvalue.Discount = (int)v.DiscountPercentage;
                SaleQuotationvalue.DiscountAmount = (int)v.DiscountAmount;
                SaleQuotationvalue.GrossAmount = (int)v.GrossAmount;
                SaleQuotationvalue.NetAmount = (int)v.NetAmount;
                SaleQuotationvalue.ProductId = v.ProductId;

                value.SaleQuotationDetail.Add(SaleQuotationvalue);


            }
            return value;
        }
    }
}
