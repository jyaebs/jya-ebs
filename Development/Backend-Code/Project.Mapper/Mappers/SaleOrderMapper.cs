﻿using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Mapper.Mappers
{
    public static class SaleOrderMapper 
    {
        public static DB_SALE_ORDER DBMapper(this DB_SALE_ORDER db_SALE_ORDER, SaleOrderViewModel value)
        {
            if (db_SALE_ORDER == null)
            {
                db_SALE_ORDER = new DB_SALE_ORDER();
                db_SALE_ORDER.ValidFlag = true;
                db_SALE_ORDER.CreatedOn = value.ChangedOn;
                db_SALE_ORDER.CreatedBy = value.ChangedBy;
            }
            else
            {
                db_SALE_ORDER.UpdatedBy = value.ChangedBy;
                db_SALE_ORDER.UpdatedOn = value.ChangedOn;
            }

            db_SALE_ORDER.LocationId = value.LocationId;
            db_SALE_ORDER.CustomerId = value.CustomerId;
            db_SALE_ORDER.VoucherNo = value.VoucherNo;
            db_SALE_ORDER.OrderDate = value.OrderDate;
           
           
            db_SALE_ORDER.StatusId = (int)EnumSaleOrder.Open;


            foreach (var item in value.SaleOrderDetail)
            {
                DB_SALE_ORDER_DETAIL Details = new DB_SALE_ORDER_DETAIL();

                Details.TotalQuantity = item.TotalQuantity;
                Details.SecondaryQuantity = item.SecondaryQuantity;
                Details.PrimaryQuantity = item.PrimaryQuantity;
                Details.ProductId = item.ProductId;
                Details.NetAmount = item.NetAmount;
                Details.SalePrice = item.UnitPrice;
                Details.GrossAmount = item.GrossAmount;
                Details.DiscountAmount = item.DiscountAmount;
                Details.DiscountPercentage = item.Discount;

                db_SALE_ORDER.GrossAmount += item.GrossAmount;
                db_SALE_ORDER.DiscountAmount += item.DiscountAmount;
                db_SALE_ORDER.NetAmount += item.NetAmount;

                Details.CreatedBy = value.ChangedBy;
                Details.CreatedOn = value.ChangedOn;
                db_SALE_ORDER.DB_SALE_ORDER_DETAIL.Add(Details);
            }

            db_SALE_ORDER.ReferenceNumber = "REF-0001-01";

            return db_SALE_ORDER;
        }

        public static SaleOrderViewModel ValueMapper(DB_SALE_ORDER dbValue)
        {
           
            SaleOrderViewModel value = new SaleOrderViewModel();

            value.SaleOrderDetail = new List<SaleOrderDetailViewModel>();

            value.LocationId = dbValue.LocationId;
            value.CustomerId = dbValue.CustomerId;
            value.CustomerName = dbValue.DB_CUSTOMER.Name;
            value.VoucherNo = dbValue.VoucherNo;
            value.OrderDate = dbValue.OrderDate;
            value.TotalGrossAmount = (int)dbValue.GrossAmount;
            value.TotalDiscountAmount = (int)dbValue.DiscountAmount;
            value.TotalNetAmount = (int)dbValue.NetAmount;
            value.ReferenceNumber = dbValue.ReferenceNumber;


            foreach (var v in dbValue.DB_SALE_ORDER_DETAIL)
            {

                SaleOrderDetailViewModel SaleOrdervalue = new SaleOrderDetailViewModel();

                SaleOrdervalue.PrimaryQuantity = (int)v.PrimaryQuantity;
                SaleOrdervalue.SecondaryQuantity = (int)v.SecondaryQuantity;
                SaleOrdervalue.ProductName = v.DB_PRODUCT.Name;
                SaleOrdervalue.TotalQuantity = (int)v.TotalQuantity;
                SaleOrdervalue.UnitPrice = (int)v.SalePrice;
                SaleOrdervalue.Discount = (int)v.DiscountPercentage;
                SaleOrdervalue.DiscountAmount = (int)v.DiscountAmount;
                SaleOrdervalue.GrossAmount = (int)v.GrossAmount;
                SaleOrdervalue.NetAmount = (int)v.NetAmount;
                SaleOrdervalue.Id = v.Id;
                SaleOrdervalue.ProductId = v.ProductId;
                value.SaleOrderDetail.Add(SaleOrdervalue);
            }
             return value;
        }
    }
}
