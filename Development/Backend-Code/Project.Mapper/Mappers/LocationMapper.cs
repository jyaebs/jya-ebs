﻿using Project.DatabaseModels;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Project.Mapper.Mappers
{
    public static class LocationMapper
    {
        public static DB_LOCATION DBMapper(this DB_LOCATION db_location, LocationViewModel value)
        {
          if (db_location != null)
            {
                db_location.ValidFlag = true;
                db_location.UpdatedOn = value.ChangedOn;
                db_location.UpdatedBy = value.ChangedBy;
                
            }
            else
            {
                db_location = new DB_LOCATION();
                db_location.ParentId = value.ParentId;
                db_location.Type = value.TypeId;
                db_location.CreatedOn = value.ChangedOn;
                db_location.CreatedBy = value.ChangedBy;
                
            }
            db_location.CountryId = value.CountryId;
            db_location.CityId = value.CityId;
            db_location.Code = value.Code;
            db_location.CreatedBy = value.ChangedBy;
            db_location.CreatedOn = value.ChangedOn;
           
            db_location.NTN = value.NTN;
            db_location.ContactNo = value.ContactNumber;
            db_location.AlternateContactNo = value.AlternateContactNo;
            db_location.STRN = value.STRN;
            db_location.Name = value.Name;
            db_location.ValidFlag = true;
            db_location.IsWarehouse = value.IsWarehouse;
            if (value.IsWarehouse)
                db_location.IsCompanyWarehouse = value.IsCompanyWarehouse;
            else
                db_location.IsCompanyWarehouse = false;

            HttpFileCollection file = HttpContext.Current.Request.Files;
            if (file.Count>0)
            {
                Guid guid = Guid.NewGuid();
                string extension = Path.GetExtension(file[0].FileName);
                string imageName = guid + extension;
                string imagePath = Path.Combine(HttpContext.Current.Server.MapPath("/ImagesStorageDB"), imageName);
                file[0].SaveAs(imagePath);
                db_location.Image = imageName;
            }
            
            return db_location;
        }

        public static LocationViewModel ValueMapper(DB_LOCATION _dbvalue)
        {
            LocationViewModel _LocationViewModel = new LocationViewModel();
            _LocationViewModel.Id = _dbvalue.Id;
            _LocationViewModel.Code = _dbvalue.Code;
            _LocationViewModel.Name = _dbvalue.Name;
            _LocationViewModel.CountryId = _dbvalue.DB_COUNTRY.Id;
            _LocationViewModel.CityId = _dbvalue.DB_CITY.Id;
            _LocationViewModel.IsWarehouse = _dbvalue.IsWarehouse;
            _LocationViewModel.IsCompanyWarehouse = _dbvalue.IsCompanyWarehouse;
            _LocationViewModel.ValidFlag = _dbvalue.ValidFlag;
            _LocationViewModel.TypeId = _dbvalue.Type;
            _LocationViewModel.Image = _dbvalue.Image;
            _LocationViewModel.NTN = _dbvalue.NTN;
            _LocationViewModel.ContactNumber = _dbvalue.ContactNo;
            _LocationViewModel.STRN = _dbvalue.STRN;
            _LocationViewModel.AlternateContactNo = _dbvalue.AlternateContactNo;


            return _LocationViewModel;
        }
   
    }
}
