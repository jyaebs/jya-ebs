﻿using System;

using System.Threading.Tasks;
using System.Web;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using System.Reflection;
using IdentitySample.Models;
using Project.Logging;
using Project.ViewModels.Enums;
using Project.ViewModels;
using Project.Models.ViewModels;
using Project.Utilities;
using System.Linq;

namespace Project.Web.IdentityFunctions
{
    public static class IdentityHelpers
    {

        public static ApplicationUserManager GetUserManager()
        {
            ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            return userManager;
        }

        public static async Task<FunctionResult> createUser(UserViewModel user)
        {
            ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser appuser = new ApplicationUser { UserName = user.Username, PhoneNumber = user.ContactNumber, Email = user.Email };
            IdentityResult result = await userManager.CreateAsync(appuser, user.Password);
            
            if (result.Succeeded)
            {
                string role = AuthorizationUtility.getUserRoleNamebyRoleId(user.Role);
                var result1 = userManager.AddToRole(appuser.Id, role);
                return new FunctionResult { success = true, message = appuser.Id };
            }
            else
            {


                String error = "";
                foreach (var err in result.Errors)
                {
                    //if (err.Contains("taken"))
                    //{
                    //    //canUpdate = true;
                    //    break;
                    //}
                    error += err + " ";
                }

                return new FunctionResult { success = false, message = error };

            }

        }

        public async static Task<FunctionResult> updateUser(UserViewModel user)
        {
            try
            {
                ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();


                var Memuser = userManager.FindByName(user.Username);
                Memuser.UserName = user.Username;
                Memuser.Email = user.Email;
                Memuser.PhoneNumber = user.ContactNumber;

                var oldRoleId = Memuser.Roles.FirstOrDefault();
                string oldroleName = AuthorizationUtility.getUserRoleNamebyRoleId(oldRoleId.RoleId);
                string newroleName = AuthorizationUtility.getUserRoleNamebyRoleId(user.Role);
                userManager.RemoveFromRole(Memuser.Id, oldroleName);
                userManager.AddToRole(Memuser.Id, newroleName);

                IdentityResult result = null;
                if (user.Password != null)
                {
                    if (Memuser.PasswordHash == null || !Memuser.PasswordHash.Equals(user.Password))
                    {
                        userManager.RemovePassword(Memuser.Id);
                        result = await userManager.AddPasswordAsync(Memuser.Id, user.Password);
                    }
                    if (result.Succeeded)
                    {
                        return new FunctionResult { success = true };
                    }
                    else
                    {
                        String error = "";
                        foreach (var err in result.Errors)
                        {
                            error += err + " ";
                        }
                        return new FunctionResult { success = false, message = error };
                    }
                }
                return new FunctionResult { success = true };

            }
            catch (Exception ex)
            {
                //TODO Log Exception in Error Log
                return new FunctionResult { success = false, message = ex.Message };
            }

        }
        public static async Task<FunctionResult> ChangePassword(ChangePasswordViewModel model)
        {
            ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByNameAsync(model.Username);
            string errorMessage;
            if (user == null)
            {
                errorMessage = "No such user found";
                return new FunctionResult { success = false, message = errorMessage };
            }
            var result = await userManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return new FunctionResult { success = true };
            }
            errorMessage = "An error occurred while processing your request. Please try again later";
            return new FunctionResult { success = false, message = errorMessage };
        }

        public static async Task<FunctionResult> ResetPassword(ResetPasswordViewModel model)
        {
            ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByNameAsync(model.Username);
            string errorMessage;
            if (user == null)
            {
                errorMessage = "No such user found";
                return new FunctionResult { success = false, message = errorMessage };
            }
            userManager.RemovePassword(user.Id);
            var result = await userManager.AddPasswordAsync(user.Id, model.Password);

            if (result.Succeeded)
            {
                if (user.Email != null && user.Email != "")
                {
                }
                return new FunctionResult { success = true };
            }
            errorMessage = "An error occurred while processing your request. Please try again later";
            return new FunctionResult { success = false, message = errorMessage };
        }

        public static bool IsValidaspnetUser(string userName, string password)
        {
            using (new FunctionTracer("IsValidUser", MethodBase.GetCurrentMethod().Name, new object[] { userName }))
            {
                try
                {
                    ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

                    var user = userManager.Find(userName, password);
                    if (user == null)
                        return false;
                    else
                        return true;

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return false;
                }
            }
        }

    }
}