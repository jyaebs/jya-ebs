﻿using Project.Logging;
using System;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Project.ViewModels.Enums;
using Project.ViewModels;
using Project.Utilities;
using Project.Web.IdentityFunctions;

namespace Project.Web.CustomeAnnotations
{
public class ApiCustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string privilege { get; set; }

        /// <summary>
        /// returns whether a user is authorized for particular view or not
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        /// 
        public ApiCustomAuthorizeAttribute(string value = null)
        {
            if (value != null)
                privilege = value;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (IsIdentityAuthorized(actionContext)) return true;
            else if (IsLoginAuthorized(actionContext)) return true;

            return false;
        }
        private bool IsIdentityAuthorized(HttpActionContext actionContext)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                if (privilege == null)
                    return HttpContext.Current.User.Identity.IsAuthenticated;
                else
                {

                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        
                            if (AuthorizationUtility.userHasPrivilege(HttpContext.Current.User.Identity.Name, privilege))
                            {
                                return true;
                                //break;
                            }
                        
                        return false;
                    }

                    return false;
                }
            }
        }
        private bool IsLoginAuthorized(HttpActionContext actionContext)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {

                try
                {
                    Credentials credentials = AuthorizationUtility.GetUserCredentialsFromAuthorizationHeader(actionContext.Request);
                    if (credentials == null)
                    {
                        return false;
                    }

                    bool isValidUser = IdentityHelpers.IsValidaspnetUser(credentials.UserName, credentials.Password);
                    if (!isValidUser)
                    {
                        return false;
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return false;
                }
            }
        }
    }
		
}