﻿using Project.Utilities;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.CustomeAnnotations
{
    public class MvcCustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string privilege { get; set; }

        /// <summary>
        /// returns whether a user is authorized for particular view or not
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (string.IsNullOrEmpty(privilege))
                return HttpContext.Current.User.Identity.IsAuthenticated;
            else
                return (HttpContext.Current.User.Identity.IsAuthenticated && AuthorizationUtility.userHasPrivilege(HttpContext.Current.User.Identity.Name, privilege));
        }
    }
}