﻿using System.Web.Optimization;

namespace IdentitySample
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

           
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
               //"~/Scripts/kendo/jquery.min.js",
               //"~/Scripts/kendo/kendo.aspnetmvc.min.js"
               "~/Content/kendojs/jquery.min.js",
               "~/Content/kendojs/kendo.aspnetmvc.min.js"
               ));



            bundles.Add(new StyleBundle("~/bundles/kendoStyle").Include(
              "~/Content/kendo/kendo.common-bootstrap.min.css",
            //  "~/Content/kendo/kendo.default.min.css",
           //   "~/Content/kendo/kendo.default.mobile.min.css",
             // "~/Content/kendo/kendo.common.min.css",
              "~/Content/kendo/kendo.bootstrap.min.css"));
            

            bundles.Add(new ScriptBundle("~/bundles/views/common")
            .Include(
              "~/Scripts/Views/Common.js",
              "~/Scripts/Views/Loader.js"
              ));
            
            bundles.IgnoreList.Clear();


        }
    }
}
