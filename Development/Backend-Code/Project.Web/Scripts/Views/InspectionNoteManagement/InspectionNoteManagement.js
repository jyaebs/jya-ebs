﻿



function initializeForm() {

    var today = new Date().toISOString().split('T')[0];
    $("#inwardDate").attr("max", today);

    dataTable.initialize();
    $(".ModaldataDiv").addClass("d-none");
    $("#addBtn").addClass("d-none");


    // Select Inward Gate Pass Data from Modal
    $("#GateInwardDetailTable tbody").on("click", "tr", function () {
        var table = $('#GateInwardDetailTable').DataTable();

        var dataId = table.rows().data()[this.rowIndex - 1].Id

        var ReferenceNumber = table.rows().data()[this.rowIndex - 1].ReferenceNumber;
        var LocationName = table.rows().data()[this.rowIndex - 1].Name;
        var LocationId = table.rows().data()[this.rowIndex - 1].LocationId;
        var VendorId = table.rows().data()[this.rowIndex - 1].VendorId;
        var VendorName = table.rows().data()[this.rowIndex - 1].VendorName;
        var Voucher = table.rows().data()[this.rowIndex - 1].VoucherNo;
        var InwardDate = table.rows().data()[this.rowIndex - 1].InwardDate;

        $("#ReferenceNumber").val(ReferenceNumber);
        $("#GateInwardId").val(dataId);
        $("#Location").val(LocationName);
        $("#LocationId").val(LocationId);
        $("#Vendor").val(VendorName);
        $("#VendorId").val(VendorId);
        $("#VoucherNo").val(Voucher);
        $("#InwardDate").val(InwardDate.substr(0, 10));
        $("#GateInwardDetailModal").modal("hide");
        $(".ModaldataDiv").removeClass("d-none");
        $('#GateInwardDetailTable').DataTable().clear().draw();


        $.ajax({
            url: "../api/InwardGatePass/GetById?id=" + dataId,
            type: 'GET',
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                console.log(data)
                $("#addBtn").removeClass("d-none");
                $('#GateInwardTable').DataTable().clear().draw();

                var table = $('#GateInwardTable').DataTable();
                data.InwardGatePassDetails.forEach(function (inward, index) {

                    var rowData = {};
                    rowData.ProductId = inward.ProductId;
                    rowData.ProductName = inward.ProductName;
                    rowData.PrimaryQuantity = inward.PrimaryQuantity == 0 ? "-" : inward.PrimaryQuantity;
                    rowData.SecondaryQuantity = inward.SecondaryQuantity == 0 ? "-" : inward.SecondaryQuantity;;
                    rowData.TotalQuantity = inward.ReceivedQuantity;
                    rowData.UnitPrice = inward.UnitPrice;
                    rowData.NetAmount = inward.NetAmount;

                    table.row.add({
                        "ProductName": rowData.ProductName,
                        "ProductId": rowData.ProductId,
                        "PrimaryQuantity": rowData.PrimaryQuantity,
                        "SecondaryQuantity": rowData.SecondaryQuantity,
                        "TotalQuantity": rowData.TotalQuantity,
                        "ApprovedQuantity": rowData.TotalQuantity,
                        "RejectQuantity": 0,
                        "UnitPrice": rowData.UnitPrice,
                        "NetAmount": rowData.NetAmount,
                    }).draw();
                })
            }
        });
    });
}



var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#GateInwardTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,

            "columns": [
                { "data": "ProductName" },
                { "data": "ProductId", "visible": false },
                { "data": "PrimaryQuantity", className: "text-center" },
                { "data": "SecondaryQuantity", className: "text-center" },
                { "data": "TotalQuantity", className: "text-center" },
                { "data": "ApprovedQuantity", className: "text-center" },
                { "data": "RejectQuantity", className: "text-center" },
                { "data": "UnitPrice", className: "text-center" },
                { "data": "NetAmount", className: "text-center" }


            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        if (row.RejectQuantity == 0) {
                            return inputField = `<input class="form-control text-center rejectItems" value="0"  type="number" onkeyup='calculateNetAmount()'/>`
                        }
                        else {
                            return inputField = `${row.ApprovedQuantity}`
                        }
                    },
                    "targets": -3
                },
                {
                    "render": function (data, type, row) {
                        return rowText = `<span class="text-center" id="netAmountText-${row.NetAmount}">${row.NetAmount}</span>`
                    },
                    "targets": -1
                },

                {
                    orderable: false,
                    targets: "no-sort",
                }
            ],
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};



function calculateNetAmount() {
    var table = $('#GateInwardTable').DataTable();
    var data = table.rows().data();
    var NetAmount = 0
    data.each(function (value, index) {
        let rejectQty = table.rows(index).nodes().to$().find(".rejectItems").val();
        let approvedQty = value.ApprovedQuantity;
        var netAmountText = value.NetAmount;
        let unitPrice = value.UnitPrice;
        netAmount = (rejectQty == "" ? (approvedQty * unitPrice ) : ((approvedQty * unitPrice) - parseFloat(rejectQty) * unitPrice));
        NetAmount = NetAmount + netAmount;
        $("#netAmountText-" + netAmountText).text(netAmount);
    });

    $("#totalNetAmount").text(NetAmount)
}


function validateRejectQty() {
    var table = $('#GateInwardTable').DataTable();
    var data = table.rows().data();
    var validateQty = false;
    data.each(function (value, index) {
        let rejectQty = table.rows(index).nodes().to$().find(".rejectItems").val();
        if (value.ApprovedQuantity < rejectQty) {
            toastr.warning(`Reject Quantity of ${value.ProductName} Cannot be Greater than Approved Quantity`, { timeOut: 5000 });
            validateQty = true;
        }
    });
    if (validateQty) {
        return false;
    }
    return true;
}

function GetInwardGatePass() {
    $("#GateInwardDetailModal").toggle("modal");
    initializeDataTable();
}



function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/InwardGatePass/GetAll`,
        tableId: 'GateInwardDetailTable'
    };
    $('#GateInwardDetailTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "ReferenceNumber" },
            { "data": "Name" },
            { "data": "LocationId", "visible": false },
            { "data": "VendorName" },
            { "data": "VendorId", "visible": false },
            { "data": "VoucherNo" },
            { "data": "InwardDateString" },
            { "data": "NetAmount" },

        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ],

    });
}


function Add() {
    var formId = "form";
    if (validate(formId) && validateRejectQty()) {
        var InspectionNote = {
            InwardGatePassId: $("#GateInwardId").val(),
            LocationId: $("#LocationId").val(),
            VendorId: $("#VendorId").val(),
            VoucherNo: $("#VoucherNo").val(),
            InspectionDate: $("#InspectionDate").val(),
            NetAmount: $("#totalNetAmount").text(),
            Remarks: $("#Remarks").val()
        };

        InspectionNote.InspectionNoteDetails = [];
        var table = $('#PurchaseOrderTable').DataTable();
        var data = table.rows().data();

        data.each(function (value, index) {
            let rejectQty = table.rows(index).nodes().to$().find(".rejectItems").val();

            let inspectionObject = {
                ProductId: value.ProductId,
                PrimaryQuantity: value.PrimaryQuantity,
                SecondaryQuantity: value.SecondaryQuantity,
                TotalQuantity: value.TotalQuantity,
                ApprovedQuantity: value.ApprovedQuantity,
                RejectQuantity: rejectQty,
                UnitPrice: value.UnitPrice,
              //  NetAmount: value.UnitPrice * receivedQty
                NetAmount: value.NetAmount
            };

            InspectionNote.InspectionNoteDetails.push(inspectionObject);

        });

        let remark = $("#Remarks").val();
        if (remark == "") {
            toastr.warning("Please write remarks", { timeOut: 5000 });
            return false;
        }
        ShowLoader();
        $.ajax({
            url: '../api/InwardGatePass/Post/',
            type: 'POST',
            data: JSON.stringify(InspectionNote),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Inspection Note Added Succesfully", { timeOut: 5000 });
                HideLoader();
                ResetForm();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}

function ResetForm() {
    $("#LocationId").val("");
    $("#Location").val("");
    $("#ReferenceNumber").val("");
    $("#GateInwardId").val("");
    $("#VendorId").val("");
    $("#Vendor").val("");
    $("#VoucherNo").val("");
    $("#InwardDate").val("");
    $("#Vendor").val("");
    $("#InspectionDate").val("");
    $("#totalNetAmount").text("-");
    $("#Remarks").val("");
    $(".ModaldataDiv").addClass("d-none");
    var table = $('#GateInwardTable').DataTable();
    table.clear().draw();

}