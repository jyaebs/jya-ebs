﻿var Id = 0;

function Add() {
    var formId = "form";
    if (validate(formId)) {
        var Country = {
            Name: $('#Name').val(),
            Code: $('#Code').val(),
            DialingCode: $('#DialingCode').val(),
        };
        ShowLoader();
       
        $.ajax({
         
            url: '../api/Country/Post/',
            type: 'POST',
            data: JSON.stringify(Country),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Country Added Successfully", { timeOut: 5000 });
              
                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
            
                HideLoader();
            }
        });
    }
};

function Update() {
    var formId = "form";
    if (validate(formId)) {
        var Country = {
            Name: $('#Name').val(),
            Code: $('#Code').val(),
            DialingCode: $('#DialingCode').val(),
            Id: Id
        };
        ShowLoader();
        $.ajax({
            url: '../api/Country/Put/',
            type: 'POST',
            data: JSON.stringify(Country),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Country Updated Successfully", { timeOut: 5000 });
              
                ResetForm();
                RedirectToUrl('../CountryManagement/Manage');
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
          
                HideLoader();
            }
        });
    }
}

function ResetForm() {
    $('#Name').val("");
    $('#Code').val("");
    $('#DialingCode').val("");
}

function populateData(id) {
    Id = id;
    ShowLoader();
    $.ajax({
        url: "../api/Country/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#Name').val(data.Name);
            $('#Code').val(data.Code);
            $('#DialingCode').val(data.DialingCode);
            HideLoader();
        },
        error: function (data) {
            var response = data.responseText.replace(/"/g, '');
            toastr.error(response, { timeOut: 5000 });
          
            HideLoader();
        }
    });
}
