﻿
var locationId = 0;

function initializeForm() {

    LoadKendoDropDown('LocationTypeId', '-- Please Select Access Level --', '../api/Location/GetAllLocationTypes');
    LoadKendoDropDown('LocationId', '-- Please Select Access --', '');
    LoadKendoDropDown('TypeId', '-- Please Select Access Level --', '../api/Location/GetAllLocationTypes');
    LoadKendoDropDown('Country', '-- Please Select Country --', '../api/Country/GetActiveCountries');
    LoadKendoDropDown('City', '-- Please Select City --', '');
    $(".IsCompanyImage").hide();

    $(document).on('change', '#warehouse_switch', function () {
        if ($(this).is(":checked")) {
            $('#companyWarehouseDiv').removeClass('d-none');
        }
        else {
            $('#companyWarehouseDiv').addClass('d-none');
        }
    });
  
        $(document).on('change', '#TypeId', function () {
            debugger;
            var data = $(this).val();
            if ($(this).val() == '6') {
                $(".IsCompanyImage").show();
            }
            else {
                $(".IsCompanyImage").hide();
            }
        });
    $("#CompanyImage").change(function () {
        debugger;
        readURL(this);
    });

    $(".IsCompanyImage").keydown(function () {

        //allow  backspace, tab, ctrl+A, escape, carriage return
        if (event.keyCode == 8 || event.keyCode == 9
            || event.keyCode == 27 || event.keyCode == 13
            || (event.keyCode == 65 && event.ctrlKey === true))
            return;
        if ((event.keyCode < 48 || event.keyCode > 57))
            event.preventDefault();
    });
    //$('#AlternateNo').keydown(function () {
    //    debugger;
    //    allow  backspace, tab, ctrl+A, escape, carriage return
    //    if (event.keyCode == 8 || event.keyCode == 9
    //        || event.keyCode == 27 || event.keyCode == 13
    //        || (event.keyCode == 65 && event.ctrlKey === true))
    //        return;
    //    if ((event.keyCode < 48 || event.keyCode > 57))
    //        event.preventDefault();
    //});
    //$('#NTN').keydown(function () {

    //    allow  backspace, tab, ctrl+A, escape, carriage return
    //    if (event.keyCode == 8 || event.keyCode == 9
    //        || event.keyCode == 27 || event.keyCode == 13
    //        || (event.keyCode == 65 && event.ctrlKey === true))
    //        return;
    //    if ((event.keyCode < 48 || event.keyCode > 57))
    //        event.preventDefault();
    //});
}


//Image file Upload 
function readURL(input) {
    debugger;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#userImg')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function LoadLocations() {
    var LocationTypeId = $('#LocationTypeId').val();
    if (LocationTypeId != "") {
        LoadKendoDropDown('LocationId', '-- Please Select Access --', '../api/Location/GetAllLocationsByLocationType?Id=' + LocationTypeId);
    }
    else {
        LoadKendoDropDown('LocationId', '-- Please Select Access --', '');
    }
}

function LoadCities() {
    var cityId = $('#Country').val();
    if (cityId != "") {
        LoadKendoDropDown('City', '-- Please Select City --', '../api/City/GetActiveCities?CountryId=' + cityId);
    }
    else {
        LoadKendoDropDown('City', '-- Please Select City --', '');
    }
}


$(document).on('click', '.jstree-node', function (e) {

    e.stopImmediatePropagation();
    let LocationTypeId = $(this).attr('id');

    getData(LocationTypeId);
});
   



function getData(id) {
    ShowLoader();
    $.ajax({
        url: "../api/Location/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#Name').val(data.Name);
            $('#Code').val(data.Code);
            ContactNumber: $('#Contact').val(),
            LoadKendoDropDown('Country', '-- Please Select Country --', '../api/Country/GetActiveCountries');
            $('#Country').val(data.CountryId);
            LoadKendoDropDown('City', '-- Please Select City --', '../api/City/GetActiveCities?CountryId=' + data.CountryId);
            if (data.TypeId == '6') {
                $('.IsCompanyImage').show();
            }
            else {
                $('.IsCompanyImage').hide();
            }
            $('#NTN').val(data.NTN);
            $('#STRN').val(data.STRN);
            $('#Contact').val(data.ContactNumber);
            $('#AlternateNo').val(data.AlternateContactNo)
            $('#City').val(data.CityId);
            $('#TypeId').val(data.TypeId);
            $('#LocationId').val(data.Id);
            $('#locationStatus').val(data.ValidFlag);
            debugger;
            if (data.Image != null) {
                $('#userImg').attr("src", "../../Images/" + data.Image);
            }
            else {
                $('#userImg').attr("src", "../../Images/user.jpg");
            }
            if (data.ValidFlag === true) {
                $('#deactivateLocation').removeClass('d-none');
                $('#activateLocation').addClass('d-none');
                $('#updateLocation').removeClass('offset-md-2');

            }
            else {
                $('#activateLocation').removeClass('d-none');
                $('#deactivateLocation').addClass('d-none');
                $('#updateLocation').removeClass('offset-md-2');

            }

            $('#warehouse_switch').prop('checked', data.IsWarehouse);
            $('#companyWarehouse_switch').prop('checked', data.IsCompanyWarehouse);
            if (data.IsWarehouse === true)
                $('#companyWarehouseDiv').removeClass('d-none');

            HideLoader();
        },
        error: function (data) {
            var response = data.responseText.replace(/"/g, '');
            toastr.error(response, { timeOut: 5000 });

            HideLoader();
        }
    });
}

function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var formData = new FormData();
        debugger;
        var file = document.getElementById("CompanyImage");
        formData.append("CompanyImage",file.files[0]);
        var Location = {
            ParentAccessId: $('#LocationTypeId').val(),
            ParentId: $('#LocationId').val(),
            CountryId: $("#Country").val(),
            Name: $('#Name').val(),
            Code: $('#Code').val(),
            CityId: $('#City').val(),
            TypeId: $('#TypeId').val(),
            NTN: $('#NTN').val(),
            STRN: $('#STRN').val(),
            ContactNumber: $('#Contact').val(),
            AlternateContactNo: $('#AlternateNo').val(),
            IsWarehouse: $('#warehouse_switch').prop('checked'),
            IsCompanyWarehouse: $('#companyWarehouse_switch').prop('checked')
        };
        var serializeobject = JSON.stringify(Location);
        $.ajax({
            type: 'POST',
            url: '../api/Location/Post?location=' + serializeobject, 
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                toastr.success("Location Added Successfully", { timeOut: 5000 });
                
                Reset();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
}

function Update() {
    var formId = "form";
    var formData = new FormData();
    debugger;
    var file = document.getElementById("CompanyImage");
    formData.append("CompanyImage", file.files[0]);
    if (validate(formId)) {
        var Location = {
            CountryId: $("#Country").val(),
            CityId: $("#City").val(),
            Name: $('#Name').val(),
            NTN: $('#NTN').val(),
            STRN: $('#STRN').val(),
            Code: $('#Code').val(),
            ContactNumber: $('#Contact').val(),
            AlternateContactNo: $('#AlternateNo').val(),
            IsWarehouse: $('#warehouse_switch').prop('checked'),
            IsCompanyWarehouse: $('#companyWarehouse_switch').prop('checked'),
            Id: $("#LocationId").val(),
        };
        var serializeobject = JSON.stringify(Location);
        debugger;
        ShowLoader();
        $.ajax({

            type: 'POST',
            url: '../api/Location/Put?location=' + serializeobject, 
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                toastr.success("Location Updated Successfully", { timeOut: 5000 });
                ResetForm();
                RedirectToUrl('../LocationManagement/Index')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }

}

function Reset() {
    debugger;
    $('#Name').val("");
    $('#Code').val("");
    $('#NTN').val("");
    $('#Contact').val("");
    $('#STRN').val("");
    $('#AlternateNo').val("");
    UnselectKendoDropDown("Country");
    UnselectKendoDropDown("City");
    UnselectKendoDropDown("LocationTypeId");
    UnselectKendoDropDown("LocationId");
    UnselectKendoDropDown("TypeId");
    $('#CompanyImage').val("");
    $(".IsCompanyImage").hide();
    $('#userImg').attr("src", "../../Content/dist-assets/images/placeholder.jpg");
}

function ResetForm() {
    $('#Name').val("");
    $('#Code').val("");

    UnselectKendoDropDown("Country");
    UnselectKendoDropDown("City");
}

function Activate() {
    rowName = $('#Name').val();
    swal({
        title: rowName,
        text: "Are you sure, You want to Active ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Yes, Active it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success mr-5',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        ActivateLocation(locationId);
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal('Cancelled', 'There is no change in data', 'info');
        }
    });
}

function Deactivate() {
    rowName = $('#Name').val();
    swal({
        title: rowName,
        text: "Are you sure, You want to Inactive ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Yes, Inactive it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success mr-5',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        DeactivateLocation(locationId);
    }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal('Cancelled', 'There is no change in data', 'info');
        }
    });
}

function ActivateLocation(locationId) {
    $.ajax({
        url: `../api/Location/Activate?id=` + locationId,
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: "{}",
        success: function (result) {
            RedirectToUrl('../LocationManagement/Index')
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function DeactivateLocation(locationId) {
    $.ajax({
        url: `../api/Location/Deactivate?id=` + locationId,
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: "{}",
        success: function (result) {
            if (locationId != undefined && locationId != null) {
                RedirectToUrl('../LocationManagement/Index')
            }
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

