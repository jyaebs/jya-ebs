﻿var cityId = 0;

function initializeForm() {
    
    LoadKendoDropDown('Country', '-- Please Select Country --', '../api/Country/GetActiveCountries');
};

function Add() {
    var formId = "form";
    if (validate(formId)) {
    var City = {
            CountryId: $("#Country").val(),
            Name: $('#Name').val(),
            Code: $('#Code').val(),
            DialingCode: $('#DialingCode').val(),
        };
        ShowLoader();
        $.ajax({
            url: '../api/City/Post/',
            type: 'POST',
            data: JSON.stringify(City),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("City Added Successfully", { timeOut: 5000 });
              
                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
             
                HideLoader();
            }
        });
    }
};


function Update() {
    var formId = "form";
    if (validate(formId)) {
        var City = {
            CountryId: $("#Country").val(),
            Name: $('#Name').val(),
            Code: $('#Code').val(),
            DialingCode: $('#DialingCode').val(),
            Id: cityId
        };
        ShowLoader();
        $.ajax({
            url: '../api/City/Put/',
            type: 'POST',
            data: JSON.stringify(City),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("City Updated Successfully", { timeOut: 5000 });
               
                ResetForm();
                RedirectToUrl('../CityManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
              
                HideLoader();
            }
        });
    }
};

function populateData(id) {
    cityId = id;
    ShowLoader();
    $.ajax({
        url: "../api/City/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('Country', '-- Please Select Country --', '../api/Country/GetActiveCountries');
            $('#Country').val(data.CountryId);
            $('#Name').val(data.Name);
            $('#Code').val(data.Code);
            $('#DialingCode').val(data.DialingCode);
            HideLoader();
        }
    });
};


function ResetForm() {
    $('#Name').val("");
    $('#Code').val("");
    $('#DialingCode').val("");
    UnselectKendoDropDown("Country");
}
