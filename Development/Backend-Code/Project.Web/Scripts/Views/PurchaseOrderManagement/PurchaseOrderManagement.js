﻿
var PurchaseOrderId = 0;
let Id = 0;

function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
    LoadKendoAutoComplete('Vendor', '-- Please Select Vendor --', '');

    dataTable.initialize();
    
    // Select Quotation Data from Modal
    $("#PurchaseQuotationDetailTable tbody").on("click", "tr", function () {
        
        var table = $('#PurchaseQuotationDetailTable').DataTable();

        var Id = table.rows().data()[this.rowIndex - 1].Id;

        GetPurchaseQuotationDetail(Id);
        
        $("#PurchaseQuotationDetailModal").modal("hide");
        table.clear().draw();
    });


}

function LoadVendors(id) {
    var LocationId = $("#" + id).val();
    if (LocationId != "") {
        LoadKendoAutoComplete('Vendor', '-- Please Select Vendor --', '../api/Vendor/GetActiveVendor?LocationId=' + LocationId);
    }

}

var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#PurchaseOrderTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            "columns": [
                { "data": "Product" },
                { "data": "PrimaryQuantity", className: "text-center" },
                { "data": "SecondaryQuantity", className: "text-center" },
                { "data": "TotalQuantity", className: "text-center" },
                { "data": "UnitPrice", className: "text-center" },
                { "data": "NetAmount", className: "text-center" },
                { "data": "ProductId", className: "text-center" }
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return RemoveGridProduct(row.ProductId);
                    },
                    "targets": -1
                },

                {
                    orderable: false,
                    targets: "no-sort"
                }
            ]
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};

function onSelectRecord(e) {
    var inputId = this.element[0].id;
    var dataItem = this.dataItem(e.item.index());

    $("#" + inputId + "Id").val(dataItem.Id);
    $("#" + inputId + "NameOnSelect").val(dataItem.Name);
    if (inputId == "Product") {
        getProductData(inputId);
    }

}

function OnChangeRecord(e) {
    var inputId = this.element[0].id;
    var newName = $("#" + inputId).val();
    var oldName = $("#" + inputId + "NameOnSelect").val();
    if (newName != oldName) {
        $("#" + inputId + "Id").val("");
        $("#" + inputId + "UOMDiv").addClass("d-none");
        $("#" + inputId + "QuantityDiv").addClass("d-none");
        resetPriceAndVendor();
    }
}

function getProductData(inputId) {
    var product = $("#" + inputId + "Id").val();

    $.ajax({
        url: "../api/Product/GetProductDetail?productId=" + product,
        type: 'GET',
        success: function (data) {
            if (data.SecondaryUOM == 0) {
                $("#" + inputId + "QuantityDiv").removeClass("d-none");
                $("#" + inputId + "UOMDiv").addClass("d-none");
                $("#" + inputId + "Quantity").attr("required", true);
            }
            else {
                $("#" + inputId + "QuantityDiv").addClass("d-none");
                $("#" + inputId + "UOMDiv").removeClass("d-none");
                $("#" + inputId + "Quantity").attr("required", false);


                $("#" + inputId + "PrimaryUOM").html("<b>" + data.PrimaryUOMName + "</b>");
                $("#" + inputId + "PrimaryUOMValue").val(data.PrimaryUOMName);
                $("#" + inputId + 'SecondaryUOM').html("<b>" + data.SecondaryUOMName + "</b>");
                $("#" + inputId + "SecondaryUOMValue").val(data.SecondaryUOM);
                $("#" + inputId + 'PrimaryPrice').val(data.PrimaryUnitPrice);
                $("#" + inputId + 'SecondaryPrice').val(data.SecondaryUnitPrice);
                $("#" + inputId + "conversionName").html(data.PrimaryUOMName + " per " + data.SecondaryUOMName);
                $("#" + inputId + 'Conversion').val(data.ConversionFactor);
            }

        },
        error: function (data) {
        }
    });
}

function onChangePrimaryCheckBox(inputId) {

    if ($("#" + inputId + "Primary_switch").is(":checked")) {

        $("#" + inputId + "PrimaryQuantity").prop("disabled", false);
        $("#" + inputId + "PrimaryQuantity").attr("required", true);
        $("#" + inputId + "Secondary_switch").prop("checked", false);
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").val("");
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
    }

    else {
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
        $("#" + inputId + "PrimaryQuantity").val("");
    }
}

function onChangeSecondaryCheckBox(inputId) {

    if ($("#" + inputId + "Secondary_switch").is(":checked")) {

        $("#" + inputId + "SecondaryQuantity").prop("disabled", false);
        $("#" + inputId + "SecondaryQuantity").attr("required", true);
        $("#" + inputId + "Primary_switch").prop("checked", false);
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").val("");
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
    }
    else {
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
        $("#" + inputId + "SecondaryQuantity").val("");
    }
}

function addNewRowToGrid(inputId) {

    let ProductId = $("#" + inputId + "Id").val();
  
    let primaryQuantity = 0;
    let secondaryQuantity = 0;
    let conversionFactor;
    let totalQuantity;
    let UnitPrice = 0;
   
    let QuantitydivClass = $("#ProductUOMDiv").attr("class");

    if (QuantitydivClass.includes("d-none") != true) {

        if ($("#" + inputId + "Primary_switch").prop("checked")) {
            primaryQuantity = $("#" + inputId + 'PrimaryQuantity').val() == "" ? 0 : $("#" + inputId + 'PrimaryQuantity').val();
            totalQuantity = primaryQuantity;
        }
        else {
            secondaryQuantity = $("#" + inputId + 'SecondaryQuantity').val() == "" ? 0 : $("#" + inputId + 'SecondaryQuantity').val();
            conversionFactor = $("#" + inputId + 'Conversion').val();
            totalQuantity = secondaryQuantity * conversionFactor;
        }
    }
    else {
        totalQuantity = $("#ProductQuantity").val();
    }

    UnitPrice = $("#UnitPrice").val();
    
    var rowData = {};
    rowData.ProductId = ProductId;
    rowData.ProductName = $("#ProductNameOnSelect").val();
    rowData.ProductPrimaryQuantity = primaryQuantity == 0 ? "-" : primaryQuantity;
    rowData.ProductSecondaryQuantity = secondaryQuantity == 0 ? "-" : secondaryQuantity;
    rowData.UnitPrice = $("#UnitPrice").val();
    rowData.totalQuantity = totalQuantity;
    rowData.NetAmount = totalQuantity * UnitPrice;


    if (validateAddProduct(ProductId)) {
        var table = $('#PurchaseOrderTable').DataTable();
        table.row.add({
            "Product": rowData.ProductName,
            "PrimaryQuantity": rowData.ProductPrimaryQuantity,
            "SecondaryQuantity": rowData.ProductSecondaryQuantity,
            "UnitPrice": rowData.UnitPrice,
            "TotalQuantity": rowData.totalQuantity,
            "NetAmount": rowData.NetAmount,
            "ProductId": rowData.ProductId

        }).draw();
        reset();
        CalculateNetAmount();
    }
}

function AddPurchaseOrder() {
    var formId = "form";
    
    if (validate(formId)) {
        var PurchaseOrder = {
            PurchaseQuotationId: $("#PurchaseQuotationId").val(),
            LocationId: $("#LocationId").val(),
            VendorId: $("#VendorId").val(),
            VoucherNo: $("#VoucherNo").val(),
            OrderDate: $("#OrderDate").val(),
            TotalNetAmount: $("#TotalNetAmount").text(),
            Remarks: $("#Remarks").val()
        };
        
        PurchaseOrder.PurchaseDetails = [];
        var table = $('#PurchaseOrderTable').DataTable();
        var data = table.rows().data();
        data.each(function (value, index) {
            var Detail = {};
            Detail.ProductId = value.ProductId;
            Detail.PrimaryQuantity = value.PrimaryQuantity;
            Detail.SecondaryQuantity = value.SecondaryQuantity;
            Detail.TotalQuantity = value.TotalQuantity;
            Detail.UnitPrice = value.UnitPrice;
            Detail.NetAmount = value.NetAmount;

            PurchaseOrder.PurchaseDetails.push(Detail);
        });

        ShowLoader();
        $.ajax({
            url: '../api/PurchaseOrder/Post/',
            type: 'POST',
            data: JSON.stringify(PurchaseOrder),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                HideLoader();
                toastr.success("Purchase Order Added Successfully", { timeOut: 5000 });
                ResetForm();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                HideLoader();
                toastr.error(response, { timeOut: 5000 });
            }
        });
    }
}

function RemoveGridProduct(productId) {
    var actionsHtml =
        ` <i class="bx bx-x" onclick="removeEntry(this)" title="Cancelled"></i> `;

    return actionsHtml;
}

function removeEntry(rowPointer) {

    var table = $('#PurchaseOrderTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
    var row = table
        .row($(rowPointer).closest("tr")[0])
        .remove()
        .draw();

    CalculateNetAmount();
}

function populateData(id) {

    PurchaseOrderId = id;
    $.ajax({
        url: "../api/PurchaseOrder/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            dataTable.initialize();

            $("#PurchaseReferenceNumber").val(data.PurchaseReferenceNumber);
            $("#PurchaseQuotationId").val(data.PurchaseQuotationId);
            LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
            $("#ProductId").val(data.ProductId);
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId);
            LoadKendoAutoComplete('Vendor', '-- Please Select Vendor --', '../api/Vendor/GetActiveVendor?LocationId=' + data.LocationId);
            $("#VendorId").val(data.VendorId);
            $("#Vendor").val(data.VendorName);
            $("#VendorNameOnSelect").val(data.VendorName);
            $("#VoucherNo").val(data.VoucherNo);
            $("#OrderDate").val(data.OrderDate.substr(0, 10));
            $("#TotalNetAmount").html(data.TotalNetAmount);
            totalnetamount = parseInt(data.TotalNetAmount);
            $("#Remarks").val(data.Remarks);

            if (data.OrderStatus != "Open") {
                $("#updateBtn").addClass("d-none");
                $("#AddProduct").addClass('d-none');
            }

            var table = $("#PurchaseOrderTable").DataTable();
            $.each(data.PurchaseDetails, function (index, value) {
                table.row.add(
                    {
                        "Product": value.ProductName,
                        "PrimaryQuantity": value.PrimaryQuantity,
                        "SecondaryQuantity": value.SecondaryQuantity,
                        "TotalQuantity": value.TotalQuantity,
                        "UnitPrice": value.UnitPrice,
                        "NetAmount": value.NetAmount,
                        "ProductId": value.ProductId
                    }).draw();
            });
            
        }
    });
}

function update() {
    var formId = "form";
    if (validate(formId)) {
        
        var PurchaseOrder = {

            PurchaseQuotationId: $("#PurchaseQuotationId").val(),
            LocationId: $("#LocationId").val(),
            VendorId: $("#VendorId").val(),
            VoucherNo: $("#VoucherNo").val(),
            OrderDate: $("#OrderDate").val(),
            TotalNetAmount: $("#TotalNetAmount").text(),
            Remarks: $("#Remarks").val(),
            Id: PurchaseOrderId

        };

        PurchaseOrder.PurchaseDetails = [];
        var table = $('#PurchaseOrderTable').DataTable();
        var data = table.rows().data();
        data.each(function (value, index) {
            var Detail = {};
            Detail.ProductId = value.ProductId;
            Detail.PrimaryQuantity = value.PrimaryQuantity;
            Detail.SecondaryQuantity = value.SecondaryQuantity;
            Detail.TotalQuantity = value.TotalQuantity;
            Detail.UnitPrice = value.UnitPrice;
            Detail.NetAmount = value.NetAmount;

            PurchaseOrder.PurchaseDetails.push(Detail);
        });
        ShowLoader();
        $.ajax({
            url: '../api/PurchaseOrder/Put/',
            type: 'POST',
            data: JSON.stringify(PurchaseOrder),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                HideLoader();
                ResetForm();
                toastr.success("Purchase Order Update Successfully", { timeOut: 5000 });
                RedirectToUrl('../PurchaseOrderManagement/Manage');
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}

function ResetForm() {
    UnselectKendoDropDown("LocationId");
    $("#LocationId").val("");
    $("#PurchaseReferenceNumnber").val("");
    $("#VendorId").val("");
    $("#Vendor").val("");
    $("#VoucherNo").val("");
    $("#OrderDate").val("");
    $("#Vendor").val("");
    $("#TotalNetAmount").text(0);
    $("#Remarks").val("");
    var table = $('#PurchaseOrderTable').DataTable();
    table.clear().draw();

}

function reset() {
    UnselectKendoDropDown("ProductId");
    $("#Product").val('');
    $("#UnitPrice").val('');
    $("#ProductQuantityDiv").addClass("d-none");
    $("#ProductUOMDiv").addClass("d-none");
    $("#ProductQuantity").val("");
    $("#ProductPrimaryUOM").html("");
    $("#ProductPrimary_switch").prop("checked", false);
    $("#ProductPrimaryQuantity").val('');
    $("#ProductPrimaryQuantity").prop("disabled", true);
    $("#ProductSecondaryQuantity").prop("disabled", true);
    $("#ProductSecondaryUOM").html("");
    $("#ProductSecondary_switch").prop("checked", false);
    $("#ProductSecondaryQuantity").val('');
    $("#ProductconversionName").html("");
    $("#ProductConversion").val('');

}

function resetPriceAndVendor() {
    let ProductId = $("#ProductId").val();

    if (ProductId == "") {
        $("#UnitPrice").val("");
    }
}

function validateAddProduct(productId) {
    let product = $('#ProductId').val();
    let price = $('#UnitPrice').val();
    let primary_switch = $('#ProductPrimary_switch').prop("checked");
    let secondary_switch = $('#ProductSecondary_switch').prop("checked");
    let primary_coversion_text = $("#ProductPrimaryUOM").text();
    let secondary_coversion_text = $("#ProductSecondaryUOM").text();
    let primary_quantity = $('#ProductPrimaryQuantity').val();
    let secondary_quantity = $('#ProductSecondaryQuantity').val();
    let quantity = $('#ProductQuantity').val();
    let QuantitydivClass = $("#ProductUOMDiv").attr("class");


    if (product == "") {
        toastr.warning("Please Select Product", { timeOut: 5000 });
        return false;
    }

    if (price == "") {
        toastr.warning("Please Enter Price", { timeOut: 5000 });
        return false;
    }

    if (QuantitydivClass.includes("d-none") == true) {

        if (quantity == "") {
            toastr.warning("Please Write Quantity", { timeOut: 5000 });
            return false;
        }
    }
    else {
        if (primary_switch == false && secondary_switch == false) {
            toastr.warning("Please Check either " + primary_coversion_text + " or " + secondary_coversion_text, { timeOut: 5000 });
            return false;
        }
    }

    if (primary_switch == true) {
        if (primary_quantity == "") {
            toastr.warning("Please Write Quantity", { timeOut: 5000 });
            return false;
        }

    }

    if (secondary_switch == true) {
        if (secondary_quantity == "") {
            toastr.warning("Please Write Quantity", { timeOut: 5000 });
            return false;
        }

    }


    var table = $('#PurchaseOrderTable').DataTable();
    var tableData = table.rows().data();
    var IsproductAdded = false;
    tableData.each(function (value, index) {

        if ((value.ProductId == productId)) {
            toastr.warning('This Product is already added. Please select another Product.', { timeOut: 5000 });
            IsproductAdded = true;

        }
    });
    if (IsproductAdded) {
        return false;
    }

    return true;
}

function GetPurchaseQuotation() {
    //$("#PurchaseQuotationDetailModal").toggle("modal");
    $("#PurchaseQuotationDetailModal").modal("show");
    var dtCommonParam = {
        getDataUrl: `../api/PurchaseQuotation/GetAll`,
        tableId: 'PurchaseQuotationDetailTable'
    };
    $('#PurchaseQuotationDetailTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "ReferenceNumber" },
            { "data": "Name" },
            { "data": "LocationId", "visible": false },
            { "data": "PriorityLevelName" },
            { "data": "PriorityLevel", "visible": false },
            { "data": "PurchaseTypeName" },
            { "data": "PurchaseTypeId", "visible": false }
        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ]

    });
}

function GetPurchaseQuotationDetail(id) {

    ResetForm();

    $.ajax({
        url: "../api/PurchaseQuotation/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            
            $("#PurchaseReferenceNumnber").val(data.ReferenceNumber);
            $("#PurchaseQuotationId").val(id);
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId);
            $("#LocationId").prop('readOnly', true);
            $("#Vendor").data("kendoAutoComplete").value(data.VendorName);
            $("#VendorNameOnSelect").val(data.VendorName);
            $("#Vendor").prop('readOnly', true);
            $("#VendorId").val(data.VendorId);

            $('#PurchaseOrderTable').DataTable().clear().draw();

            var table = $('#PurchaseOrderTable').DataTable();
            data.PurchaseQuotationDetail.forEach(function (quote, index) {
                if (quote.isChecked == true) {
                    table.row.add({
                        "Product": quote.Product,
                        "PrimaryQuantity": quote.PrimaryQuantity == 0 ? "-" : quote.PrimaryQuantity,
                        "SecondaryQuantity": quote.SecondaryQuantity == 0 ? "-" : quote.SecondaryQuantity,
                        "UnitPrice": quote.Price,
                        "TotalQuantity": quote.TotalQuantity,
                        "NetAmount": quote.NetAmount,
                        "ProductId": quote.ProductId
                    }).draw();
                }
            });
            CalculateNetAmount();
        }
    });
}

function CalculateNetAmount() {
    
    var table = $("#PurchaseOrderTable").DataTable();
    var data = table.rows().data();
    let totalnetamount = 0;
    data.each(function (value, index) {

        totalnetamount += value.NetAmount;
    });
    $("#TotalNetAmount").text(totalnetamount);
}