﻿var VehicleId = 0;
function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    LoadKendoDropDown('VehicleType', '-- Please Select Vehicle Type --', '../api/VehicleType/GetActiveVehicleType');
};
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var Vehicle = {
            LocationId: $("#LocationId").val(),
            VehicleTypeId: $("#VehicleType").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/Vehicle/Post/',
            type: 'POST',
            data: JSON.stringify(Vehicle),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Vehicle Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var Vehicle = {
            LocationId: $("#LocationId").val(),
            VehicleTypeId: $("#VehicleType").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: VehicleId
        };
        ShowLoader();
        $.ajax({
            url: '../api/Vehicle/Put/',
            type: 'POST',
            data: JSON.stringify(Vehicle),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Vehicle Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../VehicleManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    VehicleId = id;
    ShowLoader();
    $.ajax({
        url: "../api/Vehicle/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId),
            LoadKendoDropDown('VehicleType', '-- Please Select Vehicle Type --', '../api/VehicleType/GetActiveVehicleType');
            $("#VehicleType").val(data.VehicleTypeId),
           
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {
    $("#LocationId").val();
    $("#VehicleType").val();
    $('#Code').val("");
    $('#Name').val("");
    UnselectKendoDropDown("Location")
    UnselectKendoDropDown("VehicleType")
}