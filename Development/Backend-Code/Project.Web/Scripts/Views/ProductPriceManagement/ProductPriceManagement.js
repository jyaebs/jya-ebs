﻿var ProductPriceId = 0;  
function initializeForm() {

    LoadKendoDropDown('ProductId', '-- Please Select Product --', '../api/ProductMaster/GetAllActiveProduct');
    LoadKendoDropDown('ProductPriceTypeId', '-- Please Select Product Type --', '../api/Development/GetAllActiveProductPriceType');
};
function GetLocation() {
    debugger;
    var PriceType = $("#ProductPriceTypeId").val();
    if (PriceType == 2) {
        $(".locationtype").removeClass("d-none");
        LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    } 
    else {
        $(".locationtype").addClass("d-none");

    }
}
function Add() {
    var formId = "form";
    if (validate(formId)) {
        var ProductPrice = {

            
            ProductId: $("#ProductId").val(),
            ProductPriceTypeId: $("#ProductPriceTypeId").val(),
            Price: $("#PriceId").val(),
            LocationId: $("#LocationId").val(),
        };
        ShowLoader();
        debugger;
        $.ajax({


            url: '../api/ProductPrice/Post/',
            type: 'POST',
            data: JSON.stringify(ProductPrice),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Price Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var ProductPrice = {

            LocationId: $("#LocationId").val(),
            ProductId: $("#ProductId").val(),
            Price: $("#PriceId").val(),
            ProductPriceTypeId: $("#ProductPriceTypeId").val(),
            Id: ProductPriceId
        };
        ShowLoader();
        $.ajax({
            url: '../api/ProductPrice/Put/',
            type: 'POST',
            data: JSON.stringify(ProductPrice),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Price Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../ProductPriceManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    ProductPriceId = id;
    ShowLoader();
    $.ajax({
        url: "../api/ProductPrice/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            LoadKendoDropDown('LocationId', '-- Please Select Product --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId),
            LoadKendoDropDown('ProductId', '-- Please Select Product --', '../api/ProductMaster/GetAllActiveProduct');
            $("#ProductId").val(data.ProductId),
            LoadKendoDropDown('ProductPriceTypeId', '-- Please Select Product Type --', '../api/Development/GetAllActiveProductPriceType');
            $("#ProductPriceTypeId").val(data.ProductPriceTypeId),
            $("#PriceId").val(data.Price),
               

            HideLoader();
        }
    });
};
function ResetForm()
{
        $("#LocationId").val(),
        $("#ProductId").val(),
        $("#ProductPriceTypeId").val(),
        $("#PriceId").val(""),
        UnselectKendoDropDown("LocationId"),
        UnselectKendoDropDown("ProductId"),
        UnselectKendoDropDown("ProductPriceTypeId")


        
}