﻿let WHTId = 0;
var entryLevelId = "";

function initializeForm() {
    $("#AccountLevelTable tbody").on("click", "tr", function () {
        var table = $('#AccountLevelTable').DataTable();
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == "WHT") {
            $("#WHTAccountCode").val(rowAccountCode);
            $("#WHTAccountDescription").val(rowAccountName);
        }


        $("#AddAccountModal").modal("hide")


    });
}


function GetChartOfAccount(entryId) {
    entryLevelId = entryId;
    $("#AddAccountModal").toggle("modal");
    initializeDataTable();
}


function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetEntryLevelCOALevels`,
        tableId: 'AccountLevelTable'
    };
    $('#AccountLevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}

function Add() {
    var formId = "form";
    if (validate(formId)) {
        var WHT = {
            Name: $('#Name').val(),
            Value: $('#Value').val(),
            AccountWHT: $('#WHTAccountCode').val(),
        };
        ShowLoader();
        $.ajax({
            url: '../api/WHT/Post/',
            type: 'POST',
            data: JSON.stringify(WHT),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("With Holding Tax Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};

function Update() {
    var formId = "form";
    if (validate(formId)) {
        var WHT = {
            Value: $('#Value').val(),
            Name: $('#Name').val(),
            AccountWHT: $('#WHTAccountCode').val(),
            Id: WHTId
        };
        ShowLoader();
        $.ajax({
            url: '../api/WHT/Put/',
            type: 'POST',
            data: JSON.stringify(WHT),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("With Holding Tax Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../WHTManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};

function populateData(id) {
    WHTId = id;
    ShowLoader();
    $.ajax({
        url: "../api/WHT/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#Value').val(data.Value);
            $('#Name').val(data.Name);
            $('#WHTAccountCode').val(data.AccountWHT);
            $('#WHTAccountDescription').val(data.AccountWHTDescription);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Value').val("");
    $('#Name').val("");
    $('#WHTAccountCode').val("");
    $('#WHTAccountDescription').val("");

}