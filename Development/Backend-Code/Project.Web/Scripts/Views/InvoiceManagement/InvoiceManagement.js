﻿var InvoiceId = 0;
var DeliveryNoteId = 0;
var GstId = 0;
var GrossAmount = 0;
var TotalDiscountAmount = 0;
var NetAmount = 0;
var GAmount = 0;
var NAmount = 0;


$(document).ready(function () {

    initializeForm();
   

    $("#DeliverNoteTable tbody").on("click", "tr", function () {

        let table = $('#DeliverNoteTable').DataTable();
        let DeliveryNoteId = table.rows().data()[this.rowIndex - 1].Id;
        let Description = table.rows().data()[this.rowIndex - 1].Description;
        let PrimaryQuantity = table.rows().data()[this.rowIndex - 1].PrimaryQuantity;
        let SecondaryQuantity = table.rows().data()[this.rowIndex - 1].SecondaryQuantity;
        let TotalQuantity = table.rows().data()[this.rowIndex - 1].TotalQuantity;
        let DeliverQuantity = table.rows().data()[this.rowIndex - 1].DeliverQuantity;
        let Warehouse = table.rows().data()[this.rowIndex - 1].Warehouse;
        let GrossAmount = table.rows().data()[this.rowIndex - 1].GrossAmount;
        let Discount = table.rows().data()[this.rowIndex - 1].Discount;
        let DiscountAmount = table.rows().data()[this.rowIndex - 1].DiscountAmount;
        let NetAmount = table.rows().data()[this.rowIndex - 1].NetAmount;
        let ProductId = table.rows().data()[this.rowIndex - 1].ProductId;

        debugger;

        GetDeliveryNoteDetail(DeliveryNoteId);
        $("#GetDeliverNoteModal").modal("hide");
    });
});

function initializeForm() {

    LoadKendoDropDown('GSTId', '-- Please Select GST --', '../api/GST/GetGSTById');
    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
    LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '');


    dataTable.initialize();
}

function LoadCustomers(id) {
    debugger;
    var LocationId = $("#" + id).val();
    if (LocationId != "") {
        LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + LocationId);
    }

}


var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#InvoiceTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            destroy: true,
            "columns": [
                { "data": "Description" },
                { "data": "PrimaryQuantity" },
                { "data": "SecondaryQuantity" },
                { "data": "TotalQuantity" },
                { "data": "Warehouse" },
                { "data": "WarehouseId", "visible": false },
                { "data": "UnitPrice" },
                { "data": "GrossAmount" },
                { "data": "Discount" },
                { "data": "DiscountAmount" },
                { "data": "NetAmount" },
                { "data": "ProductId", className: "text-center" },
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        var actionsHtml =
                            ` <i class="mdi mdi-close red  font-size-14" onclick="removeEntry(this)" title="Cancelled"></i> `;

                        return actionsHtml;
                    },
                    "targets": -1
                },
            
                {
                    orderable: false,
                    targets: "no-sort",
                }
            ],
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};


function onSelectRecord(e) {
    debugger;
    var inputId = this.element[0].id;
    var dataItem = this.dataItem(e.item.index());

    $("#" + inputId + "Id").val(dataItem.Id);
    $("#" + inputId + "NameOnSelect").val(dataItem.Name);
    if (inputId == "Product") {
        getProductData(inputId);
    }

}
function OnChangeRecord(e) {
    debugger;
    var inputId = this.element[0].id;
    var newName = $("#" + inputId).val();
    var oldName = $("#" + inputId + "NameOnSelect").val();
    if (newName != oldName) {
        $("#" + inputId + "Id").val("");
        $("#" + inputId + "UOMDiv").addClass("d-none");
        $("#" + inputId + "QuantityDiv").addClass("d-none");
    }
}

function getProductData(inputId) {
    var product = $("#" + inputId + "Id").val();

    $.ajax({
        url: "../api/Product/GetProductDetail?productId=" + product,
        type: 'GET',
        success: function (data) {
            if (data.SecondaryUOM == 0) {
                $("#" + inputId + "QuantityDiv").removeClass("d-none");
                $("#" + inputId + "UOMDiv").addClass("d-none");
                $("#" + inputId + "Quantity").attr("required", true);
            }
            else {
                $("#" + inputId + "QuantityDiv").addClass("d-none");
                $("#" + inputId + "UOMDiv").removeClass("d-none");
                $("#" + inputId + "Quantity").attr("required", false);


                $("#" + inputId + "PrimaryUOM").html("<b>" + data.PrimaryUOMName + "</b>");
                $("#" + inputId + "PrimaryUOMValue").val(data.PrimaryUOMName);
                $("#" + inputId + 'SecondaryUOM').html("<b>" + data.SecondaryUOMName + "</b>");
                $("#" + inputId + "SecondaryUOMValue").val(data.SecondaryUOM);
                $("#" + inputId + 'PrimaryPrice').val(data.PrimaryUnitPrice);
                $("#" + inputId + 'SecondaryPrice').val(data.SecondaryUnitPrice);
                $("#" + inputId + "conversionName").html(data.PrimaryUOMName + " per " + data.SecondaryUOMName);
                $("#" + inputId + 'Conversion').val(data.ConversionFactor);
            }

        },
        error: function (data) {
        }
    });
}
function onChangePrimaryCheckBox(inputId) {

    if ($("#" + inputId + "Primary_switch").is(":checked")) {

        $("#" + inputId + "PrimaryQuantity").prop("disabled", false);
        $("#" + inputId + "PrimaryQuantity").attr("required", true);
        $("#" + inputId + "Secondary_switch").prop("checked", false);
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").val("");
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
    }

    else {
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
        $("#" + inputId + "PrimaryQuantity").val("");
    }
}

function onChangeSecondaryCheckBox(inputId) {

    if ($("#" + inputId + "Secondary_switch").is(":checked")) {

        $("#" + inputId + "SecondaryQuantity").prop("disabled", false);
        $("#" + inputId + "SecondaryQuantity").attr("required", true);
        $("#" + inputId + "Primary_switch").prop("checked", false);
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").val("");
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
    }
    else {
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
        $("#" + inputId + "SecondaryQuantity").val("");
    }
}

function addNewRowToGrid(inputId) {
    ProductId = $("#" + inputId + "Id").val();
    var variationId;
    var product;

    if ($(ProductId.search("-"))[0] > 0) {
        product = ProductId.split("-")[0];
        variationId = ProductId.split("-")[1];
    }
    else {
        product = ProductId;
        variationId = 0;
    }
    let primaryQuantity = 0;
    let secondaryQuantity = 0;
    let conversionFactor;
    let totalQuantity;
    let UnitPrice = 0;
    let Discount = 0;
    let DiscountAmount = 0;
    let NetAmount;
    let Warehouse = 0;
    var TotalNetAmount = 0;



    let QuantitydivClass = $("#ProductUOMDiv").attr("class");

    if (QuantitydivClass.includes("d-none") != true) {

        if ($("#" + inputId + "Primary_switch").prop("checked")) {
            primaryQuantity = $("#" + inputId + 'PrimaryQuantity').val() == "" ? 0 : $("#" + inputId + 'PrimaryQuantity').val();
            totalQuantity = primaryQuantity;
        }
        else {
            secondaryQuantity = $("#" + inputId + 'SecondaryQuantity').val() == "" ? 0 : $("#" + inputId + 'SecondaryQuantity').val();
            conversionFactor = $("#" + inputId + 'Conversion').val();
            totalQuantity = secondaryQuantity * conversionFactor;
        }

    }


    else {
        totalQuantity = $("#ProductQuantity").val();
    }
    UnitPrice = $("#UnitPrice").val();
    Discount = $("#Discount").val();
    var rowData = {}

    rowData.ProductId = ProductId;
    rowData.ProductName = $("#ProductNameOnSelect").val();
    rowData.ProductPrimaryQuantity = $("#ProductPrimaryQuantity").val();
    rowData.ProductSecondaryQuantity = $("#ProductSecondaryQuantity").val();
    rowData.Totalquantity = totalQuantity;
    rowData.UnitPrice = UnitPrice;
    rowData.GrossAmount = totalQuantity * UnitPrice;
    rowData.Discount = Discount;
    rowData.DiscountAmount = rowData.GrossAmount / Discount;
    rowData.NetAmount = rowData.GrossAmount - rowData.DiscountAmount;


    var InvoiceTable = $('#InvoiceTable').DataTable();

    var data = InvoiceTable.rows().data();
    var isAdded = true;
    data.each(function (i, item) {
        if (rowData.ProductId == i.ProductId) {
            isAdded = false;

        }
    });

    if (isAdded) {
        var table = $('#InvoiceTable').DataTable();

        table.row.add({
            "Description": rowData.ProductName,
            "PrimaryQuantity": rowData.ProductPrimaryQuantity,
            "SecondaryQuantity": rowData.ProductSecondaryQuantity,
            "TotalQuantity": rowData.Totalquantity,
            "Warehouse": rowData.Warehouse,
            "UnitPrice": rowData.UnitPrice,
            "Discount": rowData.Discount,
            "GrossAmount": rowData.GrossAmount,
            "DiscountAmount": rowData.DiscountAmount,
            "NetAmount": rowData.NetAmount,
            "ProductId": rowData.ProductId,

        }).draw();

        CalculateAmounts();

    }
    else {
        toastr.error("Product is Already Exist", { timeOut: 5000 });
    }

}

function ResetModal() {
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');

    $("#Discount").val("");
    $("#ProductSecondaryQuantity").val("");
    $("#ProductPrimaryQuantity").val("");
    $("#UnitPrice").val("");
}

function GetOpenDeliveryNote() {
    debugger;
    $("#GetDeliveryNoteModal").toggle("modal");
    initializeDeliveryNoteTable();
}

function initializeDeliveryNoteTable() {

    var dtCommonParam = {
        getDataUrl: `../api/DeliveryNote/GetOpenDeliveryNote`,
        tableId: 'DeliveryNoteDetailTable'
    };
    $('#DeliveryNoteTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "Post",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Name" },
            { "data": "CustomerName" },
            { "data": "orderdatestring" },
            { "data": "LocationId", "visible": false },

        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ],

    });
}

function GetDeliveryNoteDetail(DeliveryNoteId) {
    Id = DeliveryNoteId;
    GstId = 0;
    debugger;
    $.ajax({
        url: "../api/DeliveryNote/GetById?id=" + DeliveryNoteId,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId);
            LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + data.LocationId);
            $("#CustomerId").val(data.CustomerId);
            $("#Customer").val(data.CustomerName);
            $("#SaleReferenceNumber").val(data.ReferenceNumber);
            $("#TotalNetAmount").html(data.TotalNetAmount);
            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
            $("#TotalGrossAmount").html(data.TotalGrossAmount);
            var table = $("#InvoiceTable").DataTable();
            debugger;
            $.each(data.DeliveryNoteDetail, function (index, value) {
               debugger;
                table.row.add({
                    "Description": value.ProductName,
                    "PrimaryQuantity": value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "Warehouse": value.WarehouseName,
                    "WarehouseId": value.WarehouseId,
                    "UnitPrice": value.UnitPrice,
                    "GrossAmount": value.GrossAmount,
                    "Discount": value.Discount,
                    "DiscountAmount": value.DiscountAmount,
                    "NetAmount": value.NetAmount,
                    "Id": value.Id,
                    "ProductId": value.ProductId,
                    
                }).draw();
               
            });
            HideLoader();
        }
    });
}

function removeEntry(rowPointer) {
    debugger;

    var table = $('#InvoiceTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
    var row = table
        .row($(rowPointer).closest("tr")[0])
        .remove()
        .draw();
    CalculateAmounts();
}

function CalculateAmounts() {
    debugger;

    let GrossAmount = 0;
    let TotalDiscountAmount = 0;
    let NetAmount = 0;

    var table = $("#InvoiceTable").DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {
        debugger;
        if (GstId != 0) {
            GrossAmount += GAmount;
            TotalDiscountAmount += value.DiscountAmount;
            NetAmount += value.NetAmount;
            $("#TotalGrossAmount").text(GAmount);
            $("#TotalNetAmount").text(NAmount);
            $("#NetAmount").text(NetAmount);
        }
        else {
            GrossAmount += value.GrossAmount;
            TotalDiscountAmount += value.DiscountAmount;
            NetAmount += value.NetAmount;
            }
    });
    $("#TotalGrossAmount").text(GrossAmount);
    $("#TotalDiscountAmount").text(TotalDiscountAmount);
    $("#TotalNetAmount").text(NetAmount);
}


function GETGST() {
    if (GSTId != null) {
        var id = $("#GSTId").val();
        debugger
        if (id != null)
            $.ajax({
                url: "../api/GST/GetById?id=" + id,
                type: 'GET',
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    debugger;
                    var table = $("#InvoiceTable").DataTable();
                    var tabledata = table.data();
                    tabledata.clear().draw();
                    $.each(tabledata, function (index, value) {
                        debugger;
                        var gst = data.Value;
                        var VgrossAmount = value.GrossAmount;
                        var DAmount = value.DiscountAmount;
                        GAmount = VgrossAmount - gst;
                        NAmount = GAmount - DAmount
                        table.row.add({
                            "Description": value.Description,
                            "PrimaryQuantity": value.PrimaryQuantity,
                            "SecondaryQuantity": value.SecondaryQuantity,
                            "TotalQuantity": value.TotalQuantity,
                            "ReturnQuantity": value.ReturnQuantity,
                            "Warehouse": value.Warehouse,
                            "WarehouseId": value.WarehouseId,
                            "GSTId": data.Value,
                            "UnitPrice": value.UnitPrice,
                            "GrossAmount": GAmount,
                            "Discount": value.Discount,
                            "DiscountAmount": value.DiscountAmount,
                            "NetAmount": NAmount,
                            "Id": value.Id,
                            "ProductId": value.ProductId,
                        }).draw();
                        $(`#DeliverQuantity-${value.ProductId}`).val(value.ReturnQuantity);
                        CalculateAmounts();

                    });
                }
            })
    }

}

function AddInvoice() {

    var formId = "form";
    if (validate(formId)) {
        var Invoice = {
            DeliveryNoteId: DeliveryNoteId,
            LocationId: $("#LocationId").val(),
            CustomerId: $("#CustomerId").val(),

            DeliveryDate: $("#DeliveryDate").val(),
            TotalNetAmount: $("#TotalNetAmount").text(),
            TotalDiscountAmount: $("#TotalDiscountAmount").text(),
            TotalGrossAmount: $("#TotalGrossAmount").text(),
            GST: $("#GSTId").val(),
            Remarks: $("#Remarks").val(),
        };

        Invoice.InvoiceDetail = [];
        debugger;
        var table = $('#InvoiceTable').DataTable();

        var data = table.rows().data();
        data.each(function (value, index) {
            debugger;
            var Details = {};
            Details.ProductId = value.ProductId;
            Details.PrimaryQuantity = value.PrimaryQuantity;
            Details.SecondaryQuantity = value.SecondaryQuantity;
            Details.TotalQuantity = value.TotalQuantity;
            Details.UnitPrice = value.UnitPrice;
            Details.WarehouseId = $(`#WarehouseId-${value.ProductId}`).val();
            Details.GST = value.GSTId;
            Details.GrossAmount = value.GrossAmount;
            Details.Discount = value.Discount;
            Details.DiscountAmount = value.DiscountAmount;
            Details.NetAmount = value.NetAmount;
            Invoice.InvoiceDetail.push(Details);
        });
        debugger;
        ShowLoader();
        $.ajax({
            url: '../api/Invoice/Post/',
            type: 'POST',
            data: JSON.stringify(Invoice),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Invoice Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();

            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.warning(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}

//function GetGrossAmount(rowValue) {

//    newGrossAmount = value.GrossAmount - data.Value ;
//    table.cell({ row: row_index, column: 9 }).data(newGrossAmount);
//    CalculateAmounts();
//}