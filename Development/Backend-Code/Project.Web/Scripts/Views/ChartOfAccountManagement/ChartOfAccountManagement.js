﻿var LevelId = 0;
var AccountLevelName = "";
var AccountLevelDescription = "";


function initializeForm() {

    LoadKendoDropDown('AccountType', '-- Please Select Account Type --', '../api/Developer/GetAllChartOfAccountTypes');
    

    $("#COALevelTable tbody").on("click", "tr", function () {
        var table = $('#COALevelTable').DataTable();
      //  var data = table.rows().data();
        var rowId = table.rows().data()[this.rowIndex - 1].Id;
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (LevelId == "1") {
            $("#AccountLevel1").val(rowAccountCode);
            $("#AccountDescription1").val(rowAccountName);
        }

        else if (LevelId == "2") {
            $("#AccountLevel2").val(rowAccountCode);
            $("#AccountDescription2").val(rowAccountName);
        }
        else if (LevelId == "3") {
            $("#AccountLevel3").val(rowAccountCode);
            $("#AccountDescription3").val(rowAccountName);
        }
        else if (LevelId == "4") {
            $("#AccountLevel4").val(rowAccountCode);
            $("#AccountDescription4").val(rowAccountName);
        }
        else {
            $("#AccountLevel5").val(rowAccountCode);
            $("#AccountDescription5").val(rowAccountName);
        }

        $("#AddCOAModal").modal("hide")


    });

};


function displayCOALevel() {
    if ($("#COALevel").val() == "") {
        $("#levelDiv2").addClass("d-none")
        $("#DescriptionDiv2").addClass("d-none")
        $("#AccountLevel2").val("")
        $("#AccountDescription2").val("")

        $("#levelDiv3").addClass("d-none")
        $("#DescriptionDiv3").addClass("d-none")
        $("#AccountLevel3").val("")
        $("#AccountDescription3").val("")

        $("#levelDiv4").addClass("d-none")
        $("#DescriptionDiv4").addClass("d-none")
        $("#AccountLevel4").val("")
        $("#AccountDescription4").val("")

        $("#levelDiv5").addClass("d-none")
        $("#DescriptionDiv5").addClass("d-none")
        $("#AccountLevel5").val("")
        $("#AccountDescription5").val("")
    }


    if ($("#COALevel").val() == "2") {
        $("#levelDiv2").addClass("d-none")
        $("#DescriptionDiv2").addClass("d-none")
        $("#AccountLevel2").val("")
        $("#AccountDescription2").val("")

        $("#levelDiv3").addClass("d-none")
        $("#DescriptionDiv3").addClass("d-none")
        $("#AccountLevel3").val("")
        $("#AccountDescription3").val("")

        $("#levelDiv4").addClass("d-none")
        $("#DescriptionDiv4").addClass("d-none")
        $("#AccountLevel4").val("")
        $("#AccountDescription4").val("")

        $("#levelDiv5").addClass("d-none")
        $("#DescriptionDiv5").addClass("d-none")
        $("#AccountLevel5").val("")
        $("#AccountDescription5").val("")
    }

    if ($("#COALevel").val() == "3") {
        $("#levelDiv2").removeClass("d-none")
        $("#DescriptionDiv2").removeClass("d-none")

        $("#levelDiv3").addClass("d-none")
        $("#DescriptionDiv3").addClass("d-none")
        $("#AccountLevel3").val("")
        $("#AccountDescription3").val("")


        $("#levelDiv4").addClass("d-none")
        $("#DescriptionDiv4").addClass("d-none")
        $("#AccountLevel4").val("")
        $("#AccountDescription4").val("")

        $("#levelDiv5").addClass("d-none")
        $("#DescriptionDiv5").addClass("d-none")
        $("#AccountLevel5").val("")
        $("#AccountDescription5").val("")
    }

    if ($("#COALevel").val() == "4") {
        $("#levelDiv3").removeClass("d-none")
        $("#DescriptionDiv3").removeClass("d-none")

        $("#levelDiv2").removeClass("d-none")
        $("#DescriptionDiv2").removeClass("d-none")

        $("#levelDiv4").addClass("d-none")
        $("#DescriptionDiv4").addClass("d-none")
        $("#AccountLevel4").val("")
        $("#AccountDescription4").val("")
    }

    if ($("#COALevel").val() == "5") {
        $("#levelDiv4").removeClass("d-none")
        $("#DescriptionDiv4").removeClass("d-none")


        $("#levelDiv3").removeClass("d-none")
        $("#DescriptionDiv3").removeClass("d-none")


        $("#levelDiv2").removeClass("d-none")
        $("#DescriptionDiv2").removeClass("d-none")
    }
}


function GetChartOfAccount(level) {
    LevelId = level;
    $("#AddCOAModal").toggle("modal");
    initializeDataTable(level);
}


function initializeDataTable(LevelId) {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetAllCOALevels?LevelId=${LevelId}`,
        tableId: 'COALevelTable'
    };
    $('#COALevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
            // { "data": "ValidFlag", "visible": false },
            //{ "data": "Id", "title": "Action", className: "text-center" }
        ],
        "columnDefs": [
            //{
            //    "render": function (data, type, row) {
            //        var actionsHtml = `<label class="switch pr-4 switch-primary" >
            //                <input type="checkbox" id="Primary_switch" class="generateDispatchSwitch" /> <span class="slider"></span>
            //                    </label>`
            //        return actionsHtml;
            //    },
            //    "targets": -6
            //},
            //{
            //    "render": function (data, type, row) {
            //        return GetgridValidFlagDescription(row.ValidFlag);

            //    },
            //    "targets": -2
            //},

            //{
            //    "render": function (data, type, row) {
            //        return GetAction(row.ValidFlag, row.Id, row.InvoiceReferenceNo, dtCommonParam);
            //    },
            //    "targets": -1
            //},
            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}



function Add() {

    var formId = "form";
    if (validate(formId)) {
        let parentCode;
        let coaLevel = $("#COALevel").val();

        if (coaLevel == "2") {
            parentCode = $("#AccountLevel1").val();
            if (parentCode == "") {
                toastr.error("Please Select Chart of Account Level to Proceed", { timeOut: 5000 });
                return false;
            }
        }

        if (coaLevel == "3") {
            parentCode = $("#AccountLevel2").val();
            if (parentCode == "" || $("#AccountLevel1").val() == "" ) {
                toastr.error("Please Select Chart of Account Level to Proceed", { timeOut: 5000 });
                return false;
            }
        }

        if (coaLevel == "4") {
            parentCode = $("#AccountLevel3").val();
            if (parentCode == "" || $("#AccountLevel1").val() == "" || $("#AccountLevel2").val() == "") {
                toastr.error("Please Select Chart of Account Level to Proceed", { timeOut: 5000 });
                return false;
            }
        }


        if (coaLevel == "5") {
            parentCode = $("#AccountLevel4").val();
            if (parentCode == "" || $("#AccountLevel1").val() == "" || $("#AccountLevel2").val() == "" || $("#AccountLevel3").val() == "") {
                toastr.error("Please Select Chart of Account Level to Proceed", { timeOut: 5000 });
                return false;
            }
        }

        var COA = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            AccountTypeId: $("#AccountType").val(),
            ParentCode: parentCode,
            LevelID: $("#COALevel").val(),
        };
        ShowLoader();
        $.ajax({
            url: '../api/ChartOfAccount/Post/',
            type: 'POST',
            data: JSON.stringify(COA),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Chart of Account Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};


function ResetForm() {
    $('#Code').val('');
    $('#Name').val('');
    UnselectKendoDropDown("AccountType");
    $('#AccountType').val('');
    $('#COALevel').val('');
    $("#AccountLevel1").val("")
    $("#AccountDescription1").val("")
    $("#levelDiv2").addClass("d-none")
    $("#DescriptionDiv2").addClass("d-none")
    $("#AccountLevel2").val("")
    $("#AccountDescription2").val("")

    $("#levelDiv3").addClass("d-none")
    $("#DescriptionDiv3").addClass("d-none")
    $("#AccountLevel3").val("")
    $("#AccountDescription3").val("")

    $("#levelDiv4").addClass("d-none")
    $("#DescriptionDiv4").addClass("d-none")
    $("#AccountLevel4").val("")
    $("#AccountDescription4").val("")

    $("#levelDiv5").addClass("d-none")
    $("#DescriptionDiv5").addClass("d-none")
    $("#AccountLevel5").val("")
    $("#AccountDescription5").val("")
}

