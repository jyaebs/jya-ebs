﻿var BankBranchId = 0;
var entryLevelId = "";

function initializeForm() {

    LoadKendoDropDown('Bank', '-- Please Select Bank --', '../api/Bank/GetActiveBank');


    $("#AccountLevelTable tbody").on("click", "tr", function () {
        var table = $('#AccountLevelTable').DataTable();
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == "BBA") {
            $("#AccountBankBalanceCode").val(rowAccountCode);
            $("#AccountBankBalanceDescription").val(rowAccountName);
        }


        $("#AddAccountModal").modal("hide")


    });
};


function GetChartOfAccount(entryId) {
    entryLevelId = entryId;
    $("#AddAccountModal").toggle("modal");
    initializeDataTable();
}


function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetEntryLevelCOALevels`,
        tableId: 'AccountLevelTable'
    };
    $('#AccountLevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}

function Add() {

    var formId = "form";
    if (validate(formId)) {
        var BankBranch = {
            BankId: $("#Bank").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            AccountBankBalance: $('#AccountBankBalanceCode').val()
        };
        ShowLoader();

        $.ajax({
            url: '../api/BankBranch/Post/',
            type: 'POST',
            data: JSON.stringify(BankBranch),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Bank Branch Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};

function Update() {
    var formId = "form";
    if (validate(formId)) {
        var BankBranch = {
            BankId: $("#Bank").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            AccountBankBalance: $('#AccountBankBalanceCode').val(),
            Id: BankBranchId
        };
        ShowLoader();
        $.ajax({
            url: '../api/BankBranch/Put/',
            type: 'POST',
            data: JSON.stringify(BankBranch),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Bank Branch Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../BankBranchManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    BankBrachId = id;
    ShowLoader();
    $.ajax({
        url: "../api/BankBranch/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('Bank', '-- Please Select Bank --', '../api/Bank/GetActiveBank');
            $("#Bank").val(data.BankId),
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            $('#AccountBankBalanceCode').val(data.AccountBankBalance);
            $('#AccountBankBalanceDescription').val(data.AccountBankBalanceDescription);
            HideLoader();
        }
    });
};

function ResetForm() {
    UnselectKendoDropDown("Bank")
    $("#Bank").val("");
    $('#Code').val("");
    $('#Name').val("");
    $('#AccountBankBalanceCode').val("");
    $('#AccountBankBalanceDescription').val("");

}
