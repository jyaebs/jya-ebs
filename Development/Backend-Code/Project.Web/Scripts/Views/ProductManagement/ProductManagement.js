﻿var ProductMasterId = 0;
var entryLevelId = "";

function initializeForm() {

    LoadKendoDropDown('ProductBrandId', '-- Please Select Product Brand --', '../api/ProductBrand/GetAllActiveProductBrand');
    LoadKendoDropDown('ProductTypeId', '-- Please Select Product Type --', '../api/Developer/GetAllActiveProductType');
    LoadKendoDropDown('ProductCategoryId', '-- Please Select Product Category --', '../api/ProductCategory/GetAllActiveProductCategory');
    LoadKendoDropDown('ProductSubCategoryId', '-- Please Select Product SubCategory --', '');
    LoadKendoDropDown('PrimaryUOMId', '-- Please Select Primary UOM --', '../api/PrimaryUOM/GetAllActivePrimaryUOM');
    LoadKendoDropDown('SecondaryUOMId', '-- Please Select Secondary UOM --', '');


    $(".fixedAsset").addClass("d-none");
    $("#secondaryUOMDiv").addClass("d-none");


    $("#AccountLevelTable tbody").on("click", "tr", function () {
        var table = $('#AccountLevelTable').DataTable();
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == "DP") {
            $("#DepreciationCodeId").val(rowAccountCode);
            $("#DepreciationAccountDescriptionId").val(rowAccountName);
        }

        else if (entryLevelId == "IA") {
            $("#InventoryAccountCodeId").val(rowAccountCode);
            $("#InventoryAccountDescriptionId").val(rowAccountName);
        }
        else {
            $("#COGSAccountCodeId").val(rowAccountCode);
            $("#COGSAccountDescriptionId").val(rowAccountName);
        }



        $("#AddAccountModal").modal("hide")


    });

}

// Used Only in Update 
function initializeFormUpdate() {

    $(".fixedAsset").addClass("d-none");
    $("#secondaryUOMDiv").addClass("d-none");


    $("#AccountLevelTable tbody").on("click", "tr", function () {
        var table = $('#AccountLevelTable').DataTable();
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == "DP") {
            $("#DepreciationCodeId").val(rowAccountCode);
            $("#DepreciationAccountDescriptionId").val(rowAccountName);
        }

        else if (entryLevelId == "IA") {
            $("#InventoryAccountCodeId").val(rowAccountCode);
            $("#InventoryAccountDescriptionId").val(rowAccountName);
        }
        else if (entryLevelId == "COGS") {
            $("#COGSAccountCodeId").val(rowAccountCode);
            $("#COGSAccountDescriptionId").val(rowAccountName);
        }
        else {
            $("#IncomeAccountCodeId").val(rowAccountCode);
            $("#IncomeAccountDescriptionId").val(rowAccountName);
        }


        $("#AddAccountModal").modal("hide")


    });

}

function checkProductType() {
    let productType = $("#ProductTypeId").data("kendoDropDownList").text();
    if (productType == "Fixed Asset") {
        $(".fixedAsset").removeClass("d-none");
        $("#DepreciationMethodid").attr("required", true);
        $("#DepreciationTypeId").attr("required", true);
        $("#DepreciationRateId").attr("required", true);
        $("#ScrapeValueId").attr("required", true);
        $("#UsagelifeId").attr("required", true);
        $("#DepreciationCodeId").attr("required", true);
    }
    else {
        $(".fixedAsset").addClass("d-none");
        $("#DepreciationMethodid").removeAttr("required", true);
        $("#DepreciationMethodid").val("");

        $("#DepreciationTypeId").removeAttr("required", true);
        $("#DepreciationTypeId").val("");

        $("#DepreciationRateId").removeAttr("required", true);
        $("#DepreciationRateId").val("");

        $("#ScrapeValueId").removeAttr("required", true);
        $("#ScrapeValueId").val("");

        $("#UsagelifeId").removeAttr("required", true);
        $("#UsagelifeId").val("");

        $("#DepreciationCodeId").removeAttr("required", true);
        $("#DepreciationCodeId").val("");
    }
}

function checkPrimaryUOM() {
    let primaryUOM = $("#PrimaryUOMId").val();

    if (primaryUOM != "") {
        $("#secondaryUOMDiv").removeClass("d-none");
        //$("#SecondaryUOMId").attr("required", true);
        //$("#ConversionFactorId").attr("required", true);
    }
    else {
        $("#secondaryUOMDiv").addClass("d-none");
        //$("#SecondaryUOMId").removeAttr("required", true);
        //$("#ConversionFactorId").removeAttr("required", true);
    }
}

function LoadProductData() {
    var Data1 = $("#ProductCategoryId").val();
    if (Data1 == "") {
        LoadKendoDropDown('ProductSubCategoryId', '-- Please Select ProductSubCategory --', '');
    }
    else {
        LoadKendoDropDown('ProductSubCategoryId', '-- Please Select ProductSubCategory --', '../api/ProductSubCategory/GetAllActiveProductSubCategory?id=' + Data1);
    }
}

function LoadMasterData() {
    var Data2 = $("#PrimaryUOMId").val();
    if (Data2 == "") {
        LoadKendoDropDown('SecondaryUOMId', '-- Please Select SecondaryUOM --', '');
    }
    else {
        LoadKendoDropDown('SecondaryUOMId', '-- Please Select SecondaryUOM --', '../api/SecondaryUOM/GetAllActiveSecondaryUOM?id=' + Data2);
    }
}

function GetChartOfAccount(entryId) {
    entryLevelId = entryId;
    $("#AddAccountModal").toggle("modal");
    initializeDataTable();
}

function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetEntryLevelCOALevels`,
        tableId: 'AccountLevelTable'
    };
    $('#AccountLevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}

function Add() {
    var formId = "form";
    if (validate(formId)) {
        var formdata = new FormData();
        var file = document.getElementById("UserImage");
        if (file != undefined)
            formdata.append("UserImage", file.files[0]);

        var ProductMaster = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Barcode: $('#BarCode').val(),
            ProductTypeId: $('#ProductTypeId').val(),
            ProductBrandId: $("#ProductBrandId").val(),
            Category: $("#ProductCategoryId").val(),
            SubCategory: $("#ProductSubCategoryId").val(),
            PrimaryUOM: $("#PrimaryUOMId").val(),
            SecondaryUOM: $("#SecondaryUOMId").val(),
            ConversionFactor: $('#ConversionFactorId').val(),
            Trendy: $("#trendy").prop("checked"),
            DepreciationMethod: $("#DepreciationMethodid").val(),
            DepreciationType: $("#DepreciationTypeId").val(),
            DepreciationRate: $("#DepreciationRateId").val(),
            ScrapeValue: $("#ScrapeValueId").val(),
            UsageLife: $("#UsagelifeId").val(),
            DepreciationAccount: $('#DepreciationCodeId').val(),
            InventoryAccount: $('#InventoryAccountCodeId').val(),
            COGSAccount: $("#COGSAccountCodeId").val()

        };

        if ($("#SecondaryUOMId").val() != "") {
            if ($("#ConversionFactorId").val() == "") {
                toastr.error("Please Enter Conversion Factor", { timeOut: 5000 });
                return false;
            }
        }

        if ($("#DepreciationRateId").val() > 100) {
            toastr.error("Depreciation Rate cannot be more than 100", { timeOut: 5000 });
            return false;

        }

        var serilizeobj = JSON.stringify(ProductMaster);
        ShowLoader();
        $.ajax({
            url: '../api/Product/Post?Product=' + serilizeobj,
            data: formdata,
            type: 'POST',
            dataType: 'Json',
            contentType: false,
            processData: false,
            success: function (data) {
                toastr.success("Product Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }   
}

function Update() {
    var formId = "form";
    if (validate(formId)) {
        var formdata = new FormData();
        var file = document.getElementById("UserImage");
        if (file != undefined)
            formdata.append("UserImage", file.files[0]);

        var ProductMaster = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Barcode: $('#BarCode').val(),
            ProductTypeId: $('#ProductTypeId').val(),
            ProductBrandId: $("#ProductBrandId").val(),
            Category: $("#ProductCategoryId").val(),
            SubCategory: $("#ProductSubCategoryId").val(),
            PrimaryUOM: $("#PrimaryUOMId").val(),
            SecondaryUOM: $("#SecondaryUOMId").val(),
            ConversionFactor: $('#ConversionFactorId').val(),
            Trendy: $("#trendy").prop("checked"),
            DepreciationMethod: $("#DepreciationMethodid").val(),
            DepreciationType: $("#DepreciationTypeId").val(),
            DepreciationRate: $("#DepreciationRateId").val(),
            ScrapeValue: $("#ScrapeValueId").val(),
            UsageLife: $("#UsagelifeId").val(),
            DepreciationAccount: $('#DepreciationCodeId').val(),
            InventoryAccount: $('#InventoryAccountCodeId').val(),
            COGSAccount: $("#COGSAccountCodeId").val(),
            Id: ProductMasterId
        };
        if ($("#DepreciationRateId").val() > 100) {
            toastr.error("Depreciation Rate cannot be more than 100", { timeOut: 5000 });
            return false;

        }
        var serilizeobj = JSON.stringify(ProductMaster);
        ShowLoader();
        $.ajax({
            url: '../api/Product/Put?Product=' + serilizeobj,
            data: formdata,
            type: 'POST',
            dataType: 'Json',
            contentType: false,
            processData: false,
            success: function (data) {
                HideLoader();
                toastr.success("Product Updated Successfully", { timeOut: 5000 });
                RedirectToUrl('../ProductManagement/Manage')
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}

function populateData(id) {
    ProductMasterId = id;
    ShowLoader();
    $.ajax({
        url: "../api/Product/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            console.log(data)
            LoadKendoDropDown('ProductBrandId', '-- Please Select Product Brand --', '../api/ProductBrand/GetAllActiveProductBrand');
            $("#ProductBrandId").val(data.ProductBrandId);
            LoadKendoDropDown('ProductTypeId', '-- Please Select Product Type --', '../api/Developer/GetAllActiveProductType');
            $('#ProductTypeId').val(data.ProductTypeId);
            LoadKendoDropDown('ProductCategoryId', '-- Please Select Product Category --', '../api/ProductCategory/GetAllActiveProductCategory');
            $("#ProductCategoryId").val(data.Category);
            LoadKendoDropDown('ProductSubCategoryId', '-- Please Select ProductSubCategory --', '../api/ProductSubCategory/GetAllActiveProductSubCategory?id=' + data.SubCategory);
            $("#ProductSubCategoryId").val(data.SubCategory);
            LoadKendoDropDown('PrimaryUOMId', '-- Please Select Primary UOM --', '../api/PrimaryUOM/GetAllActivePrimaryUOM');
            $("#PrimaryUOMId").val(data.PrimaryUOM);
            if (data.SecondaryUOM) {
                $("#secondaryUOMDiv").removeClass("d-none");
                LoadKendoDropDown('SecondaryUOMId', '-- Please Select Secondary UOM --', '../api/SecondaryUOM/GetAllActiveSecondaryUOM?id=' + data.PrimaryUOM);
                $("#SecondaryUOMId").val(data.SecondaryUOM);
                $('#ConversionFactorId').val(data.ConversionFactor);
            }
            else {
                $("#secondaryUOMDiv").addClass("d-none");
            }
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            $('#BarCode').val(data.Barcode);
            $("#trendy").prop("checked", data.Trendy)
            if (data.ProductTypeName == "Fixed Asset") {
                $(".fixedAsset").removeClass("d-none");
                $("#DepreciationMethodid").val(data.DepreciationMethod);
                $("#DepreciationTypeId").val(data.DepreciationType);
                $("#DepreciationRateId").val(data.DepreciationRate);
                $("#ScrapeValueId").val(data.ScrapeValue);
                $("#UsagelifeId").val(data.UsageLife);
                $("#DepreciationCodeId").val(data.DepreciationAccount);
                $('#DepreciationAccountDescriptionId').val(data.DepreciationAccountDescription);

            }

            $('#InventoryAccountCodeId').val(data.InventoryAccount);
            $('#InventoryAccountDescriptionId').val(data.InventoryAccountDescription);
            $("#COGSAccountCodeId").val(data.COGSAccount);
            $('#COGSAccountDescriptionId').val(data.COGSAccountDescription);
            if (data.Image == null)
                $('#userImg').attr("src", "../../Content/dist-assets/images/placeholder.jpg");
            else
                $('#userImg').attr("src", "../../ImagesStorageDB/" + data.Image);
            //if (data.Image != null) {
            //    $("#userImg").attr("src", "../ImagesStorageDB/" + data.Image);
            //}
            HideLoader();
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#userImg')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function ResetForm() {
    $('.nav-justified a[href="#progress-seller-details"]').tab('show');
    $('#Code').val("");
    $('#Name').val("");
    $('#BarCode').val("");
    UnselectKendoDropDown("ProductBrandId")
    $('#ProductBrandId').val("");
    UnselectKendoDropDown("ProductTypeId")
    $('#ProductTypeId').val("");
    UnselectKendoDropDown("ProductCategoryId")
    $('#ProductCategoryId').val("");
    UnselectKendoDropDown("ProductSubCategoryId")
    $('#ProductSubCategoryId').val("");
    UnselectKendoDropDown("PrimaryUOMId")
    $('#PrimaryUOMId').val("");
    $("#secondaryUOMDiv").addClass("d-none");
    UnselectKendoDropDown("SecondaryUOMId")
    $('#SecondaryUOMId').val("");
    $('#ConversionFactorId').val("");
    $("#trendy").prop("checked", false)
    $('#userImg').attr("src", "../../Content/dist-assets/images/placeholder.jpg");
    $(".fixedAsset").addClass("d-none");
    $('#DepreciationMethodid').val("")
    $('#DepreciationTypeId').val("");
    $('#DepreciationRateId').val("");
    $('#ScrapeValueId').val("");
    $('#UsagelifeId').val("");
    $('#DepreciationCodeId').val("");
    $('#DepreciationAccountDescriptionId').val("");
    $('#InventoryAccountCodeId').val("");
    $('#InventoryAccountDescriptionId').val("");
    $('#COGSAccountCodeId').val("");
    $('#COGSAccountDescriptionId').val("");
}




