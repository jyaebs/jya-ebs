﻿var WarehouseId = 0;

function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
};
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var Warehouse = {
            LocationId: $("#LocationId").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/Warehouse/Post/',
            type: 'POST',
            data: JSON.stringify(Warehouse),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Warehouse Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};

function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var Warehouse = {
            LocationId: $("#LocationId").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: WarehouseId
        };
        ShowLoader();
        $.ajax({
            url: '../api/Warehouse/Put/',
            type: 'POST',
            data: JSON.stringify(Warehouse),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Warehouse Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../WarehouseManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    WarehouseId = id;
    ShowLoader();
    $.ajax({
        url: "../api/Warehouse/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId),
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {
    $("#LocationId").val();
    $('#Code').val("");
    $('#Name').val("");
    UnselectKendoDropDown("Location")

}