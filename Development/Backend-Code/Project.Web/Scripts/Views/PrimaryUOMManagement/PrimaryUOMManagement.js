﻿var PrimaryUOMId = 0;
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var PrimaryUOM = {

            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/PrimaryUOM/Post/',
            type: 'POST',
            data: JSON.stringify(PrimaryUOM),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Primary UOM Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var PrimaryUOM = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: PrimaryUOMId
        };
        ShowLoader();
        $.ajax({
            url: '../api/PrimaryUOM/Put/',
            type: 'POST',
            data: JSON.stringify(PrimaryUOM),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Primary UOM Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../PrimaryUOMManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    PrimaryUOMId = id;
    ShowLoader();
    $.ajax({
        url: "../api/PrimaryUOM/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}