﻿var ProductTypeId = 0;
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var ProductType = {

            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({

            url: '../api/ProductType/Post/',
            type: 'POST',
            data: JSON.stringify(ProductType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Type Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var ProductType = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: ProductTypeId
        };
        ShowLoader();
        $.ajax({
            url: '../api/ProductType/Put/',
            type: 'POST',
            data: JSON.stringify(ProductType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Type Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../ProductTypeManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    ProductTypeId = id;
    ShowLoader();
    $.ajax({
        url: "../api/ProductType/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}