﻿var DriverId = 0;
function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
};
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var Driver = {
            LocationId: $("#LocationId").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/Driver/Post/',
            type: 'POST',
            data: JSON.stringify(Driver),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Driver Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};

function Update() {
    debugger;
    
    var formId = "form";
    if (validate(formId)) {
        var Driver = {
            LocationId: $("#LocationId").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: DriverId
        };
        ShowLoader();
        $.ajax({
            url: '../api/Driver/Put/',
            type: 'POST',
            data: JSON.stringify(Driver),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Driver Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../DriverManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    DriverId = id;
    ShowLoader();
    $.ajax({
        url: "../api/Driver/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId);
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {
    $("#LocationId").val();
    $('#Code').val("");
    $('#Name').val("");
    UnselectKendoDropDown("Location")
  
}