﻿let CustomerTypeId = 0;
function Add() {
    var formId = "form";
    if (validate(formId)) {
        var CustomerType = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
        };
        ShowLoader();
        $.ajax({


            url: '../api/CustomerType/Post/',
            type: 'POST',
            data: JSON.stringify(CustomerType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Customer Type Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    var formId = "form";
    if (validate(formId)) {
        var CustomerType = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: CustomerTypeId
        };
        ShowLoader();
        $.ajax({
            url: '../api/CustomerType/Put/',
            type: 'POST',
            data: JSON.stringify(CustomerType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Customer Type Updated Successfully", { timeOut: 5000 });
                ResetForm();
                RedirectToUrl('../CustomerTypeManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    CustomerTypeId = id;
    ShowLoader();
    $.ajax({
        url: "../api/CustomerType/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}