﻿var SaleOrderId = 0;
var SaleQuotationId = 0;
var TotalDiscountAmount = 0;
var GrossAmount = 0;
var NetAmount = 0;

$(document).ready(function () {

    initializeForm();
    $("#SaleQuotationDetailTable tbody").on("click", "tr", function () {

        let table = $('#SaleQuotationDetailTable').DataTable();
        let SaleQuotationId = table.rows().data()[this.rowIndex - 1].Id;
       
        GetSaleQuotationDetail(SaleQuotationId)
        $("#SaleQuotationDetailModal").modal("hide");
    });
});

function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
    //LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '');

    dataTable.initialize();
}

//function LoadCustomers(id) {
//    debugger;
//    var LocationId = $("#" + id).val();
//    if (LocationId != "") {
//        LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + LocationId);
//    }

//}

var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#SaleOrderTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            destroy: false,


            "columns": [
                { "data": "Description" },
                { "data": "PrimaryQuantity" },
                { "data": "SecondaryQuantity" },
                { "data": "TotalQuantity" },
                { "data": "UnitPrice" },
                { "data": "GrossAmount" },
                { "data": "Discount" },
                { "data": "DiscountAmount" },
                { "data": "NetAmount" },
                { "data": "ProductId", className: "text-center" },
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        var actionsHtml =
                            ` <i class="mdi mdi-close red  font-size-14" onclick="removeEntry(this)" title="Cancelled"></i> `;

                        return actionsHtml;
                    },
                    "targets": -1
                },

                {
                    orderable: false,
                    targets: "no-sort",
                }
            ],
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};

function onSelectRecord(e) {
    debugger;
    var inputId = this.element[0].id;
    var dataItem = this.dataItem(e.item.index());

    $("#" + inputId + "Id").val(dataItem.Id);
    $("#" + inputId + "NameOnSelect").val(dataItem.Name);
    if (inputId == "Product") {
        getProductData(inputId);
    }

}
function OnChangeRecord(e) {
    debugger;
    var inputId = this.element[0].id;
    var newName = $("#" + inputId).val();
    var oldName = $("#" + inputId + "NameOnSelect").val();
    if (newName != oldName) {
        $("#" + inputId + "Id").val("");
        $("#" + inputId + "UOMDiv").addClass("d-none");
        $("#" + inputId + "QuantityDiv").addClass("d-none");
    }
}

function getProductData(inputId) {
    var product = $("#" + inputId + "Id").val();

    $.ajax({
        url: "../api/Product/GetProductDetail?productId=" + product,
        type: 'GET',
        success: function (data) {
            if (data.SecondaryUOM == 0) {
                $("#" + inputId + "QuantityDiv").removeClass("d-none");
                $("#" + inputId + "UOMDiv").addClass("d-none");
                $("#" + inputId + "Quantity").attr("required", true);
            }
            else {
                $("#" + inputId + "QuantityDiv").addClass("d-none");
                $("#" + inputId + "UOMDiv").removeClass("d-none");
                $("#" + inputId + "Quantity").attr("required", false);


                $("#" + inputId + "PrimaryUOM").html("<b>" + data.PrimaryUOMName + "</b>");
                $("#" + inputId + "PrimaryUOMValue").val(data.PrimaryUOMName);
                $("#" + inputId + 'SecondaryUOM').html("<b>" + data.SecondaryUOMName + "</b>");
                $("#" + inputId + "SecondaryUOMValue").val(data.SecondaryUOM);
                $("#" + inputId + 'PrimaryPrice').val(data.PrimaryUnitPrice);
                $("#" + inputId + 'SecondaryPrice').val(data.SecondaryUnitPrice);
                $("#" + inputId + "conversionName").html(data.PrimaryUOMName + " per " + data.SecondaryUOMName);
                $("#" + inputId + 'Conversion').val(data.ConversionFactor);
            }

        },
        error: function (data) {
        }
    });
}
function onChangePrimaryCheckBox(inputId) {

    if ($("#" + inputId + "Primary_switch").is(":checked")) {

        $("#" + inputId + "PrimaryQuantity").prop("disabled", false);
        $("#" + inputId + "PrimaryQuantity").attr("required", true);
        $("#" + inputId + "Secondary_switch").prop("checked", false);
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").val("");
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
    }

    else {
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
        $("#" + inputId + "PrimaryQuantity").val("");
    }
}

function onChangeSecondaryCheckBox(inputId) {

    if ($("#" + inputId + "Secondary_switch").is(":checked")) {

        $("#" + inputId + "SecondaryQuantity").prop("disabled", false);
        $("#" + inputId + "SecondaryQuantity").attr("required", true);
        $("#" + inputId + "Primary_switch").prop("checked", false);
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").val("");
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
    }
    else {
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
        $("#" + inputId + "SecondaryQuantity").val("");
    }
}

function addNewRowToGrid(inputId) {
    ProductId = $("#" + inputId + "Id").val();
    var variationId;
    var product;

    if ($(ProductId.search("-"))[0] > 0) {
        product = ProductId.split("-")[0];
        variationId = ProductId.split("-")[1];
    }
    else {
        product = ProductId;
        variationId = 0;
    }
    let primaryQuantity = 0;
    let secondaryQuantity = 0;
    let conversionFactor;
    let totalQuantity;
    let UnitPrice = 0;
    let Discount = 0;
    let DiscountAmount = 0;
    let NetAmount;
    var TotalNetAmount = 0;



    let QuantitydivClass = $("#ProductUOMDiv").attr("class");

    if (QuantitydivClass.includes("d-none") != true) {

        if ($("#" + inputId + "Primary_switch").prop("checked")) {
            primaryQuantity = $("#" + inputId + 'PrimaryQuantity').val() == "" ? 0 : $("#" + inputId + 'PrimaryQuantity').val();
            totalQuantity = primaryQuantity;
        }
        else {
            secondaryQuantity = $("#" + inputId + 'SecondaryQuantity').val() == "" ? 0 : $("#" + inputId + 'SecondaryQuantity').val();
            conversionFactor = $("#" + inputId + 'Conversion').val();
            totalQuantity = secondaryQuantity * conversionFactor;
        }

    }


    else {
        totalQuantity = $("#ProductQuantity").val();
    }
    UnitPrice = $("#UnitPrice").val();
    Discount = $("#Discount").val();
    var rowData = {}

    rowData.ProductId = ProductId;
    rowData.ProductName = $("#ProductNameOnSelect").val();
    rowData.ProductPrimaryQuantity = $("#ProductPrimaryQuantity").val();
    rowData.ProductSecondaryQuantity = $("#ProductSecondaryQuantity").val();
    rowData.Totalquantity = totalQuantity;
    rowData.UnitPrice = UnitPrice;
    rowData.GrossAmount = totalQuantity * UnitPrice;
    rowData.Discount = Discount;
    rowData.DiscountAmount = rowData.GrossAmount / Discount;
    rowData.NetAmount = rowData.GrossAmount - rowData.DiscountAmount;


    var SaleTable = $('#SaleOrderTable').DataTable();

    var data = SaleTable.rows().data();
    var isAdded = true;
    data.each(function (i, item) {
        if (rowData.ProductId == i.ProductId) {
            isAdded = false;

        }
    });

    if (isAdded) {
        var table = $('#SaleOrderTable').DataTable();

        table.row.add({
            "Description": rowData.ProductName,
            "PrimaryQuantity": rowData.ProductPrimaryQuantity,
            "SecondaryQuantity": rowData.ProductSecondaryQuantity,
            "TotalQuantity": rowData.Totalquantity,
            "UnitPrice": rowData.UnitPrice,
            "Discount": rowData.Discount,
            "GrossAmount": rowData.GrossAmount,
            "DiscountAmount": rowData.DiscountAmount,
            "NetAmount": rowData.NetAmount,
            "ProductId": rowData.ProductId,

        }).draw();

        CalculateAmounts();

    }
    else {
        toastr.error("Product is Already Exist", { timeOut: 5000 });
    }

}

function ResetModal() {
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');

    $("#Discount").val("");
    $("#ProductSecondaryQuantity").val("");
    $("#ProductPrimaryQuantity").val("");
    $("#UnitPrice").val("");
}

function AddSaleOrder() {
    debugger;
    var formId = "form";
     var SaleOrder = {
            SaleQuotationId: SaleQuotationId,
            LocationId: $("#LocationId").val(),
            CustomerId: $("#CustomerId").val(),
            VoucherNo: $("#VoucherNo").val(),
            OrderDate: $("#OrderDate").val(),
            TotalNetAmount: $("#TotalNetAmount").text(),
            TotalDiscountAmount: $("#TotalDiscountAmount").text(),
            TotalGrossAmount: $("#TotalGrossAmount").text(),
        };

        SaleOrder.SaleOrderDetail = [];
        debugger;
        var table = $('#SaleOrderTable').DataTable();
        var data = table.rows().data();
        data.each(function (value, index) {
            var Details = {};
            Details.ProductId = value.ProductId;
            Details.PrimaryQuantity = value.PrimaryQuantity;
            Details.SecondaryQuantity = value.SecondaryQuantity;
            Details.TotalQuantity = value.TotalQuantity;
            Details.UnitPrice = value.UnitPrice;
            Details.GrossAmount = value.GrossAmount;
            Details.Discount = value.Discount;
            Details.DiscountAmount = value.DiscountAmount;
            Details.NetAmount = value.NetAmount;
            SaleOrder.SaleOrderDetail.push(Details);
        });
        debugger;
        ShowLoader();
        $.ajax({
            url: '../api/SaleOrder/Post/',
            type: 'POST',
            data: JSON.stringify(SaleOrder),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Sale Order Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();

            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.warning(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    
}

function RemoveGridProduct(productId) {
    var actionsHtml =
        ` <i class="mdi mdi-close red  font-size-14" onclick="removeEntry(this)" title="Cancelled"></i> `;

    return actionsHtml;
}


function removeEntry(rowPointer) {

    var table = $('#SaleOrderTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
        var row = table
            .row($(rowPointer).closest("tr")[0])
            .remove()
        .draw();
        CalculateAmounts();
}

function populateData(id) {
    SaleOrderId = id;
    ShowLoader();
    $.ajax({
        url: "../api/SaleOrder/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
            $("#ProductId").val(data.ProductId);
            LoadKendoDropDown('LocationId', '-- Please Select Product --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId),
            LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + data.LocationId);
            $("#CustomerId").val(data.CustomerId),
            $("#Customer").val(data.CustomerName),
            $("#VoucherNo").val(data.VoucherNo),
            $("#OrderDate").val(data.OrderDate.substr(0, 10));
            $("#TotalNetAmount").html(data.TotalNetAmount);
            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
            $("#TotalGrossAmount").html(data.TotalGrossAmount);

            var table = $("#SaleOrderTable").DataTable();
            $.each(data.SaleOrderDetail, function (index, value) {
                var rowData = {};
                rowData.ProductName = value.ProductName;
                rowData.PrimaryQuantity = value.PrimaryQuantity;
                rowData.SecondaryQuantity = value.SecondaryQuantity;
                rowData.TotalQuantity = value.TotalQuantity;
                rowData.UnitPrice = value.UnitPrice;
                rowData.GrossAmount = value.GrossAmount;
                rowData.Discount = value.Discount;
                rowData.DiscountAmount = value.DiscountAmount;
                rowData.NetAmount = value.NetAmount;
                rowData.Id = value.Id;

                table.row.add({
                    "Description": rowData.ProductName,
                    "PrimaryQuantity": rowData.PrimaryQuantity,
                    "SecondaryQuantity": rowData.SecondaryQuantity,
                    "TotalQuantity": rowData.TotalQuantity,
                    "UnitPrice": rowData.UnitPrice,
                    "GrossAmount": rowData.GrossAmount,
                    "Discount": rowData.Discount,
                    "DiscountAmount": rowData.DiscountAmount,
                    "NetAmount": rowData.NetAmount,
                    "ProductId": rowData.Id
                }).draw();
            });
            HideLoader();
        }
    });
}

function Update() {

    var Amount = $("#TotalNetAmount").text();

    var SaleOrder = {
        LocationId: $("#LocationId").val(),
        CustomerId: $("#CustomerId").val(),
        VoucherNo: $("#VoucherNo").val(),
        OrderDate: $("#OrderDate").val(),
        TotalNetAmount: Amount,
        
    };

    SaleOrder.SaleOrderDetail = [];
    debugger;
    var table = $('#SaleOrderTable').DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {
        var Details = {};
        Details.ProductId = ProductId;
        Details.PrimaryQuantity = value.PrimaryQuantity;
        Details.SecondaryQuantity = value.SecondaryQuantity;
        Details.TotalQuantity = value.TotalQuantity;
        Details.UnitPrice = value.UnitPrice;
        Details.GrossAmount = value.GrossAmount;
        Details.Discount = value.Discount;
        Details.DiscountAmount = value.DiscountAmount;
        Details.NetAmount = value.NetAmount;
        SaleOrder.SaleOrderDetail.push(Details);
    });
    debugger;
    ShowLoader();
    $.ajax({
        url: '../api/SaleOrder/Put/',
        type: 'POST',
        data: JSON.stringify(SaleOrder),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            toastr.success("Sale Order Update Successfully", { timeOut: 5000 });
            RedirectToUrl('../SaleOrderManagement/Manage')
            ResetForm();
            HideLoader();

        },
        error: function (data) {
            var response = data.responseText.replace(/"/g, '');
            toastr.warning(response, { timeOut: 5000 });
            ResetForm();
            HideLoader();
        }
    });

}

function ResetForm() {
    TotalDiscountAmount = 0;
    GrossAmount = 0;
    netAmount = 0;
    $("#LocationId").val("");
    $("#CustomerId").val("");
    $("#VoucherNo").val("");
    $("#OrderDate").val("");
    $("#TotalNetAmount").text(0);
    $("#TotalGrossAmount").text(0);
    $("#TotalDiscountAmount").text(0);
    LoadKendoAutoComplete('Customer', '-- Please Select Location --', '');
    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    var table = $('#SaleOrderTable').DataTable();
    table.clear().draw();

}

function GetOpenSaleQuotation() {
    $("#SaleQuotationDetailModal").toggle("modal");
    initializeSaleQuotationTable();
}

function initializeSaleQuotationTable() {

    var dtCommonParam = {
        getDataUrl: `../api/SaleQuotation/GetOpenSaleQuotation`,
        tableId: 'SaleQuotationDetailTable'
    };
    $('#SaleQuotationDetailTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "ReferenceNumber"},
            { "data": "Name"},
            { "data": "CustomerName"},
            { "data": "LocationId", "visible": false },

        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ],

    });
}

function GetSaleQuotationDetail(SaleQuotationId) {
    SaleQuotationId;
    debugger;
    $.ajax({
        url: "../api/SaleQuotation/GetById?id=" + SaleQuotationId,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $("#LocationId").val(data.LocationId);
            $("#LocationId").val(data.Name);
            $("#CustomerId").val(data.CustomerId);
            $("#Customer").val(data.CustomerName);
            $("#SaleReferenceNumber").val(data.ReferenceNumber);
            $("#TotalNetAmount").html(data.TotalNetAmount);
            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
            $("#TotalGrossAmount").html(data.TotalGrossAmount);
            
            var table = $("#SaleOrderTable").DataTable();
            $.each(data.SaleQuotationDetail, function (index, value) {
                var rowData = {};
                rowData.ProductName = value.ProductName;
                rowData.PrimaryQuantity = value.PrimaryQuantity;
                rowData.SecondaryQuantity = value.SecondaryQuantity;
                rowData.TotalQuantity = value.TotalQuantity;
                rowData.UnitPrice = value.UnitPrice;
                rowData.GrossAmount = value.GrossAmount;
                rowData.Discount = value.Discount;
                rowData.DiscountAmount = value.DiscountAmount;
                rowData.NetAmount = value.NetAmount;
                rowData.Id = value.Id;
                rowData.ProductId = value.ProductId;

                table.row.add({
                    "Description": rowData.ProductName,
                    "PrimaryQuantity": rowData.PrimaryQuantity,
                    "SecondaryQuantity": rowData.SecondaryQuantity,
                    "TotalQuantity": rowData.TotalQuantity,
                    "UnitPrice": rowData.UnitPrice,
                    "GrossAmount": rowData.GrossAmount,
                    "Discount": rowData.Discount,
                    "DiscountAmount": rowData.DiscountAmount,
                    "NetAmount": rowData.NetAmount,
                    "ProductId": rowData.ProductId ,
                }).draw();
            });
            HideLoader();
        }
    });
}

function CalculateAmounts() {
    debugger;
    var table = $("#SaleOrderTable").DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {
        
        NetAmount += value.NetAmount;
        GrossAmount += value.GrossAmount;
        TotalDiscountAmount += value.DiscountAmount;
    });
    $("#TotalGrossAmount").text(GrossAmount);
    $("#TotalDiscountAmount").text(TotalDiscountAmount);
    $("#TotalNetAmount").text(NetAmount);
}