﻿function validate(id) {
    var form = document.getElementById(id);
    var inputs, i, textarea;
    inputs = $('#' + id + ' input:not([type = hidden])');
    textarea = form.getElementsByTagName("textarea");
    for (i = 0; i < inputs.length; i++) {
        var required = inputs[i].required;
        var value = inputs[i].value;
        var inputId = inputs[i].id;
        var pattern = inputs[i].pattern;

        if (inputId !== "") {
            var em = inputs[i].closest('div');
            var classes = $(em).attr('class');
            if (required == true && (value == "" || value == null)) {
                $("#" + inputId).addClass("is-invalid");
                if (classes.includes('defaultDropdown')) {
                    $('.k-invalid-msg').text('Required*');
                    $("span[data-for='" + inputId + "']").show();
                    return false;
                }
                else if (classes.includes('defaultAutoComplete')) {
                    $('.k-invalid-msg').text('Required*');
                    $("span[data-for='" + inputId + "']").show();
                    return false;
                }
                else {
                    $("#required-error").remove();
                    $(em).append('<div id="required-error" class="invalid-feedback">Required*</div>');
                    return false;
                }
            }
            else {
                if (classes !== undefined) {
                    if (classes.includes('defaultAutoComplete')) {
                        var oldName = $("#" + inputId + "NameOnSelect").val();
                        if (value != oldName) {
                            $('.k-invalid-msg').text('Invalid ' + inputId);
                            $("span[data-for='" + inputId + "']").show();
                            return false;
                        }
                        else {
                            $("span[data-for='" + inputId + "']").hide();
                        }
                    }
                    $("#" + inputId).removeClass("is-invalid");
                }
            }

            if (pattern != null && pattern != "") {
                var re = RegExp(pattern);
                if (!re.test(value)) {
                    $("#" + inputId).addClass("is-invalid");
                    em = inputs[i].closest('div');
                    classes = $(em).attr('class');
                    $("#required-error").remove();
                    $(em).append('<div id="required-error" class="invalid-feedback">Please enter the valid format</div>');
                    return false;
                }
            }
        }
    }
    if (textarea.length > 0) {
        for (j = 0; j < textarea.length; j++) {
            required = textarea[j].required;
            value = textarea[j].value;
            var textareaId = textarea[j].id;
            var textaeadiv = textarea[j].parentElement;
            if (required == true && (value == "" || value == null)) {
                $("#" + textareaId).addClass("is-invalid");
                $("#required-error").remove();
                $(textaeadiv).append('<div id="required-error" class="invalid-feedback">Required*</div>');
                return false;
            }
        }
    }
    return true;
}

function IsDateGreater(DateA, DateB) {
    var a = new Date(DateA);
    var b = new Date(DateB);

    var msDateA = Date.UTC(a.getFullYear(), a.getMonth() + 1, a.getDate());
    var msDateB = Date.UTC(b.getFullYear(), b.getMonth() + 1, b.getDate());

    if (parseFloat(msDateA) > parseFloat(msDateB))
        return 1;
    else
        0;
}

//Invalid input check for autocomplete fields
function AutoCompleteInputCheck(id) {
    var InputId = id + "Id";
    var Error = id + "Error";
    var FieldInput = $("#" + id).val();
    var ErrorField = document.getElementById(Error);
    if ($("#" + InputId).val() === "" && FieldInput !== "") {
        ErrorField.innerHTML = 'Invalid ' + id;
    }
    else {
        ErrorField.innerHTML = '';
    }
}

//Current Date Check
function DateCheck(id) {
    var Error = id + "Error";
    var FieldInput = $("#" + id).val();
    var ErrorField = document.getElementById(Error);
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    var CurrentDate = yyyy + '-' + mm + '-' + dd;
    if (IsDateGreater(CurrentDate, FieldInput)) {
        ErrorField.innerHTML = "Date should be greater than current date.";
    }
    else {
        ErrorField.innerHTML = "";
    }
}

function DateOfBirthCheck(id) {
    var Error = id + "Error";
    var FieldInput = $("#" + id).val();
    var ErrorField = document.getElementById(Error);
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    var CurrentDate = yyyy + '-' + mm + '-' + dd;
    if (IsDateGreater(FieldInput, CurrentDate)) {
        ErrorField.innerHTML = "Date should be less than current date.";
    }
    else {
        ErrorField.innerHTML = "";
    }
}