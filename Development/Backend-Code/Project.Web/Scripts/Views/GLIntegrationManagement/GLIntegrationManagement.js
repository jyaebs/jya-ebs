﻿
var entryLevelId = "";

function initializeForm() {

    LoadKendoDropDown('fiscalYear', '-- Please Select Fiscal Year --', '../api/Developer/GetAllActiveFiscalYear');

    $.ajax({
        url: "../api/Developer/GetAllActiveGLIntegrationField",
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {


            // Get Tabs Name
            let tabsName = []
            $.each(data, function (i, tabs) {
                let obj = tabsName.find(o => o.TabName === tabs.TabName);
                if ((typeof obj) != "object") {
                    tabsName.push({
                        Id: tabs.Id,
                        TabName: tabs.TabName,
                    })
                }
            });


            // Get Field Name on basis of Tabs
            $.each(tabsName, function (p, tab) {
                tab.fields = [];
                var tempArray = $.grep(data, function (v) {
                    return v.TabName === tab.TabName;
                });

                $.each(tempArray, function (c, field) {
                    let obj = tab.fields.find(o => o.FieldName === field.FieldName);
                    if ((typeof obj) != "object") {
                        tab.fields.push({
                            Id: field.Id,
                            Code: field.Code,
                            FieldName: field.FieldName,
                        });
                    }
                });
            });


            $.each(tabsName, function (i, name) {
                let tab = ` <li class="nav-item" tabId = ${name.Id}>
                        <a class="nav-link" data-toggle="tab" href="#appr_${name.TabName.split(" ")[0]}" role="tab" id="tab-${name.Id}">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">${name.TabName}</span>
                        </a>
                    </li>`
                $('#tabName').append(tab);
                $('#fields').append(`<div class="tab-pane fade" id="appr_${name.TabName.split(" ")[0]}"></div>`);

                $.each(name.fields, function (i, fname) {

                    let field = `<div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label></label>
                                        <input class="form-control form1Fields" type="text" value="${fname.FieldName}" readonly />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label></label>
                                        <input class="form-control form1Fields fieldValue" id="${fname.Id}" name="${fname.Id}" type="text" placeholder="Select ${fname.FieldName} Code" onclick="GetChartOfAccount('${fname.Id}')" data-toggle="modal" data-target="#AddAccountModal" readonly />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label></label>
                                        <input class="form-control form1Fields fieldDesc" id="${fname.Id}Desc" name="${fname.Id}Desc" type="text" readonly />
                                    </div>
                                </div>
                            </div>`;

                    $(`#appr_${name.TabName.split(" ")[0]}`).append(field);
                });
            });
            $(".nav-tabs").find('li').first().find('a').addClass("active");
            $('.tab-pane').first().removeClass('fade');
            $('.tab-pane').first().addClass('active');
        },
        error: function (data) {
            var response = data.responseText.replace(/"/g, '');
            toastr.error(response, { timeOut: 5000 });
        }
    });



    $("#AccountLevelTable tbody").on("click", "tr", function () {
        var table = $('#AccountLevelTable').DataTable();
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == entryLevelId) {
            $("#" + entryLevelId).val(rowAccountCode);
            $("#" + entryLevelId + "Desc").val(rowAccountName);
        }


        $("#AddAccountModal").modal("hide")


    });

};



function getDataByFiscalYear() {
    let fiscalYear = $('#fiscalYear').val();
    let fields = $("#fields").find('.fieldValue');

    if (fiscalYear != "") {
        ShowLoader();
        $.ajax({
            url: "../api/GLIntegration/GetByFiscalYear?fiscalYear=" + fiscalYear,
            type: 'GET',
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.length > 0) {
                    $("#fields").find('.fieldValue').val("");
                    $("#fields").find('.fieldDesc').val("");
                    $.each(fields, function (i, field) {
                        if (data[i] != undefined) {
                            $("#" + data[i].FieldId).val(data[i].AccountCode);
                            $("#" + data[i].FieldId + "Desc").val(data[i].AccountCodeDescription);
                        }
                    })
                }
                else {
                    $("#fields").find('.fieldValue').val("");
                    $("#fields").find('.fieldDesc').val("");
                }
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
    else {
        $("#fields").find('.fieldValue').val("");
        $("#fields").find('.fieldDesc').val("");
    }
}



function GetChartOfAccount(entryId) {
    entryLevelId = entryId;
    $("#AddAccountModal").toggle("modal");
    initializeDataTable();

}


function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetEntryLevelCOALevels`,
        tableId: 'AccountLevelTable'
    };
    $('#AccountLevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}





function Add() {

    let GLIntegrationAccountCode = [];
    let fields = $("#fields").find('.fieldValue');

    $.each(fields, function (i, field) {
        let accountCode = $("#fields").find('.fieldValue').eq(i).val();
        let fieldId = $("#fields").find('.fieldValue').eq(i).attr('id');

        var AccountObj = {
            "FiscalYearId": $('#fiscalYear').val(),
            "FieldId": fieldId,
            "AccountCode": accountCode
        }

        GLIntegrationAccountCode.push(AccountObj);

    })


    var formId = "form";
    if (validate(formId)) {
        var GLD = {
            GLIntegrationAccountCode: GLIntegrationAccountCode
        };
        ShowLoader();
        $.ajax({
            url: '../api/GLIntegration/Post/',
            type: 'POST',
            data: JSON.stringify(GLD),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("GL Integration Saved Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};



function ResetForm() {
    UnselectKendoDropDown("fiscalYear");
    $('#fiscalYear').val("");
    GLIntegrationAccountCode = [];
    $(".nav-tabs").find('li').find('a').removeClass("active");
    $(".nav-tabs").find('li').first().find('a').addClass("active");
    $(".tab-content").find(".tab-pane").first().addClass("active");
    $("#fields").find('.fieldValue').val("");
    $("#fields").find('.fieldDesc').val("");

}