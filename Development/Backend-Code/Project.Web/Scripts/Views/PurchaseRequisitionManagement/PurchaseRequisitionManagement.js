﻿


function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Product --', '../api/Location/GetAllFactoryByLocationType');
    LoadKendoDropDown('PurchaseTypeId', '-- Please Select PurchaseType --', '../api/Developer/GetAllActivePurchaseType');
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
    dataTable.initialize();
}

var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#PurchaseRequisitionTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            "columns": [
                { "data": "ProductName" },
                { "data": "PrimaryQuantity" },
                { "data": "SecondaryQuantity" },
                { "data": "TotalQuantity" },
                { "data": "Id" }
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return RemoveGridProduct(row.ProductId);
                    },
                    "targets": -1
                },

                {
                    orderable: false,
                    targets: "no-sort"
                }
            ]
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};

function OnChangeRecord(e) {

    var inputId = this.element[0].id;
    var newName = $("#" + inputId).val();
    var oldName = $("#" + inputId + "NameOnSelect").val();
    if (newName != oldName) {
        $("#" + inputId + "Id").val("");
        $("#" + inputId + "UOMDiv").addClass("d-none");
        $("#" + inputId + "QuantityDiv").addClass("d-none");
    }
}

function onSelectRecord(e) {

    var inputId = this.element[0].id;
    var dataItem = this.dataItem(e.item.index());

    $("#" + inputId + "Id").val(dataItem.Id);
    $("#" + inputId + "NameOnSelect").val(dataItem.Name);
    if (inputId == "Product") {
        getProductData(inputId);
    }

}

function getProductData(inputId) {
    var product = $("#" + inputId + "Id").val();

    $.ajax({
        url: "../api/Product/GetProductDetail?productId=" + product,
        type: 'GET',
        success: function (data) {
            
            if (data.SecondaryUOM == 0) {
                $("#" + inputId + "QuantityDiv").removeClass("d-none");
                $("#" + inputId + "UOMDiv").addClass("d-none");
                $("#" + inputId + "Quantity").attr("required", true);
            }
            else {
                $("#" + inputId + "QuantityDiv").addClass("d-none");
                $("#" + inputId + "UOMDiv").removeClass("d-none");
                $("#" + inputId + "Quantity").attr("required", false);


                $("#" + inputId + "PrimaryUOM").html("<b>" + data.PrimaryUOMName + "</b>");
                $("#" + inputId + "PrimaryUOMValue").val(data.PrimaryUOMName);
                $("#" + inputId + 'SecondaryUOM').html("<b>" + data.SecondaryUOMName + "</b>");
                $("#" + inputId + "SecondaryUOMValue").val(data.SecondaryUOM);
                $("#" + inputId + 'PrimaryPrice').val(data.PrimaryUnitPrice);
                $("#" + inputId + 'SecondaryPrice').val(data.SecondaryUnitPrice);
                $("#" + inputId + "conversionName").html(data.PrimaryUOMName + " per " + data.SecondaryUOMName);
                $("#" + inputId + 'Conversion').val(data.ConversionFactor);
            }

        },
        error: function (data) {
        }
    });
}

function onChangePrimaryCheckBox(inputId) {

    if ($("#" + inputId + "Primary_switch").is(":checked")) {

        $("#" + inputId + "PrimaryQuantity").prop("disabled", false);
        $("#" + inputId + "PrimaryQuantity").attr("required", true);
        $("#" + inputId + "Secondary_switch").prop("checked", false);
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").val("");
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
    }

    else {
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
        $("#" + inputId + "PrimaryQuantity").val("");
    }
}

function onChangeSecondaryCheckBox(inputId) {

    if ($("#" + inputId + "Secondary_switch").is(":checked")) {

        $("#" + inputId + "SecondaryQuantity").prop("disabled", false);
        $("#" + inputId + "SecondaryQuantity").attr("required", true);
        $("#" + inputId + "Primary_switch").prop("checked", false);
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").val("");
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
    }
    else {
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
        $("#" + inputId + "SecondaryQuantity").val("");
    }
}

function addNewRowToGrid(inputId) {

    var ProductId = $("#" + inputId + "Id").val();

    let primaryQuantity = 0;
    let secondaryQuantity = 0;
    let conversionFactor;
    let totalQuantity;


    let QuantitydivClass = $("#ProductUOMDiv").attr("class");
    
    if (QuantitydivClass.includes("d-none") != true) {

        if ($("#" + inputId + "Primary_switch").prop("checked")) {
            primaryQuantity = $("#" + inputId + 'PrimaryQuantity').val() == "" ? 0 : $("#" + inputId + 'PrimaryQuantity').val();
            totalQuantity = primaryQuantity;
        }
        else {
            secondaryQuantity = $("#" + inputId + 'SecondaryQuantity').val() == "" ? 0 : $("#" + inputId + 'SecondaryQuantity').val();
            conversionFactor = $("#" + inputId + 'Conversion').val();
            totalQuantity = secondaryQuantity * conversionFactor;
        }

    }
    else {
        totalQuantity = $("#ProductQuantity").val();
        primaryQuantity = $("#ProductQuantity").val();
    }
    var rowData = {};
    rowData.ProductId = ProductId;
    rowData.ProductName = $("#ProductNameOnSelect").val();
    rowData.ProductPrimaryQuantity = primaryQuantity;
    rowData.ProductSecondaryQuantity = secondaryQuantity;
    rowData.totalQuantity = totalQuantity;
    var Purchasetable = $('#PurchaseRequisitionTable').DataTable();

    var data = Purchasetable.rows().data();
    var isAdded = true;
    data.each(function (i, item) {

        if (rowData.ProductId == i.ProductId) {
            isAdded = false;

        }

    });
    if (isAdded) {
        var table = $('#PurchaseRequisitionTable').DataTable();
        table.row.add({
            "ProductName": rowData.ProductName,
            "PrimaryQuantity": rowData.ProductPrimaryQuantity,
            "SecondaryQuantity": rowData.ProductSecondaryQuantity,
            "TotalQuantity": rowData.totalQuantity,
            "ProductId": rowData.ProductId
        }).draw();
        ResetModal();
    }
    else {
        toastr.error("Product is Already Exist", { timeOut: 5000 });
    }

}

function ResetModal() {

    UnselectKendoDropDown("ProductId");
    $("#Product").val('');
    $("#ProductQuantityDiv").addClass("d-none");
    $("#ProductUOMDiv").addClass("d-none");
    $("#ProductQuantity").val("");
    $("#ProductPrimaryUOM").html("");
    $("#ProductPrimary_switch").prop("checked", false);
    $("#ProductPrimaryQuantity").val('');
    $("#ProductPrimaryQuantity").prop("disabled", true);
    $("#ProductSecondaryQuantity").prop("disabled", true);
    $("#ProductSecondaryUOM").html("");
    $("#ProductSecondary_switch").prop("checked", false);
    $("#ProductSecondaryQuantity").val('');
    $("#ProductconversionName").html("");
    $("#ProductConversion").val('');

}

function RemoveGridProduct(productId) {
    var actionsHtml =
        ` <i class="bx bx-x" onclick="removeEntry(this)" title="Cancelled"></i> `;

    return actionsHtml;
}

function removeEntry(rowPointer) {

    var table = $('#PurchaseRequisitionTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
    var row = table
        .rows(function (idx, data, node) {
            if (data.ProductId == rowData.ProductId) {

                return true;
            }
        })
        .remove()
        .draw();

}

function AddPurchaseRequisition() {

    var formId = "form";
    if (validate(formId)) {

        var PurchaseRequisition = {
            
            LocationId: $("#LocationId").val(),
            PurchaseTypeId: $("#PurchaseTypeId").val(),
            PriorityLevelId: $("#PriorityLevelId").val(),
            Remarks: $("#Remarks").val()
        };

        PurchaseRequisition.PurchaseRequisitionDetail = [];

        var table = $('#PurchaseRequisitionTable').DataTable();
        var data = table.rows().data();
        if (data.length > 0) {
            data.each(function (value, index) {
                var Detail = {};
                Detail.ProductId = value.ProductId;
                Detail.PrimaryQuantity = value.PrimaryQuantity;
                Detail.SecondaryQuantity = value.SecondaryQuantity;
                Detail.TotalQuantity = value.TotalQuantity;
                PurchaseRequisition.PurchaseRequisitionDetail.push(Detail);
            });

            ShowLoader();
            $.ajax({
                url: '../api/PurchaseRequisition/Post/',
                type: 'POST',
                data: JSON.stringify(PurchaseRequisition),
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    toastr.success("Purchase Requisition Added Successfully", { timeOut: 5000 });
                    HideLoader();
                    ResetForm();
                },
                error: function (data) {
                    var response = data.responseText.replace(/"/g, '');
                    toastr.warning(response, { timeOut: 5000 });
                    ResetForm();
                    HideLoader();
                }
            });
        }
        else {
            toastr.warning("Please Add Product", { timeOut: 5000 });
            return false;
        }
    }
}

function populateData(id) {
    
    ShowLoader();
    $.ajax({
        url: "../api/PurchaseRequisition/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
            LoadKendoDropDown('LocationId', '-- Please Select Product --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId),
            LoadKendoDropDown('PurchaseTypeId', '-- Please Select PurchaseType --', '../api/Developer/GetAllActivePurchaseType');
            $("#PurchaseTypeId").val(data.PurchaseTypeId);
            $("#PriorityLevelId").val(data.PriorityLevelId);
            $("#Remarks").val(data.Remarks);

            dataTable.initialize();
            var table = $("#PurchaseRequisitionTable").DataTable();
            $.each(data.PurchaseRequisitionDetail, function (index, value) {

                table.row.add({
                    "ProductName": value.ProductName,
                    "PrimaryQuantity": value.PrimaryQuantity === null ? "" : value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "ProductId": value.ProductId
                }).draw();

                HideLoader();
            });
        }
    });

}

function Update() {

    var formId = "form";
    if (validate(formId)) {

        var PurchaseRequisition = {

            Id: $("#PurchaseRequisitionId").val(),
            LocationId: $("#LocationId").val(),
            PurchaseTypeId: $("#PurchaseTypeId").val(),
            PriorityLevelId: $("#PriorityLevelId").val(),
            Remarks: $("#Remarks").val()
        };

        PurchaseRequisition.PurchaseRequisitionDetail = [];

        var table = $('#PurchaseRequisitionTable').DataTable();
        var data = table.rows().data();
        if (data.length > 0) {
            data.each(function (value, index) {
                var Detail = {};
                Detail.ProductId = value.ProductId;
                Detail.PrimaryQuantity = value.PrimaryQuantity;
                Detail.SecondaryQuantity = value.SecondaryQuantity;
                Detail.TotalQuantity = value.TotalQuantity;
                PurchaseRequisition.PurchaseRequisitionDetail.push(Detail);
            });
            ShowLoader();
            $.ajax({
                url: '../api/PurchaseRequisition/Put/',
                type: 'POST',
                data: JSON.stringify(PurchaseRequisition),
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    ResetForm();
                    HideLoader();
                    toastr.success("Purchase Requisition Added Successfully", { timeOut: 5000 });
                    RedirectToUrl('../PurchaseRequisitionManagement/Manage');
                },
                error: function (data) {
                    var response = data.responseText.replace(/"/g, '');
                    HideLoader();
                    toastr.warning(response, { timeOut: 5000 });
                }
            });
        }
        else {
            toastr.warning("Please Add Product", { timeOut: 5000 });
            return false;
        }
    }
}

function ResetForm() {
    LoadKendoDropDown('LocationId', '-- Please Select Product --', '');
    LoadKendoDropDown('PurchaseTypeId', '-- Please Select PurchaseType --', '');
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '');
    $("#LocationId").val("");
    $("#PurchaseTypeId").val("");
    $("#PriorityLevelId").val("");
    $("#Remarks").val("");
    var table = $('#PurchaseRequisitionTable').DataTable();
    table.clear().draw();
}