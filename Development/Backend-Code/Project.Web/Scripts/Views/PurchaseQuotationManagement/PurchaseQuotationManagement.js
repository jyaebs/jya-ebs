﻿var PurchaseQuotationId = 0;
var array = [];
var rowId = 0;


function initializeForm() {

    $(".hideDiv").addClass("d-none");
    $("#addButton").addClass("d-none");

    // Vendor Modal Opening button On/Off
    $(document).on("change", ".Requisitionitem", function () {
        $("tr").removeClass("focusedRow");
        if ($(this).is(":checked")) {
            array.push($(this).attr("id"));
            $(this).closest("tr").addClass("focusedRow");
        }
        else {
            array.splice(array.indexOf($(this).attr("id")), 1);
        }
    });

    // Append Left side Datatable on basis of Requisition
    $('#RequisitionDetailTable').DataTable({
        processing: false,
        searching: false, paging: false, info: false,
        serverSide: false,
        autoWidth: true,
        "columns": [
            { "data": "Id" },
            { "data": "PrimaryQuantity", "visible": false },
            { "data": "SecondaryQuantity", "visible": false },
            { "data": "TotalQuantity", "visible": false },
            { "data": "RequisitionId", "visible": false },
            { "data": "ProductId", "visible": false },
            { "data": "ProductName" },
            { "data": "IsFinalized", "visible": false }
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    let button = `<div class="form-check mb-3 custom-radio-success mb-3">
                                  <input type="radio" id="itemChecked-${row.Id}"  name="itemChecked"  onchange="hideShowRowData(${row.Id},${row.IsFinalized})" class="form-check-input Requisitionitem">
                                  <label class="form-check-label" for="itemChecked-${row.Id}"></label>
                                  </div>`;
                    return button;
                },
                "targets": -8
            },
            {
                orderable: false,
                targets: "no-sort"
            },
            { width: "10px", targets: 6 }
        ],
        rowId: 'Id'
    });

    // Append right side Datatable data after selecting vendor and price
    $('#QuotationDetailTable').DataTable({
        processing: false,
        searching: false, paging: false, info: false,
        serverSide: false,
        autoWidth: false,
        "columns": [
            { "data": "RequisitionDetailId" },
            { "data": "Product" },
            { "data": "ProductId", "visible": false },
            { "data": "Vendor" },
            { "data": "VendorId", "visible": false },
            { "data": "PrimaryQuantity" },
            { "data": "SecondaryQuantity" },
            { "data": "TotalQuantity" },
            { "data": "Price" },
            { "data": "NetAmount" },
            { "data": "" }
        ],
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    let actionButton = `<a href="#"  class="text-danger mr-2" onclick="removeEntry(${row.ProductId}, ${row.VendorId})">
                    <i class="mdi mdi-close QuotationDetailDeleteSign-${row.RequisitionDetailId} font-size-14" title="Remove"></i>
                    </a>`;
                    return actionButton;
                },
                "targets": -1
            },
            {
                "render": function (data, type, row) {
                    let button = `<div class="form-check mb-3 custom-radio-success mb-3">
                                  <input type="radio" onchange=FinalizeQuotation(this) Id="QuotationDetail-${row.ProductId}-${row.VendorId}" class="form-check-input QuotationDetail-${row.RequisitionDetailId}">
                                  <label class="form-check-label"></label>
                                  </div>`;
                    return button;
                },
                "targets": -11
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        rowId: 'RequisitionDetailId'
    });

    // Select Requisiton Data from Modal
    $("#PurchaseRequisitionTable tbody").on("click", "tr", function () {

        var table = $('#PurchaseRequisitionTable').DataTable();
        $(".hideDiv").removeClass("d-none");

        let RequisitionId = table.rows().data()[this.rowIndex - 1].Id;

        GetRequisitionDetail(RequisitionId);

        $("#addButton").removeClass("d-none");
        $('#QuotationDetailTable').DataTable().clear().draw();


        $("#PurchaseRequisitionModal").modal("hide");

    });

}

function FinalizeQuotation(rowPointer) {

    var row = $(rowPointer).closest("tr")[0];
    var table = $('#QuotationDetailTable').DataTable();

    var productId = table.rows().data()[row.rowIndex - 1].ProductId;
    let productName = table.rows().data()[row.rowIndex - 1].Product;
    var VendorId = table.rows().data()[row.rowIndex - 1].VendorId;

    if ($("#QuotationDetail-" + productId + "-" + VendorId).attr("checked", true)) {

        // Change Color and Style of selected row 
        $(rowPointer).closest("tr").addClass("focusedRow");

        $(`.QuotationDetail-${row.id}`).attr("disabled", true);
        $(`.QuotationDetailDeleteSign-${row.id}`).removeClass("mdi-close");

        $("#AddVendor").addClass("d-none");

        IsFinalizedcheck(productName);
    }
}

// Finalize Quotation for an item
function IsFinalizedcheck(ProductValue) {

    var table = $('#RequisitionDetailTable').DataTable();
    var rowData = table.rows().data();

    $.each(rowData, function (i, item) {
        if (item.ProductName == ProductValue) {
            item.IsFinalized = true;
        }
    });
}

function GetPurchaseRequisition() {

     $("#PurchaseRequisitionModal").toggle("modal");

    var dtCommonParam = {
        getDataUrl: `../api/PurchaseRequisition/GetOpenRequistion`
    };
    $('#PurchaseRequisitionTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "ReferenceNumber" },
            { "data": "LocationName" },
            { "data": "LocationId", "visible": false },
            { "data": "PriorityLevel" },
            { "data": "PriorityLevelId", "visible": false },
            { "data": "PurchaseTypeName" },
            { "data": "PurchaseTypeId", "visible": false }

        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ]
    });
}

// Fetch Requisition Detail
function GetRequisitionDetail(id) {

    $.ajax({
        url: "../api/PurchaseRequisition/GetById?id=" + id,
        type: 'GET',
        success: function (data) {

            $("#PurchaseReferenceNumnber").val(data.ReferenceNumber);
            $("#PurchaseRequisitionId").val(data.Id);
            $("#PurchaseTypeId").val(data.PurchaseTypeId);
            $("#PurchaseTypeName").val(data.PurchaseTypeName);
            $("#PriorityLevelId").val(data.PriorityLevelId);
            $("#PriorityLevel").val(data.PriorityLevel);
            $("#Location").val(data.LocationName);
            $("#LocationId").val(data.LocationId);

            LoadKendoAutoComplete('Vendor', '-- Please Select Vendor --', '../api/Vendor/GetActiveVendor?LocationId=' + data.LocationId);

            var table = $('#RequisitionDetailTable').DataTable();
            table.clear().draw();

            $.each(data.PurchaseRequisitionDetail, function (index, value) {
                table.row.add({
                    "Id": value.Id,
                    "RequisitionId": value.RequisitionId,
                    "ProductId": value.ProductId,
                    "ProductName": value.ProductName,
                    "PrimaryQuantity": value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "IsFinalized": false
                }).draw();
            });
        }
    });
}

// Hide row Data on the basis of Products Selection
function hideShowRowData(rowDataId, isFinalized) {

    rowId = rowDataId;

    var table = $('#QuotationDetailTable').DataTable();

    table.rows(function (idx, data, node) {
        if (data.RequisitionDetailId === rowDataId) {
            table.rows(idx).nodes().to$().removeClass("d-none");
        }
        else {
            table.rows(idx).nodes().to$().addClass("d-none");

        }
    }).draw();

    if (isFinalized == false) {
        $("#AddVendor").removeClass("d-none");
    }
    else {
        $("#AddVendor").addClass("d-none");
    }

}

// Remove data row from right datatable
function removeEntry(rowProduct, rowVendor) {
    var table = $('#QuotationDetailTable').DataTable();

    table.rows(function (idx, data, node) {
        if (data.ProductId === rowProduct && data.VendorId == rowVendor) {
            return true;
        }
    }).remove().draw();

}

function resetPriceAndVendor() {
    let VendorId = $("#VendorId").val();

    if (VendorId == "") {
        $("#Price").val("");
    }
}

function onSelectRecord(e) {
    var inputId = this.element[0].id;
    var dataItem = this.dataItem(e.item.index());

    $("#" + inputId + "Id").val(dataItem.Id);
    $("#" + inputId + "NameOnSelect").val(dataItem.Name);


}

function OnChangeRecord(e) {
    var inputId = this.element[0].id;
    var newName = $("#" + inputId).val();
    var oldName = $("#" + inputId + "NameOnSelect").val();
    if (newName != oldName) {
        $("#" + inputId + "Id").val("");
        resetPriceAndVendor();
    }
}

// Adding new row data in right side table
function addNewRowToGrid() {

    let Vendor = $("#VendorNameOnSelect").val();
    let VendorId = $("#VendorId").val();
    let Price = $("#Price").val();

    if (VendorId == "") {
        toastr.warning("Please Select Vendor to proceed", { timeOut: 5000 });
        return false;
    }

    if (Price == "") {
        toastr.warning("Please Enter Price to proceed", { timeOut: 5000 });
        return false;
    }

    if (Price <= 0) {
        toastr.warning("Price Cannot be less than or equal to Zero", { timeOut: 5000 });
        return false;
    }

    let table = $('#RequisitionDetailTable').DataTable();
    var rowData = {};
    table.rows(function (idx, data, node) {
        if (data.Id === rowId) {

            rowData = data;
        }
    });

    var Purchasetable = $('#QuotationDetailTable').DataTable();
    var data = Purchasetable.rows().data();
    var isAdded = false;

    data.each(function (i, item) {
        if (VendorId === i.VendorId && rowData.ProductId === i.ProductId) {
            toastr.error("Quotation of this Vendor against this item is Already Added", { timeOut: 5000 });
            isAdded = true;
        }
    });
    if (isAdded == false) {
        var tableData = $('#QuotationDetailTable').DataTable();
        tableData.row.add({
            "RequisitionDetailId": rowData.Id,
            "Product": rowData.ProductName,
            "ProductId": rowData.ProductId,
            "Vendor": Vendor,
            "VendorId": VendorId,
            "PrimaryQuantity": rowData.PrimaryQuantity,
            "SecondaryQuantity": rowData.SecondaryQuantity,
            "TotalQuantity": rowData.TotalQuantity,
            "Price": Price,
            "NetAmount": rowData.TotalQuantity * Price
        }).draw();

        $("#PurchaseQuotationDetailModal").modal("hide");
        $("#Price").val("");
        $("#Vendor").val("");
    }
}

function Add() {

    let formId = "form";
    if (validate(formId)) {

        let IsFinalizedProduct = true;
        let leftTable = $('#RequisitionDetailTable').DataTable();
        let leftTableData = leftTable.rows().data();

        leftTableData.each(function (value, index) {
            let checkFinalize = leftTable.rows().data()[index].IsFinalized;
            if (checkFinalize == false) {
                IsFinalizedProduct = false;
            }
        });
        if (IsFinalizedProduct == true) {
            let VendorList = [];
            let PurchaseQuotationDetail = [];
            let table = $('#QuotationDetailTable').DataTable();
            let data = table.rows().data();
            data.each(function (value, index) {

                let radioStatus = $('#QuotationDetail-' + value.ProductId + "-" + value.VendorId).prop('checked');
                let CheckedquotationObject = {
                    PurchaseRequisitionDetailId: value.RequisitionDetailId,
                    VendorId: value.VendorId,
                    ProductId: value.ProductId,
                    PrimaryQuantity: value.PrimaryQuantity,
                    SecondaryQuantity: value.SecondaryQuantity,
                    TotalQuantity: value.TotalQuantity,
                    Price: value.Price,
                    NetAmount: value.NetAmount,
                    isChecked: radioStatus
                };
                PurchaseQuotationDetail.push(CheckedquotationObject);
                if (radioStatus) {
                    if (!VendorList.includes(value.VendorId))
                        VendorList.push(value.VendorId);
                }
            });
            
            if (VendorList.length === 1) {

                let Quotation = {
                    LocationId: $("#LocationId").val(),
                    PurchaseRequisitionId: $("#PurchaseRequisitionId").val(),
                    PurchaseTypeId: $("#PurchaseTypeId").val(),
                    PriorityLevel: $("#PriorityLevelId").val(),
                    Remarks: $("#Remarks").val(),
                    PurchaseQuotationDetail: PurchaseQuotationDetail
                };

                ShowLoader();
                $.ajax({
                    url: '../api/PurchaseQuotation/Post/',
                    type: 'POST',
                    data: JSON.stringify(Quotation),
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {
                        toastr.success("Purchase Quotation Added Successfully", { timeOut: 5000 });
                        HideLoader();
                        ResetForm();
                    },
                    error: function (data) {
                        var response = data.responseText.replace(/"/g, '');
                        toastr.error(response, { timeOut: 5000 });
                        HideLoader();
                    }

                });
            }
            else {
                toastr.warning("Select one Vendor against selected Quotation", { timeOut: 5000 });
                return false;
            }
        }
        else {
            toastr.warning("Please Add / Finalize Quotation", { timeOut: 5000 });
            return false;
        }
    }
}

function ResetForm() {

    $("#LocationId").val("");
    $("#Location").val("");
    $("#PurchaseRequisitionId").val("");
    $("#PurchaseReferenceNumnber").val("");
    $("#PurchaseTypeId").val("");
    $("#PurchaseTypeName").val("");
    $("#PriorityLevelId").val("");
    $("#PriorityLevel").val("");
    $("#Remarks").val("");
    $(".hideDiv").addClass("d-none");
    $("#AddVendor").addClass("d-none");
    $('#RequisitionDetailTable').DataTable().clear().draw();
    $('#QuotationDetailTable').DataTable().clear().draw();

}

function populateData(id) {
    PurchaseQuotationId = id;
    ShowLoader();
    $.ajax({
        url: "../api/PurchaseQuotation/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#PurchaseReferenceNumnber').val(data.ReferenceNumber);
            $('#PurchaseTypeName').val(data.PurchaseTypeName);
            $('#PriorityLevel').val(data.PriorityLevelName);
            $('#Location').val(data.LocationName);
            $("#Remarks").val(data.Remarks);


            const key = 'Product';
            const arrayUniqueByKey = [...new Map(data.PurchaseQuotationDetail.map(item =>
                [item[key], item])).values()];

            var Requisitiontable = $('#RequisitionDetailTable').DataTable();
            Requisitiontable.clear().draw();

            $.each(arrayUniqueByKey, function (index, value) {
                Requisitiontable.row.add({
                    "Id": value.PurchaseRequisitionDetailId,
                    "RequisitionId": value.PurchaseRequisitionDetailId,
                    "ProductId": value.ProductId,
                    "ProductName": value.Product,
                    "PrimaryQuantity": value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "IsFinalized": true
                }).draw();
                $(`#itemChecked-${value.PurchaseRequisitionDetailId}`).prop('checked', true);
                hideShowRowData(value.PurchaseRequisitionDetailId, true);
            });

            var table = $('#QuotationDetailTable').DataTable();
            $.each(data.PurchaseQuotationDetail, function (index, value) {
                table.row.add({
                    "RequisitionDetailId": value.PurchaseRequisitionDetailId,
                    "Product": value.Product,
                    "ProductId": value.ProductId,
                    "Vendor": value.Vendor,
                    "VendorId": value.VendorId,
                    "PrimaryQuantity": value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "Price": value.Price,
                    "NetAmount": value.NetAmount
                }).draw();

                $("#QuotationDetail-" + value.ProductId + "-" + value.VendorId).prop("checked", value.isChecked);
                $(`.QuotationDetail-${value.PurchaseRequisitionDetailId}`).prop("disabled", true);
                $(`.QuotationDetailDeleteSign-${value.PurchaseRequisitionDetailId}`).removeClass("mdi-close");
            });
            
            $(".hideDiv").removeClass("d-none");
            HideLoader();
        }
    });
}





