﻿var ProductBrandId = 0;
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var ProductBrand = {

            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/ProductBrand/Post/',
            type: 'POST',
            data: JSON.stringify(ProductBrand),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Brand Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var ProductBrand = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: ProductBrandId
        };
        ShowLoader();
        $.ajax({
            url: '../api/ProductBrand/Put/',
            type: 'POST',
            data: JSON.stringify(ProductBrand),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Brand Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../ProductBrandManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    ProductBrandId = id;
    ShowLoader();
    $.ajax({
        url: "../api/ProductBrand/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}