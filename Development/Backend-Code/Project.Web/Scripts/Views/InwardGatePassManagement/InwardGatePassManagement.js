﻿
var InwardGatePassId = 0;

function initializeForm() {

    var today = new Date().toISOString().split('T')[0];
    $("#inwardDate").attr("max", today);

    dataTable.initialize();
    $(".ModaldataDiv").addClass("d-none");
    $("#addBtn").addClass("d-none");

    // Select Purchase Order Data from Modal
    $("#PurchaseOrderDetailTable tbody").on("click", "tr", function () {
        var table = $('#PurchaseOrderDetailTable').DataTable();

        var dataId = table.rows().data()[this.rowIndex - 1].Id;
        
        GetPurchaseOrderDetail(dataId);

        $("#PurchaseOrderDetailModal").modal("hide");
        $(".ModaldataDiv").removeClass("d-none");
        $('#PurchaseOrderDetailTable').DataTable().clear().draw();


       
    });
}

var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#PurchaseOrderTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            autoWidth: false,
            "columns": [
                { "data": "ProductName"},
                { "data": "ProductId", "visible": false },
                { "data": "PrimaryQuantity", className: "text-center" },
                { "data": "SecondaryQuantity", className: "text-center" },
                { "data": "TotalQuantity", className: "text-center" },
                { "data": "ReceivedQuantity", className: "text-center" },
                { "data": "UnitPrice", className: "text-center" },
                { "data": "NetAmount", className: "text-center" }
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return inputField = `<input class="form-control text-center receivedItems" value="${row.ReceivedQuantity}"  type="number" onkeyup='calculateNetAmount()'/>`;
                    },
                    "targets": -3
                },
                {
                    "render": function (data, type, row) {
                        return rowText = `<span class="text-center" id="netAmountText-${row.NetAmount}">${row.NetAmount}</span>`
                    },
                    "targets": -1
                },

                {
                    orderable: false,
                    targets: "no-sort"
                }
            ]
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};

function calculateNetAmount() {
    var table = $('#PurchaseOrderTable').DataTable();
    var data = table.rows().data();
    var NetAmount = 0
    data.each(function (value, index) {
        let receivedQty = table.rows(index).nodes().to$().find(".receivedItems").val();
        var netAmountText= value.NetAmount;
        let unitPrice = value.UnitPrice;
        netAmount = (receivedQty == "" ? 0 : parseFloat(receivedQty)) * unitPrice;
        NetAmount = NetAmount + netAmount;
        $("#netAmountText-" + netAmountText).text(netAmount);
    });

    $("#totalNetAmount").text(NetAmount)
}

function validateReceivedQty() {
    var table = $('#PurchaseOrderTable').DataTable();
    var data = table.rows().data();
    var validateQty = false;
    data.each(function (value, index) {
        let receivedQty = table.rows(index).nodes().to$().find(".receivedItems").val();
        if (value.TotalQuantity < receivedQty) {
            toastr.warning(`Received Quantity of ${value.ProductName} Cannot be Greater than Total Quantity`, { timeOut: 5000 });
            validateQty = true;
        }
    });
    if (validateQty) {
        return false;
    }
    return true;
}

function Add() {
    var formId = "form";
    if (validate(formId) && validateReceivedQty()) {
        var InwardGatePass = {
            PurchaseOrderId: $("#PurchaseOrderId").val(),
            LocationId: $("#LocationId").val(),
            VendorId: $("#VendorId").val(),
            VoucherNo: $("#VoucherNo").val(),
            InwardDate: $("#inwardDate").val(),
            NetAmount: $("#totalNetAmount").text(),
            Remarks: $("#Remarks").val()
        };

        InwardGatePass.InwardGatePassDetails = [];
        var table = $('#PurchaseOrderTable').DataTable();
        var data = table.rows().data();

        data.each(function (value, index) {
            let receivedQty = table.rows(index).nodes().to$().find(".receivedItems").val();

            let inwardObject = {
                ProductId: value.ProductId,
                PrimaryQuantity: value.PrimaryQuantity,
                SecondaryQuantity: value.SecondaryQuantity,
                TotalQuantity: value.TotalQuantity,
                ReceivedQuantity: receivedQty,
                UnitPrice: value.UnitPrice,
                NetAmount: value.UnitPrice * receivedQty
            };

            InwardGatePass.InwardGatePassDetails.push(inwardObject);

        });
        ShowLoader();
        $.ajax({
            url: '../api/InwardGatePass/Post/',
            type: 'POST',
            data: JSON.stringify(InwardGatePass),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Inward Gate Pass Added Succesfully", { timeOut: 5000 });
                HideLoader();
                ResetForm();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}

function GetPurchaseOrder() {
    $("#PurchaseOrderDetailModal").toggle("modal");

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/PurchaseOrder/GetAll`,
        tableId: 'PurchaseOrderDetailTable'
    };
    $('#PurchaseOrderDetailTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "PurchaseReferenceNumber" },
            { "data": "Name" },
            { "data": "LocationId", "visible": false },
            { "data": "VendorName" },
            { "data": "VendorId", "visible": false },
            { "data": "VoucherNo" },
            { "data": "OrderDateString" }
        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ],

    });
}

function GetPurchaseOrderDetail(id) {

    $.ajax({
        url: "../api/PurchaseOrder/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $("#PurchaseReferenceNumber").val(data.PurchaseReferenceNumber);
            $("#PurchaseOrderId").val(id);
            $("#Location").val(data.Name);
            $("#LocationId").val(data.LocationId);
            $("#Vendor").val(data.VendorName);
            $("#VendorId").val(data.VendorId);
            $("#VoucherNo").val(data.VoucherNo);
            $("#OrderDate").val(data.OrderDate.substr(0, 10));

            $("#addBtn").removeClass("d-none");
            $('#PurchaseOrderTable').DataTable().clear().draw();

            var table = $('#PurchaseOrderTable').DataTable();
            data.PurchaseDetails.forEach(function (order, index) {
                
                table.row.add({
                    "ProductId": order.ProductId,
                    "ProductName": order.ProductName,
                    "PrimaryQuantity": order.PrimaryQuantity == 0 ? "-" : order.PrimaryQuantity,
                    "SecondaryQuantity": order.SecondaryQuantity == 0 ? "-" : order.SecondaryQuantity,
                    "UnitPrice": order.UnitPrice,
                    "TotalQuantity": order.TotalQuantity,
                    "ReceivedQuantity": 0,
                    "NetAmount": order.NetAmount
                }).draw();
            });
        }
    });
}

function ResetForm() {
    $("#LocationId").val("");
    $("#Location").val("");
    $("#PurchaseReferenceNumber").val("");
    $("#PurchaseOrderId").val("");
    $("#VendorId").val("");
    $("#Vendor").val("");
    $("#VoucherNo").val("");
    $("#OrderDate").val("");
    $("#Vendor").val("");
    $("#inwardDate").val("");
    $("#totalNetAmount").text("-");
    $("#Remarks").val("");
    $(".ModaldataDiv").addClass("d-none");
    var table = $('#PurchaseOrderTable').DataTable();
    table.clear().draw();

}

function populateData(id) {
    InwardGatePassId = id;
    ShowLoader();
    $.ajax({
        url: "../api/InwardGatePass/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            dataTable.initialize();

            $('#PurchaseReferenceNumber').val(data.ReferenceNumber);
            $('#Vendor').val(data.VendorName);
            $('#VoucherNo').val(data.VoucherNo);
            $('#Location').val(data.Name);
            $("#Remarks").val(data.Remarks);
            $("#OrderDate").val(data.OrderDate.substr(0, 10));
            $("#inwardDate").val(data.InwardDate.substr(0, 10));
            $("#totalNetAmount").text(data.NetAmount);
            
            var table = $('#PurchaseOrderTable').DataTable();
            data.InwardGatePassDetails.forEach(function (inward, index) {
                
                table.row.add({
                    "ProductName": inward.ProductName,
                    "ProductId": inward.ProductId,
                    "PrimaryQuantity": inward.PrimaryQuantity == 0 ? "-" : inward.PrimaryQuantity,
                    "SecondaryQuantity": inward.SecondaryQuantity == 0 ? "-" : inward.SecondaryQuantity,
                    "TotalQuantity": inward.TotalQuantity,
                    "ReceivedQuantity": inward.ReceivedQuantity,
                    "UnitPrice": inward.UnitPrice,
                    "NetAmount": inward.NetAmount
                }).draw();
            });
            HideLoader();
        }
    });
}