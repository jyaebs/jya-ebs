﻿var GSTId = 0;
var entryLevelId = "";

function initializeForm() {
    debugger;
    $("#AccountLevelTable tbody").on("click", "tr", function () {
        debugger;
        var table = $('#AccountLevelTable').DataTable();
        debugger;
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        debugger;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == "GST") {
            $("#GSTAccountCode").val(rowAccountCode);
            $("#GSTAccountDescription").val(rowAccountName);
        }


        $("#AddAccountModal").modal("hide")


    });
}


function GetChartOfAccount(entryId) {
    entryLevelId = entryId;
    $("#AddAccountModal").toggle("modal");
    initializeDataTable();
}


function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetEntryLevelCOALevels`,
        tableId: 'AccountLevelTable'
    };
    $('#AccountLevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}


function Add() {
    var formId = "form";
    if (validate(formId)) {
        var GST = {
            Name: $('#Name').val(),
            Value: $('#Value').val(),
            AccountGST: $('#GSTAccountCode').val(),
        };
        ShowLoader();
        $.ajax({
            url: '../api/GST/Post/',
            type: 'POST',
            data: JSON.stringify(GST),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("GST Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    var formId = "form";
    if (validate(formId)) {
        var GST = {
            Value: $('#Value').val(),
            Name: $('#Name').val(),
            AccountGST: $('#GSTAccountCode').val(),
            Id: GSTId
        };
        ShowLoader();
        $.ajax({
            url: '../api/GST/Put/',
            type: 'POST',
            data: JSON.stringify(GST),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("GST Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../GSTManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function populateData(id) {
    GSTId = id;
    ShowLoader();
    $.ajax({
        url: "../api/GST/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Value').val(data.Value);
            $('#Name').val(data.Name);
            $('#GSTAccountCode').val(data.AccountGST);
            $('#GSTAccountDescription').val(data.AccountGSTDescription);
            HideLoader();
        }
    });
};


function ResetForm() {

    $('#Value').val("");
    $('#Name').val("");
    $('#GSTAccountCode').val("");
    $('#GSTAccountDescription').val("");
}