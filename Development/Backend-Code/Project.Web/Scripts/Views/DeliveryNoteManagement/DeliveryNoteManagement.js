﻿var DeliveryNoteId = 0;
var SaleOrderId = 0;
var GrossAmount = 0;
var TotalDiscountAmount = 0;
var NetAmount = 0;

$(document).ready(function () {

    initializeForm();
   
    $("#GetSaleOrderDetailTable tbody").on("click", "tr", function () {

        let table = $('#GetSaleOrderDetailTable').DataTable();
        let SaleOrderId = table.rows().data()[this.rowIndex - 1].Id;
        let Description = table.rows().data()[this.rowIndex - 1].Description;
        let PrimaryQuantity = table.rows().data()[this.rowIndex - 1].PrimaryQuantity;
        let SecondaryQuantity = table.rows().data()[this.rowIndex - 1].SecondaryQuantity;
        let TotalQuantity = table.rows().data()[this.rowIndex - 1].TotalQuantity;
        let DeliverQuantity = table.rows().data()[this.rowIndex - 1].DeliverQuantity;
        let Warehouse = table.rows().data()[this.rowIndex - 1].Warehouse;
        let GrossAmount = table.rows().data()[this.rowIndex - 1].GrossAmount;
        let Discount = table.rows().data()[this.rowIndex - 1].Discount;
        let DiscountAmount = table.rows().data()[this.rowIndex - 1].DiscountAmount;
        let NetAmount = table.rows().data()[this.rowIndex - 1].NetAmount;
        let ProductId = table.rows().data()[this.rowIndex - 1].ProductId;

       

        GetSaleOrderDetail(SaleOrderId);
        $("#GetSaleOrderDetailModal").modal("hide");
    });
});

function initializeForm() {

  
    dataTable.initialize();
}



var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#DeliveryNoteTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            destroy: false,
            "columns": [
                { "data": "Description" },
                { "data": "PrimaryQuantity" },
                { "data": "SecondaryQuantity" },
                { "data": "TotalQuantity" },
                { "data": "DeliverQuantity" },
                { "data": "Warehouse"},
                { "data": "UnitPrice" },
                { "data": "GrossAmount" },
                { "data": "Discount" },
                { "data": "DiscountAmount" },
                { "data": "NetAmount" },
                { "data": "ProductId", className: "text-center" },
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        var actionsHtml =
                            ` <i class="mdi mdi-close red  font-size-14" onclick="removeEntry(this)" title="Cancelled"></i> `;

                        return actionsHtml;
                    },
                    "targets": -1
                },


                {
                    "render": function (data, type, row) {
                        var actionsHtml =
                            `  <div class="defaultDropdown">
                                <input  class="form-control " id="WarehouseId-${row.ProductId}" name="WarehouseId-${row.ProductId}"/>
                                <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" data-for="WarehouseId" style="display:none;"></span>
                              </div>`;

                        return actionsHtml;
                    },
                    "targets": -7
                },

                {
                    "render": function (data, type, row) {
                        debugger;
                        var actionsHtml =
                            `  <input class="form-control" id="DeliverQuantity-${row.ProductId}" onkeyup="GetDeliverQuantity(this)" type="number" required />`;
                        return actionsHtml;
                    },
                    "targets": -8
                },

                {
                    orderable: false,
                    targets: "no-sort",
                }
            ],
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};

function removeEntry(rowPointer) {

    var table = $('#DeliveryNoteTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
    var row = table
        .row($(rowPointer).closest("tr")[0])
        .remove()
        .draw();
    CalculateAmounts();
}



function AddDeliveryNote() {
  
    var formId = "form";
    if (validate(formId)) {
        var DeliveryNote = {
            SaleOrderId: SaleOrderId,
            LocationId: $("#LocationId").val(),
            CustomerId: $("#CustomerId").val(),

            DeliveryDate: $("#DeliveryDate").val(),
            TotalNetAmount: $("#TotalNetAmount").text(),
            TotalDiscountAmount: $("#TotalDiscountAmount").text(),
            TotalGrossAmount: $("#TotalGrossAmount").text(), 
            Remarks: $("#Remarks").val(),
        };

        DeliveryNote.DeliveryNoteDetail = [];
        debugger;
        var table = $('#DeliveryNoteTable').DataTable();

        var data = table.rows().data();
        data.each(function (value, index) {
            debugger;
            var Details = {};
            Details.ProductId = value.ProductId;
            Details.PrimaryQuantity = value.PrimaryQuantity;
            Details.SecondaryQuantity = value.SecondaryQuantity;
            Details.TotalQuantity = value.TotalQuantity;
            Details.UnitPrice = value.UnitPrice;
            Details.DeliverQuantity = $(`#DeliverQuantity-${value.ProductId}`).val();
            Details.WarehouseId = $(`#WarehouseId-${value.ProductId}`).val();
            Details.GrossAmount = value.GrossAmount;
            Details.Discount = value.Discount;
            Details.DiscountAmount = value.DiscountAmount;
            Details.NetAmount = value.NetAmount;
            DeliveryNote.DeliveryNoteDetail.push(Details);
        });
        debugger;
        ShowLoader();
        $.ajax({
            url: '../api/DeliveryNote/Post/',
            type: 'POST',
            data: JSON.stringify(DeliveryNote),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Delivery Note Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();

            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.warning(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }   
}

//function populateData(id) {
//    debugger;

//    DeliveryNoteId = id;
//    ShowLoader();
//    $.ajax({
//        url: "../api/DeliveryNote/GetById?id=" + id,
//        type: 'GET',
//        contentType: "application/json;charset=utf-8",
//        success: function (data) {
          
//            LoadKendoDropDown('LocationId', '-- Please Select Product --', '../api/Location/GetAllFactoryByLocationType');
//            $("#LocationId").val(data.LocationId),
//            $("#CustomerId").val(data.CustomerId),
//            $("#Customer").val(data.CustomerName),
//            $("#DeliveryDate").val(data.DeliveryDate.substr(0, 10));
//            $("#TotalNetAmount").html(data.TotalNetAmount);
//            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
//            $("#TotalGrossAmount").html(data.TotalGrossAmount);
//            var table = $("#DeliveryNoteTable").DataTable();
//            $.each(data.DeliveryNoteDetail, function (index, value) {
//                table.row.add({
//                    "Description": value.ProductName,
//                    "PrimaryQuantity": value.PrimaryQuantity,
//                    "SecondaryQuantity": value.SecondaryQuantity,
//                    "TotalQuantity": value.TotalQuantity,
//                    "DeliverQuantity": value.TotalQuantity,
//                    "WarehouseId": LoadWarehouse(value.ProductId),
//                    "UnitPrice": value.UnitPrice,
//                    "GrossAmount": value.GrossAmount,
//                    "Discount": value.Discount,
//                    "DiscountAmount": value.DiscountAmount,
//                    "NetAmount": value.NetAmount,
//                    "ProductId": value.ProductId,
//                }).draw();
//                $(`#DeliverQuantity-${value.ProductId}`).val(value.TotalQuantity);
//                $(`#WarehouseId-${value.ProductId}`).val(value.WarehouseId);
//            });
//            HideLoader();
//        }
//    });


//}




function ResetForm() {
    TotalDiscountAmount = 0;
    GrossAmount = 0;
    netAmount = 0;

    LoadKendoDropDown('LocationId', '-- Please Select Location --', ' ');
    $("#LocationId").val("");
    LoadKendoAutoComplete('Customer', '-- Please Select Location --', '');
    $("#CustomerId").val("");
    $("#SaleReferenceNumber").val("");
    $("#DeliveryDate").val("");
    $("#TotalNetAmount").text(0);
    $("#TotalGrossAmount").text(0);
    $("#TotalDiscountAmount").text(0);
   
    var table = $('#DeliveryNoteTable').DataTable();
    table.clear().draw();

}

function GetOpenSaleOrder() {
    $("#GetSaleOrderDetailModal").toggle("modal");
    initializeSaleOrderTable();
}

function initializeSaleOrderTable() {

    var dtCommonParam = {
        getDataUrl: `../api/SaleOrder/GetOpenSaleOrder`,
        tableId: 'GetSaleOrderDetailTable'
    };
    $('#GetSaleOrderDetailTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "Post",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "ReferenceNumber" },
            { "data": "Name" },
            { "data": "CustomerName" },
            { "data": "LocationId", "visible": false },

        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ],

    });
}

function GetSaleOrderDetail(saleorderId) {
    SaleOrderId = saleorderId;
    debugger;
    $.ajax({
        url: "../api/SaleOrder/GetById?id=" + saleorderId,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId);
            LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + data.LocationId);
            $("#CustomerId").val(data.CustomerId);
            $("#Customer").val(data.CustomerName);
            $("#SaleReferenceNumber").val(data.ReferenceNumber);
            $("#TotalNetAmount").html(data.TotalNetAmount);
            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
            $("#TotalGrossAmount").html(data.TotalGrossAmount);
           
            var table = $("#DeliveryNoteTable").DataTable();
            debugger;
            $.each(data.SaleOrderDetail, function (index, value) {
                table.row.add({
                    "Description": value.ProductName,
                    "PrimaryQuantity": value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "DeliverQuantity": value.TotalQuantity,
                    "Warehouse": value.WarehouseId, 
                    "UnitPrice": value.UnitPrice,
                    "GrossAmount": value.GrossAmount,
                    "Discount": value.Discount,
                    "DiscountAmount": value.DiscountAmount,
                    "NetAmount": value.NetAmount,
                    "Id": value.Id,
                    "ProductId": value.ProductId,
                }).draw();
                debugger;
                $(`#DeliverQuantity-${value.ProductId}`).val(value.TotalQuantity);
                var DeliverQuantity = value.TotalQuantity;
                LoadWarehouse(value.ProductId);
                CalculateAmounts();
            });
             HideLoader();
        }
    });
}

function LoadWarehouse(ProductId)
{
    let LocationId = $("#LocationId").val();
    LoadKendoDropDown(`WarehouseId-${ProductId}`, '-- Please Select Warehouse --', '../api/Warehouse/GetActiveWarehouse?LocationId=' + LocationId);
}

function CalculateAmounts() {
    debugger;

    let GrossAmount = 0;
    let TotalDiscountAmount = 0;
    let NetAmount = 0;

    var table = $("#DeliveryNoteTable").DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {

        GrossAmount += value.GrossAmount;
        TotalDiscountAmount += value.DiscountAmount;
        NetAmount += value.NetAmount;
    });
    $("#TotalGrossAmount").text(GrossAmount);
    $("#TotalDiscountAmount").text(TotalDiscountAmount);
    $("#TotalNetAmount").text(NetAmount );
}
    
function GetDeliverQuantity(rowValue) {
    debugger;

    

    let newGrossAmount = 0;
    let newDiscountAmount = 0;
    let newNetAmount = 0;
    
    var table = $("#DeliveryNoteTable").DataTable();
    row_index = $(rowValue).closest('tr').index();
    let ProductName = $(rowValue).closest("tr").find("td:eq(0)").text();
    let PrimaryQuantity = $(rowValue).closest("tr").find("td:eq(1)").text();
    let SecoundaryQuantity = $(rowValue).closest("tr").find("td:eq(2)").text();
    let GrossAmount = $(rowValue).closest("tr").find("td:eq(7)").text();
    let Discount = $(rowValue).closest("tr").find("td:eq(8)").text();
    let DiscountAmount = $(rowValue).closest("tr").find("td:eq(9)").text();
    let NetAmount = $(rowValue).closest("tr").find("td:eq(10)").text(); 
    let ProductId = $(rowValue).closest("tr").find("td:eq(11)").text(); 

    let DeliverQuantity = $(rowValue).closest("tr").find("td:eq(4)").find('input').val();
    let TotalQuantity = $(rowValue).closest("tr").find("td:eq(3)").text();
    let UnitPrice = $(rowValue).closest("tr").find("td:eq(6)").text();
    
    if (parseInt(DeliverQuantity) > parseInt(TotalQuantity))
    {
        DeliverQuantity = TotalQuantity;
    }
 
    newGrossAmount = DeliverQuantity * UnitPrice;
    newDiscountAmount = newGrossAmount / Discount;
    newNetAmount = newGrossAmount - newDiscountAmount;
    $(rowValue).val(DeliverQuantity);
    table.cell({ row: row_index, column: 7 }).data(newGrossAmount);
    table.cell({ row: row_index, column: 9 }).data(newDiscountAmount);
    table.cell({ row: row_index, column: 10 }).data(newNetAmount);
   CalculateAmounts();
}
