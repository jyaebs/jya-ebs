﻿var count = 1;
var customerId = 0;
var entryLevelId = "";
var ReceivableAccount = "";

function initializeForm() {
    $('.credit').hide();
    $('#CreditDays').attr('required', false);
    $('#CreditAmount').attr('required', false);
    LoadKendoDropDown('CustomerType', '-- Please Select Customer Type --', '../api/CustomerType/GetActiveCustomerType');
    LoadKendoDropDown('Location', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');

    $("input, select").on("keyup change", function () {
        var dropDown = this.id;

        if (dropDown === "IsCredit") {
            var optionText = $("#IsCredit option:selected").text();
            if (optionText === 'Yes') {
                $('.credit').show();
                $('#CreditDays').attr('required', true);
                $('#CreditAmount').attr('required', true);
            }
            else {
                $('.credit').hide();
                $('#CreditDays').attr('required', false);
                $('#CreditAmount').attr('required', false);
                $('#CreditDays').val("");
                $('#CreditAmount').val("");
            }
        }
    });


    $("#AccountLevelTable tbody").on("click", "tr", function () {
        var table = $('#AccountLevelTable').DataTable();
        var rowAccountName = table.rows().data()[this.rowIndex - 1].Name;
        var rowAccountCode = table.rows().data()[this.rowIndex - 1].Code;

        if (entryLevelId == "AR") {
            $("#AccountReceivable").val(rowAccountCode);
            $("#ReceivableAccoutDescription").val(rowAccountName);
        }
        else {
            $("#DownPaymentAccount").val(rowAccountCode);
            $("#DownPaymentAccountDescription").val(rowAccountName);
        }

        $("#AddAccountModal").modal("hide")


    });


    $('#CNIC').keydown(function () {

        //allow  backspace, tab, ctrl+A, escape, carriage return
        if (event.keyCode === 8 || event.keyCode === 9
            || event.keyCode === 27 || event.keyCode === 13
            || (event.keyCode === 65 && event.ctrlKey === true))
            return;
        if (event.keyCode < 48 || event.keyCode > 57)
            event.preventDefault();

        var length = $(this).val().length;

        if (length === 5 || length === 13)
            $(this).val($(this).val() + '-');

    });
    $('#MobileNo').keydown(function () {

        //allow  backspace, tab, ctrl+A, escape, carriage return
        if (event.keyCode === 8 || event.keyCode === 9
            || event.keyCode === 27 || event.keyCode === 13
            || (event.keyCode === 65 && event.ctrlKey === true))
            return;
        if (event.keyCode < 48 || event.keyCode > 57)
            event.preventDefault();
    });
}




function GetChartOfAccount(entryId) {
    entryLevelId = entryId;
    $("#AddAccountModal").toggle("modal");
    initializeDataTable();
}


function initializeDataTable() {

    var dtCommonParam = {
        updateUrl: '#',
        getDataUrl: `../api/ChartOfAccount/GetEntryLevelCOALevels`,
        tableId: 'AccountLevelTable'
    };
    $('#AccountLevelTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "POST",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Code" },
            { "data": "Name" },
            { "data": "AccountTypeName" },
        ],
        "columnDefs": [

            {
                "render": function (data, type, row) {
                    return GetgridBoldCell(data);
                },
                "targets": [0, 1]
            },
            {
                orderable: false,
                targets: "no-sort"
            }
        ],
        "pagingType": "full_numbers",
        "order": [[0, "dsc"]]
    });
}


function checkValidate() {
    if (count === 1) {
        if (validate("form")) {
            count = 2;
        }
        else {
            var c = 'Customer-General-Information';
            $('.nav-justified a[href="#' + c + '"]').tab('show');
        }
    }
}

function Add() {
    var formId = "form";
    if (validate(formId)) {
        var Customer = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            CustomerTypeId: $('#CustomerType').val(),
            CNIC: $('#CNIC').val(),
            NTN: $('#NTN').val(),
            IsCredit: $('#IsCredit').val() == "" ? null : parseInt($('#IsCredit').val()),
            CreditDays: $('#CreditDays').val(),
            CreditAmount: $('#CreditAmount').val(),
            ContactPersonName: $('#ContactName').val(),
            MobileNo: $('#MobileNo').val(),
            Email: $('#OfficeEmail').val(),
            Address: $('#OfficeAddress').val(),
            LocationId: $('#Location').val(),
            AccountReceivable: $('#AccountReceivable').val(),
            DownPaymentAccount: $('#DownPaymentAccount').val()
        };

        ShowLoader();
        $.ajax({
            type: 'POST',
            url: '../api/Customer/Post',
            data: JSON.stringify(Customer),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Customer Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}


function ResetForm() {

    $('.nav-justified a[href="#Customer-General-Information"]').tab('show');
    $('#Code').val('');
    $('#Name').val('');
    UnselectKendoDropDown("CustomerType");
    $('#CNIC').val('');
    $('#NTN').val('');
    $('#IsCredit').val('');
    $('.credit').hide();
    $('#CreditDays').attr('required', false);
    $('#CreditAmount').attr('required', false);
    $('#CreditDays').val('');
    $('#CreditAmount').val('');
    $('#ContactName').val('');
    $('#MobileNo').val('');
    $('#OfficeEmail').val('');
    $('#OfficeAddress').val('');
    UnselectKendoDropDown("Location");
    $('#AccountReceivable').val('');
    $('#ReceivableAccoutDescription').val('');
    $('#DownPaymentAccount').val('');
    $('#DownPaymentAccountDescription').val('');
}

function populateData(id) {
    customerId = id;
    ShowLoader();
    $.ajax({
        url: "../api/Customer/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
           
            LoadKendoDropDown('Location', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $('#Location').val(data.LocationId);

            LoadKendoDropDown('CustomerType', '-- Please Select Customer Type --', '../api/CustomerType/GetActiveCustomerType');
            $('#CustomerType').val(data.CustomerTypeId);

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            $('#CNIC').val(data.CNIC);
            $('#NTN').val(data.NTN);
            $("#IsCredit").val(data.IsCredit == true ? 1 : 0);
            if (data.IsCredit) {
                $('.credit').show();
            }
            else {
                $('.credit').hide();
            }
            $('#CreditDays').val(data.CreditDays == 0 ? "" : data.CreditDays);
            $('#CreditAmount').val(data.CreditAmount == 0 ? "" : data.CreditAmount);
            $('#ContactName').val(data.ContactPersonName);
            $('#MobileNo').val(data.MobileNo);
            $('#OfficeEmail').val(data.Email);
            $('#OfficeAddress').val(data.Address);
            $('#AccountReceivable').val(data.AccountReceivable);
            $('#ReceivableAccoutDescription').val(data.AccountReceivableDescription);
            $('#DownPaymentAccount').val(data.DownPaymentAccount);
            $('#DownPaymentAccountDescription').val(data.DownPaymentAccountDescription);
            HideLoader();
        }
    });
}

function Update() {
    var formId = "form";
    if (validate(formId)) {
        var Customer = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            CustomerTypeId: $('#CustomerType').val(),
            CNIC: $('#CNIC').val(),
            NTN: $('#NTN').val(),
            IsCredit: $('#IsCredit').val() == "" ? null : parseInt($('#IsCredit').val()),
            CreditDays: $('#CreditDays').val(),
            CreditAmount: $('#CreditAmount').val(),
            ContactPersonName: $('#ContactName').val(),
            MobileNo: $('#MobileNo').val(),
            Email: $('#OfficeEmail').val(),
            Address: $('#OfficeAddress').val(),
            LocationId: $('#Location').val(),
            AccountReceivable: $('#AccountReceivable').val(),
            DownPaymentAccount: $('#DownPaymentAccount').val(),
            Id: customerId
        };

        ShowLoader();
        $.ajax({
            type: 'POST',
            url: '../api/Customer/Put',
            data: JSON.stringify(Customer),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Customer Updated Successfully", { timeOut: 5000 });
                ResetForm();
                RedirectToUrl('../CustomerManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}



function ValidateEmail() {
    var email = document.getElementById("OfficeEmail").value;
    var emailError = document.getElementById("emailError");
    emailError.innerHTML = "";
    var expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!expr.test(email)) {
        emailError.innerHTML = "Invalid email address.";
    }

    if (email === "") {
        emailError.innerHTML = "";
    }

}



