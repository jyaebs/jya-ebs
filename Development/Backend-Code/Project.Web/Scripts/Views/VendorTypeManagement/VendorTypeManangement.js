﻿var VendorTypeId = 0;
function Add() {
    var formId = "form";
    if (validate(formId)) {
        var VendorType = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        $.ajax({
            url: '../api/VendorType/Post/',
            type: 'POST',
            data: JSON.stringify(VendorType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Vendor Type Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    var formId = "form";
    if (validate(formId)) {
        var VendorType = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: VendorTypeId
        };
        ShowLoader();
        $.ajax({
            url: '../api/VendorType/Put/',
            type: 'POST',
            data: JSON.stringify(VendorType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Vendor Type Updated Successfully", { timeOut: 5000 });
                ResetForm();
                RedirectToUrl('../VendorTypeManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    VendorTypeId = id;
    ShowLoader();
    $.ajax({
        url: "../api/VendorType/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {
    $('#Code').val("");
    $('#Name').val("");

}