﻿var VehicleTypeid = 0;
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var VehicleType = {

            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/VehicleType/Post/',
            type: 'POST',
            data: JSON.stringify(VehicleType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Vehicle Type Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var VehicleType = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: VehicleTypeid
        };
        ShowLoader();
        $.ajax({
            url: '../api/VehicleType/Put/',
            type: 'POST',
            data: JSON.stringify(VehicleType),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Vehicle Type Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../VehicleTypeManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    VehicleTypeid = id;
    ShowLoader();
    $.ajax({
        url: "../api/VehicleType/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}