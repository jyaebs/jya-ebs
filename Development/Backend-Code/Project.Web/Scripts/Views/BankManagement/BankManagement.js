﻿var BankId = 0;
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var Bank = {

            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/Bank/Post/',
            type: 'POST',
            data: JSON.stringify(Bank),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Bank Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var Bank = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: BankId
        };
        ShowLoader();
        $.ajax({
            url: '../api/Bank/Put/',
            type: 'POST',
            data: JSON.stringify(Bank),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Bank Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../BankManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    BankId = id;
    ShowLoader();
    $.ajax({
        url: "../api/Bank/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}