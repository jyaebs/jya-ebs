﻿var ProductSubCategoryId = 0;

function initializeForm() {

    LoadKendoDropDown('ProductCategory', '-- Please Select Product Category --', '../api/ProductCategory/GetAllActiveProductCategory');
};
function Add() {

    var formId = "form";
    if (validate(formId)) {
        var ProductSubCategory = {
            ProdcutCategoryId: $("#ProductCategory").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        $.ajax({
            url: '../api/ProductSubCategory/Post/',
            type: 'POST',
            data: JSON.stringify(ProductSubCategory),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                 toastr.success("Product Sub Category Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {

    var formId = "form";
    if (validate(formId)) {
        var ProductSubCategory = {
            ProdcutCategoryId : $("#ProductCategory").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: ProductSubCategoryId
        };
        ShowLoader();
        $.ajax({
            url: '../api/ProductSubCategory/Put/',
            type: 'POST',
            data: JSON.stringify(ProductSubCategory),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Product Sub Category Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../ProductSubCategoryManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};

function populateData(id) {
    ProductSubCategoryId = id;
    ShowLoader();
    $.ajax({
        url: "../api/ProductSubCategory/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('ProductCategory', '-- Please Select Product Category --', '../api/ProductCategory/GetAllActiveProductCategory');
            $("#ProductCategory").val(data.ProdcutCategoryId),
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {
    $("#ProductCategory").val("");
    $('#Code').val("");
    $('#Name').val("");
    UnselectKendoDropDown("ProductCategory")
}




