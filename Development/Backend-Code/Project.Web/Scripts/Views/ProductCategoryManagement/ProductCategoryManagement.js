﻿var ProductCategoryId = 0;
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var ProductCategory = {

            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/ProductCategory/Post/',
            type: 'POST',
            data: JSON.stringify(ProductCategory),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                 toastr.success("Product Category Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                 toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;

    var formId = "form";
    if (validate(formId)) {
        var ProductCategory = {
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: ProductCategoryId
        };
        ShowLoader();
        $.ajax({
            url: '../api/ProductCategory/Put/',
            type: 'POST',
            data: JSON.stringify(ProductCategory),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Product Category Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../ProductCategoryManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    ProductCategoryId = id;
    ShowLoader();
    $.ajax({
        url: "../api/ProductCategory/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {

            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {

    $('#Code').val("");
    $('#Name').val("");

}