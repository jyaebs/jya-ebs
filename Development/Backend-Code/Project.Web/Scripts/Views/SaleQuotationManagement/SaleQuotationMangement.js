﻿var SaleQuotationId = 0;
var grossamount = 0;
var discountamount = 0;
var totalnetamount = 0;
var ProductId = 0;
var TotalGrossAmount = 0;
var TotalDiscountAmount = 0;
var Amount = 0;
var DiscountAmount = 0;
var GrossAmount = 0;
var populateGrossAmount = 0;
function initializeForm() {

    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
    LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '');

    dataTable.initialize();
}
function LoadCustomers(id) {
    debugger;
    var LocationId = $("#" + id).val();
    if (LocationId != "") {
        LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + LocationId);
    }

}
var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#SaleQuotationTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,


            "columns": [

                { "data": "Description" },
                { "data": "PrimaryQuantity" },
                { "data": "SecondaryQuantity" },
                { "data": "TotalQuantity" },
                { "data": "UnitPrice" },
                { "data": "GrossAmount" },
                { "data": "Discount" },
                { "data": "DiscountAmount" },
                { "data": "NetAmount" },
                { "data": "ProductId", className: "text-center" },
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        debugger;
                        return RemoveGridProduct(row.ProductId);
                    },
                    "targets": -1
                },

                {
                    orderable: false,
                    targets: "no-sort",
                }
            ],
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};


function onSelectRecord(e) {
    debugger;
    var inputId = this.element[0].id;
    var dataItem = this.dataItem(e.item.index());

    $("#" + inputId + "Id").val(dataItem.Id);
    $("#" + inputId + "NameOnSelect").val(dataItem.Name);
    if (inputId == "Product") {
        getProductData(inputId);
    }

}
function OnChangeRecord(e) {
    debugger;
    var inputId = this.element[0].id;
    var newName = $("#" + inputId).val();
    var oldName = $("#" + inputId + "NameOnSelect").val();
    if (newName != oldName) {
        $("#" + inputId + "Id").val("");
        $("#" + inputId + "UOMDiv").addClass("d-none");
        $("#" + inputId + "QuantityDiv").addClass("d-none");
    }
}
function getProductData(inputId) {
    var product = $("#" + inputId + "Id").val();

    $.ajax({
        url: "../api/Product/GetProductDetail?productId=" + product,
        type: 'GET',
        success: function (data) {
            if (data.SecondaryUOM == 0) {
                $("#" + inputId + "QuantityDiv").removeClass("d-none");
                $("#" + inputId + "UOMDiv").addClass("d-none");
                $("#" + inputId + "Quantity").attr("required", true);
            }
            else {
                $("#" + inputId + "QuantityDiv").addClass("d-none");
                $("#" + inputId + "UOMDiv").removeClass("d-none");
                $("#" + inputId + "Quantity").attr("required", false);


                $("#" + inputId + "PrimaryUOM").html("<b>" + data.PrimaryUOMName + "</b>");
                $("#" + inputId + "PrimaryUOMValue").val(data.PrimaryUOMName);
                $("#" + inputId + 'SecondaryUOM').html("<b>" + data.SecondaryUOMName + "</b>");
                $("#" + inputId + "SecondaryUOMValue").val(data.SecondaryUOM);
                $("#" + inputId + 'PrimaryPrice').val(data.PrimaryUnitPrice);
                $("#" + inputId + 'SecondaryPrice').val(data.SecondaryUnitPrice);
                $("#" + inputId + "conversionName").html(data.PrimaryUOMName + " per " + data.SecondaryUOMName);
                $("#" + inputId + 'Conversion').val(data.ConversionFactor);
            }

        },
        error: function (data) {
        }
    });
}
function onChangePrimaryCheckBox(inputId) {

    if ($("#" + inputId + "Primary_switch").is(":checked")) {

        $("#" + inputId + "PrimaryQuantity").prop("disabled", false);
        $("#" + inputId + "PrimaryQuantity").attr("required", true);
        $("#" + inputId + "Secondary_switch").prop("checked", false);
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").val("");
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
    }

    else {
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
        $("#" + inputId + "PrimaryQuantity").val("");
    }
}

function onChangeSecondaryCheckBox(inputId) {

    if ($("#" + inputId + "Secondary_switch").is(":checked")) {

        $("#" + inputId + "SecondaryQuantity").prop("disabled", false);
        $("#" + inputId + "SecondaryQuantity").attr("required", true);
        $("#" + inputId + "Primary_switch").prop("checked", false);
        $("#" + inputId + "PrimaryQuantity").prop("disabled", true);
        $("#" + inputId + "PrimaryQuantity").val("");
        $("#" + inputId + "PrimaryQuantity").attr("required", false);
    }
    else {
        $("#" + inputId + "SecondaryQuantity").prop("disabled", true);
        $("#" + inputId + "SecondaryQuantity").attr("required", false);
        $("#" + inputId + "SecondaryQuantity").val("");
    }
}

function addNewRowToGrid(inputId) {
    ProductId = $("#" + inputId + "Id").val();
    var variationId;
    var product;

    if ($(ProductId.search("-"))[0] > 0) {
        product = ProductId.split("-")[0];
        variationId = ProductId.split("-")[1];
    }
    else {
        product = ProductId;
        variationId = 0;
    }
    let primaryQuantity = 0;
    let secondaryQuantity = 0;
    let conversionFactor;
    let totalQuantity;
    let UnitPrice = 0;
    let Discount = 0;
    let DiscountAmount = 0;
    let NetAmount;
    var TotalNetAmount = 0;



    let QuantitydivClass = $("#ProductUOMDiv").attr("class");

    if (QuantitydivClass.includes("d-none") != true) {

        if ($("#" + inputId + "Primary_switch").prop("checked")) {
            primaryQuantity = $("#" + inputId + 'PrimaryQuantity').val() == "" ? 0 : $("#" + inputId + 'PrimaryQuantity').val();
            totalQuantity = primaryQuantity;
        }
        else {
            secondaryQuantity = $("#" + inputId + 'SecondaryQuantity').val() == "" ? 0 : $("#" + inputId + 'SecondaryQuantity').val();
            conversionFactor = $("#" + inputId + 'Conversion').val();
            totalQuantity = secondaryQuantity * conversionFactor;
        }

    }


    else {
        totalQuantity = $("#ProductQuantity").val();
    }
    UnitPrice = $("#UnitPrice").val();
    Discount = $("#Discount").val();
    var rowData = {}

    rowData.ProductId = ProductId;
    rowData.ProductName = $("#ProductNameOnSelect").val();
    rowData.ProductPrimaryQuantity = $("#ProductPrimaryQuantity").val();
    rowData.ProductSecondaryQuantity = $("#ProductSecondaryQuantity").val();
    rowData.Totalquantity = totalQuantity;
    rowData.UnitPrice = UnitPrice;
    rowData.GrossAmount = totalQuantity * UnitPrice;
    rowData.Discount = Discount;
    rowData.DiscountAmount = rowData.GrossAmount / Discount;
    rowData.NetAmount = rowData.GrossAmount - rowData.DiscountAmount;


    var SaleTable = $('#SaleQuotationTable').DataTable();

    var data = SaleTable.rows().data();
    var isAdded = true;
    data.each(function (i, item) {
        if (rowData.ProductId == i.ProductId) {
            isAdded = false;

        }
    });

    if (isAdded) {
        var table = $('#SaleQuotationTable').DataTable();


        table.row.add({
            "Description": rowData.ProductName,
            "PrimaryQuantity": rowData.ProductPrimaryQuantity,
            "SecondaryQuantity": rowData.ProductSecondaryQuantity,
            "TotalQuantity": rowData.Totalquantity,
            "UnitPrice": rowData.UnitPrice,
            "Discount": rowData.Discount,
            "GrossAmount": rowData.GrossAmount,
            "DiscountAmount": rowData.DiscountAmount,
            "NetAmount": rowData.NetAmount,
            "ProductId": rowData.ProductId,

        }).draw();
        debugger;
        GrossAmount = parseFloat($("#TotalGrossAmount").text());
        GrossAmount += parseFloat(rowData.GrossAmount);
        $("#TotalGrossAmount").text(GrossAmount);

        DiscountAmount = parseFloat($("#TotalDiscountAmount").text());
        DiscountAmount += parseFloat(rowData.DiscountAmount);
        $("#TotalDiscountAmount").text(DiscountAmount);

        Amount += parseFloat(rowData.NetAmount);
        $("#TotalNetAmount").text(Amount);

    }
    else {
        toastr.error("Product is Already Exist", { timeOut: 5000 });
    }

}

function ResetModal() {
    LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');

    $("#Discount").val("");
    $("#ProductSecondaryQuantity").val("");
    $("#ProductPrimaryQuantity").val("");
    $("#UnitPrice").val("");
}


function AddSaleQuotation() {
    debugger;
    var SaleQuotation = {

        LocationId: $("#LocationId").val(),
        CustomerId: $("#CustomerId").val(),
        VoucherNo: $("#VoucherNo").val(),
        QuotationDate: $("#QuotationDate").val(),
        TotalNetAmount: $("#TotalNetAmount").text(),
        TotalDiscountAmount: $("#TotalDiscountAmount").text(),
        TotalGrossAmount: $("#TotalGrossAmount").text(),
    };

    SaleQuotation.SaleQuotationDetail = [];
    debugger;
    var table = $('#SaleQuotationTable').DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {
        var Detail = {};
        Detail.ProductId = ProductId;
        Detail.PrimaryQuantity = value.PrimaryQuantity;
        Detail.SecondaryQuantity = value.SecondaryQuantity;
        Detail.TotalQuantity = value.TotalQuantity;
        Detail.UnitPrice = value.UnitPrice;
        Detail.GrossAmount = value.GrossAmount;
        Detail.Discount = value.Discount;
        Detail.DiscountAmount = value.DiscountAmount;
        Detail.NetAmount = value.NetAmount;
        SaleQuotation.SaleQuotationDetail.push(Detail);
    });
    debugger;
    ShowLoader();
    $.ajax({
        url: '../api/SaleQuotation/Post/',
        type: 'POST',
        data: JSON.stringify(SaleQuotation),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            toastr.success("Sale Quotation Added Successfully", { timeOut: 5000 });
            ResetForm();
            HideLoader();

        },
        error: function (data) {
            var response = data.responseText.replace(/"/g, '');
            //toastr.error("Voucher Number Is already exist", { timeOut: 5000 });
            ResetForm();
            HideLoader();
        }
    });

}
function RemoveGridProduct(productId) {
    var actionsHtml =
        ` <i class="mdi mdi-close red  font-size-14" onclick="removeEntry(this)" title="Cancelled"></i> `;

    return actionsHtml;
}


function removeEntry(rowPointer) {
    debugger;

    var table = $('#SaleQuotationTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
    var row = table
        .rows(function (idx, data, node) {
            if (data.ProductId == rowData.ProductId) {

                var netAmount = $(rowPointer).closest("tr").find("td:eq(8)").text();

                var discountAmount = $(rowPointer).closest("tr").find("td:eq(7)").text();

                var grossamount = $(rowPointer).closest("tr").find("td:eq(5)").text();

                GrossAmount = parseInt($("#TotalGrossAmount").text());
                GrossAmount = GrossAmount - parseInt(grossamount);
                TotalGrossAmount = $("#TotalGrossAmount").text(GrossAmount);

                DiscountAmount = parseInt($("#TotalDiscountAmount").text());
                DiscountAmount = DiscountAmount - parseInt(discountAmount);
                var TotalDiscountAmount = $("#TotalDiscountAmount").text(DiscountAmount);


                Amount = Amount - parseInt(netAmount);
                var TotalNetAmount = $("#TotalNetAmount").text(Amount);
                return true;
            }
        })
        .remove()
        .draw();


}

function populateData(id) {
    SaleQuotationId = id;
    debugger;
    HideLoader();
    $.ajax({
        url: "../api/SaleQuotation/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            debugger;
            LoadKendoAutoComplete('Product', '-- Please Select Product --', '../api/Product/GetAllActiveProduct');
            $("#ProductId").val(data.ProductId);
            LoadKendoDropDown('LocationId', '-- Please Select Product --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId),
                LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + data.LocationId);
            $("#CustomerId").val(data.CustomerId),
                $("#Customer").val(data.CustomerName),
                $("#VoucherNo").val(data.VoucherNo),
                $("#OrderDate").val(data.OrderDate.substr(0, 10));
            $("#TotalNetAmount").html(data.TotalNetAmount);
            Amount = parseInt(data.TotalNetAmount);
            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
            $("#TotalGrossAmount").html(data.TotalGrossAmount);
           
            var table = $("#SaleQuotationTable").DataTable();
            debugger;
            $.each(data.SaleQuotationDetail, function (index, value) {
                var rowData = {};
                
                rowData.ProductName = value.ProductName;
                rowData.PrimaryQuantity = value.PrimaryQuantity;
                rowData.SecondaryQuantity = value.SecondaryQuantity;
                rowData.TotalQuantity = value.TotalQuantity;
                rowData.UnitPrice = value.UnitPrice;
                rowData.GrossAmount = value.GrossAmount;
                rowData.Discount = value.Discount;
                rowData.DiscountAmount = value.DiscountAmount;
                rowData.NetAmount = value.NetAmount;
                rowData.ProductId = value.ProductId;
                debugger;
                table.row.add({
                    "Description": rowData.ProductName,
                    "PrimaryQuantity": rowData.PrimaryQuantity,
                    "SecondaryQuantity": rowData.SecondaryQuantity,
                    "TotalQuantity": rowData.TotalQuantity,
                    "UnitPrice": rowData.UnitPrice,
                    "GrossAmount": rowData.GrossAmount,
                    "Discount": rowData.Discount,
                    "DiscountAmount": rowData.DiscountAmount,
                    "NetAmount": rowData.NetAmount,
                    "ProductId": rowData.ProductId,
                }).draw();
              HideLoader();
            });
        }
    });
}

function update() {
debugger;
    var Amount = $("#TotalNetAmount").text();
    var SaleQuotation = {
        LocationId: $("#LocationId").val(),
        CustomerId: $("#CustomerId").val(),
        VoucherNo: $("#VoucherNo").val(),
        QuotationDate: $("#QuotationDate").val(),
        TotalNetAmount: Amount,
        Id: $("#SaleQuotationId").val(),
        
    };

    SaleQuotation.SaleQuotationDetail = [];
    debugger;
    var table = $('#SaleQuotationTable').DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {
        var Detail = {};
        Detail.ProductId = ProductId;
        Detail.PrimaryQuantity = value.PrimaryQuantity;
        Detail.SecondaryQuantity = value.SecondaryQuantity;
        Detail.TotalQuantity = value.TotalQuantity;
        Detail.UnitPrice = value.UnitPrice;
        Detail.GrossAmount = value.GrossAmount;
        Detail.Discount = value.Discount;
        Detail.DiscountAmount = value.DiscountAmount;
        Detail.NetAmount = value.NetAmount;
        SaleQuotation.SaleQuotationDetail.push(Detail);
    });
    debugger;
    ShowLoader();
    $.ajax({
        url: '../api/SaleQuotation/Put/',
        type: 'POST',
        data: JSON.stringify(SaleQuotation),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            toastr.success("Sale Quotation Update Successfully", { timeOut: 5000 });
            RedirectToUrl('../SaleQuotationManagement/Manage')
            ResetForm();
            HideLoader();

        },
        error: function (data) {
            var response = data.responseText.replace(/"/g, '');
            toastr.warning(response, { timeOut: 5000 });
            ResetForm();
            HideLoader();
        }
    });

}

function ResetForm() {
    debugger;
    TotalNetAmount = 0;
    TotalDiscountAmount = 0;
    TotalGrossAmount = 0;
    $("#LocationId").val("");
    $("#CustomerId").val("");
    $("#Customer").val("");
    $("#VoucherNo").val("");
    $("#QuotationDate").val("");
 
    $("#TotalNetAmount").text(0);
    $("#TotalGrossAmount").text(0);
    $("#TotalDiscountAmount").text(0);
    LoadKendoAutoComplete('Customer', '-- Please Select Location --', '');
    LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
    var table = $('#SaleQuotationTable').DataTable();
    table.clear().draw();





}
