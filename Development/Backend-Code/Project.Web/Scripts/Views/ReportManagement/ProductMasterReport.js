﻿
var locationId = 0;

$(document).ready(function () {
   // LoadjsTreee();
    url = '../api/Report/ProductMasterReport';
    getDynamicReportJson(url);
});

function ExportToExcel()
{
    url = '../api/ReportExport/ProductMasterReport';
    window.open(url);
}

function generateReportFromJson(jsonObj) {
    // EXTRACT VALUE FOR HTML HEADER. 
    var col = [];
    for (var i = 0; i < jsonObj.length; i++) {
        for (var key in jsonObj[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    table = document.getElementById("ReportGrid");

    var tHead = table.createTHead();      // TABLE HEADER.

    var trHead = tHead.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        trHead.appendChild(th);
    }


    var tBody = table.createTBody();
    // ADD JSON DATA TO THE TABLE AS ROWS.

    for (var i = 0; i < jsonObj.length; i++) {

        tr = tBody.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = jsonObj[i][col[j]];
            tabCell.classList.add("min-width-cell");

            if (j == 0 || j == 1) {
                tabCell.classList.add("text-primary");
                tabCell.classList.add("t-font-boldest");
            }

        }
    }

}

function ExportToPdf() {
    toastr.warning("This Report Is Not Avaialble in Pdf Format!", { timeOut: 5000 });
}