﻿
var locationId = 0;
var date;
var url1;
$(document).ready(function () {
    LoadjsTreee();
});
function GenerateReport(LocationId) {
    locationId = LocationId;

}

function ExportToExcel() {

    url = '../api/ReportExport/OrderbookerKPIReport?LocationId=' + locationId + '&date=' + date;
    window.open(url);
}


function getReport() {
    date = $("#currentDate").val();

    if (locationId == "" || locationId == undefined) {
        toastr.warning("Please Select Location from Sidebar", { timeout: 5000 });
        return false;

    }

    if (date == "") {
        toastr.warning("Please Select Date to Proceed", { timeout: 5000 });
        return false;
    }


    url = '../api/Report/OrderbookerKPIReport?LocationId=' + locationId + '&date=' + date;
    getDynamicReportJson(url);

    url1 = '../api/Report/OrderbookerKPIFooter?LocationId=' + locationId + '&date=' + date;
    getFooterDynamicReportJson(url1);

}


function generateReportFromJson(jsonObj) {
    var col = [];
    for (var i = 0; i < jsonObj.length; i++) {
        for (var key in jsonObj[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    table = document.getElementById("ReportGrid");

    var tHead = table.createTHead();      // TABLE HEADER.

    var trHead = tHead.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        trHead.appendChild(th);
    }
    var tBody = table.createTBody();
    // ADD JSON DATA TO THE TABLE AS ROWS.

    for (var i = 0; i < jsonObj.length; i++) {

        tr = tBody.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = jsonObj[i][col[j]];

            tabCell.classList.add("min-width-cell");

            if (j == 0 || j == 1 || j == 5) {
                tabCell.classList.add("text-primary");
                tabCell.classList.add("t-font-boldest");
            }


        }
    }

}

function ExportToPdf() {
    var date = $("#currentDate").val();
    WindowOpen("../api/ReportExportToPdf/ReportExportOrderBookerKPI?LocationId=" + locationId + "&Date=" + date);
}



function getFooterDynamicReportJson(url) {

    var reportFooterJson;
    $.ajax({
        url: url,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {

            reportFooterJson = result;
            clearFooterDataTableGrid();

            generateFooterReportFromJson(reportFooterJson);
            table = $('#ReportGrid1').DataTable({
                searching: false,
                destroy: true,
                "ordering": false
            }).draw();

        },
        error: function (errormessage) {
            reportFooterJson = "";
            HideLoader();
            toastr.error(errormessage.responseText, { timeOut: 5000 });
        }
    });
}


function generateFooterReportFromJson(jsonObj) {
    
    var col = [];
    for (var i = 0; i < jsonObj.length; i++) {
        for (var key in jsonObj[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    table = document.getElementById("ReportGrid1");

    var tHead = table.createTHead();      // TABLE HEADER.

    var trHead = tHead.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        trHead.appendChild(th);
    }
    var tBody = table.createTBody();
    // ADD JSON DATA TO THE TABLE AS ROWS.

    for (var i = 0; i < jsonObj.length; i++) {

        tr = tBody.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = jsonObj[i][col[j]];

            tabCell.classList.add("min-width-cell");

            if (j == 0) {
                tabCell.classList.add("text-primary");
                tabCell.classList.add("t-font-boldest");
            }


        }
    }

}


function clearFooterDataTableGrid() {
    if (table) {
        if ($.fn.DataTable.isDataTable('#ReportGrid1')) {
            $('#ReportGrid1').DataTable().destroy();
            $('#ReportGrid1').empty();
        }
    }
}