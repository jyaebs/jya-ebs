﻿

var table;


$(document).on('click', '.jstree-node', function (e) {
    debugger;
    e.stopImmediatePropagation();
    locationId = $(this).attr('id');
    GenerateReport(locationId);
});


function getDynamicReportJson(url) {
    ShowLoader();
    var reportJson;
    $.ajax({
        url: url,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {

            reportJson = result;
            clearDataTableGrid();
            if (!reportJson || reportJson.length === 0) {
                toastr.warning("No data found", { timeOut: 5000 });
                $('#exportBtn').addClass('d-none');
            }
            else {
                generateReportFromJson(reportJson);
                table = $('#ReportGrid').DataTable({
                    searching: false,
                    destroy: true,
                    "ordering": false
                }).draw();
                $('#exportBtn').removeClass('d-none');
                ExportBtn();
            }
            HideLoader();
        },
        error: function (errormessage) {
            reportJson = "";
            HideLoader();
            toastr.error(errormessage.responseText, { timeOut: 5000 });
        }
    });
}

function ExportBtn() {
    var buton = `<button class="btn bg-white _r_btn border-0 float-right ml-auto " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="exportBtn">
                                    <span class="_dot _inline-dot bg-primary"></span>
                                    <span class="_dot _inline-dot bg-primary"></span>
                                    <span class="_dot _inline-dot bg-primary"></span>
                                </button>
                                <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(698px, -149px, 0px);">
                                    <a class="dropdown-item" href="#" onclick="ExportToPdf()">Export To PDF</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" onclick="ExportToExcel()">Export To Excel</a>
                                </div>`;
    $("#ReportGrid_wrapper> .row:first>div:nth-child(2)").html(buton);

}
function clearDataTableGrid() {
    if (table) {
        if ($.fn.DataTable.isDataTable('#ReportGrid')) {
            $('#ReportGrid').DataTable().destroy();
            $('#ReportGrid').empty();
        }
    }
}