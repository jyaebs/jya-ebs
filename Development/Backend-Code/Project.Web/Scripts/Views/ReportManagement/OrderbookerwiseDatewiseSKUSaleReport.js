﻿
var locationId = 0;
var Fromdate;
var Todate;




$(document).ready(function () {
    LoadjsTreee();
    LoadKendoMultiselect('Product', '-- Please Select Product --', '../api/Product/GetActiveProductsAndVariationByDistributor?LocationId=0');
    LoadKendoMultiselect('Orderbooker', '-- Please Select Orderbooker --', '../api/User/GetOrderBookerByLocation?LocationId=0');
});

function LoadProductsAndOrderbookers(locationId) {


    if (locationId != "") {

        var promiseObject = GetPromise('../api/User/GetOrderBookerByLocation?LocationId=' + locationId);
        promiseObject
            .then(data =>
                RefreshOrderbookerList(data))
            .catch(error =>
                console.log("KENDO MULTISELECT ERROR" + "/r/n" + error));

        var promiseObject1 = GetPromise('../api/Product/GetActiveProductsAndVariationByDistributor?LocationId=' + locationId);
        promiseObject1
            .then(data =>
                RefreshProductList(data))
            .catch(error =>
                console.log("KENDO MULTISELECT ERROR" + "/r/n" + error));

    }
}

function RefreshOrderbookerList(data) {
    var orderbookerList = $("#Orderbooker").data("kendoMultiSelect");
    orderbookerList.setDataSource(data);
    orderbookerList.refresh();
}

function RefreshProductList(data) {
    var ProductList = $("#Product").data("kendoMultiSelect");
    ProductList.setDataSource(data);
    ProductList.refresh();
}

function GenerateReport(LocationId) {
    locationId = LocationId;
    LoadProductsAndOrderbookers(locationId);
}

function ExportToExcel() {

    Fromdate = $("#FromDate").val();
    Todate = $("#ToDate").val();

    if (locationId == "" || locationId == undefined) {
        toastr.warning("Please Select Location from Sidebar", { timeout: 5000 });
        return false;

    }


    if (Fromdate == "") {
        toastr.warning("Please Select From Date to Proceed", { timeout: 5000 });
        return false;
    }

    if (Todate == "") {
        toastr.warning("Please Select To Date to Proceed", { timeout: 5000 });
        return false;
    }

    let productIds = $("#Product").data("kendoMultiSelect").value();
    let orderbookerIds = $("#Orderbooker").data("kendoMultiSelect").value();

    url = `../api/ReportExport/OrderbookerwiseDatwiseSKUSaleReport?LocationId=${locationId}&fromDate=${Fromdate}&ToDate=${Todate}&OrderbookerIds=${orderbookerIds}&ProductIds=${productIds}`;
    window.open(url);
}


function getReport() {

    Fromdate = $("#FromDate").val();
    Todate = $("#ToDate").val();


    if (locationId == "" || locationId == undefined) {
        toastr.warning("Please Select Location from Sidebar", { timeout: 5000 });
        return false;

    }

    if (Fromdate == "") {
        toastr.warning("Please Select From Date to Proceed", { timeout: 5000 });
        return false;
    }

    if (Todate == "") {
        toastr.warning("Please Select To Date to Proceed", { timeout: 5000 });
        return false;
    }

    let productIds = $("#Product").data("kendoMultiSelect").value();
    let orderbookerIds = $("#Orderbooker").data("kendoMultiSelect").value();

    url = `../api/Report/OrderbookerwiseDatwiseSKUSaleReport?LocationId=${locationId}&fromDate=${Fromdate}&ToDate=${Todate}&OrderbookerIds=${orderbookerIds}&ProductIds=${productIds}`;
    getDynamicReportJson(url);
}

function generateReportFromJson(jsonObj) {
    // EXTRACT VALUE FOR HTML HEADER. 
    var col = [];
    for (var i = 0; i < jsonObj.length; i++) {
        for (var key in jsonObj[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    table = document.getElementById("ReportGrid");

    var tHead = table.createTHead();      // TABLE HEADER.

    var trHead = tHead.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        trHead.appendChild(th);
    }


    var tBody = table.createTBody();
    // ADD JSON DATA TO THE TABLE AS ROWS.

    for (var i = 0; i < jsonObj.length; i++) {

        tr = tBody.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = jsonObj[i][col[j]];
            tabCell.classList.add("min-width-cell");

        }
    }

}

function ExportToPdf() {
    toastr.warning("This Report Is Not Avaialble in Pdf Format!", { timeOut: 5000 });
}