﻿

var locationId = 0;

$(document).ready(function () {
    LoadjsTreee();
    LoadKendoMultiselect('Orderbooker', '-- Please Select Orderbooker --', '../api/User/GetOrderBookerByLocation?LocationId=0');
});

function LoadOrderbookers(locationId) {
    
    if (locationId != "") {

        var promiseObject = GetPromise('../api/User/GetOrderBookerByLocation?LocationId=' + locationId);
        promiseObject
            .then(data =>
                RefreshOrderbookerList(data))
            .catch(error =>
                console.log("KENDO MULTISELECT ERROR" + "/r/n" + error));
    }
}

function RefreshOrderbookerList(data) {
    var orderbookerList = $("#Orderbooker").data("kendoMultiSelect");
    orderbookerList.setDataSource(data);
    orderbookerList.refresh();
}

function GenerateReport(LocationId) {
    locationId = LocationId;
    LoadOrderbookers(locationId);
}

function ExportToExcel() {
    
    if (locationId == "" || locationId == undefined) {
        toastr.warning("Please Select Location from Sidebar", { timeout: 5000 });
        return false;

    }
    
    let orderbookerIds = $("#Orderbooker").data("kendoMultiSelect").value();

    url = `../api/ReportExport/OutletwiseCreditReport?LocationId=${locationId}&OrderbookerIds=${orderbookerIds}`;
    window.open(url);
}

function getReport() {

   
    if (locationId == "" || locationId == undefined) {
        toastr.warning("Please Select Location from Sidebar", { timeout: 5000 });
        return false;

    }
    
    let orderbookerIds = $("#Orderbooker").data("kendoMultiSelect").value();

    url = `../api/Report/OutletwiseCreditReport?LocationId=${locationId}&OrderbookerIds=${orderbookerIds}`;
    getDynamicReportJson(url);
}

function generateReportFromJson(jsonObj) {
    // EXTRACT VALUE FOR HTML HEADER. 
    var col = [];
    for (var i = 0; i < jsonObj.length; i++) {
        for (var key in jsonObj[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    table = document.getElementById("ReportGrid");

    var tHead = table.createTHead();      // TABLE HEADER.

    var trHead = tHead.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = col[i];
        trHead.appendChild(th);
    }


    var tBody = table.createTBody();
    // ADD JSON DATA TO THE TABLE AS ROWS.

    for (var i = 0; i < jsonObj.length; i++) {

        tr = tBody.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = jsonObj[i][col[j]];
            tabCell.classList.add("min-width-cell");

        }
    }

}

function ExportToPdf() {
    toastr.warning("This Report Is Not Avaialble in Pdf Format!", { timeOut: 5000 });
}