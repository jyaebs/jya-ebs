﻿var SecondaryUOMId = 0;
function initializeForm() {

    LoadKendoDropDown('PrimaryUOMId', '-- Please Select PrimaryUOM --', '../api/PrimaryUOM/GetAllActivePrimaryUOM');
};
function Add() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var SecondaryUOM = {
            PrimaryUOMId: $("#PrimaryUOMId").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),

        };
        ShowLoader();
        debugger;
        $.ajax({



            url: '../api/SecondaryUOM/Post/',
            type: 'POST',
            data: JSON.stringify(SecondaryUOM),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Secondary UOM Added Successfully", { timeOut: 5000 });

                ResetForm();
                HideLoader();
            },
            error: function (data) {
                debugger;
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });

                HideLoader();
            }
        });
    }
};
function Update() {
    debugger;
    var a = SecondaryUOMId;
    var formId = "form";
    if (validate(formId)) {
        var SecondaryUOM = {
            PrimaryUOMId: $("#PrimaryUOMId").val(),
            Code: $('#Code').val(),
            Name: $('#Name').val(),
            Id: SecondaryUOMId
        };
        ShowLoader();
        $.ajax({
            url: '../api/SecondaryUOM/Put/',
            type: 'POST',
            data: JSON.stringify(SecondaryUOM),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                debugger;
                toastr.success("Secondary UOM Updated Successfully", { timeOut: 5000 });

                ResetForm();
                RedirectToUrl('../SecondaryUOMManagement/Manage')
                HideLoader();
            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.error(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
};
function populateData(id) {
    debugger;
    SecondaryUOMId = id;
    ShowLoader();
    $.ajax({
        url: "../api/SecondaryUOM/GetById?id=" + id,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('PrimaryUOMId', '-- Please Select PrimaryUOM --', '../api/PrimaryUOM/GetActivePrimaryUOM');
            $('#PrimaryUOMId').val(data.PrimaryUOMId);
            $('#Code').val(data.Code);
            $('#Name').val(data.Name);
            HideLoader();
        }
    });
};
function ResetForm() {
    $('#PrimaryUOMId').val();
    $('#Code').val("");
    $('#Name').val("");
    UnselectKendoDropDown("PrimaryUOMId")

}