﻿var DeliveryReturnId = 0;
var Id = 0;

var GrossAmount = 0;
var TotalDiscountAmount = 0;
var NetAmount = 0;

$(document).ready(function () {

    initializeForm();
    
    $("#DeliveryNoteDetailTable tbody").on("click", "tr", function () {

        let table = $('#DeliveryNoteDetailTable').DataTable();
        let DeliveryNoteId = table.rows().data()[this.rowIndex - 1].Id;
        let Description = table.rows().data()[this.rowIndex - 1].Description;
        let PrimaryQuantity = table.rows().data()[this.rowIndex - 1].PrimaryQuantity;
        let SecondaryQuantity = table.rows().data()[this.rowIndex - 1].SecondaryQuantity;
        let TotalQuantity = table.rows().data()[this.rowIndex - 1].TotalQuantity;
        let DeliverQuantity = table.rows().data()[this.rowIndex - 1].DeliverQuantity;
        let Warehouse = table.rows().data()[this.rowIndex - 1].Warehouse;
        let GrossAmount = table.rows().data()[this.rowIndex - 1].GrossAmount;
        let Discount = table.rows().data()[this.rowIndex - 1].Discount;
        let DiscountAmount = table.rows().data()[this.rowIndex - 1].DiscountAmount;
        let NetAmount = table.rows().data()[this.rowIndex - 1].NetAmount;
        let ProductId = table.rows().data()[this.rowIndex - 1].ProductId;



        GetDeliveryNoteDetail(DeliveryNoteId);
        $("#GetDeliveryNoteDetailModal").modal("hide");
    });
});

function initializeForm() {


    dataTable.initialize();
}



var dataTable = {
    table: null,
    initializeDataTable: function () {
        var $table = $("#DeliveryReturnTable");
        dataTable.table = $table.DataTable({
            processing: false,
            searching: false, paging: false, info: false,
            serverSide: false,
            destroy: false,
            "columns": [
                { "data": "Description" },
                { "data": "PrimaryQuantity" },
                { "data": "SecondaryQuantity" },
                { "data": "TotalQuantity" },
                { "data": "ReturnQuantity" },
                { "data": "Warehouse" },
                { "data": "WarehouseId", "visible": false },
                { "data": "UnitPrice" },
                { "data": "GrossAmount" },
                { "data": "Discount" },
                { "data": "DiscountAmount" },
                { "data": "NetAmount" },
                { "data": "ProductId", className: "text-center" },
            ],
            "pagingType": "full_numbers",
            "ordering": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        var actionsHtml =
                            ` <i class="mdi mdi-close red  font-size-14" onclick="removeEntry(this)" title="Cancelled"></i> `;

                        return actionsHtml;
                    },
                    "targets": -1
                },


                

                {
                    "render": function (data, type, row) {
                        debugger;
                        var actionsHtml =
                            `  <input class="form-control" id="DeliverQuantity-${row.ProductId}"  type="number" r />`;
                        return actionsHtml;
                    },
                    "targets": -9
                },

                {
                    orderable: false,
                    targets: "no-sort",
                }
            ],
        });
    },
    initialize: function () {
        dataTable.initializeDataTable();
    }
};

function removeEntry(rowPointer) {

    var table = $('#DeliveryReturnTable').DataTable();
    var parentRow = $(rowPointer).closest("tr")[0];
    var rowData = table.row(parentRow).data();
    var row = table
        .row($(rowPointer).closest("tr")[0])
        .remove()
        .draw();
    CalculateAmounts();
}



function AddDeliveryReturn() {
    debugger;
    var formId = "form";
    if (validate(formId)) {
        var DeliveryReturn = {
            Id: Id,
            LocationId: $("#LocationId").val(),
            CustomerId: $("#CustomerId").val(),

            DeliveryDate: $("#DeliveryDate").val(),
            TotalNetAmount: $("#TotalNetAmount").text(),
            TotalDiscountAmount: $("#TotalDiscountAmount").text(),
            TotalGrossAmount: $("#TotalGrossAmount").text(),
            Remarks: $("#Remarks").val(),
        };

        DeliveryReturn.DeliveryNoteDetail = [];
        debugger;
        var table = $('#DeliveryReturnTable').DataTable();

        var data = table.rows().data();
        data.each(function (value, index) {
            
            debugger;
            var Details = {};
            Details.ProductId = value.ProductId;
            Details.PrimaryQuantity = value.PrimaryQuantity;
            Details.SecondaryQuantity = value.SecondaryQuantity;
            Details.TotalQuantity = value.TotalQuantity;
            Details.UnitPrice = value.UnitPrice;
            Details.DeliverQuantity = $(`#DeliverQuantity-${value.ProductId}`).val();
            Details.WarehouseId = value.Warehouse;
            Details.WarehouseId = value.WarehouseId;
            Details.GrossAmount = value.GrossAmount;
            Details.Discount = value.Discount;
            Details.DiscountAmount = value.DiscountAmount;
            Details.NetAmount = value.NetAmount;
            DeliveryReturn.DeliveryNoteDetail.push(Details);
        });
        debugger;
        ShowLoader();
        $.ajax({
            url: '../api/DeliveryReturn/Post/',
            type: 'POST',
            data: JSON.stringify(DeliveryReturn),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                toastr.success("Delivery Return Added Successfully", { timeOut: 5000 });
                ResetForm();
                HideLoader();

            },
            error: function (data) {
                var response = data.responseText.replace(/"/g, '');
                toastr.warning(response, { timeOut: 5000 });
                HideLoader();
            }
        });
    }
}


function ResetForm() {
    TotalDiscountAmount = 0;
    GrossAmount = 0;
    netAmount = 0;

    LoadKendoDropDown('LocationId', '-- Please Select Location --', ' ');
    $("#LocationId").val("");
    LoadKendoAutoComplete('Customer', '-- Please Select Location --', '');
    $("#CustomerId").val("");
    $("#SaleReferenceNumber").val("");
    $("#DeliveryDate").val("");
    $("#TotalNetAmount").text(0);
    $("#TotalGrossAmount").text(0);
    $("#TotalDiscountAmount").text(0);

    var table = $('#DeliveryReturnTable').DataTable();
    table.clear().draw();

}

function GetOpenDeliveryNote() {
    $("#GetDeliveryNoteDetailModal").toggle("modal");
    initializeDeliveryNoteTable();
}

function initializeDeliveryNoteTable() {

    var dtCommonParam = {
        getDataUrl: `../api/DeliveryNote/GetOpenDeliveryNote`,
        tableId: 'DeliveryNoteDetailTable'
    };
    $('#DeliveryNoteDetailTable').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        "pageLength": 10,
        oLanguage: { sProcessing: "<div class='loader-bubble loader-bubble-primary mb-3'></div>" },
        ajax:
        {
            url: dtCommonParam.getDataUrl,
            type: "Post",
            dataSrc: "data"
        },
        "columns": [
            { "data": "Id", "visible": false },
            { "data": "Name" },
            { "data": "CustomerName" },
            { "data": "orderdatestring" },
            { "data": "LocationId", "visible": false },

        ],
        "columnDefs": [
            {
                orderable: false,
                targets: "no-sort"
            }
        ],

    });
}

function GetDeliveryNoteDetail(DeliveryNoteId) {
    Id = DeliveryNoteId;
    debugger;
    $.ajax({
        url: "../api/DeliveryNote/GetById?id=" + DeliveryNoteId,
        type: 'GET',
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            LoadKendoDropDown('LocationId', '-- Please Select Location --', '../api/Location/GetAllFactoryByLocationType');
            $("#LocationId").val(data.LocationId);
            LoadKendoAutoComplete('Customer', '-- Please Select Customer --', '../api/Customer/GetActiveCustomer?LocationId=' + data.LocationId);
            $("#CustomerId").val(data.CustomerId);
            $("#Customer").val(data.CustomerName);
            $("#SaleReferenceNumber").val(data.ReferenceNumber);
            $("#TotalNetAmount").html(data.TotalNetAmount);
            $("#TotalDiscountAmount").html(data.TotalDiscountAmount);
            $("#TotalGrossAmount").html(data.TotalGrossAmount);

            var table = $("#DeliveryReturnTable").DataTable();
            debugger;
            $.each(data.DeliveryNoteDetail, function (index, value) {
               DeliveredQuantity = value.DeliverQuantity;
                table.row.add({
                    "Description": value.ProductName,
                    "PrimaryQuantity": value.PrimaryQuantity,
                    "SecondaryQuantity": value.SecondaryQuantity,
                    "TotalQuantity": value.TotalQuantity,
                    "ReturnQuantity": value.TotalQuantity,
                    "Warehouse": value.WarehouseName,
                    "WarehouseId": value.WarehouseId,
                    "UnitPrice": value.UnitPrice,
                    "GrossAmount": value.GrossAmount,
                    "Discount": value.Discount,
                    "DiscountAmount": value.DiscountAmount,
                    "NetAmount": value.NetAmount,
                    "Id": value.Id,
                    "ProductId": value.ProductId,
                }).draw();
                debugger;
                $(`#DeliverQuantity-${value.ProductId}`).val(value.TotalQuantity);
                var ReturnQuantity = value.TotalQuantity;
               
                CalculateAmounts();
            });
            HideLoader();
        }
    });
}



function CalculateAmounts() {
    debugger;

    let GrossAmount = 0;
    let TotalDiscountAmount = 0;
    let NetAmount = 0;

    var table = $("#DeliveryReturnTable").DataTable();
    var data = table.rows().data();
    data.each(function (value, index) {

        GrossAmount += value.GrossAmount;
        TotalDiscountAmount += value.DiscountAmount;
        NetAmount += value.NetAmount;
    });
    $("#TotalGrossAmount").text(GrossAmount);
    $("#TotalDiscountAmount").text(TotalDiscountAmount);
    $("#TotalNetAmount").text(NetAmount);
}

function GetDeliverQuantity(rowValue) {
    debugger;

    

    let newGrossAmount = 0;
    let newDiscountAmount = 0;
    let newNetAmount = 0;
    
    var table = $("#DeliveryReturnTable").DataTable();
    row_index = $(rowValue).closest('tr').index();
    let ProductName = $(rowValue).closest("tr").find("td:eq(0)").text();
    let PrimaryQuantity = $(rowValue).closest("tr").find("td:eq(1)").text();
    let SecoundaryQuantity = $(rowValue).closest("tr").find("td:eq(2)").text();
    let GrossAmount = $(rowValue).closest("tr").find("td:eq(7)").text();
    let Discount = $(rowValue).closest("tr").find("td:eq(8)").text();
    let DiscountAmount = $(rowValue).closest("tr").find("td:eq(9)").text();
    let NetAmount = $(rowValue).closest("tr").find("td:eq(10)").text(); 
    let ProductId = $(rowValue).closest("tr").find("td:eq(11)").text(); 

    let ReturnQuantity = $(rowValue).closest("tr").find("td:eq(4)").find('input').val();
    let TotalQuantity = $(rowValue).closest("tr").find("td:eq(3)").text();
    let UnitPrice = $(rowValue).closest("tr").find("td:eq(6)").text();
    
    if (parseInt(ReturnQuantity) > parseInt(TotalQuantity))
    {
        ReturnQuantity = TotalQuantity;
    }
 
    newGrossAmount = ReturnQuantity * UnitPrice;
    newDiscountAmount = newGrossAmount / Discount;
    newNetAmount = newGrossAmount - newDiscountAmount;
    $(rowValue).val(DeliverQuantity);
    table.cell({ row: row_index, column: 7 }).data(newGrossAmount);
    table.cell({ row: row_index, column: 9 }).data(newDiscountAmount);
    table.cell({ row: row_index, column: 10 }).data(newNetAmount);
   CalculateAmounts();
}
