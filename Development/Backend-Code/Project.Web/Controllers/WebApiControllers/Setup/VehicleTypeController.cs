﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers.Setup
{
    public class VehicleTypeController : ApiController
    {
        private VehicleTypeService _VehicleTypeService = new VehicleTypeService();

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_VEHICLE_TYPE)]
        public HttpResponseMessage Post(VehicleTypeViewModel VehicleType)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                VehicleType.ChangedBy = User.Identity.Name;
                VehicleType.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _VehicleTypeService.Add(VehicleType);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_VEHICLE_TYPE)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _VehicleTypeService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }


            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_VEHICLE_TYPE)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                VehicleTypeViewModel VehicleType = new VehicleTypeViewModel();

                VehicleType.ChangedBy = User.Identity.Name;
                VehicleType.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                VehicleType.Id = id;

                ResponseViewModel response = _VehicleTypeService.Deactivate(VehicleType);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_VEHICLE_TYPE)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {

                VehicleTypeViewModel VehicleType = new VehicleTypeViewModel();

                VehicleType.ChangedBy = User.Identity.Name;
                VehicleType.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                VehicleType.Id = id;

                ResponseViewModel response = _VehicleTypeService.Activate(VehicleType);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _VehicleTypeService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_VEHICLE_TYPE)]
        public HttpResponseMessage Put(VehicleTypeViewModel VehicleType)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                VehicleType.ChangedBy = User.Identity.Name;
                VehicleType.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _VehicleTypeService.Update(VehicleType);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetActiveVehicleType()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _VehicleTypeService.GetActiveVehicleType();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

    }
}
