﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers
{
    public class GSTController : ApiController
    {
        private GSTService _GSTService = new GSTService();

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_GST)]
        public HttpResponseMessage Post(GSTViewModel GST)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                GST.ChangedBy = User.Identity.Name;
                GST.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _GSTService.Add(GST);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_GST)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _GSTService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _GSTService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_GST)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                GSTViewModel GST = new GSTViewModel();

                GST.ChangedBy = User.Identity.Name;
                GST.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                GST.Id = id;

                ResponseViewModel response = _GSTService.Deactivate(GST);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_BANK)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                GSTViewModel GST = new GSTViewModel();

                GST.ChangedBy = User.Identity.Name;
                GST.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                GST.Id = id;

                ResponseViewModel response = _GSTService.Activate(GST);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_GST)]
        public HttpResponseMessage Put(GSTViewModel GST)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                GST.ChangedBy = User.Identity.Name;
                GST.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _GSTService.Update(GST);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpGet]
    
        public HttpResponseMessage GetGSTById()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _GSTService.GetGSTById();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
    }
}

