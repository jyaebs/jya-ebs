﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.ViewControllers
{
    public class ProductPriceController : ApiController
    {
        private ProductPriceService _ProductPriceService = new ProductPriceService();

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_PRODUCT_PRICE)]
        public HttpResponseMessage Post(ProductPriceViewModel ProductPrice)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                ProductPrice.ChangedBy = User.Identity.Name;
                ProductPrice.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _ProductPriceService.Add(ProductPrice);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_PRODUCT_PRICE)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _ProductPriceService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }
        
        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_PRODUCT_PRICE)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ProductPriceViewModel ProductPrice = new ProductPriceViewModel();

                ProductPrice.ChangedBy = User.Identity.Name;
                ProductPrice.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                ProductPrice.Id = id;

                ResponseViewModel response = _ProductPriceService.Deactivate(ProductPrice);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ProductPriceViewModel ProductPrice = new ProductPriceViewModel();

                ProductPrice.ChangedBy = User.Identity.Name;
                ProductPrice.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                ProductPrice.Id = id;

                ResponseViewModel response = _ProductPriceService.Activate(ProductPrice);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _ProductPriceService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_PRODUCT_PRICE)]
        public HttpResponseMessage Put(ProductPriceViewModel ProductPrice)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                ProductPrice.ChangedBy = User.Identity.Name;
                ProductPrice.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _ProductPriceService.Update(ProductPrice);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }


    }
}
