﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers.Setup
{
    public class BankBranchController : ApiController
    {
        private BankBranchService _BankBranchService = new BankBranchService();

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_BANK_BRANCH)]
        public HttpResponseMessage Post(BankBranchViewModel BankBranch)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                BankBranch.ChangedBy = User.Identity.Name;
                BankBranch.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _BankBranchService.Add(BankBranch);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_BANK_BRANCH)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _BankBranchService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_BANK_BRANCH)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                BankBranchViewModel BankBranch = new BankBranchViewModel();

                BankBranch.ChangedBy = User.Identity.Name;
                BankBranch.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                BankBranch.Id = id;

                ResponseViewModel response = _BankBranchService.Deactivate(BankBranch);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_BANK_BRANCH)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {

                BankBranchViewModel BankBranch = new BankBranchViewModel();

                BankBranch.ChangedBy = User.Identity.Name;
                BankBranch.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                BankBranch.Id = id;

                ResponseViewModel response = _BankBranchService.Activate(BankBranch);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _BankBranchService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_BANK_BRANCH)]
        public HttpResponseMessage Put(BankBranchViewModel BankBranch)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                BankBranch.ChangedBy = User.Identity.Name;
                BankBranch.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _BankBranchService.Update(BankBranch);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }
    }
}
