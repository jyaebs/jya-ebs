﻿using Project.Logging;
using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Project.DataAccessLayer;
using Project.ViewModels.Enums;
using Project.Utilities;
using Project.Models.ViewModels;
using Project.DataAccessLayer.Services;
using Project.Models.ViewModels.Common;

namespace Project.Web.Controllers.WebApiControllers
{
    public class LisenceSecurityController : ApiController
    {
        private LisenceSecurityService _LisenceSecurityService = new LisenceSecurityService();

        [HttpGet]
        public HttpResponseMessage IsLisenced()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _LisenceSecurityService.IsLisenced();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetMACAddress()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                try
                {
                    string securirtyKey = LisenceSecurityUtility.GetKey();
                    return Request.CreateResponse(HttpStatusCode.OK, securirtyKey);   
                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Unable to generate key");
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage PostKey(LisenceKeyViewModel _LisenceKeyViewModel)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _LisenceSecurityService.Add(_LisenceKeyViewModel);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }
    }
}
