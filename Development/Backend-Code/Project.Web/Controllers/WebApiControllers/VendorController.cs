﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
namespace Project.Web.Controllers.WebApiControllers
{
    public class VendorController : ApiController
    {
        private VendorService _VendorService = new VendorService();
        [HttpPost]
        public HttpResponseMessage Post(VendorViewModel Vendor)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                Vendor.ChangedBy = User.Identity.Name;
                Vendor.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _VendorService.Add(Vendor);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }


        [HttpPost]
       // [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                    int LocationId = CommonUtility.GetLocationIdbyUser(User.Identity.Name);
                    _PaginationSearchModel.LocationId = LocationId;
                }
                try
                {
                    var data = _VendorService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }



        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _VendorService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
       // [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                VendorViewModel Vendor = new VendorViewModel();

                Vendor.ChangedBy = User.Identity.Name;
                Vendor.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                Vendor.Id = id;

                ResponseViewModel response = _VendorService.Deactivate(Vendor);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_COUNTRY)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                VendorViewModel Vendor = new VendorViewModel();

                Vendor.ChangedBy = User.Identity.Name;
                Vendor.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                Vendor.Id = id;

                ResponseViewModel response = _VendorService.Activate(Vendor);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
       // [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.)]
        public HttpResponseMessage Put(VendorViewModel Vendor)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                Vendor.ChangedBy = User.Identity.Name;
                Vendor.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _VendorService.Update(Vendor);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        //Get All Active Vendor
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetActiveVendor(int LocationId)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _VendorService.GetActiveVendor(LocationId);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

    }
}
