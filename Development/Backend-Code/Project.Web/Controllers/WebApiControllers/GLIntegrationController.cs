﻿using Project.DataAccessLayer.Services;
using Project.Logging;

using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;

using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers
{
    public class GLIntegrationController : ApiController
    {
        private GLIntegrationService _GLIntegrationService = new GLIntegrationService();

        [HttpPost]
      //  [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.)]
        public HttpResponseMessage Post(GLIntegrationViewModel GL)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                GL.ChangedBy = User.Identity.Name;
                GL.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _GLIntegrationService.Add(GL);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }



        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetByFiscalYear(int fiscalYear)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _GLIntegrationService.GetByFiscalYear(fiscalYear);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
    }
}