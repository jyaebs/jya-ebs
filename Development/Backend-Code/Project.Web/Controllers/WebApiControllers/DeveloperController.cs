﻿using Project.DataAccessLayer.Services;
using Project.Logging;

using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;

using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers
{
    public class DeveloperController : ApiController
    {
        private DeveloperService _DeveloperService = new DeveloperService();

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllLocationTypes()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllLocationTypes();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
        
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllChartOfAccountTypes()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllChartOfAccountypes();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllActiveProductType()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllActiveProductType();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        public HttpResponseMessage GetAllActiveProductPriceType()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllActiveProductPriceType();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        public HttpResponseMessage GetAllActivePurchaseType()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllActivePurchaseType();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }


        public HttpResponseMessage GetAllActiveFiscalYear()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllActiveFiscalYear();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }


        public HttpResponseMessage GetAllActiveGLIntegrationField()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _DeveloperService.GetAllActiveGLIntegrationField();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

    }
}