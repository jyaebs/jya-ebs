﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers
{
    public class SaleQuotationController : ApiController
    {
        private SaleQuotaionService _SaleQuotationService = new SaleQuotaionService();

        [HttpPost]
        public HttpResponseMessage Post(SaleQuotationViewModel SaleQuotation)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {

                SaleQuotation.ChangedBy = User.Identity.Name;
                SaleQuotation.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _SaleQuotationService.Add(SaleQuotation);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _SaleQuotationService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        //[HttpPost]

        //public HttpResponseMessage Deactivate(int id)
        //{
        //    using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
        //    {
        //        SaleOrderViewModel SaleOrder = new SaleOrderViewModel();

        //        SaleOrder.ChangedBy = User.Identity.Name;
        //        SaleOrder.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
        //        SaleOrder.Id = id;

        //        ResponseViewModel response = _SaleQuotationService.Deactivate(SaleQuotation);
        //        Logger.Instance.Write(response.LogLevel, response.Message);
        //        return Request.CreateResponse(response.Status, response.obj);
        //    }
        //}

        //[HttpPost]
        //public HttpResponseMessage Activate(int id)
        //{
        //    using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
        //    {
        //        SaleOrderViewModel SaleOrder = new SaleOrderViewModel();

        //        SaleOrder.ChangedBy = User.Identity.Name;
        //        SaleOrder.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
        //        SaleOrder.Id = id;

        //        ResponseViewModel response = _SaleOrderService.Activate(SaleOrder);
        //        Logger.Instance.Write(response.LogLevel, response.Message);
        //        return Request.CreateResponse(response.Status, response.obj);
        //    }
        //}


        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _SaleQuotationService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]

        public HttpResponseMessage CancelById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                SaleQuotationViewModel SaleQuotation = new SaleQuotationViewModel();

                SaleQuotation.ChangedBy = User.Identity.Name;
                SaleQuotation.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                SaleQuotation.Id = id;

                ResponseViewModel response = _SaleQuotationService.CancelById(SaleQuotation);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
        [HttpPost]
        public HttpResponseMessage Put(SaleQuotationViewModel SaleQuotation)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                SaleQuotation.ChangedBy = User.Identity.Name;
                SaleQuotation.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _SaleQuotationService.Update(SaleQuotation);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetOpenSaleQuotation()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _SaleQuotationService.GetOpenSaleQuotation(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }


        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _SaleQuotationService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
    }
}

    