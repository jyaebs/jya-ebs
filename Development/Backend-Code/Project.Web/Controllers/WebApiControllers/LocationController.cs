﻿using Newtonsoft.Json;
using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.Enums;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers
{
    public class LocationController : ApiController
    {
        LocationService _LocationService = new LocationService();

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetLocationTree(int id = 0)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                bool IsRootNode = false;
                if (id == 0)
                {
                    //User Location
                    id = CommonUtility.GetLocationIdbyUser(User.Identity.Name);
                    IsRootNode = true;
                }
                ResponseViewModel response = _LocationService.GetLocationTree(IsRootNode, id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetCompleteLocationTree(int id = 0)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                bool IsRootNode = false;
                if (id == 0)
                {
                    //User Location
                    id = CommonUtility.GetLocationIdbyUser(User.Identity.Name);
                    IsRootNode = true;
                }
                ResponseViewModel response = _LocationService.GetCompleteLocationTree(IsRootNode, id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _LocationService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }


        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_CITY)]
        public HttpResponseMessage Post(string location)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                LocationViewModel Location = JsonConvert.DeserializeObject<LocationViewModel>(location);
                Location.ChangedBy = User.Identity.Name;
                Location.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _LocationService.Add(Location);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_LOCATION)]
        public HttpResponseMessage Put(string location)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                LocationViewModel Location = JsonConvert.DeserializeObject<LocationViewModel>(location);
                Location.ChangedBy = User.Identity.Name;
                Location.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _LocationService.Update(Location);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_LOCATION)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                LocationViewModel location = new LocationViewModel();

                location.ChangedBy = User.Identity.Name;
                location.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                location.Id = id;

                ResponseViewModel response = _LocationService.Deactivate(location);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_LOCATION)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                LocationViewModel location = new LocationViewModel();

                location.ChangedBy = User.Identity.Name;
                location.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                location.Id = id;

                ResponseViewModel response = _LocationService.Activate(location);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllLocationTypes()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _LocationService.GetAllLocationTypes(User.Identity.Name);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllLocationsByLocationType(int Id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _LocationService.GetAllLocationsByLocationType(Id, User.Identity.Name);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllFactoryByLocationType()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                int factoryId = (int)EnumLocationType.Factory;
                ResponseViewModel response = _LocationService.GetAllLocationsByLocationType(factoryId, User.Identity.Name);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetActiveCompanyWarehouse()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _LocationService.GetActiveCompanyWarehouse(User.Identity.Name);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
    }
}
