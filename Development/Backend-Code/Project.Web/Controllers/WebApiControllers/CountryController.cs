﻿using Project.DataAccessLayer.Services;
using Project.Logging;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace Project.Web.Controllers.WebApiControllers
{
    public class CountryController : ApiController
    {
        private CountryService _CountryService = new CountryService();

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_COUNTRY)]
        public  HttpResponseMessage Post(CountryViewModel country)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                
                country.ChangedBy = User.Identity.Name;
                country.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _CountryService.Add(country);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_COUNTRY)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                    NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;

                }
                try
                {
                    var data = _CountryService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _CountryService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_COUNTRY)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                CountryViewModel country = new CountryViewModel();

                country.ChangedBy = User.Identity.Name;
                country.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                country.Id = id;

                ResponseViewModel response = _CountryService.Deactivate(country);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_COUNTRY)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                CountryViewModel country = new CountryViewModel();

                country.ChangedBy = User.Identity.Name;
                country.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                country.Id = id;

                ResponseViewModel response = _CountryService.Activate(country);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_COUNTRY)]
        public HttpResponseMessage Put(CountryViewModel country)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                country.ChangedBy = User.Identity.Name;
                country.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _CountryService.Update(country);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        //Get All Active Countries
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetActiveCountries()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _CountryService.ActiveCountries();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

    }
}
