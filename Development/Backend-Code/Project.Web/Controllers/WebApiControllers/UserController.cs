﻿
using Project.Web.CustomeAnnotations;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using Project.Logging;
using Project.ViewModels.Enums;
using Project.ViewModels;
using Project.Web.IdentityFunctions;
using Project.Utilities;
using Project.DataAccessLayer.Services;
using System.Collections.Specialized;
using System;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Newtonsoft.Json;

namespace Project.Web.Controllers.WebApiControllers
{
    public class UserController : ApiController
    {
        private UserService _UserService = new UserService();

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_USER)]
        public async Task<HttpResponseMessage> Post(string user)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                UserViewModel users = JsonConvert.DeserializeObject<UserViewModel>(user);
                FunctionResult result = await IdentityHelpers.createUser(users);

                if (!result.success)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result.message);
                }
                users.UserId = result.message;
                users.ChangedBy = User.Identity.Name;
                users.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _UserService.Add(users);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_USER)]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                PaginationSearchModel _PaginationSearchModel = new PaginationSearchModel();
                HttpContent requestContent = Request.Content;
                bool jsonContent = requestContent.IsFormData();

                if (jsonContent)
                {
                     NameValueCollection coll = requestContent.ReadAsFormDataAsync().Result;
                    _PaginationSearchModel.PageStart = Int32.Parse(coll["start"]);
                    _PaginationSearchModel.PageSize = Int32.Parse(coll["length"]);
                    _PaginationSearchModel.Draw = Int32.Parse(coll["draw"]);
                    _PaginationSearchModel.Search = coll["search[value]"];
                    _PaginationSearchModel.sorting = Int32.Parse(coll["order[0][column]"]);
                    _PaginationSearchModel.direction = coll["order[0][dir]"];
                    _PaginationSearchModel.User = User.Identity.Name;
                    
                    int LocationId = CommonUtility.GetLocationIdbyUser(User.Identity.Name);
                    _PaginationSearchModel.LocationId = LocationId;

                }
                try
                {
                    var data = _UserService.GetPaginatedRecords(_PaginationSearchModel);
                    return Request.CreateResponse(HttpStatusCode.OK, data);

                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetById(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                if (id == 0)
                {
                   id= CommonUtility.GetIdbyUser(User.Identity.Name);
                }
                ResponseViewModel response = _UserService.GetById(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
          [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_USER)]
        public HttpResponseMessage Deactivate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                UserViewModel user = new UserViewModel();

                user.ChangedBy = User.Identity.Name;
                user.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                user.Id = id;

                ResponseViewModel response = _UserService.Deactivate(user);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
         [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_USER)]
        public HttpResponseMessage Activate(int id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                UserViewModel user = new UserViewModel();

                user.ChangedBy = User.Identity.Name;
                user.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();
                user.Id = id;

                ResponseViewModel response = _UserService.Activate(user);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_USER)]
        public async Task<HttpResponseMessage> Put(string user)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                UserViewModel Users = JsonConvert.DeserializeObject<UserViewModel>(user);
                FunctionResult result = await IdentityHelpers.updateUser(Users);

                if (!result.success)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result.message);
                }
                UserViewModel users = JsonConvert.DeserializeObject<UserViewModel>(user);

                users.ChangedBy = User.Identity.Name;
                users.ChangedOn = DateTimeUtility.DateTimeNowInPKTimeZone();

                ResponseViewModel response = _UserService.Update(users);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute]
        public async Task<HttpResponseMessage> ChangePassword(ChangePasswordViewModel model)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                try
                {
                    model.Username = User.Identity.Name;
                    FunctionResult result = await IdentityHelpers.ChangePassword(model);
                    if (!result.success)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, result.message);
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    return response;
                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }
        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_USER)]
        public async Task<HttpResponseMessage> ResetPassword(ResetPasswordViewModel model)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { }))
            {
                try
                {
                    FunctionResult result = await IdentityHelpers.ResetPassword(model);
                    if (!result.success)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, result.message);
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    return response;
                }
                catch (Exception ex)
                {
                    Logger.Instance.Write(EnumLogLevel.Error, ex.Message, ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetOrderBookerByLocation(int LocationId)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _UserService.GetOrderBookerByLocation(LocationId);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }
    }
}
