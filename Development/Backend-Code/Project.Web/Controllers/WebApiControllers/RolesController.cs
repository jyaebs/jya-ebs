﻿using Project.Logging;
using System;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Project.ViewModels;
using Project.DataAccessLayer.Services;
using Project.Models.ViewModels;
using Project.Web.CustomeAnnotations;
using Project.ViewModels.Enums;
using Project.Models.ViewModels.Common;


namespace Project.Web.Controllers.WebApiControllers
{
    public class RolesController : ApiController
    {
        private RoleService _RoleService = new RoleService();
        
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage Get(String roleorPrivilege)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { roleorPrivilege }))
            {
                ResponseViewModel response = _RoleService.Get(roleorPrivilege);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage Get(String roleorPrivilege, Guid id)
        {

            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { roleorPrivilege }))
            {
                ResponseViewModel response = _RoleService.Get(roleorPrivilege, id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }

        }

        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage Get(String id, string roleOrUser)
        {

            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { id }))
            {
                ResponseViewModel response = _RoleService.Get(id, roleOrUser);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }


        }

        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_ROLE)]
        public HttpResponseMessage Post(RolesViewModel value)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { value.Id }))
            {
                if (!ModelState.IsValid)
                {

                }
                ResponseViewModel response =_RoleService.Add(value);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpPut]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_ROLE)]
        public HttpResponseMessage Put(RolesViewModel value)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { value.Id }))
            {
                 
                ResponseViewModel response = _RoleService.Update(value);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        [HttpDelete]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.INACTIVE_ROLE)]
        public HttpResponseMessage Delete(String id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { id }))
            {
                ResponseViewModel response = _RoleService.Delete(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

        //Get All Active Roles
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAll()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _RoleService.GetAllRoles();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        //Get All Active Distributor Role Value
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetDistributorValue()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _RoleService.GetDistributorValue();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        //Get All Active Roles and Privileges
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAllPrivilegesAndRole()
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            { 
                ResponseViewModel response = _RoleService.GetAllPrivilegeAndRole();
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        //Get All Assign Privileges
        [HttpGet]
        [ApiCustomAuthorizeAttribute]
        public HttpResponseMessage GetAssignPrivilegeByRoleId(string id)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            { 
                ResponseViewModel response = _RoleService.GetAssignPrivilegeByRoleId(id);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.obj);
            }
        }

        //Post All Assign Privileges
        [HttpPost]
        [ApiCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_ROLE)]
        public HttpResponseMessage AssignPrivilegeToRole(AssignPrivilegesViewModel[] value)
        {
            using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
                ResponseViewModel response = _RoleService.AssignViewsToRole(value);
                Logger.Instance.Write(response.LogLevel, response.Message);
                return Request.CreateResponse(response.Status, response.Message);
            }
        }

    }
}
