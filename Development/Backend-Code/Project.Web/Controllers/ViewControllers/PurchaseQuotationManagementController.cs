﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class PurchaseQuotationManagementController : Controller
    {
        // GET: PurchaseQuotationManagement
        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Manage()
        {
            return View();
        }

        public ActionResult ViewPurchaseQuotation(int id)
        {
            ViewBag.Id = id;
            return View();
        }

    }
}