﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class RoleManagementController : Controller
    {
        // GET: RoleManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_ROLE)]
        public ActionResult Index()
        {
            return View();
        }
        
    }
}