﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class InwardGatePassManagementController : Controller
    {
        // GET: InwardGatePassManagement
        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Manage()
        {
            return View();
        }

        public ActionResult ViewInwardGatePass(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}