﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;

namespace Project.Web.Controllers.ViewControllers
{
    public class VendorManagementController : Controller
    {
        // GET: VendorManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_VENDOR)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_VENDOR)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_VENDOR)]
        public ActionResult Update(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
    }
}