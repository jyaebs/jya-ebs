﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class PurchaseRequisitionManagementController : Controller
    {
        // GET: PurchaseRequisitionManagement
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Manage()
        {
            return View();
        }
        public ActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}