﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class CityManagementController : Controller
    {
        // GET: CityManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ACTIVE_CITY)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_CITY)]
        public ActionResult Update(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_CITY)]
        public ActionResult Manage()
        {
            return View();
        }
    }
}