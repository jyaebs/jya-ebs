﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class VehicleTypeManagementController : Controller
    {
        // GET: VehicalTypeManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_VEHICLE_TYPE)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_VEHICLE_TYPE)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_VEHICLE_TYPE)]
        public ActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}