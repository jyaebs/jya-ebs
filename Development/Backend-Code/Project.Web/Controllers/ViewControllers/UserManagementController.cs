﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class UserManagementController : Controller
    {
        // GET: UserManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_USER)]
        public ActionResult Add()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_USER)]
        public ActionResult Manage()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_USER)]
        public ActionResult Update(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_USER)]
        public ActionResult ResetPassword(string username)
        {
            ViewBag.Username = username;
            return View();
        }
        [MvcCustomAuthorizeAttribute]
        public ActionResult ChangePassword()
        {
            
            return View();
        }

           
    }
}