﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class LocationManagementController : Controller
    {
        // GET: LocationManagement
        
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_LOCATION)]
        public ActionResult Index()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_LOCATION)]
        public ActionResult Add()
        {
            return View();
        }
    }
}