﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class CountryManagementController : Controller
    {

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_COUNTRY)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_COUNTRY)]
        public ActionResult Manage()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_COUNTRY)]
        public ActionResult Update(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
    }
}