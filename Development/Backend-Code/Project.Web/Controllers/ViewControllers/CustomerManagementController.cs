﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;

namespace Project.Web.Controllers.ViewControllers
{
    public class CustomerManagementController : Controller
    {
        // GET: CustomerManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_CUSTOMER)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_CUSTOMER)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_CUSTOMER)]
        public ActionResult Update(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
    }
}