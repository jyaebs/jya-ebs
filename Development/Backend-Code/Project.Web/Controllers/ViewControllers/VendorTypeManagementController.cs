﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class VendorTypeManagementController : Controller
    {
        // GET: VendorTypeManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_VENDOR_TYPE)]
        public ActionResult Add()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_VENDOR_TYPE)]
        public ActionResult Manage()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_VENDOR_TYPE)]
        public ActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}