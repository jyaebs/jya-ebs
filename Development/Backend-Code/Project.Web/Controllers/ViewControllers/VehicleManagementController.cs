﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class VehicleManagementController : Controller
    {
        // GET: VehicleManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_VEHICLE)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_VEHICLE)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_VEHICLE)]
        public ActionResult Update(int id)
        {
            ViewBag.id = id;
            return View();
        }
    }
}