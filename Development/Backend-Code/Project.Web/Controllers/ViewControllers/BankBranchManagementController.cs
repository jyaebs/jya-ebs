﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class BankBranchManagementController : Controller
    {
        // GET: BankBranchManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_BANK_BRANCH)]
        public ActionResult Add()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_BANK_BRANCH)]
        public ActionResult Manage()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_BANK_BRANCH)]
        public ActionResult Update(int id)

        {
            ViewBag.Id = id;
            return View();
        }
    }
}