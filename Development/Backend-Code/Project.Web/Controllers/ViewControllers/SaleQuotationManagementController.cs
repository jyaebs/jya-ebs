﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class SaleQuotationManagementController : Controller
    {
        // GET: SaleRequisitionManagement
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Manage()
        {
            return View();
        }
        public ActionResult Update( int Id)
        {
            ViewBag.Id = Id;
            return View();
        }
    }
}