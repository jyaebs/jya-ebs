﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class ProductManagementController : Controller
    {
        // GET: ProductManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_PRODUCT)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_PRODUCT)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_PRODUCT)]
        public ActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}