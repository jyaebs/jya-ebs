﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class SecondaryUOMManagementController : Controller
    {
        // GET: SecondaryUOMManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_SECONDARYUOM)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_SECONDARYUOM)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_SECONDARYUOM)]
        public ActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}