﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class WHTManagementController : Controller
    {
        // GET: WHTManagement
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_WHT)]
        public ActionResult Add()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_WHT)]
        public ActionResult Manage()
        {
            return View();
        }
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_WHT)]
        public ActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}