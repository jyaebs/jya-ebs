﻿using Project.ViewModels.Enums;
using Project.Web.CustomeAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers.ViewControllers
{
    public class ProductCategoryManagementController : Controller
    {
        // GET: ProductCategory
        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.ADD_PRODUCT_CATEGORY)]
        public ActionResult Add()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.VIEW_PRODUCT_CATEGORY)]
        public ActionResult Manage()
        {
            return View();
        }

        [MvcCustomAuthorizeAttribute(privilege = EnumPrivilegesName.UPDATE_PRODUCT_CATEGORY)]
        public ActionResult Update( int id)
        {
            ViewBag.Id = id;
              return View();
        }
    }
}