﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ChartViewModels
{
    public class BarChartViewModel
    {
       public BarChartDataViewModel data { get; set; }
    }
        public class BarChartDataViewModel
    {
        public List<string> labels { get; set; }
        public List<BarChartDetailViewModel> datasets { get; set; }
    }


    public class BarChartDetailViewModel
    {
        public string label { get; set; }
        public string backgroundColor { get; set; }
        public List<decimal> data { get; set; }
    }
}
