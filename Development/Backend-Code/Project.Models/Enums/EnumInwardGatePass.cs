﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
     public enum EnumInwardGatePass
    {
        Open = 1,
        InspectionNoteGenerated = 2,
    }
}
