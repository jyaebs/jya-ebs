﻿
namespace Project.ViewModels.Enums
{
    public static class EnumPrivilegesName
    {
        
        /// <summary>
        /// Role Management
        /// </summary>
        public const string ADD_ROLE = "Add Role";
        public const string VIEW_ROLE = "View Role";
        public const string UPDATE_ROLE = "Update Role";
        public const string INACTIVE_ROLE = "Inactive Role";

        /// <summary>    
        /// User Management
        /// </summary>
        public const string ADD_USER = "Add User";
        public const string VIEW_USER = "View User";
        public const string UPDATE_USER = "Update User";
        public const string INACTIVE_USER = "Inactive User";
        public const string ACTIVE_USER = "Active User";


        /// <summary>    
        /// Country Management
        /// </summary>
        public const string ADD_COUNTRY = "Add Country";
        public const string VIEW_COUNTRY = "View Country";
        public const string UPDATE_COUNTRY = "Update Country";
        public const string INACTIVE_COUNTRY = "Inactive Country";
        public const string ACTIVE_COUNTRY = "Active Country";

        /// <summary>    
        /// City Management
        /// </summary>
        public const string ADD_CITY = "Add City";
        public const string VIEW_CITY = "View City";
        public const string UPDATE_CITY = "Update City";
        public const string INACTIVE_CITY = "Inactive City";
        public const string ACTIVE_CITY = "Active City";

        /// <summary>    
        /// Bank Management
        /// </summary>
        public const string ADD_BANK = "Add Bank";
        public const string VIEW_BANK = "View Bank";
        public const string UPDATE_BANK = "Update Bank";
        public const string INACTIVE_BANK = "Inactive Bank";
        public const string ACTIVE_BANK = "Active Bank";

        /// <summary>    
        /// Bank Branch Management
        /// </summary>
        public const string ADD_BANK_BRANCH = "Add Bank Branch";
        public const string VIEW_BANK_BRANCH = "View Bank Branch";
        public const string UPDATE_BANK_BRANCH = "Update Bank Branch";
        public const string INACTIVE_BANK_BRANCH = "Inactive Bank Branch";
        public const string ACTIVE_BANK_BRANCH = "Active Bank Branch";

        /// <summary>    
        /// Location Management
        /// </summary>
        public const string ADD_LOCATION = "Add Location";
        public const string VIEW_LOCATION = "View Location";
        public const string UPDATE_LOCATION = "Update Location";
        public const string INACTIVE_LOCATION = "Inactive Location";
        public const string ACTIVE_LOCATION = "Active Location";
        

        /// <summary>    
        /// Primary UOM Management
        /// </summary>
        public const string ADD_PRIMARYUOM = "Add Primary UOM";
        public const string VIEW_PRIMARYUOM = "View Primary UOM";
        public const string UPDATE_PRIMARYUOM = "Update Primary UOM";
        public const string INACTIVE_PRIMARYUOM = "Inactive Primary UOM";
        public const string ACTIVE_PRIMARYUOM = "Active Primary UOM";

        /// <summary>    
        /// Secondary UOM Management
        /// </summary>
        public const string ADD_SECONDARYUOM = "Add Secondary UOM";
        public const string VIEW_SECONDARYUOM = "View Secondary UOM";
        public const string UPDATE_SECONDARYUOM = "Update Secondary UOM";
        public const string INACTIVE_SECONDARYUOM = "Inactive Secondary UOM";
        public const string ACTIVE_SECONDARYUOM = "Active Secondary UOM";
        
        
        /// <summary>    
        /// Product Category Management
        /// </summary>
        public const string ADD_PRODUCT_CATEGORY = "Add Product Category";
        public const string VIEW_PRODUCT_CATEGORY = "View Product Category";
        public const string UPDATE_PRODUCT_CATEGORY = "Update Product Category";
        public const string INACTIVE_PRODUCT_CATEGORY = "Inactive Product Category";
        public const string ACTIVE_PRODUCT_CATEGORY = "Active Product Category";

        /// <summary>    
        /// Product Sub Category Management
        /// </summary>
        public const string ADD_PRODUCT_SUB_CATEGORY = "Add Product Sub Category";
        public const string VIEW_PRODUCT_SUB_CATEGORY = "View Product Sub Category";
        public const string UPDATE_PRODUCT_SUB_CATEGORY = "Update Product Sub Category";
        public const string INACTIVE_PRODUCT_SUB_CATEGORY = "Inactive Product Sub Category";
        public const string ACTIVE_PRODUCT_SUB_CATEGORY = "Active Product Sub Category";


        /// <summary>    
        /// Product Brand Management
        /// </summary>
        public const string ADD_PRODUCT_BRAND = "Add Product Brand";
        public const string VIEW_PRODUCT_BRAND = "View Product Brand";
        public const string UPDATE_PRODUCT_BRAND = "Update Product Brand";
        public const string INACTIVE_PRODUCT_BRAND = "Inactive Product Brand";
        public const string ACTIVE_PRODUCT_BRAND = "Active Product Brand";

       
        /// <summary>    
        /// Product Management
        /// </summary>
        public const string ADD_PRODUCT = "Add Product";
        public const string VIEW_PRODUCT = "View Product";
        public const string UPDATE_PRODUCT = "Update Product";
        public const string INACTIVE_PRODUCT = "Inactive Product";
        public const string ACTIVE_PRODUCT = "Active Product";

        /// <summary>    
        /// Product Price Management
        /// </summary>
        public const string ADD_PRODUCT_PRICE = "Add Product Price";
        public const string VIEW_PRODUCT_PRICE = "View Product Price";
        public const string UPDATE_PRODUCT_PRICE = "Update Product Price";
        public const string INACTIVE_PRODUCT_PRICE = "Inactive Product Price";

        /// <summary>    
        /// GST
        /// </summary>
        public const string ADD_GST = "Add GST";
        public const string VIEW_GST = "View GST";
        public const string UPDATE_GST = "Update GST";
        public const string INACTIVE_GST = "Inactive GST";
        public const string ACTIVE_GST = "Active GST";

        /// <summary>    
        /// Warehouse
        /// </summary>
        public const string ADD_WAREHOUSE = "Add Warehouse";
        public const string VIEW_WAREHOUSE = "View Warehouse";
        public const string UPDATE_WAREHOUSE = "Update Warehouse";
        public const string INACTIVE_WAREHOUSE = "Inactive Warehouse";
        public const string ACTIVE_WAREHOUSE = "Active Warehouse";

        /// <summary>    
        /// WHT
        /// </summary>
        public const string ADD_WHT = "Add WHT";
        public const string VIEW_WHT = "View WHT";
        public const string UPDATE_WHT = "Update WHT";
        public const string INACTIVE_WHT = "Inactive WHT";
        public const string ACTIVE_WHT = "Active WHT";

        /// <summary>    
        /// Customer Type
        /// </summary>
        public const string ADD_CUSTOMER_TYPE = "Add Customer Type";
        public const string VIEW_CUSTOMER_TYPE = "View Customer Type";
        public const string UPDATE_CUSTOMER_TYPE = "Update Customer Type";
        public const string INACTIVE_CUSTOMER_TYPE = "Inactive Customer Type";
        public const string ACTIVE_CUSTOMER_TYPE = "Active Customer Type";

        /// <summary>    
        /// Customer 
        /// </summary>
        public const string ADD_CUSTOMER = "Add Customer";
        public const string VIEW_CUSTOMER = "View Customer";
        public const string UPDATE_CUSTOMER = "Update Customer";
        public const string INACTIVE_CUSTOMER = "Inactive Customer";
        public const string ACTIVE_CUSTOMER = "Active Customer";

        /// <summary>    
        /// Vendor Type
        /// </summary>
        public const string ADD_VENDOR_TYPE = "Add Vendor Type";
        public const string VIEW_VENDOR_TYPE = "View Vendor Type";
        public const string UPDATE_VENDOR_TYPE = "Update Vendor Type";
        public const string INACTIVE_VENDOR_TYPE = "Inactive Vendor Type";
        public const string ACTIVE_VENDOR_TYPE = "Active Vendor Type";

        /// <summary>    
        /// Vendor
        /// </summary>
        public const string ADD_VENDOR = "Add Vendor";
        public const string VIEW_VENDOR = "View Vendor";
        public const string UPDATE_VENDOR = "Update Vendor";
        public const string INACTIVE_VENDOR = "Inactive Vendor";
        public const string ACTIVE_VENDOR = "Active Vendor";

        /// <summary>    
        /// Vehicle Type
        /// </summary>
        public const string ADD_VEHICLE_TYPE = "Add Vehicle Type";
        public const string VIEW_VEHICLE_TYPE = "View Vehicle Type";
        public const string UPDATE_VEHICLE_TYPE = "Update Vehicle Type";
        public const string INACTIVE_VEHICLE_TYPE = "Inactive Vehicle Type";
        public const string ACTIVE_VEHICLE_TYPE = "Active Vehicle Type";


        /// <summary>    
        /// Vehicle
        /// </summary>
        public const string ADD_VEHICLE = "Add Vehicle";
        public const string VIEW_VEHICLE = "View Vehicle";
        public const string UPDATE_VEHICLE = "Update Vehicle";
        public const string INACTIVE_VEHICLE = "Inactive Vehicle";
        public const string ACTIVE_VEHICLE = "Active Vehicle";


        /// <summary>    
        /// Driver
        /// </summary>
        public const string ADD_DRIVER = "Add Driver";
        public const string VIEW_DRIVER = "View Driver";
        public const string UPDATE_DRIVER = "Update Driver";
        public const string INACTIVE_DRIVER = "Inactive Driver";
        public const string ACTIVE_DRIVER = "Active Driver";
        
    }
}
