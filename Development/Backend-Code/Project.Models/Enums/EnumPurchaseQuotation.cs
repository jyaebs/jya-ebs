﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
    public enum EnumPurchaseQuotation
    {
        Open = 1,
        OrderCreated = 2,
        Cancelled = 3,
    }
}
