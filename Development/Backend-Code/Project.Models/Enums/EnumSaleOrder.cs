﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
   public enum EnumSaleOrder
   {

        Open = 1,
        FullyDelivered = 2,
        PartialDelivered = 3,
        Cancelled=4,
   }
}
