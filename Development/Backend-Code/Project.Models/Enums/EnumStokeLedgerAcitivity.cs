﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
    public enum  EnumStokeLedgerAcitivity
    {
        DirectAdd = 1,
        DeliveryNote = 2,
        DeliveryReturn = 3,
        SaleInvoice = 4,
        SaleReturn = 5,
        GRN = 6,
        GRNReverse = 7,
        PurchaseInvoice = 8,

    }
}
