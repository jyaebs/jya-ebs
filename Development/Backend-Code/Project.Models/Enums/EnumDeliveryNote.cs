﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
   public enum EnumDeliveryNote
    {
        Open = 01,
        FullReturn = 02,
        PartialReturn = 03,
        InvoiceGenerated = 04,

    }
}
