﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
     public enum EnumPurchaseOrder
    {
        Open = 1,
        PartialReceived = 2,
        FullyReceived = 3,
        Cancelled = 4,
    }
}
