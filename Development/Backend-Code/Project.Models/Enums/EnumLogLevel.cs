﻿

namespace Project.ViewModels.Enums
{
    public enum EnumLogLevel
    {
        Error = 10,
        Warning = 20,
        Information = 30,
        Debug = 40,
        FunctionTrace = 50,
    }
}
