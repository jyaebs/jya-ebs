﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
    public enum EnumLocationType
    {
        Company  = 1,
        Production = 2,
        Sales = 3,
        Factory = 4,
        Territory = 5,
        Area = 6,
        Region = 7,
        Zone = 8,
    }
}
