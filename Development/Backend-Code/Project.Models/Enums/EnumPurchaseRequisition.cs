﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
    public enum EnumPurchaseRequisition
    {
        Open = 1,
        QuotationCreated = 2,
        Cancelled = 3,
    }
}
