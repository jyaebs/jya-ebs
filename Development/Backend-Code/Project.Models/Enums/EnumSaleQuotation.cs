﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Enums
{
   public enum EnumSaleQuotation
    {
        Open = 1,
        OrderGenerated = 2,
        Cancelled = 3,
    }
}
