﻿
namespace Project.ViewModels.Enums
{
    public static class EnumPrivilegesPortal
    {
        /// <summary>    
        /// Potal
        /// </summary>
        /// 
        public const string DATA_DICIONARY = "Setup";
        public const string USER_MASTER = "User Master";
        public const string LOCATION_MASTER = "Location Master";
        public const string PRODUCT_MASTER = "Product Master";
        public const string SALES_MASTER = "Sales Master";
    }
}
