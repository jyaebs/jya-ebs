﻿
namespace Project.ViewModels.Enums
{
    public static class EnumPrivilegesCategory
    {

        /// <summary>
        /// Setup
        /// </summary>
        public const string PRIMARY_UOM_MANAGEMENT = "Primary UOM Management";
        public const string SECONDARY_UOM_MANAGEMENT = "Secondary UOM Management";
        public const string GST_MANAGEMENT = "GST Management";
        public const string WHT_MANAGEMENT = "WHT Management";
        public const string BANK_MANAGEMENT = "Bank Management";
        public const string BANK_BRANCH_MANAGEMENT = "Bank Branch Management";
        public const string WAREHOUSE_MANAGEMENT = "Warehouse Management";
        public const string VEHICLE_TYPE_MANAGEMENT = "Vehicle Type Management";
        public const string CUSTOMER_TYPE_MANAGEMENT = "Customer Type Management";
        public const string VENDOR_TYPE_MANAGEMENT = "Vendor Type Management";
        public const string VENDOR_MANAGEMENT = "Vendor Management";
        public const string COUNTRY_MANAGEMENT = "Country Management";
        public const string CITY_MANAGEMENT = "City Management";
        public const string LOCATION_MANAGEMENT = "Location Management";

        
        /// <summary>
        /// User Master
        /// </summary>
        public const string ROLE_MANAGEMENT = "Role Management";
        public const string USER_MANAGEMENT = "User Management";

        
        /// <summary>
        /// Sales Master
        /// </summary>
        public const string VEHICLE_MANAGEMENT = "Vehicle Management";
        public const string DRIVER_MANAGEMENT = "Driver Management";
        public const string CUSTOMER_MANAGEMENT = "Customer Management";


        /// <summary>
        /// Product Master
        /// </summary>
        public const string PRODUCT_CATEGORY_MANAGEMENT = "Product Category Management";
        public const string PRODUCT_SUB_CATEGORY_MANAGEMENT = "Product Sub Category Management";
        public const string PRODUCT_BRAND_MANAGEMENT = "Product Brand Management";
        public const string PRODUCT_MANAGEMENT = "Product Management";
        public const string PRODUCT_PRICE_MANAGEMENT = "Product Price Management";

        
    }
}
