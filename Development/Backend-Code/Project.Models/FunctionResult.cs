﻿

namespace Project.ViewModels
{
    public class FunctionResult
    {
        public bool success { get; set; }
        public string message { get; set; }
    }
}
