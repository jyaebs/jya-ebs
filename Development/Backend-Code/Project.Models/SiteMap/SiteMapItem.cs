﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.SiteMap
{
    public class SiteMapItem
    {
        public String ItemName { get; set; }
        public String URL { get; set; }
        public String Icon { get; set; }
        public String DataItem { get; set; }
        
        public List<SiteMapItem> listSiteMap;

        public SiteMapItem(String itemName, String url,String icon,String dataItem, List<SiteMapItem> ChildList)
        {
            ItemName = itemName;
            URL = url;
            Icon = icon;
            DataItem = dataItem;
            listSiteMap = ChildList;
        }
        public SiteMapItem(String itemName, String url)
        {

            ItemName = itemName;
            URL = url;
            Icon ="";
            DataItem = "";
        }

    }
}
