﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class ProductPriceViewModel : BaseViewModel
    {
        public int OutletId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int LocationId {get;set;}
        public string LocationName { get; set; }
        public decimal Price { get; set; }
        public int ProductPriceTypeId { get; set;}
        public string ProductPriceTypeName { get; set; }
            
    }
}
