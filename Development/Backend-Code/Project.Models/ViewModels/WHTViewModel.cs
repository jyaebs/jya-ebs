﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class WHTViewModel : BaseViewModel
    {
        public decimal Value { set; get; }
        public string AccountWHT { get; set; }
        public string AccountWHTDescription { get; set; }
    }
}
