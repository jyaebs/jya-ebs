﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class SaleQuotationViewModel:BaseViewModel
    {
        public int LocationId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string ReferenceNumber { get; set; }
        public string VoucherNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string orderdatestring { get; set; }
        public decimal TotalNetAmount { get; set; }
        public decimal TotalGrossAmount { get; set; }
        public decimal TotalDiscountAmount { get; set; }
       

        public int NetAmount { get; set; }

        public List<SaleQuotationDetailViewModel> SaleQuotationDetail { get; set; }
    }
}
