﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class CountryViewModel : BaseViewModel
    {
        public string DialingCode { get; set; }

    }
}
