﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class EmployeeViewModel : BaseViewModel
    {
        public string FatherName { get; set; }
        public string FatherOccupation { get; set; }
        public DateTime DateofBirth { get; set; }
        public string Religion { get; set; }
        public int Nationality { get; set; }
        public int PlaceofBirth { get; set; }
        public string Gender { get; set; }
        public string CNIC { get; set; }
        public string Marital { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string EmergencyContact { get; set; }
        public bool PreviousEmployee { get; set; }
        public string PreviousEmpCode { get; set; }
        public bool Illness { get; set; }
        public string IllnessDetail { get; set; }
        public string ReferenceEmpCode { get; set; }
        public int Department { get; set; }
        public string DepartmentName { get; set; }
        public string Role { get; set; }
        public string RoleName { get; set; }
        public int Allocation { get; set; }
        public string ImageId { set; get; }
        public string JoiningDate { get; set; }
        public string PresentAddress { get; set; }
        public int PresentCity { get; set; }
        public string PermanentAddress { get; set; }
        public int PermanentCity { get; set; }

      

        public int EmploymentType { get; set; }

        public string EmploymentTypeName { get; set; }

        public ICollection<EducationDetailViewModel> Education { get; set; }

        public ICollection<ExperienceDetailViewModel> Experience { get; set; }

        public int BankName  { get; set; }
        public string AccountNumber { get; set; }
        public string IBAN { get; set; }

        public string BankBranchname { get; set; }
        public string CountryName { get; set; }

       

        public class EducationDetailViewModel
        {
            public int QualificationId { get; set; }
            public int EducationType { get; set; }
            public string Qualification { get; set; }
            public string Institute { get; set; }
            public string YearofPassing { get; set; }
            public double Score { get; set; }
        }

        public class ExperienceDetailViewModel
        {
            public int ExperienceId { get; set; }
            public string EmployerName { get; set; }
            public string Address { get; set; }
            public string Contact { get; set; }
            public string JobTitle { get; set; }
            public DateTime JobFrom { get; set; }
            public DateTime JobTo { get; set; }
            public int Salary { get; set; }
            public string LeavingReason { get; set; }
        }

    }
}
