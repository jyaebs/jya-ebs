﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class SaleInvoiceViewModel : SaleOrderViewModel
    {
        public string InvoiceReferenceNo { get; set; }
        public DateTime InvoiceDate { get; set; }

        public String InvoiceDateString { get; set; }

        public int OrderId { get; set; }

        
        public decimal PaidAmount { get; set; }
        public decimal RemainingAmount { get; set; }
    }
    public class SaleInvoicePrintViewModel
    {
        public List<int> InvoiceRefrencelist { get; set; }
        public string Message { get; set; }
    }
}
