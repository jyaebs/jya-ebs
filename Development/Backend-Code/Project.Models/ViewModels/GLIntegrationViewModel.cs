﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class GLIntegrationViewModel : BaseViewModel
    {
        

        public IEnumerable<GLIntegrationDetailViewModel> GLIntegrationAccountCode{ get; set; }

    }


    public class GLIntegrationDetailViewModel
    {
        public int FiscalYearId { get; set; }
        public int FieldId { get; set; }
        public string AccountCode { get; set; }
        public string AccountCodeDescription { get; set; }
    }
}
