﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class PJPViewModel : BaseViewModel
    {
        public int LocationId { get; set; }
        public int OrderbookerId { get; set; }
        public string Orderbooker { get; set; }
        public List<PJPDetailViewModel> PjpDetailList { get; set; }
    }
    public class PJPDetailViewModel
    {
        public int Id { get; set; }
        public long OutletId { get; set; }
        public string OutletCode { get; set; }
        public string OutletName { get; set; }
        public string OwnerName { get; set; }
        public string Mobile { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int ChannelTypeId { get; set; }
        public string ChannelTypeName { get; set; }
        public string Address { get; set; }
        public string Brick { get; set; }
        public string Nearby { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public int RepeatEvery { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsMonday { get; set; }
        public bool IsTuesday { get; set; }
        public bool IsWednesday { get; set; }
        public bool IsThursday { get; set; }
        public bool IsFriday { get; set; }
        public bool IsSaturday { get; set; }
        public bool IsSunday { get; set; }
        public int SortingOrder { get; set; }
        public string error { get; set; }
    }
    public class PJPAssignmentModel : BaseViewModel
    {
        public int PJPId { get; set; }
        public int LocationId { get; set; }
        public int OldOrderbookerId { get; set; }
        public int NewOrderbookerId { get; set; }
    }
    public class PJPDetailViewModelExcell
    {
        public int SortOrder { get; set; }
        public string OutletCode { get; set; }
        public string OutletName { get; set; }
        public int RepeatWeek { get; set; }
        public DateTime StartDate { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
    }
}
