﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class StockViewModel : BaseViewModel
    {
        public string Id { get; set; }
        public int StokeId { get;set; }
        public int ProductId { get; set; }
        public int ProductVariationId { get; set; }
        public string ProductName { get; set; }
        public string ProductVariationName { get; set; }
        public decimal OpeningBalance { get; set; }
        public decimal Quantity { get; set; }
        public string BatchNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string ExpiryDateString { get; set; }
        public decimal NewStock { get; set; }
        public string Warehouse { get; set; }
        public decimal TotalStock { get; set; }

        public decimal AdjustStock { get; set; }
        public int AdjustmentReasonId { get; set; }

        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }

    }

    public class StockListViewModel : BaseViewModel
    {
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public List<StockViewModel> StockDetailList { get; set; }
    }
}
