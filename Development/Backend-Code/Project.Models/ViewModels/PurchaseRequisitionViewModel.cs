﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class PurchaseRequisitionViewModel:BaseViewModel
    {
        public string ReferenceNumber { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int RequisitionId { get; set; }
        public string RequisitionName { get; set; }
        public int PurchaseTypeId { get; set; }
        public string PurchaseTypeName { get; set; }
        public string PriorityLevel { get; set; }
        public string PriorityLevelId { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
        public string ProductName { get; set; }
        public int PrimaryQuantity { get; set; }
        public int SecondaryQuantity { get; set; }
        public int TotalQuantity { get; set; }
        public int statusId { get; set; }
        public string StatusString { get; set; } 
        public List<PurchaseRequisitionDetailViewModel> PurchaseRequisitionDetail { get; set; }
    }
}
