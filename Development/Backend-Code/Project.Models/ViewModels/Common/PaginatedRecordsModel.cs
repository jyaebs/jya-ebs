﻿
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models.ViewModels.Common
{
 
    //Dont change variables name
public class PaginatedRecordsModel<T>
{
    public IEnumerable<T> data { get; set; }
    public int recordsTotal { get; set; }
    public int recordsFiltered { get; set; }
    public int draw { get; set; }
}
}
