﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels.Common
{
    public class PaginationSearchModel
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public string User { get; set; }
        public string Search { get; set; }
        public int PageStart { get; set; }
        public int PageSize { get; set; }
        public int Draw { get; set; }
        public string direction { get; set; }
        public int sorting { get; set; }

        public int type { get; set; }
        public int RequisitionId { get; set; }
        public int LevelId { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
