﻿

using Project.ViewModels.Enums;
using System.Net;

namespace Project.Models.ViewModels.Common
{
    public class ResponseViewModel
    {
        public HttpStatusCode Status { get; set; }
        public string Message { get; set; }
        public EnumLogLevel LogLevel { get; set; }
        public object obj { get; set; }

    }
}
