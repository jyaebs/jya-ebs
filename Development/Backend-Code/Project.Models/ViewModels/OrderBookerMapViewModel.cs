﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class OrderBookerMapViewModel:BaseViewModel
    {
        public long OutletId { get; set; }
        public int OrderBookerId { get; set; }
        public string Reason { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public DateTime Date { get; set; }
        public string OutletCode { get; set; }
        public string OutletName { get; set; }
        public string Label { get; set; }
        public bool OrderTaken { get; set; }
    }
}
