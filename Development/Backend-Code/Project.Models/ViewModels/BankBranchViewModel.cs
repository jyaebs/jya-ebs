﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class BankBranchViewModel:BaseViewModel
    {
        public int BankId { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountBankBalance { get; set; }
        public string AccountBankBalanceDescription { get; set; }
    }
}
