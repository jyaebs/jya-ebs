﻿
using Project.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace Project.Models.ViewModels
{
    public class LocationViewModel : BaseViewModel
    {
        public int ParentId { get; set; }
        public int ParentAccessId { get; set; }
        public bool IsLeaf { get; set; }
        public int TypeId { get; set; }
        public string TypeDescription { get; set; }
        public bool HasChildren { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public bool IsWarehouse { get; set; }
        public string Image { get; set; }
        public bool IsCompanyWarehouse { get; set; }
        public string NTN { get; set; }
        public string STRN { get; set; }
        public string ContactNumber { get; set; }
        public string AlternateContactNo { get; set; }
    }
}
