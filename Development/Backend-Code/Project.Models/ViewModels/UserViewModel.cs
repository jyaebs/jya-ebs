﻿
using Project.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace Project.Models.ViewModels
{
    public class UserViewModel : BaseViewModel
    {

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CNIC { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Role { get; set; }
        public int LocationTypeId { get; set; }
        public string LocationType { get; set; }
        public int LocationId { get; set; }
        public bool IsDistributor { get; set; }
        public bool IsOrderbooker { get; set; }
        public decimal Margin { get; set; }
        public bool IsCredited { get; set; }
        public int CreditDays { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal Discount { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string MacAddress { get; set; }
        public bool IsMobileUser { get; set; }
        public string Image { get; set; }
    }
}
