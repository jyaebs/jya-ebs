﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class AssignPrivilegesViewModel : BaseViewModel
    {
        public int PrivilegeId { get; set; } 
        public string RoleId { get; set; } 
        public string Type { get; set; }
        public string Category { get; set; }
        public string Portal { get; set; }

        public IEnumerable<RolesModel> Roles { get; set; }
        public IEnumerable<PrivilegesModel> Privileges { get; set; } 
    }
    public class RolesModel : AssignPrivilegesViewModel
    {

    }
    public class PrivilegesModel : AssignPrivilegesViewModel
    {
        
    }
    
}
