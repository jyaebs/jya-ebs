﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class BackOrderConfirmationViewModel : BaseViewModel
    {
        public int DistributorId { get; set; }
        public long OutletId { get; set; }
        public int OrderBookerId { get; set; }
        public DateTime OrderDate { get; set; }


        public string OutletCode { get; set; }
        public string OutletName { get; set; }
        public string OrderBookerName { get; set; }
        public string OrderReferenceNo { get; set; }
        public string OrderDateString { get; set; }

        public bool IsBackOrderPossible { get; set; }
        public bool IsCancelOrder { get; set; }
        public string Message { get; set; }
        public List<BackOrderDetailViewModel> BackOrderDetail{ get; set; }

    }

    public class BackOrderDetailViewModel : SaleOrderDetailViewModel
    {
        public Boolean isbackOrderItem { get; set; }
        public decimal StockQuantity { get; set; }
    }
}
