﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class DispatchViewModel :BaseViewModel
    {
        public string DispatchReferenceNo { get; set; }
        public DateTime DispatchDate { get; set; }
        public string DispatchDateString { get; set; }
        public int DistributorId { get; set; }

        public int DriverId { get; set; }
        public int VehicleId { get; set; }
        public string VehicleDescription { get; set; }
        public string DriverDescription { get; set; }
        public List<DispatchDetailViewModel> DispatchDetail{ get; set; }
    }



    public class DispatchDetailViewModel : SaleInvoiceViewModel
    {
        public int InvoiceId { get; set; }
    }
}
