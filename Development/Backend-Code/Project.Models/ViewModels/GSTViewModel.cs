﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class GSTViewModel : BaseViewModel
    {
        public decimal Value { get; set; }
        public string AccountGST { get; set; }
        public string AccountGSTDescription { get; set; }
    }
}
