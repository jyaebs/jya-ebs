﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class PromotionViewModel : BaseViewModel
    {
        public int LocationTypeId { get; set; }
        public int LocationId { get; set; }
        public int ProductId { get; set; }
        public int PromotionTypeId { get; set; }
        public string PromotionType { get; set; }
        public int ChannelType { get; set; }
        public int ProductVariationId { get; set; }
        public decimal PrimaryQuantity { get; set; }
        public decimal SecondaryQuantity { get; set; }
        public decimal ProductConversionFactor { get; set; }
        public decimal TotalQuantity { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateString { get; set; }
        public DateTime EndDate { get; set; }
        public string EndDateString { get; set; }

        public string ParentProductVariationName { get; set; }
        public string ParentProductName { get; set; }
       

        public List<PromotionDetailViewModel> PromotionDetail { get; set; }

    }

    public class PromotionDetailViewModel
    {
        public int ChildProductId { get; set; }
        public int ChildProductVariationId { get; set; }
        public decimal ChildPrimaryQuantity { get; set; }
        public decimal ChildSecondaryQuantity { get; set; }
        public decimal ChildTotalQuantity { get; set; }
        public decimal ChildProductConversionFactor { get; set; }

        public string ChildProductVariationName { get; set; }
        public string ChildProductName { get; set; }

        public int PromotionId { get; set; }
    }
}
