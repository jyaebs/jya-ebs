﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class MobileMasterDataViewModel
    {
        public object data { get; set; }
        public string Entity { get; set; }
    }
    public class MobileCountryViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileCityViewModel
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileChannelTypeViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileProductCategoryViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileProductSubCategoryViewModel
    {
        public int Id { get; set; }
        public int ProductCategoryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileCompetitorViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileDistributorViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MobileProductFormatViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
