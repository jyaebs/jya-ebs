﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class InvoiceViewModel : BaseViewModel
    {
        public int LocationId { get; set; }
        public int SaleOrderId { get; set; }
        public string LocationName { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string ReferenceNumber { get; set; }
        public string Remarks { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string orderdatestring { get; set; }
        public int TotalNetAmount { get; set; }
        public int TotalGrossAmount { get; set; }
        public int TotalDiscountAmount { get; set; }
        public int NetAmount { get; set; }
        public List<InvoiceDetailViewModel> InvoiceDetail { get; set; }
        public string WarehouseId { get; set; }
    }
}
