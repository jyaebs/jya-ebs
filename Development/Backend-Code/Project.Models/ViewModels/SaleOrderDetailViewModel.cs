﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Project.Models.ViewModels

{
    public class SaleOrderDetailViewModel : BaseViewModel
    {

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int PrimaryQuantity { get; set; }
        public int SecondaryQuantity { get; set; }
        public int TotalQuantity { get; set; }
        public int DeliveredQuantity { get; set; }
        public int UnitPrice { get; set; }
        public int GrossAmount { get; set; }
        public int Discount { get; set; }
        public int DiscountAmount { get; set; }
        public int NetAmount { get; set; }

        //public string UniqueId { get; set; }


        //public int PromotionId { get; set; }

        //public bool IsReturn { get; set; }
        //public bool IsFOC { get; set; }
        //public decimal RetailPrice { get; set; }
        //public decimal TaxAmount { get; set; }
        //public decimal Margin { get; set; }
        //public decimal TradePrice { get; set; }
        //public decimal DefaultDiscountAmount { get; set; }

        //public decimal NetTradePrice { get; set; }
        //public bool IsFixedDiscount { get; set; }
        //public decimal OrderBookerDiscount { get; set; }
        //public decimal OrderBookerDiscountAmount { get; set; }
        //public decimal NetPrice { get; set; }
    }
}
