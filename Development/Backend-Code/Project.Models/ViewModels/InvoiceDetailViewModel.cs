﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class InvoiceDetailViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public int PrimaryQuantity { get; set; }
        public int SecondaryQuantity { get; set; }
        public int TotalQuantity { get; set; }
        public int GST { get; set; }
        public int UnitPrice { get; set; }
        public int GrossAmount { get; set; }
        public int Discount { get; set; }
        public int DiscountAmount { get; set; }
        public int NetAmount { get; set; }
       
    }
}

