﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class StockLimitViewModel : BaseViewModel
    {

        public string Id { get; set; }
        public int ProductId { get; set; }
        public int ProductVariationId { get; set; }
        public string ProductName { get; set; }
        public string ProductVariationName { get; set; }
        public decimal MinStock { get; set; }
        public decimal MaxStock { get; set; }
        public decimal ROL { get; set; }
        public string Warehouse { get; set; }
        public int WarehouseId { get; set; }
    }
    public class StockLimitListViewModel : BaseViewModel
    {
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public List<StockLimitViewModel> StockDetailLimitList { get; set; }
    }
}
