﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class ProductOrderDetailViewModel
    {
        public int OutletId { get; set; }
        public int ProductId { get; set; }
        public int VariationId { get; set; }
        public decimal PrimaryQuantity { get; set; }
        public decimal SecondaryQuantity { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal Discount { get; set; }
        public DateTime OrderDate { get; set; }
    }

}
