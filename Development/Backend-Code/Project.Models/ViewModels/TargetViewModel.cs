﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class TargetViewModel : BaseViewModel
    {
        public string id { get; set; }
        public int ProductId { get; set; }
        public int ProductVariationId { get; set; }
        public string ProductName { get; set; }
        public string ProductVariationName { get; set; }
        public decimal MinStock { get; set; }
        public decimal MaxStock { get; set; }
        public decimal ROL { get; set; }
        public string Pjp { get; set; }
        public int PjpId { get; set; }
        public int LocationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FromDateString { get; set; }
        public string EndDateString { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSubCategory { get; set; }
    }
    public class TargetLimitListViewModel : TargetViewModel
    {
        public string PjpIdName { get; set; }
        public int TargetId { get; set; }
        public string LocationIdName { get; set; }
        public List<TargetViewModel> TargetDetailLimitList { get; set; }
    }
}

