﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class StockIssuanceViewModel : StockViewModel
    {
        public decimal IssuedStock { get; set; }
        public int OrderBookerId { get; set; }
        public int StockId { get; set; }

    }
    public class StockIssuanceListViewModel : BaseViewModel
    {
        public int OrderBookerId { get; set; }
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public List<StockIssuanceViewModel> StockIssuanceList { get; set; }
    }


}
