﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class OutletViewModel : BaseViewModel
    {
        public long Id { get; set; }
        public string Ntn { get; set; }
        public string OwnerName { get; set; }
        public string CNIC { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string AlternateNo { get; set; }
        public string PurchaserName { get; set; }
        public string PurchaserMobile { get; set; }
        public int ChannelTypeId { get; set; }
        public int LocationId { get; set; }
        public string Address { get; set; }
        public string Brick { get; set; }
        public string Nearby { get; set; }
        public TimeSpan OpeningTime { get; set; }
        public TimeSpan ClosingTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }    
        public bool IsCredited { get; set; }
        public int CreditDays { get; set; }
        public decimal CreditAmount { get; set; }
        public List<OutletDetailViewModel> OutletDetails { get; set; }
    }
}
