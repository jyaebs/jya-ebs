﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class InvoicePaymentViewModel : BaseViewModel
    {
        public int InvoiceId { get; set; }
        public decimal ReceivedAmount { get; set; }
        public int DistributorId { get; set; }
        public long OutletId { get; set; }

        public bool isNormalPayment { get; set; }

    }
}
