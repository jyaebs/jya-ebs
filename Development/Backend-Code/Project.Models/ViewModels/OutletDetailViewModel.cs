﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class OutletDetailViewModel : BaseViewModel
    {
        public long DetailId { get; set; }
        public long OutletId { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public int SubCategoryId { get; set; }
        public string SubCategory { get; set; }
        public int CompetitorId { get; set; }
        public string Competitor { get; set; }
        public string SKU { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}
