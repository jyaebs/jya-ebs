﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class PurchaseQuotationViewModel : BaseViewModel
    {
        public string ReferenceNumber { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int PurchaseRequisitionId { get; set; }
        public int PurchaseTypeId { get; set; }
        public string PurchaseTypeName { get; set; }
        public string PriorityLevel { get; set; }
        public string PriorityLevelName { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public int StatusId { get; set; }
        public string Remarks { get; set; }
        
        public List<PurchaseQuotationDetailViewModel> PurchaseQuotationDetail { get; set; }
    }

    public class PurchaseQuotationDetailViewModel
    {
        public int PurchaseQuotationId { get; set; }
        public int PurchaseRequisitionDetailId { get; set; }
        public int VendorId { get; set; }
        public string Vendor { get; set; }
        public int ProductId { get; set; }
        public string Product { get; set; }
        public decimal PrimaryQuantity { get; set; }
        public decimal SecondaryQuantity { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal NetAmount { get; set; }
        public bool isChecked { get; set; }
    }



}
