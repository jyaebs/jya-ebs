﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class VehicleViewModel : BaseViewModel
    {

        public int LocationId { get; set; }
        public string LocationName { get; set; }


        public int VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }
    }
}
