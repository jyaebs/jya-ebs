﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class PurchaseOrderViewModel : BaseViewModel
    {
        public string PurchaseReferenceNumber { get; set; }
        public int PurchaseQuotationId { get; set; }
        public int LocationId { get; set; }
        public int VendorId { get; set; }
        public decimal TotalNetAmount { get; set; }
        public string VendorName { get; set; }
        public int GrossAmount { get; set; }
        public int DiscountAmount { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderDateString { get; set; }
        public string VoucherNo { get; set; }
        public int NetAmount { get; set; }
        public string Remarks { get; set; }
        public string OrderStatus { get; set; }

        public List<PurchaseOrderDetailViewModel> PurchaseDetails { get; set; }

    }
}
