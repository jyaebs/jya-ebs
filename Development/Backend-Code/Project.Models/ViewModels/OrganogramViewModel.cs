﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class OrganogramViewModel
    {
        public int id { get; set; }
        public string text { get; set; }
        public bool children { get; set; }       
        public string TotalSales { get; set; }
        public string GrossSale { get; set; }
        public string TotalTo { get; set; }
        public string Discount { get; set; }
    }
}
