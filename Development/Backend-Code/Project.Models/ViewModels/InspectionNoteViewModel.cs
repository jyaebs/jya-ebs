﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project.Models.ViewModels
{
    public class InspectionNoteViewModel : BaseViewModel
    {
        public string ReferenceNumber { get; set; }
        public int InwardGatePassId { get; set; }
        public int LocationId { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string VoucherNo { get; set; }
        public DateTime InspectionDate { get; set; }
        public DateTime OrderDate { get; set; }
        public string InspectionDateString { get; set; }
        public decimal NetAmount { get; set; }
        public int StatusId { get; set; }
        public string Remarks { get; set; }

        public List<InspectionNoteDetailViewModel> InspectionNoteDetails { get; set; }
    }

    public class InspectionNoteDetailViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal PrimaryQuantity { get; set; }
        public decimal SecondaryQuantity { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal ApprovedQuantity { get; set; }
        public decimal RejectQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal NetAmount { get; set; }
    }
}
