﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;


namespace Project.Models.ViewModels
{
    public class VendorViewModel : BaseViewModel
    {
        public int VendorTypeId { get; set; }
        public string VendorTypeName { get; set; }
        public string CNIC { get; set; }
        public string NTN { get; set; }
        public bool IsCredit { get; set; }
        public int CreditDays { get; set; }
        public decimal CreditAmount { get; set; }
        public string ContactPersonName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string AccountPayable { get; set; }
        public string AccountPayableDescription { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string DownPaymentAccount { get; set; }
        public string DownPaymentAccountDescription { get; set; }
    }
}
