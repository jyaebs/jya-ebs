﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class DistributorViewModel : BaseViewModel
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CNIC { get; set; }
        public string Email { get; set; }
        public decimal Margin { get; set; }
        public string ContactNo { get; set; }
        public bool IsCredited { get; set; }
        public int CreditDays { get; set; }
        public decimal CreditAmount { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string DistributorId { get; set; }
        public string UserId { get; set; }

    }
}
