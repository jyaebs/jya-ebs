﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Project.Models.ViewModels
{
    public class SaleOrderViewModel : BaseViewModel
    {

        //public int DistributorId { get; set; }
        //public int OutletId { get; set; }
        //public string OutletCode { get; set; }
        //public string OutletName { get; set; }
        //public string OutletAddress { get; set; }
        //public string OrderBookerName { get; set; }
        //public string DistributorName { get; set; }
        //public string OrderReferenceNo { get; set; }
        //public int OrderBookerId { get; set; }
        //public string Latitude { get; set; }
        //public string Longitude { get; set; }
        //public string MobileAppVersion { get; set; }
        //public string WebAppVersion { get; set; }
        //public decimal GrossAmount { get; set; }
        //public decimal GstAmount { get; set; }
        //public decimal DefaultDiscountAmount { get; set; }
        //public decimal OrderbookerDiscountAmount { get; set; }
        //public decimal NetAmount { get; set; }
        //public string Remarks { get; set; }
        //public int StatusId { get; set; }
        //public DateTime OrderDate { get; set; }
        //public string OrderDateString { get; set; }
        //public decimal TotalQuantity { get; set; }
        //public string CustomerName { get; set; }

        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string VoucherNo { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string orderdatestring { get; set; }
        public int TotalNetAmount { get; set; }
        public int TotalGrossAmount { get; set; }
        public int TotalDiscountAmount { get; set; }
       
       
        public int NetAmount { get; set; }

        public List<SaleOrderDetailViewModel> SaleOrderDetail { get; set; }

       

    }
   

}
