﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class OrderBookerKPIViewModel : BaseViewModel
    {
        public int PJPId { get; set; }
        public string OrderbookerName { get; set; }
        public string ScheduledVisit { get; set; }
        public string VisitedShops { get; set; }
        public string ProductiveShops { get; set; }
        public string Productivity { get; set; }
        public string AvgSKUPerBill { get; set; }
        public string AvgPerBillCashValue { get; set; }
        public string TotalAmount { get; set; }
        public int OrderBookerId { get; set; }
    }
}
