﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class PriceSchemeViewModel : BaseViewModel
    {
        public string ProductName { get; set; }
        public int PriceId { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal TradePrice { get; set; }
        public decimal NetTradePrice { get; set; }
        public string TypeName { get; set; }
        public int TypeId { get; set; }
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public string ChannelTypeName { get; set; }
        public int ChannelTypeId { get; set; }
        public int PJPId { get; set; }
        public string OutletName { get; set; }
        public int OutletId { get; set; }
        public decimal Margin { get; set; }
        public string MarginType { get; set; }
        public int DiscountType { get; set; }
        public string DiscountTypeName { get; set; }
        public decimal Quantity { get; set; }
        public decimal DefaultDiscount { get; set; }
        public decimal NetPrice { get; set; }
        public bool IsMarginTypeFixed { get; set; }
    }
}
