﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class SecondaryUOMViewModel : BaseViewModel
    {
        public int UOMId { get; set; }
        public string UOMName { get; set; }
        public int PrimaryUOMId { get; set; }
        public string PrimaryUOMName { get; set; }
    }
}
