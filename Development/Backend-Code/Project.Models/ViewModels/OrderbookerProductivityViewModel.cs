﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class OrderbookerProductivityViewModel
    {
        public string OrderbookerName { get; set; }
        public int ScheduledVisit { get; set; }
        public int VisitedShops { get; set; }
        public int ProductiveShops { get; set; }
        public decimal Productivity { get; set; }
        public decimal AvgSKUPerBill { get; set; }
        public decimal AvgPerBillCashValue { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
