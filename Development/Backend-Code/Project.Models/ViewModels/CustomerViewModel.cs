﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class CustomerViewModel : BaseViewModel
    {
        public int CustomerTypeId { get; set; }
        public string CustomerTypeName { get; set; }
        public string CNIC { get; set; }
        public string NTN { get; set; }
        public bool IsCredit { get; set; }
        public int CreditDays { get; set; }
        public decimal CreditAmount { get; set; }
        public string ContactPersonName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string AccountReceivable { get; set; }
        public string AccountReceivableDescription { get; set; }
        public string DownPaymentAccount { get; set; }
        public string DownPaymentAccountDescription { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}
