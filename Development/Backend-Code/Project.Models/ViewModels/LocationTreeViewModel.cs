﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class LocationTreeViewModel 
    {
        public int id { get; set; }
        public string text { get; set; }
        public bool children { get; set; }
    }
}
