﻿

namespace Project.Models.ViewModels
{
    public class LisenceKeyViewModel
    {
        public int Id { get; set; }

        public string Key { get; set; }

    }
}
