﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class ProductPriceDetailViewModel : BaseViewModel
    {
        public int MobileTypeId { get; set; }
        public int ChannelTypeId { get; set; }
        public long OutletId { get; set; }
        public int TypeId { get; set; }
        public int ProductId { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Margin { get; set; }
        public decimal TradePrice { get; set; }
        public decimal DefaultDiscount { get; set; }
        public decimal NetPrice { get; set; }

    }
}
