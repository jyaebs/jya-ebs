﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class PrimaryPriceSchemeViewModel : BaseViewModel
    {
        public int TypeId { get; set; }
        public int LocationId { get; set; }
        public decimal Margin { get; set; }
        public int PriceId { get; set; }
        public bool IsMarginTypeFixed { get; set; }
        public string TypeName { get; set; }
        public string ProductName { get; set; }
        public decimal RetailPrice { get; set;}
        public string MarginType { get; set; }
        public decimal NetPrice { get; set;}
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
