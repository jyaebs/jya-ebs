﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class MobileUserViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }    
        public int LocationId { get; set; }
        public decimal AllowedDiscount { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public int OrderNumber { get; set; }
        public DateTime Date { get; set; }
        public string ComapanyName { get; set; }
    }
}
