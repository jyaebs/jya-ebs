﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class StockSettlementViewModel:StockViewModel
    {
    }
    public class StockIssuancedListViewModel : BaseViewModel
    {
        public int OrderBookerId { get; set; }
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public List<StockSettlementViewModel> StockIssuancedList { get; set; }
    }
}
