﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class PostMarketSaleViewModel:BaseViewModel
    {
        public int DistributorId { get; set; }
        public string DistributorName { get; set; }
        public int OutletId { get; set; }
        public decimal TotalAmount { get; set; }
        public List<PostSaleReturnDetailsViewModel> PostSaleReturnDetails { get; set; }

    }
    public class PostSaleReturnDetailsViewModel
    {
  
        public int ReturnTypeId { get; set; }
        public string ReturnType { get; set; }
        public int ProductId { get; set; }
        public int ProductVariationId { get; set; }
        public string BatchNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal PrimaryQuantity { get; set; }
        public decimal SecondaryQuantity { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal NetAmount { get; set; }
        
    }

}
