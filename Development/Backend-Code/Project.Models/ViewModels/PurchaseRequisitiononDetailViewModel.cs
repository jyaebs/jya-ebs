﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class PurchaseRequisitiononDetailViewModel:BaseViewModel
    {
        public int RequisitionId { set; get; }
        public int ProductId { get; set; }
        public decimal PrimaryQuantity { get; set; }
        public decimal SecondaryQuantity { get; set; }
        public decimal TotalQuantity { get; set; }

    }
}
