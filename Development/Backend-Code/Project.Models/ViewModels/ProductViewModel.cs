﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ViewModels.Base;

namespace Project.Models.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
    

        public string Barcode { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeName { get; set; }
        public int ProductBrandId { get; set; }
        public int Category { get; set; }
        public int SubCategory { get; set; }
        public int PrimaryUOM { get; set; }
        public string PrimaryUOMName { get; set; }
        public int SecondaryUOM { get; set; }
        public string SecondaryUOMName { get; set; }
        public decimal ConversionFactor { get; set; }
        public string Image { get; set; }
        public bool Trendy { get; set; }
        public string DepreciationMethod { get; set; } 
        public string DepreciationType { get; set; }
        public decimal DepreciationRate { get; set;}
        public int UsageLife { get; set; }
        public decimal ScrapeValue { get; set; }
        public string InventoryAccount { get; set; }
        public string InventoryAccountDescription { get; set; } 
        public string COGSAccount { get; set; }
        public string COGSAccountDescription { get; set; }
        public string DepreciationAccount { get; set; } 
        public string DepreciationAccountDescription { get; set; }

    }
   
}
