﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class ReportExportViewModel
    {
        public string FileName { get; set; }
        public string FolderPath { get; set; }
        public MemoryStream stream { get; set; }
    }
}
