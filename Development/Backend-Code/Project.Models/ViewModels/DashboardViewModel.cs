﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
    public class DashboardViewModel : BaseViewModel
    {

        public int TotalOutlets { get; set; }
        public int NewOutlets { get; set; }
        public int TotalInvoices { get; set; }
        public decimal TotalSales { get; set; }     
        public int TotalOrders { get; set; }
        public decimal TotalOrdersAmount { get; set; }
        public int TotalUsers { get; set; }
        public int TotalOrderBookers { get; set; }
        public int TotalPendingInvoices { get; set; }
        public decimal PendingInvoiceAmount { get; set; }
        public int UnsettledInvoice { get; set; }
        public decimal UnsettledInvoiceAmount { get; set; }
        public IEnumerable<TopSellingProductDetails> TopSellingProductDetails { get; set;}
        
    }
    public class TopSellingProductDetails
    {
       public string ProductName { get; set; }
        public string TotalQuantity { get; set; }
        public string PrimaryUnit { get; set; }
        public string Image { get; set; }
    }

}