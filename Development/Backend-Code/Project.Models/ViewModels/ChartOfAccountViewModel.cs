﻿using Project.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.ViewModels
{
   public class ChartOfAccountViewModel : BaseViewModel
    {
        public string ParentCode { get; set; }
        public int LevelID { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountTypeName { get; set; }
    }
}
