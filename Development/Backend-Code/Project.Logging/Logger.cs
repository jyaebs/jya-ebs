﻿
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using log4net;
using Project.ViewModels.Enums;

namespace Project.Logging
{
    public class Logger
    {
        private static Logger _instance = null;
        private static object instanceMutex = new object();
        private ILog _log;
        private ILog _logforAll;
        private ILog _serviceLog;


        public static Logger Instance
        {
            get
            {
                // we want to ensure that only one instance is created
                if (_instance == null)
                {
                    lock (instanceMutex)
                    {
                        if (_instance == null)
                        {
                            _instance = new Logger();
                        }

                    }
                }

                return _instance;
            }
        }

        List<string> loggerNames;
        private void updateLog()
        {
            var stackTrace = new StackTrace();
            List<string> callStack = new List<string>();
            foreach (var stack in stackTrace.GetFrames())
            {
                MethodBase method = stack.GetMethod();
                string methodName = method.Name;
                if (method.ReflectedType != null)
                {
                    string classNameSpace = method.ReflectedType.Namespace;

                    callStack.Add(classNameSpace);
                }
            }

            List<string> result = callStack.Intersect(loggerNames).ToList();
            if (result == null || result.Count == 0) _log = _logforAll;
            else
                switch (result[0])
                {
                    case "Project.Web.Controllers.WebApiControllers":
                        _log = _serviceLog;
                        break;
                    default:
                        _log = _logforAll;
                        break;
                }
        }

        private Logger()
        {
            loggerNames = new List<string>();
            loggerNames.Add("Project.Web.Controllers.WebApiControllers");

            log4net.Config.XmlConfigurator.Configure();

            _log = LogManager.GetLogger(typeof(Logger));
            _logforAll = LogManager.GetLogger(typeof(Logger));
            _serviceLog = LogManager.GetLogger("Project.Web.Controllers.WebApiControllers");
        }

        public void Write(EnumLogLevel logLevel, string className, string methodName, string logMessage)
        {
            string message = className + "." + methodName + "() ===> " + logMessage;
            Write(logLevel, message);
        }

        public void Write(EnumLogLevel logLevel, string logMessage)
        {
            updateLog();
            switch (logLevel)
            {
                case EnumLogLevel.Error:
                    Instance._log.ErrorFormat(logMessage);
                    break;
                case EnumLogLevel.Warning:
                    Instance._log.WarnFormat(logMessage);
                    break;
                case EnumLogLevel.Debug:
                    Instance._log.DebugFormat(logMessage);
                    break;
                case EnumLogLevel.Information:
                    Instance._log.InfoFormat(logMessage);
                    break;
                case EnumLogLevel.FunctionTrace:
                    Instance._log.InfoFormat(logMessage);
                    break;
                default:
                    Instance._log.InfoFormat(logMessage);
                    break;
            }
        }

        public void Write(EnumLogLevel logLevel, string logMessage, object obj)
        {
            updateLog();
            switch (logLevel)
            {
                case EnumLogLevel.Error:
                    Instance._log.ErrorFormat(logMessage, obj);
                    break;
                case EnumLogLevel.Warning:
                    Instance._log.WarnFormat(logMessage, obj);
                    break;
                case EnumLogLevel.Debug:
                    Instance._log.DebugFormat(logMessage, obj);
                    break;
                case EnumLogLevel.Information:
                    Instance._log.InfoFormat(logMessage, obj);
                    break;
                case EnumLogLevel.FunctionTrace:
                    Instance._log.InfoFormat(logMessage, obj);
                    break;
                default:
                    Instance._log.InfoFormat(logMessage, obj);
                    break;
            }
        }
    }
}
