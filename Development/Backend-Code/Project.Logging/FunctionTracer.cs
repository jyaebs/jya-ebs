﻿using Project.ViewModels.Enums;
using System;
using System.Diagnostics;

namespace Project.Logging
{
    public class FunctionTracer : IDisposable
    {
        private static readonly Logger Logger;

        static FunctionTracer()
        {
            FunctionTracer.Logger = Logger.Instance;
        }
        private String className;
        private String methodName;
        private Stopwatch stopwatch = new Stopwatch();

        public FunctionTracer(string className, string methodName, params object[] args)
        {
            string message = string.Empty;

            message = message + className + "." + methodName;
            if (args != null)
            {
                message = message + "(";

                for (int index = 0; index < args.Length; ++index)
                {
                    if (args[index] != null)
                    {
                        message += args[index].ToString();
                    }
                    else
                    {
                        message += "NULL";
                    }
                    if (index != args.Length - 1)
                    {
                        message += ", ";
                    }
                }

                /*foreach (object i in args)
                {
                    if (i != null)
                    {
                        message +=  i.ToString();
                        if (!i.Equals(args.Last()))
                        {
                            message +=  ", ";
                        }
                    }
                }*/

                message = message + ")";
            }

            this.className = className;
            this.methodName = methodName;
            this.MakeLogEntry(message);
        }

        private void MakeLogEntry(string message)
        {
            //Logger.logger.DebugFormat(message + "| --> Start");
            FunctionTracer.Logger.Write(EnumLogLevel.FunctionTrace, message + "| --> Start");
            this.stopwatch.Start();
        }

        public void Dispose()
        {
            this.stopwatch.Stop();
            FunctionTracer.Logger.Write(EnumLogLevel.FunctionTrace, className + "." + methodName + "| --> End " + this.stopwatch.Elapsed);
        }
    }
}
