﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class VehicleService : IBaseService<VehicleViewModel>
    {
        private VehicleRepository _VehicleRepository = new VehicleRepository();
        public ResponseViewModel Activate(VehicleViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_VEHICLE dbValue = _VehicleRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _VehicleRepository.Update(dbValue);
                _VehicleRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(VehicleViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VEHICLE dbValue = _VehicleRepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }
                dbValue = VehicleMapper.DBMapper(dbValue, value);


                _VehicleRepository.Add(dbValue);
                _VehicleRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Vehicle Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(VehicleViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VEHICLE dbValue = _VehicleRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _VehicleRepository.Update(dbValue);
                _VehicleRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_VEHICLE dbValue = _VehicleRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                VehicleViewModel Vehicle = VehicleMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Vehicle;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<VehicleViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<VehicleViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                iSortCol == 3 ? t.VehicleTypeName :
                iSortCol == 4 ? t.LocationName :
                t.Id.ToString()
            );
            IEnumerable<VehicleViewModel> data = _VehicleRepository.GetAll().Select(p => new VehicleViewModel()
            {
                VehicleTypeName = p.DB_VEHICLE_TYPE.Name,
                LocationName = p.DB_LOCATION.Name,
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<VehicleViewModel> dataObject = new PaginatedRecordsModel<VehicleViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(VehicleViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VEHICLE db_VEHICLE = _VehicleRepository.Get(p => p.Id == value.Id);
                if (db_VEHICLE == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_VEHICLE = VehicleMapper.DBMapper(db_VEHICLE, value);

                _VehicleRepository.Update(db_VEHICLE);
                _VehicleRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Vehicle  Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
    }
}
