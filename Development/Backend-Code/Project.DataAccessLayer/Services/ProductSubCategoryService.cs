﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class ProductSubCategoryService : IBaseService<ProductSubCategoryViewModel>
    {
        private  ProductSubCategoryRepository _ProductSubCategoryRepository = new ProductSubCategoryRepository();
        public ResponseViewModel Activate(ProductSubCategoryViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT_SUB_CATEGORY dbValue = _ProductSubCategoryRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductSubCategoryRepository.Update(dbValue);
                _ProductSubCategoryRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    

        public ResponseViewModel Add(ProductSubCategoryViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_SUB_CATEGORY dbValue = _ProductSubCategoryRepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                dbValue = ProductSubCategoryMapper.DBMapper(dbValue,value);

               
                _ProductSubCategoryRepository.Add(dbValue);
                _ProductSubCategoryRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Product Category Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(ProductSubCategoryViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_SUB_CATEGORY dbValue = _ProductSubCategoryRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductSubCategoryRepository.Update(dbValue);
                _ProductSubCategoryRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT_SUB_CATEGORY dbValue = _ProductSubCategoryRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                ProductSubCategoryViewModel ProductCategory = ProductSubCategoryMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductCategory;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<ProductSubCategoryViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ProductSubCategoryViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
               
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.ProdcutCategoryName :
                iSortCol == 3 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<ProductSubCategoryViewModel> data = _ProductSubCategoryRepository.GetAll().Select(p => new ProductSubCategoryViewModel()
            {
                ProdcutCategoryName = p.DB_PRODUCT_CATEGORY.Name,
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ProductSubCategoryViewModel> dataObject = new PaginatedRecordsModel<ProductSubCategoryViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(ProductSubCategoryViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_SUB_CATEGORY db_PRODUCT_SUB_CATEGORY = _ProductSubCategoryRepository.Get(p => p.Id == value.Id);
                if (db_PRODUCT_SUB_CATEGORY == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_PRODUCT_SUB_CATEGORY = ProductSubCategoryMapper.DBMapper(db_PRODUCT_SUB_CATEGORY, value);

                _ProductSubCategoryRepository.Update(db_PRODUCT_SUB_CATEGORY);
                _ProductSubCategoryRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Product Category Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }

        }
        public ResponseViewModel GetAllActiveProductSubCategory(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductSubCategoryViewModel> ProductSubCategory = _ProductSubCategoryRepository.GetMany(p => p.ValidFlag == true && p.ProductCategoryId==id)
                 .Select(p => new ProductSubCategoryViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductSubCategory;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }


    }



}
