﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class ProductService : IBaseService<ProductViewModel>
    {
        private ProductRepository _ProductRepository = new ProductRepository();

        public ResponseViewModel Activate(ProductViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT dbValue = _ProductRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductRepository.Update(dbValue);
                _ProductRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(ProductViewModel value)
        {
            
                ResponseViewModel response = new ResponseViewModel();
                try
                {
                    DB_PRODUCT dbValue = _ProductRepository.Get(x => x.Code == value.Code);
                    if (dbValue != null)
                    {
                        response.LogLevel = EnumLogLevel.Warning;
                        response.Status = HttpStatusCode.BadRequest;
                        response.obj = "Code Already Exist";
                        response.Message = "Code Already Exist";
                        return response;
                    }
                    dbValue = ProductMapper.DBMapper(dbValue, value);


                _ProductRepository.Add(dbValue);
                _ProductRepository.Commit();

                    response.Status = HttpStatusCode.Created;
                    response.Message = "Product Added Successfully ";
                    response.LogLevel = EnumLogLevel.Information;

                    return response;

                }
                catch (Exception ex)
                {
                    response.Status = HttpStatusCode.InternalServerError;
                    response.Message = ex.Message;
                    response.LogLevel = EnumLogLevel.Error;
                    return response;
                }
            

        }

        public ResponseViewModel Deactivate(ProductViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT dbValue = _ProductRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductRepository.Update(dbValue);
                _ProductRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT dbValue = _ProductRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                ProductViewModel Product = ProductMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Product;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<ProductViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ProductViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<ProductViewModel> data = _ProductRepository.GetAll().Select(p => new ProductViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ProductViewModel> dataObject = new PaginatedRecordsModel<ProductViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(ProductViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT db_DB_PRODUCT = _ProductRepository.Get(p => p.Id == value.Id);
                if (db_DB_PRODUCT == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_DB_PRODUCT = ProductMapper.DBMapper(db_DB_PRODUCT, value);

                _ProductRepository.Update(db_DB_PRODUCT);
                _ProductRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Product Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel GetAllActiveProduct()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductViewModel> Product = _ProductRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ProductViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Product;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
        
        public ResponseViewModel GetProductDetail(int productId)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT db_value = _ProductRepository.Get(p => p.Id == productId);
                ProductViewModel Product = ProductMapper.DBValue(db_value);
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Product;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
        
    }
}
