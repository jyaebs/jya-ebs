﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{


    public class ChartOfAccountService : IBaseService<ChartOfAccountViewModel>
    {
        private ChartOfAccountRepository _COARepository = new ChartOfAccountRepository();

        public ResponseViewModel Activate(ChartOfAccountViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_CHART_OF_ACCOUNT dbValue = _COARepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _COARepository.Update(dbValue);
                _COARepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(ChartOfAccountViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CHART_OF_ACCOUNT dbValue = _COARepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }


                DB_CHART_OF_ACCOUNT _dbValue = _COARepository.Get(x => x.Name == value.Name);
                if (_dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Name Already Exist";
                    response.Message = "Name Already Exist";
                    return response;
                }
                dbValue = ChartOfAccountMapper.DBMapper(_dbValue, value);


                _COARepository.Add(dbValue);
                _COARepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Chart of Account Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(ChartOfAccountViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CHART_OF_ACCOUNT dbValue = _COARepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _COARepository.Update(dbValue);
                _COARepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_CHART_OF_ACCOUNT dbValue = _COARepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                ChartOfAccountViewModel COA = ChartOfAccountMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = COA;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<ChartOfAccountViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ChartOfAccountViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.AccountTypeName :
                iSortCol == 3 ? t.LevelID.ToString() :
                iSortCol == 4 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<ChartOfAccountViewModel> data = _COARepository.GetAll().Select(p => new ChartOfAccountViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                AccountTypeName = p.DB_DEV_CHART_OF_ACCOUNT_TYPE.Name,
                LevelID = p.LevelID,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                || x.LevelID.ToString().ToUpper().Contains(model.Search.ToUpper())
                || x.AccountTypeName.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ChartOfAccountViewModel> dataObject = new PaginatedRecordsModel<ChartOfAccountViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public PaginatedRecordsModel<ChartOfAccountViewModel> GetPaginatedLevels(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ChartOfAccountViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<ChartOfAccountViewModel> data = _COARepository.GetMany(p => p.LevelID == model.LevelId && p.ValidFlag == true).Select(p => new ChartOfAccountViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                AccountTypeName = p.DB_DEV_CHART_OF_ACCOUNT_TYPE.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ChartOfAccountViewModel> dataObject = new PaginatedRecordsModel<ChartOfAccountViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public PaginatedRecordsModel<ChartOfAccountViewModel> GetPaginatedEntryLevelChartOfAccount(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ChartOfAccountViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<ChartOfAccountViewModel> data = _COARepository.GetMany(p => p.LevelID == 3 && p.ValidFlag == true).Select(p => new ChartOfAccountViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                AccountTypeName = p.DB_DEV_CHART_OF_ACCOUNT_TYPE.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ChartOfAccountViewModel> dataObject = new PaginatedRecordsModel<ChartOfAccountViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }


        public ResponseViewModel Update(ChartOfAccountViewModel value)
        {
            throw new NotImplementedException();
           
        }
    }
}
