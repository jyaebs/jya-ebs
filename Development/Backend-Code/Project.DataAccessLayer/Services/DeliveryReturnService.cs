﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.Mapper.Mappers;
using Project.DatabaseModels;
using System.Net;
using Project.ViewModels.Enums;

namespace Project.DataAccessLayer.Services { }

public class DeliveryReturnService : IBaseService<DeliveryNoteViewModel>

{
    public ResponseViewModel Activate(DeliveryNoteViewModel value)
    {
        throw new NotImplementedException();
    }

    public ResponseViewModel Add(DeliveryNoteViewModel value)
    {
        throw new NotImplementedException();
    }

    public ResponseViewModel Deactivate(DeliveryNoteViewModel value)
    {
        throw new NotImplementedException();
    }

    public ResponseViewModel GetById(int id)
    {
        throw new NotImplementedException();
    }

    public PaginatedRecordsModel<DeliveryNoteViewModel> GetPaginatedRecords(PaginationSearchModel model)
    {
        throw new NotImplementedException();
    }

    public ResponseViewModel Update(DeliveryNoteViewModel value)
    {
        throw new NotImplementedException();
    }
}