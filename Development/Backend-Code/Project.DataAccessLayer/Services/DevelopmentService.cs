﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Project.DataAccessLayer.Services
{
    public class DevelopmentService
    {
        private LocationTypeRepository _LocationTypeRepository = new LocationTypeRepository();
        private ChartOfAccountTypeRepository _ChartOfAccountTypeRepository = new ChartOfAccountTypeRepository();
        private ProductTypeRepository _ProductTypeRepository = new ProductTypeRepository();

        private ProductPriceTypeRepository _ProductPriceTypeRepository = new ProductPriceTypeRepository();

        //Get Active Location Types
        public ResponseViewModel GetAllLocationTypes()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<LocationTypeViewModel> LocationType = _LocationTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new LocationTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = LocationType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetAllActiveProductType()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductPriceTypeViewModel> ProductType = _ProductPriceTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ProductPriceTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductType;

                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }
        public ResponseViewModel GetAllActiveProductPriceType()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductPriceTypeViewModel> ProductPriceType = _ProductPriceTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ProductPriceTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductPriceType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetAllChartOfAccountypes()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ChartOfAccountTypeViewModel> ProductType = _ChartOfAccountTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ChartOfAccountTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductType;

                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }
        
    }
}
    
