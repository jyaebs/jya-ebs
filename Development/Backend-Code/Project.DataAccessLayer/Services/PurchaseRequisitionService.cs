﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.Mapper.Mappers;
using System.Net;
using Project.ViewModels.Enums;
using Project.DatabaseModels;
using Project.Models.Enums;
using Project.Utilities.Utilities;

namespace Project.DataAccessLayer.Services
{
    public class PurchaseRequisitionService : IBaseService<PurchaseRequisitionViewModel>
    {
        private PurchaseRequisitionRepository _PurchaseRequisitionRepository = new PurchaseRequisitionRepository();

        public ResponseViewModel Activate(PurchaseRequisitionViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(PurchaseRequisitionViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PURCHASE_REQUISITION dbValue = PurchaseRequisitionMapper.DBMapper(value);
                dbValue.ReferenceNumber = ReferenceNumberUtility.GetPurchaseRequisitionNumber(value.LocationId);

                _PurchaseRequisitionRepository.Add(dbValue);
                _PurchaseRequisitionRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Purchase Requisition Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }

        }

        public ResponseViewModel Deactivate(PurchaseRequisitionViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel CancelById(PurchaseRequisitionViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PURCHASE_REQUISITION dbValue = _PurchaseRequisitionRepository.Get(p => p.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                dbValue.ValidFlag = false;
                dbValue.StatusId = (int)EnumPurchaseRequisition.Cancelled;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _PurchaseRequisitionRepository.Update(dbValue);
                _PurchaseRequisitionRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PURCHASE_REQUISITION dbValue = _PurchaseRequisitionRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                PurchaseRequisitionViewModel model = PurchaseRequisitionMapper.DBValue(dbValue);


                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = model;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public PaginatedRecordsModel<PurchaseRequisitionViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<PurchaseRequisitionViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.PriorityLevelId.ToString() :
                iSortCol == 2 ? t.PurchaseTypeId.ToString() :
                iSortCol == 3 ? t.Remarks :
                iSortCol == 4 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<PurchaseRequisitionViewModel> data = _PurchaseRequisitionRepository.GetMany(x => x.ValidFlag == true
            ).Select(p => new PurchaseRequisitionViewModel()
            {
                Id = p.Id,
                ReferenceNumber = p.ReferenceNumber,
                LocationName = p.DB_LOCATION.Name,
                PriorityLevel = p.PriorityLevel == "H" ? "High" : p.PriorityLevel == "M" ? "Medium" : "Low",
                PurchaseTypeName = p.DB_DEV_PURCHASE_TYPE.Name,
                StatusString = p.DB_DEV_PURCHASE_REQUISITION_STATUS.Name,
                ValidFlag = p.ValidFlag

            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<PurchaseRequisitionViewModel> dataObject = new PaginatedRecordsModel<PurchaseRequisitionViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public PaginatedRecordsModel<PurchaseRequisitionViewModel> GetOpenPurchaseRequisition(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<PurchaseRequisitionViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.ReferenceNumber :
                iSortCol == 1 ? t.LocationName :
                iSortCol == 2 ? t.PurchaseTypeName :
                iSortCol == 3 ? t.PriorityLevel :

                t.Id.ToString()
            );
            IEnumerable<PurchaseRequisitionViewModel> data = _PurchaseRequisitionRepository.GetMany(x => x.ValidFlag == true
            && x.StatusId == (int)EnumPurchaseRequisition.Open
            ).Select(p => new PurchaseRequisitionViewModel()
            {
                Id = p.Id,
                ReferenceNumber = p.ReferenceNumber,
                LocationId = p.LocationId,
                LocationName = p.DB_LOCATION.Name,
                PriorityLevel = p.PriorityLevel == "H" ? "High" : p.PriorityLevel == "M" ? "Medium" : "Low",
                PurchaseTypeName = p.DB_DEV_PURCHASE_TYPE.Name,
                ValidFlag = p.ValidFlag
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {
                data = data.Where(x => x.ReferenceNumber.ToUpper().Contains(model.Search.ToUpper())
                || x.LocationName.ToUpper().Contains(model.Search.ToUpper())
                || x.PriorityLevel.ToUpper().Contains(model.Search.ToUpper())
                || x.PurchaseTypeName.ToUpper().Contains(model.Search.ToUpper())
                );
            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<PurchaseRequisitionViewModel> dataObject = new PaginatedRecordsModel<PurchaseRequisitionViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(PurchaseRequisitionViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                int? parentId = null;
                DB_PURCHASE_REQUISITION old_dbValue = _PurchaseRequisitionRepository.Get(p => p.Id == value.Id);
                if (old_dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                else
                {
                    parentId = old_dbValue.Id;
                    
                    old_dbValue.ValidFlag = false;
                    old_dbValue.UpdatedBy = value.ChangedBy;
                    old_dbValue.UpdatedOn = value.ChangedOn;
                    _PurchaseRequisitionRepository.Update(old_dbValue);
                    
                }

                DB_PURCHASE_REQUISITION new_dbValue = PurchaseRequisitionMapper.DBMapper(value);

                new_dbValue.ReferenceNumber = ReferenceNumberUtility.GetPurchaseRequisitionNumber(value.LocationId);
                new_dbValue.ParentId = parentId;

                _PurchaseRequisitionRepository.Add(new_dbValue);
                _PurchaseRequisitionRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = " Purchase Requisition Updated Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

    }
}
