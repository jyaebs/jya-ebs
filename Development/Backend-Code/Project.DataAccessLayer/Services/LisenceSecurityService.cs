﻿using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using System;
using System.Net;

namespace Project.DataAccessLayer.Services
{
    public class LisenceSecurityService
    {
        private LisenceSecurityRepository _LisenceSecurityRepository = new LisenceSecurityRepository();


        public ResponseViewModel Add(LisenceKeyViewModel _LisenceKeyViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SECURITY _JYA_SCRTY = new DB_SECURITY();
                _JYA_SCRTY.LisenceKey = _LisenceKeyViewModel.Key;

                _LisenceSecurityRepository.Add(_JYA_SCRTY);
                _LisenceSecurityRepository.Commit();


                response.Status = HttpStatusCode.OK;
                response.Message = "Key Added";
                response.LogLevel = EnumLogLevel.Information;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }

        }


        public ResponseViewModel IsLisenced()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                string securirtyKey = LisenceSecurityUtility.GetEncryptedKey();
                DB_SECURITY _JYA_SCRTY = _LisenceSecurityRepository.Get(p => p.LisenceKey.Equals(securirtyKey));
                if (_JYA_SCRTY == null)
                {
                    response.Status = HttpStatusCode.BadRequest;
                    response.LogLevel = EnumLogLevel.Information;
                    response.obj = false;
                    return response;
                }
                response.Status = HttpStatusCode.OK;
                response.LogLevel = EnumLogLevel.Information;
                response.obj = true;

                return response;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                response.obj = false;
                return response;
            }

        }
    }
}
