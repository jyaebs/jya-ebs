﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Mapper.Mappers;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;

namespace Project.DataAccessLayer.Services
{
    public class CustomerService : IBaseService<CustomerViewModel>
    {
        private CustomerRepository _CustomerRepository = new CustomerRepository();

        public ResponseViewModel Add(CustomerViewModel Customer)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER db_customer = _CustomerRepository.Get(p => p.Code == Customer.Code);
                if (db_customer != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                db_customer = CustomerMapper.DBMapper(db_customer, Customer);

                _CustomerRepository.Add(db_customer);
                _CustomerRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Customer has been Created.";
                response.LogLevel = EnumLogLevel.Information;

                return response;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Update(CustomerViewModel Customer)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER db_customer = _CustomerRepository.Get(p => p.Id == Customer.Id);
                if (db_customer == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_customer = CustomerMapper.DBMapper(db_customer, Customer);

                _CustomerRepository.Update(db_customer);
                _CustomerRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Customer Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Activate(CustomerViewModel Customer)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER _dbvalue = _CustomerRepository.Get(p => p.Id == Customer.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = true;
                _dbvalue.UpdatedBy = Customer.ChangedBy;
                _dbvalue.UpdatedOn = Customer.ChangedOn;

                _CustomerRepository.Update(_dbvalue);
                _CustomerRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Deactivate(CustomerViewModel Customer)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER _dbvalue = _CustomerRepository.Get(p => p.Id == Customer.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = false;
                _dbvalue.UpdatedBy = Customer.ChangedBy;
                _dbvalue.UpdatedOn = Customer.ChangedOn;

                _CustomerRepository.Update(_dbvalue);
                _CustomerRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_CUSTOMER _dbvalue = _CustomerRepository.Get(p => p.Id == id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                CustomerViewModel Customer = CustomerMapper.DBValue(_dbvalue);



                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Customer;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<CustomerViewModel> GetPaginatedRecords(PaginationSearchModel value)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = value.PageStart / value.PageSize;

            //Get the Sorting column
            Func<CustomerViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.CustomerTypeName :
                iSortCol == 3 ? t.CNIC :
                t.Id.ToString()
            );

            List<int> LocationIds = CommonUtility.GetChildLocationsByLocation(value.LocationId).ToList();

            IEnumerable<CustomerViewModel> data = _CustomerRepository.GetMany(t => LocationIds.Contains(t.LocationId)).Select(p => new CustomerViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                CNIC = p.CNIC,
                CustomerTypeName = p.DB_CUSTOMER_TYPE.Name,
                ValidFlag = p.ValidFlag

            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(value.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(value.Search.ToUpper())
                   || x.Name.ToUpper().Contains(value.Search.ToUpper())
                      || x.CustomerTypeName.ToUpper().Contains(value.Search.ToUpper())
                        || x.CNIC.ToUpper().Contains(value.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (value.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, value.sorting))
                .Skip(pageNo * value.PageSize)
                .Take(value.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, value.sorting))
                .Skip(pageNo * value.PageSize)
                .Take(value.PageSize);
            }


            PaginatedRecordsModel<CustomerViewModel> dataObject = new PaginatedRecordsModel<CustomerViewModel>();
            dataObject.draw = value.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }


        public ResponseViewModel GetActiveCustomer(int LocationId)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<CustomerViewModel> country = _CustomerRepository.GetMany(p => p.ValidFlag == true && p.LocationId == LocationId )
                 .Select(p => new CustomerViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = country;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}