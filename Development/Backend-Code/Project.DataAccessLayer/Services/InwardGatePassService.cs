﻿using Project.DataAccessLayer.BaseService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.DatabaseModels;
using System.Net;
using Project.ViewModels.Enums;
using Project.Mapper.Mappers;
using Project.Models.Enums;
using Project.Utilities.Utilities;

namespace Project.DataAccessLayer.Services
{
    public class InwardGatePassService : IBaseService<InwardGatePassViewModel>
    {
        private InwardGatePassRepository _InwardGatePassRepository = new InwardGatePassRepository();
        private PurchaseOrderRepository _PurchaseOrderRepository = new PurchaseOrderRepository();


        public ResponseViewModel Activate(InwardGatePassViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(InwardGatePassViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_INWARD_GATE_PASS dbValue = InwardGatePassMapper.DBMapper(value);
                dbValue.ReferenceNumber = ReferenceNumberUtility.GetInwardGatePassNumber(value.LocationId);
                
                DB_PURCHASE_ORDER db_purchase_order = _PurchaseOrderRepository.Get(x => x.Id == value.PurchaseOrderId);

                if (db_purchase_order != null)
                {

                    bool orderReceived = false;
                    foreach (var data in value.InwardGatePassDetails)
                    {
                        if (data.ReceivedQuantity < data.TotalQuantity)
                        {
                            orderReceived = false;
                        }
                        else if (data.ReceivedQuantity == data.TotalQuantity)
                        {
                            orderReceived = true;
                        }
                    }
                    if (orderReceived)
                    {
                        db_purchase_order.StatusId = (int)EnumPurchaseOrder.FullyReceived;
                    }
                    else
                    {
                        db_purchase_order.StatusId = (int)EnumPurchaseOrder.PartialReceived;
                    }

                    _PurchaseOrderRepository.Update(db_purchase_order);
                    
                }
                _InwardGatePassRepository.Add(dbValue);
                _InwardGatePassRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Inward Gate Pass Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(InwardGatePassViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_INWARD_GATE_PASS _dbvalue = _InwardGatePassRepository.Get(p => p.Id == id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                InwardGatePassViewModel inward_gate_pass = InwardGatePassMapper.ValueMapper(_dbvalue);
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = inward_gate_pass;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<InwardGatePassViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<InwardGatePassViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.ReferenceNumber :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.VendorName :
                iSortCol == 3 ? t.VoucherNo :
                iSortCol == 4 ? t.InwardDateString :
                iSortCol == 6 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<InwardGatePassViewModel> data = _InwardGatePassRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumInwardGatePass.Open).Select(p => new InwardGatePassViewModel()
            {
                ReferenceNumber = p.ReferenceNumber,
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                LocationId = p.LocationId,
                VoucherNo = p.VoucherNo,
                VendorName = p.DB_VENDOR.Name,
                VendorId = p.VendorId,
                InwardDateString = p.InwardDate.Month + @"/" + p.InwardDate.Day + @"/" + p.InwardDate.Year,
                InwardDate = p.InwardDate,
                ValidFlag = p.ValidFlag,
                NetAmount = (int)p.NetAmount,
            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {
                data = data.Where(x => x.ReferenceNumber.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                || x.VendorName.ToUpper().Contains(model.Search.ToUpper())
                || x.VoucherNo.ToUpper().Contains(model.Search.ToUpper())
                || x.InwardDateString.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<InwardGatePassViewModel> dataObject = new PaginatedRecordsModel<InwardGatePassViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(InwardGatePassViewModel value)
        {
            throw new NotImplementedException();
        }
    }
}
