﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class GSTService : IBaseService<GSTViewModel>
    {
        private GSTRepository _GSTRepository = new GSTRepository();
        public ResponseViewModel Activate(GSTViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_GST dbValue = _GSTRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _GSTRepository.Update(dbValue);
                _GSTRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }


        public ResponseViewModel Add(GSTViewModel model)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_GST dbValue = _GSTRepository.Get(x => x.Value == model.Value);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Value Already Exist";
                    response.Message = "Value Already Exist";
                    return response;
                }


                dbValue = GSTMapper.DBMapper(dbValue, model);

                _GSTRepository.Add(dbValue);
                _GSTRepository.Commit();
                response.Status = HttpStatusCode.Created;
                response.Message = "General Sales Tax Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }


        }

        public ResponseViewModel Deactivate(GSTViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_GST dbValue = _GSTRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _GSTRepository.Update(dbValue);
                _GSTRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_GST dbValue = _GSTRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                GSTViewModel GST = GSTMapper.GValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = GST;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<GSTViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<GSTViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Value.ToString() :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<GSTViewModel> data = _GSTRepository.GetAll().Select(p => new GSTViewModel()
            {
                Id = p.Id,
                Name = p.Name,
                Value = p.Value,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<GSTViewModel> dataObject = new PaginatedRecordsModel<GSTViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(GSTViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_GST dbGST = _GSTRepository.Get(p => p.Id == value.Id);
                if (dbGST == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                dbGST = GSTMapper.DBMapper(dbGST, value);

                _GSTRepository.Update(dbGST);
                _GSTRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "General-Sales-TAX Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }


        public ResponseViewModel GetGSTById()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<GSTViewModel> GST = _GSTRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new GSTViewModel()
                 {
                     Id = p.Id,
                     Name =p.Name,
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = GST;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }

}
