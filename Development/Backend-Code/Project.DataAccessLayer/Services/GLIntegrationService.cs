﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;


namespace Project.DataAccessLayer.Services
{
    public class GLIntegrationService : IBaseService<GLIntegrationViewModel>
    {
        private GLAccountIntegrationRepository _GLIntegrationRepository = new GLAccountIntegrationRepository();

        public ResponseViewModel Activate(GLIntegrationViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(GLIntegrationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                foreach (var account in value.GLIntegrationAccountCode)
                {
                    DB_GL_ACCOUNT_INTEGRATION db_GL_Integration = _GLIntegrationRepository.Get(x => x.FiscalYearId == account.FiscalYearId && x.FieldId == account.FieldId);

                    if (db_GL_Integration == null)
                    {
                        if (string.IsNullOrEmpty(account.AccountCode))
                        {
                            continue;
                        }
                        db_GL_Integration = new DB_GL_ACCOUNT_INTEGRATION();

                        db_GL_Integration.FiscalYearId = account.FiscalYearId;
                        db_GL_Integration.FieldId = account.FieldId;
                        db_GL_Integration.AccountCode = account.AccountCode;


                        _GLIntegrationRepository.Add(db_GL_Integration);
                    }

                    else
                    {
                        db_GL_Integration.AccountCode = account.AccountCode;
                        _GLIntegrationRepository.Update(db_GL_Integration);
                    }

                }

                _GLIntegrationRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "GL Account Integration Saved Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(GLIntegrationViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel GetByFiscalYear(int fiscalYear)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                List<DB_GL_ACCOUNT_INTEGRATION> _dbvalue = _GLIntegrationRepository.GetMany(p => p.FiscalYearId == fiscalYear).ToList();
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                

                List<GLIntegrationDetailViewModel> IntegrationDetail = new List<GLIntegrationDetailViewModel>();


                foreach (var detail in _dbvalue)
                {
                    GLIntegrationDetailViewModel Gl_Integration = new GLIntegrationDetailViewModel();

                    Gl_Integration.FieldId = detail.FieldId;
                    Gl_Integration.AccountCode = detail.AccountCode;
                    Gl_Integration.AccountCodeDescription = detail.DB_CHART_OF_ACCOUNT.Name;

                    IntegrationDetail.Add(Gl_Integration);
                }

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = IntegrationDetail;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<GLIntegrationViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Update(GLIntegrationViewModel value)
        {
            throw new NotImplementedException();
        }
    }
}
