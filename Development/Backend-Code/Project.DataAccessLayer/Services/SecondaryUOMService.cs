﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class SecondaryUOMService : IBaseService<SecondaryUOMViewModel>
    {
        private SecondaryUOMRepository _SecondaryUOMRepository = new SecondaryUOMRepository();
        public ResponseViewModel Activate(SecondaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SECONDARY_UOM dbValue = _SecondaryUOMRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _SecondaryUOMRepository.Update(dbValue);
                _SecondaryUOMRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(SecondaryUOMViewModel value)
        {
             ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_SECONDARY_UOM dbValue = _SecondaryUOMRepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }
                dbValue = SecondaryUOMMapper.DBMapper(dbValue, value);


                _SecondaryUOMRepository.Add(dbValue);
                _SecondaryUOMRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Secondary UOM Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(SecondaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_SECONDARY_UOM dbValue = _SecondaryUOMRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _SecondaryUOMRepository.Update(dbValue);
                _SecondaryUOMRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SECONDARY_UOM dbValue = _SecondaryUOMRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                SecondaryUOMViewModel SecondaryUOM = SecondaryUOMMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = SecondaryUOM;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<SecondaryUOMViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<SecondaryUOMViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.PrimaryUOMName :
                iSortCol == 3 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<SecondaryUOMViewModel> data = _SecondaryUOMRepository.GetAll().Select(p => new SecondaryUOMViewModel()
            {
                PrimaryUOMName = p.DB_PRIMARY_UOM.Name,
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<SecondaryUOMViewModel> dataObject = new PaginatedRecordsModel<SecondaryUOMViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(SecondaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_SECONDARY_UOM db_SECONDARY_UOM = _SecondaryUOMRepository.Get(p => p.Id == value.Id);
                if (db_SECONDARY_UOM == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_SECONDARY_UOM = SecondaryUOMMapper.DBMapper(db_SECONDARY_UOM, value);

                _SecondaryUOMRepository.Update(db_SECONDARY_UOM);
                _SecondaryUOMRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Secondary UOM Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
        public ResponseViewModel GetAllActiveSecondaryUOM(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<SecondaryUOMViewModel> SecondaryUOM = _SecondaryUOMRepository.GetMany(p => p.ValidFlag == true && p.PrimaryUOMId == id)
                 .Select(p => new SecondaryUOMViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = SecondaryUOM;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
