﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;
using Project.Models.Enums;
using Project.Utilities.Utilities;

namespace Project.DataAccessLayer.Services
{
    public class PurchaseQuotationService : IBaseService<PurchaseQuotationViewModel>
    {
        private PurchaseQuotationRepository _PurchaseQuotationRepository = new PurchaseQuotationRepository();

        public ResponseViewModel Activate(PurchaseQuotationViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(PurchaseQuotationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                PurchaseRequisitionRepository _PurchaseRequisitionRepository = new PurchaseRequisitionRepository();

                DB_PURCHASE_REQUISITION dB_PURCHASE_Requisition = _PurchaseRequisitionRepository.Get(x => x.Id == value.PurchaseRequisitionId);
                dB_PURCHASE_Requisition.StatusId =(int) EnumPurchaseRequisition.QuotationCreated;
                dB_PURCHASE_Requisition.UpdatedBy =value.ChangedBy;
                dB_PURCHASE_Requisition.UpdatedOn =value.ChangedOn;

                DB_PURCHASE_QUOTATION dbValue = PurchaseQuotationMapper.DBMapper(value);
                dbValue.ReferenceNumber = ReferenceNumberUtility.GetPurchaseQuotationNumber(value.LocationId);

                _PurchaseQuotationRepository.Add(dbValue);
                _PurchaseRequisitionRepository.Update(dB_PURCHASE_Requisition);

                _PurchaseQuotationRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Purchase Quotation Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(PurchaseQuotationViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel CancelById(PurchaseQuotationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PURCHASE_QUOTATION dbValue = _PurchaseQuotationRepository.Get(p => p.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                dbValue.ValidFlag = false;
                dbValue.StatusId = (int)EnumPurchaseQuotation.Cancelled;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _PurchaseQuotationRepository.Update(dbValue);
                _PurchaseQuotationRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Cancelled";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PURCHASE_QUOTATION _dbvalue = _PurchaseQuotationRepository.Get(p => p.Id == id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                PurchaseQuotationViewModel purchase_quotation = PurchaseQuotationMapper.ValueMapper(_dbvalue);
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = purchase_quotation;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<PurchaseQuotationViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<PurchaseQuotationViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.ReferenceNumber.ToString() :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.PurchaseTypeName :
                iSortCol == 3 ? t.PriorityLevelName :
                t.Id.ToString()
            );
            IEnumerable<PurchaseQuotationViewModel> data = _PurchaseQuotationRepository.GetMany(x => x.ValidFlag == true
           && x.StatusId == (int)EnumPurchaseQuotation.Open
            ).Select(p => new PurchaseQuotationViewModel()
            {
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                LocationId = p.LocationId,
                ReferenceNumber = p.ReferenceNumber,
                PriorityLevel = p.PriorityLevel,
                PriorityLevelName = p.PriorityLevel == "H" ? "High" : p.PriorityLevel == "M" ? "Medium" : "Low",
                PurchaseTypeId = p.PurchaseTypeId,
                PurchaseTypeName = p.DB_DEV_PURCHASE_TYPE.Name,
                Remarks = p.Remarks,
                StatusDescription = p.DB_DEV_PURCHASE_QUOTATION_STATUS.Name,
                ValidFlag = p.ValidFlag,
                VendorId = p.VendorId,
                VendorName = p.DB_VENDOR.Name

            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<PurchaseQuotationViewModel> dataObject = new PaginatedRecordsModel<PurchaseQuotationViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(PurchaseQuotationViewModel value)
        {
            throw new NotImplementedException();
        }
    }
}
