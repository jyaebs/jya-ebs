﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels;
using Project.Mapper.Mappers;
using Project.DatabaseModels.Repository;

namespace Project.DataAccessLayer.Services
{
    public class ProductPriceService : IBaseService<ProductPriceViewModel>
    {
        private ProductPriceRepository _ProductPriceRepository = new ProductPriceRepository();
        private ProductPriceHistoryRepository _ProductPriceHistoryRepository = new ProductPriceHistoryRepository();
        public ResponseViewModel Activate(ProductPriceViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT_PRICE dbValue = _ProductPriceRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductPriceRepository.Update(dbValue);
                _ProductPriceRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(ProductPriceViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_PRICE dbValue = _ProductPriceRepository.Get(x => x.ProductId == value.ProductId);
                if (dbValue != null)
                {

                    DB_PRODUCT_PRICE_HISTORY db_Value_History = ProductPriceHistroryMapper.DBMapper(dbValue, value);
                    _ProductPriceHistoryRepository.Add(db_Value_History);

                    dbValue = ProductPriceMapper.DBMapper(dbValue, value);
                    _ProductPriceRepository.Update(dbValue);
                }
                else
                {
                    dbValue = ProductPriceMapper.DBMapper(dbValue, value);
                    _ProductPriceRepository.Add(dbValue);
                }

                _ProductPriceRepository.Commit();
                _ProductPriceHistoryRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Product Price Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(ProductPriceViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_PRICE dbValue = _ProductPriceRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {


                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                DB_PRODUCT_PRICE_HISTORY db_Value_History = ProductPriceHistroryMapper.DBMapper(dbValue, value);
                _ProductPriceHistoryRepository.Add(db_Value_History);
             
                _ProductPriceRepository.Update(dbValue);
                _ProductPriceRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_PRICE dbValue = _ProductPriceRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                ProductPriceViewModel ProductPrice = ProductPriceMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductPrice;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<ProductPriceViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ProductPriceViewModel, Int32, string> getColName = (
                (t, iSortCol) =>

                iSortCol == 0 ? t.LocationName :
                iSortCol == 1 ? t.ProductName :
                iSortCol == 2 ? t.ProductPriceTypeName:
                iSortCol == 3 ? t.Price.ToString() :
                t.Price.ToString()


            );
            IEnumerable<ProductPriceViewModel> data = _ProductPriceRepository.GetAll().Select(p => new ProductPriceViewModel()
            {
                Id=p.Id,
                LocationName = p.DB_LOCATION.Name,
                ProductName = p.DB_PRODUCT.Name,
                ProductPriceTypeName = p.DB_DEV_PRODUCT_PRICE_TYPE.Name.ToString(),
                Price = p.Price,
                ValidFlag=p.ValidFlag,
               
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ProductPriceViewModel> dataObject = new PaginatedRecordsModel<ProductPriceViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        
    }
        public ResponseViewModel Update(ProductPriceViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_PRICE db_DB_PRODUCT_PRICE = _ProductPriceRepository.Get(p => p.Id == value.Id);
                if (db_DB_PRODUCT_PRICE == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_DB_PRODUCT_PRICE = ProductPriceMapper.DBMapper(db_DB_PRODUCT_PRICE, value);

                _ProductPriceRepository.Update(db_DB_PRODUCT_PRICE);
                _ProductPriceRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Product Price Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
    }
}    
       