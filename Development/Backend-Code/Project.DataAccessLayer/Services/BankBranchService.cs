﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class BankBranchService : IBaseService<BankBranchViewModel>
    {
        private BankBranchRepository _BankBranchRepository = new BankBranchRepository();
        public ResponseViewModel Activate(BankBranchViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_BANK_BRANCH dbValue = _BankBranchRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _BankBranchRepository.Update(dbValue);
                _BankBranchRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(BankBranchViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_BANK_BRANCH dbValue = _BankBranchRepository.Get(x => x.AccountNumber == value.AccountNumber);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Account Number Already Exist";
                    response.Message = "Account Number Already Exist";
                    return response;
                }
                dbValue = BankBranchMapper.DBMapper(dbValue, value);


                _BankBranchRepository.Add(dbValue);
                _BankBranchRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Bank Branch Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(BankBranchViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_BANK_BRANCH dbValue = _BankBranchRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _BankBranchRepository.Update(dbValue);
                _BankBranchRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_BANK_BRANCH dbValue = _BankBranchRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                BankBranchViewModel BankBranch = BankBranchMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = BankBranch;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<BankBranchViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<BankBranchViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.BankName:
                iSortCol == 3 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<BankBranchViewModel> data = _BankBranchRepository.GetAll().Select(p => new BankBranchViewModel()
            {
                BankName = p.DB_BANK.Name,
                Id = p.Id,
                Code = p.AccountNumber,
                Name = p.AccountName,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<BankBranchViewModel> dataObject = new PaginatedRecordsModel<BankBranchViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        
        }

        public ResponseViewModel Update(BankBranchViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_BANK_BRANCH db_BANK_BRANCH = _BankBranchRepository.Get(p => p.Id == value.Id);
                if (db_BANK_BRANCH == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_BANK_BRANCH = BankBranchMapper.DBMapper(db_BANK_BRANCH, value);

                _BankBranchRepository.Update(db_BANK_BRANCH);
                _BankBranchRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Bank Branch Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
    }
}