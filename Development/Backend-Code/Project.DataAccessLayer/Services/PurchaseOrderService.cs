﻿using Project.DataAccessLayer.BaseService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.DatabaseModels;
using System.Net;
using Project.ViewModels.Enums;
using Project.Mapper.Mappers;
using Project.Models.Enums;
using Project.Utilities.Utilities;

namespace Project.DataAccessLayer.Services
{
    public class PurchaseOrderService : IBaseService<PurchaseOrderViewModel>
    {
        private PurchaseOrderRepository _PurchaseOrderRepository = new PurchaseOrderRepository();
        
        public ResponseViewModel Activate(PurchaseOrderViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(PurchaseOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                PurchaseQuotationRepository _PurchaseQuotationRepository = new PurchaseQuotationRepository();

                DB_PURCHASE_ORDER dbValue = _PurchaseOrderRepository.Get(x => x.VoucherNo == value.VoucherNo);

                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Voucher No. Already Exist";
                    response.Message = "Voucher No. Already Exist";
                    return response;
                }
                if (value.PurchaseQuotationId != 0)
                {
                    DB_PURCHASE_QUOTATION Db_Purchase_Quotation = _PurchaseQuotationRepository.Get(x => x.Id == value.PurchaseQuotationId);
                    Db_Purchase_Quotation.StatusId = (int)EnumPurchaseQuotation.OrderCreated;
                    Db_Purchase_Quotation.UpdatedBy = value.ChangedBy;
                    Db_Purchase_Quotation.UpdatedOn = value.ChangedOn;
                    _PurchaseQuotationRepository.Update(Db_Purchase_Quotation);
                }

                dbValue = PurchaseOrderMapper.DBMapper(value);
                dbValue.ReferenceNumber = ReferenceNumberUtility.GetPurchaseOrderNumber(value.LocationId);


                _PurchaseOrderRepository.Add(dbValue);
                _PurchaseOrderRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = " Purchase Order Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }

        }

        public ResponseViewModel Deactivate(PurchaseOrderViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel CancelById(PurchaseOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PURCHASE_ORDER dbValue = _PurchaseOrderRepository.Get(p => p.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.StatusId = (int)EnumPurchaseOrder.Cancelled;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _PurchaseOrderRepository.Update(dbValue);
                _PurchaseOrderRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Cancelled";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PURCHASE_ORDER dbValue = _PurchaseOrderRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                PurchaseOrderViewModel PurchaseOrder = PurchaseOrderMapper.ValueMapper(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = PurchaseOrder;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<PurchaseOrderViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<PurchaseOrderViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.PurchaseReferenceNumber :
                iSortCol == 1 ? t.LocationId.ToString() :
                iSortCol == 2 ? t.VendorName :
                iSortCol == 3 ? t.VoucherNo :
                iSortCol == 4 ? t.OrderDateString :
                iSortCol == 6 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<PurchaseOrderViewModel> data = _PurchaseOrderRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumPurchaseOrder.Open).Select(p => new PurchaseOrderViewModel()
            {
                PurchaseReferenceNumber = p.ReferenceNumber,
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                LocationId = p.LocationId,
                VoucherNo = p.VoucherNo,
                VendorName = p.DB_VENDOR.Name,
                VendorId = p.VendorId,
                OrderDateString = p.OrderDate.ToString("dd/MM/yyyy"),
                OrderDate = p.OrderDate,
                ValidFlag = p.ValidFlag,
                NetAmount = (int)p.NetAmount,
            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {
                data = data.Where(x => x.PurchaseReferenceNumber.ToUpper().Contains(model.Search.ToUpper())
                || x.LocationId.ToString().ToUpper().Contains(model.Search.ToUpper())
                || x.VendorName.ToUpper().Contains(model.Search.ToUpper())
                || x.VoucherNo.ToUpper().Contains(model.Search.ToUpper())
                || x.OrderDateString.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<PurchaseOrderViewModel> dataObject = new PaginatedRecordsModel<PurchaseOrderViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }

        public PaginatedRecordsModel<PurchaseOrderViewModel> ViewAllPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<PurchaseOrderViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.VendorName :
                iSortCol == 2 ? t.VoucherNo :
                 iSortCol == 3 ? t.OrderDateString :
                iSortCol == 4 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<PurchaseOrderViewModel> data = _PurchaseOrderRepository.GetMany(x => x.ValidFlag == true).Select(p => new PurchaseOrderViewModel()
            {
                PurchaseReferenceNumber = p.ReferenceNumber,
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                VoucherNo = p.VoucherNo,
                VendorName = p.DB_VENDOR.Name,
                OrderDateString = p.OrderDate.Month + @"/" + p.OrderDate.Day + @"/" + p.OrderDate.Year,
                ValidFlag = p.ValidFlag,
                StatusDescription = p.DB_DEV_PURCHASE_ORDER_STATUS.Name,

            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Name.ToUpper().Contains(model.Search.ToUpper())
                || x.VoucherNo.ToUpper().Contains(model.Search.ToUpper())
                || x.VendorName.ToUpper().Contains(model.Search.ToUpper())
                || x.OrderDateString.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<PurchaseOrderViewModel> dataObject = new PaginatedRecordsModel<PurchaseOrderViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }
        
        public ResponseViewModel Update(PurchaseOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                int? parentId = null;
                DB_PURCHASE_ORDER old_dbValue = _PurchaseOrderRepository.Get(p => p.Id == value.Id);
                if (old_dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                else
                {
                    parentId = old_dbValue.Id;
                    old_dbValue.ValidFlag = false;
                    old_dbValue.UpdatedBy = value.ChangedBy;
                    old_dbValue.UpdatedOn = value.ChangedOn;
                    _PurchaseOrderRepository.Update(old_dbValue);
                }

                DB_PURCHASE_ORDER _dbValue = PurchaseOrderMapper.DBMapper(value);
                _dbValue.ReferenceNumber = ReferenceNumberUtility.GetPurchaseOrderNumber(value.LocationId);
                _dbValue.ParentId = parentId;

                _PurchaseOrderRepository.Add(_dbValue);
                _PurchaseOrderRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Purchase Order Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

    }
}