﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Mapper.Mappers;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;

namespace Project.DataAccessLayer.Services
{
    public class VendorService : IBaseService<VendorViewModel>
    {
        private VendorRepository _VendorRepository = new VendorRepository();

        public ResponseViewModel Add(VendorViewModel Vendor)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VENDOR db_vendor = _VendorRepository.Get(p => p.Code == Vendor.Code);
                if (db_vendor != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                db_vendor = VendorMapper.DBMapper(db_vendor, Vendor);

                _VendorRepository.Add(db_vendor);
                _VendorRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Vendor has been Created.";
                response.LogLevel = EnumLogLevel.Information;

                return response;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Update(VendorViewModel Vendor)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VENDOR db_vendor = _VendorRepository.Get(p => p.Id == Vendor.Id);
                if (db_vendor == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_vendor = VendorMapper.DBMapper(db_vendor, Vendor);

                _VendorRepository.Update(db_vendor);
                _VendorRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Vendor Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Activate(VendorViewModel Vendor)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VENDOR _dbvalue = _VendorRepository.Get(p => p.Id == Vendor.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = true;
                _dbvalue.UpdatedBy = Vendor.ChangedBy;
                _dbvalue.UpdatedOn = Vendor.ChangedOn;

                _VendorRepository.Update(_dbvalue);
                _VendorRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Deactivate(VendorViewModel Vendor)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VENDOR _dbvalue = _VendorRepository.Get(p => p.Id == Vendor.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = false;
                _dbvalue.UpdatedBy = Vendor.ChangedBy;
                _dbvalue.UpdatedOn = Vendor.ChangedOn;

                _VendorRepository.Update(_dbvalue);
                _VendorRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_VENDOR _dbvalue = _VendorRepository.Get(p => p.Id == id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                VendorViewModel Vendor = VendorMapper.DBValue(_dbvalue);



                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Vendor;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<VendorViewModel> GetPaginatedRecords(PaginationSearchModel value)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = value.PageStart / value.PageSize;

            //Get the Sorting column
            Func<VendorViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.VendorTypeName :
                iSortCol == 3 ? t.CNIC :
                t.Id.ToString()
            );

            List<int> LocationIds = CommonUtility.GetChildLocationsByLocation(value.LocationId).ToList();

            IEnumerable<VendorViewModel> data = _VendorRepository.GetMany(t => LocationIds.Contains(t.LocationId)).Select(p => new VendorViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                CNIC = p.CNIC,
                VendorTypeName = p.DB_VENDOR_TYPE.Name,
                ValidFlag = p.ValidFlag

            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(value.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(value.Search.ToUpper())
                   || x.Name.ToUpper().Contains(value.Search.ToUpper())
                      || x.VendorTypeName.ToUpper().Contains(value.Search.ToUpper())
                        || x.CNIC.ToUpper().Contains(value.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (value.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, value.sorting))
                .Skip(pageNo * value.PageSize)
                .Take(value.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, value.sorting))
                .Skip(pageNo * value.PageSize)
                .Take(value.PageSize);
            }


            PaginatedRecordsModel<VendorViewModel> dataObject = new PaginatedRecordsModel<VendorViewModel>();
            dataObject.draw = value.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }


        public ResponseViewModel GetActiveVendor(int LocationId)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<VendorViewModel> vendor = _VendorRepository.GetMany(p => p.ValidFlag == true && p.LocationId == LocationId)
                 .Select(p => new VendorViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = vendor;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

    }
}