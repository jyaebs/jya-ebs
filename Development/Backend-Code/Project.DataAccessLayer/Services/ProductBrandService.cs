﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels;
using Project.Mapper.Mappers;
using Project.DatabaseModels.Repository;

namespace Project.DataAccessLayer.Services
{
    public class ProductBrandService : IBaseService<ProductBrandViewModel>
    {
        private  ProductBrandRepository _ProductBrandRepository = new ProductBrandRepository();

        public ResponseViewModel Activate(ProductBrandViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT_BRAND dbValue = _ProductBrandRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductBrandRepository.Update(dbValue);
                _ProductBrandRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(ProductBrandViewModel model)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_BRAND dbValue = _ProductBrandRepository.Get(x => x.Code == model.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }
                dbValue = ProductBrandMapper.DBMapper(dbValue, model);


                _ProductBrandRepository.Add(dbValue);
                _ProductBrandRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Product Brand Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(ProductBrandViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_BRAND dbValue = _ProductBrandRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _ProductBrandRepository.Update(dbValue);
                _ProductBrandRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRODUCT_BRAND dbValue = _ProductBrandRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                ProductBrandViewModel ProductBrand = ProductBrandMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductBrand;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<ProductBrandViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<ProductBrandViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<ProductBrandViewModel> data = _ProductBrandRepository.GetAll().Select(p => new ProductBrandViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<ProductBrandViewModel> dataObject = new PaginatedRecordsModel<ProductBrandViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(ProductBrandViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRODUCT_BRAND db_PRODUCT_CATEGORY = _ProductBrandRepository.Get(p => p.Id == value.Id);
                if (db_PRODUCT_CATEGORY == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_PRODUCT_CATEGORY = ProductBrandMapper.DBMapper(db_PRODUCT_CATEGORY, value);

                _ProductBrandRepository.Update(db_PRODUCT_CATEGORY);
                _ProductBrandRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Product Brand Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
        public ResponseViewModel GetAllActiveProductBrand()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductBrandViewModel> ProductBrand = _ProductBrandRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ProductBrandViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductBrand;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
