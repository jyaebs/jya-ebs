﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class VendorTypeService : IBaseService<VendorTypeViewModel>
    {
        private VendorTypeRepository _VendorTypeRepository = new VendorTypeRepository();
        public ResponseViewModel Activate(VendorTypeViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_VENDOR_TYPE dbValue = _VendorTypeRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _VendorTypeRepository.Update(dbValue);
                _VendorTypeRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(VendorTypeViewModel model)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VENDOR_TYPE dbValue = _VendorTypeRepository.Get(x => x.Code == model.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                dbValue = VendorTypeMapper.DBMapper(dbValue, model);



                _VendorTypeRepository.Add(dbValue);
                _VendorTypeRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Vendor Type Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(VendorTypeViewModel model)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_VENDOR_TYPE dbValue = _VendorTypeRepository.Get(x => x.Code == model.Code);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue = VendorTypeMapper.DBMapper(dbValue, model);

                _VendorTypeRepository.Update(dbValue);
                _VendorTypeRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_VENDOR_TYPE dbValue = _VendorTypeRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                VendorTypeViewModel VendorType = VendorTypeMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = VendorType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<VendorTypeViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<VendorTypeViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<VendorTypeViewModel> data = _VendorTypeRepository.GetAll().Select(p => new VendorTypeViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<VendorTypeViewModel> dataObject = new PaginatedRecordsModel<VendorTypeViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(VendorTypeViewModel value)
        {
                ResponseViewModel response = new ResponseViewModel();
                try
                {
                DB_VENDOR_TYPE db_VENDOR_TYPE = _VendorTypeRepository.Get(p => p.Id == value.Id);
                    if (db_VENDOR_TYPE == null)
                    {
                        response.LogLevel = EnumLogLevel.Warning;
                        response.Status = HttpStatusCode.BadRequest;
                        response.obj = "Not Exist";
                        response.Message = "Not Exist";
                        return response;
                    }


                    db_VENDOR_TYPE = VendorTypeMapper.DBMapper(db_VENDOR_TYPE, value);

                _VendorTypeRepository.Update(db_VENDOR_TYPE);
                _VendorTypeRepository.Commit();

                    response.Status = HttpStatusCode.OK;
                    response.Message = "Vendor Type Updated";
                    response.LogLevel = EnumLogLevel.Information;

                    return response;

                }
                catch (Exception ex)
                {
                    response.Status = HttpStatusCode.InternalServerError;
                    response.Message = ex.Message;
                    response.LogLevel = EnumLogLevel.Error;
                    return response;
                }
            
        }

        //Get Active Vendors
        public ResponseViewModel GetAllVendorType()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<VendorTypeViewModel> VendorTypeViewModel = _VendorTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new VendorTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = VendorTypeViewModel;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }

}
