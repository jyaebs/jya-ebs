﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.Mapper.Mappers;
using System.Net;
using Project.ViewModels.Enums;
using Project.Models.Enums;
using Project.Utilities.Utilities;
using Project.Utilities;

namespace Project.DataAccessLayer.Services
{
    public class DeliveryNoteService : IBaseService<DeliveryNoteViewModel>
    {
        private ProductRepository _ProductRepository = new ProductRepository();
        private GeneralLedgerRepository _GeneralLedgerRepository = new GeneralLedgerRepository();
        private GeneralLedgerDetailRepository _GeneralLedgerDetailRepository = new GeneralLedgerDetailRepository();
        private DeliveryNoteRepository _DeliveryNoteRepository = new DeliveryNoteRepository();
        private StockRepository _StockRepository = new StockRepository();
        private SaleOrderRepository _SaleOrderRepository = new SaleOrderRepository();
        private DeliveryNoteDetailRepository _DeliveryNoteDetailRepository = new DeliveryNoteDetailRepository();
        private SaleOrderDetailRepository _SaleOrderDetailRepository = new SaleOrderDetailRepository();
        public ResponseViewModel Activate(DeliveryNoteViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(DeliveryNoteViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                bool IsInventoryAvailable = InventoryUtility.CheckInventory(value, out string message);
                if (!IsInventoryAvailable)
                {
                    response.Status = HttpStatusCode.InternalServerError;
                    response.Message = message;
                    response.LogLevel = EnumLogLevel.Error;
                    return response;
                }

                DB_DELIVERY_NOTE dbValue = DeliveryNoteMapper.DBMapper(null, value);

              
                foreach (var item in dbValue.DB_DELIVERY_NOTE_DETAIL)
                {
                    IEnumerable<DB_STOCK> db_stock_list = _StockRepository.GetMany(x => x.ProductId == item.ProductId && x.WarehouseId == item.WarehouseId);

                    decimal DeliverQuantity = item.TotalQuantity;
                    foreach (var db_stoke in db_stock_list)
                    {
                        if (DeliverQuantity > 0)
                        {
                            if (DeliverQuantity >= db_stoke.Quantity)
                            {
                                DeliverQuantity -= db_stoke.Quantity;

                                DB_STOCK_LEDGER db_STOKE_LEDGER = StockLedgerMapper.DBMapper(db_stoke.Id, db_stoke.LocationId, db_stoke.WarehouseId, db_stoke.ProductId, 2, (-1) * (int)db_stoke.Quantity, dbValue.Id, dbValue.CreatedOn, dbValue.CreatedBy);
                                dbValue.DB_STOCK_LEDGER.Add(db_STOKE_LEDGER);

                                db_stoke.Quantity = 0;
                                db_stoke.ValidFlag = false;
                                _StockRepository.Update(db_stoke);
                            }
                            else if (DeliverQuantity < db_stoke.Quantity)
                            {
                                DB_STOCK_LEDGER db_stock_ledger = StockLedgerMapper.DBMapper(db_stoke.Id, db_stoke.LocationId, db_stoke.WarehouseId,
                                    db_stoke.ProductId, 2, (-1) * (int)DeliverQuantity, dbValue.Id, dbValue.CreatedOn, dbValue.CreatedBy);
                                dbValue.DB_STOCK_LEDGER.Add(db_stock_ledger);

                                db_stoke.Quantity -= DeliverQuantity;
                                DeliverQuantity = 0;

                                if (db_stoke.Quantity == 0)
                                {
                                    db_stoke.ValidFlag = false;
                                }
                                _StockRepository.Update(db_stoke);
                            }
                        }

                        else
                        {
                            break;
                        }
                    }
                    int id = CommonUtility.GetFiscalYearIdbyDate(value.DeliveryDate);

                    foreach (var GL in value.DeliveryNoteDetail)
                    {
                        DB_GENERAL_LEDGER db_GENERAL_LEDGER = GeneralLedgerMapper.DBMapper(value.LocationId, GL.WarehouseId, id, (int)EnumGeneralLedgerType.DeliveryNote, 1, 1, 1, value.Remarks, null, 2, value.CustomerId, value.DeliveryDate, value.ChangedOn, value.ChangedBy);
                        _GeneralLedgerRepository.Add(db_GENERAL_LEDGER);
                        IEnumerable<DB_PRODUCT> Product = _ProductRepository.GetMany(p => p.Id == item.ProductId);
                        foreach (var GLD in Product)
                        {
                            DB_GENERAL_LEDGER_DETAIL _DB_GENERAL_LEDGER_DETAIL = GeneralLedgerDetailMapper.DBMapper(GLD.InventoryAccount, GLD.COGSAccount, 1, 1);
                            db_GENERAL_LEDGER.DB_GENERAL_LEDGER_DETAIL.Add(_DB_GENERAL_LEDGER_DETAIL);
                        }
                        foreach (var GLD in Product)
                        {
                            DB_GENERAL_LEDGER_DETAIL _DB_GENERAL_LEDGER_DETAIL = GeneralLedgerDetailMapper.DBMapper(GLD.COGSAccount, GLD.COGSAccount, 1, 1);
                            db_GENERAL_LEDGER.DB_GENERAL_LEDGER_DETAIL.Add(_DB_GENERAL_LEDGER_DETAIL);
                        }
                    }

                }
                _DeliveryNoteRepository.Add(dbValue);

                DB_DELIVERY_NOTE dbValu = _DeliveryNoteRepository.Get(x => x.Id == value.Id);
                if (dbValu != null)
                {
                    dbValu.ValidFlag = false;
                    _DeliveryNoteRepository.Update(dbValu);
                }
                else
                {
                    _DeliveryNoteRepository.Add(dbValue);
                }
                IEnumerable<DB_SALE_ORDER_DETAIL> detail = _SaleOrderDetailRepository.GetMany(p => p.SaleOrderId == dbValue.SaleOrderId);

                foreach (var p in value.DeliveryNoteDetail)
                    foreach (var SaleDetail in detail)
                    {
                        SaleDetail.DeliveredQuantity = p.DeliverQuantity;
                        _SaleOrderDetailRepository.Update(SaleDetail);
                    }
                foreach (var Status in detail)
                {
                    if (Status.DeliveredQuantity == Status.TotalQuantity)
                    {
                        DB_SALE_ORDER _DB_SALE_ORDER = _SaleOrderRepository.Get(p => p.Id == value.SaleOrderId);
                        _DB_SALE_ORDER.StatusId = (int)EnumSaleOrder.FullyDelivered;
                    }
                    else
                    {
                        DB_SALE_ORDER _DB_SALE_ORDER = _SaleOrderRepository.Get(p => p.Id == value.SaleOrderId);
                        _DB_SALE_ORDER.StatusId = (int)EnumSaleOrder.PartialDelivered;
                    }
                }

                _DeliveryNoteRepository.Commit();


                response.Status = HttpStatusCode.Created;
                response.Message = "Delivery Note Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(DeliveryNoteViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DeliveryNoteViewModel model = new DeliveryNoteViewModel();
                model.DeliveryNoteDetail = new List<DeliveryNoteDetailViewModel>();

                DB_DELIVERY_NOTE dbValue = _DeliveryNoteRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                DeliveryNoteViewModel DeliveryNote = DeliveryNoteMapper.ValueMapper(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = DeliveryNote;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<DeliveryNoteViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<DeliveryNoteViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.CustomerName :
                iSortCol == 2 ? t.DeliveryDate.ToShortDateString() :
                iSortCol == 3 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<DeliveryNoteViewModel> data = _DeliveryNoteRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumDeliveryNote.Open).Select(p => new DeliveryNoteViewModel()
            {
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                CustomerName = p.DB_CUSTOMER.Name,
                orderdatestring = p.DeliveryDate.Year + @"/" + p.DeliveryDate.Month + @"/" + p.DeliveryDate.Day,
                ValidFlag = p.ValidFlag,
                StatusDescription = p.DB_DEV_DELIVERY_NOTE_STATUS.Name,

            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<DeliveryNoteViewModel> dataObject = new PaginatedRecordsModel<DeliveryNoteViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }


        public ResponseViewModel Update(DeliveryNoteViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_DELIVERY_NOTE db_DB_DELIVERY_NOTE = _DeliveryNoteRepository.Get(p => p.Id == value.Id);
                if (db_DB_DELIVERY_NOTE != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_DB_DELIVERY_NOTE = DeliveryNoteMapper.DBMapper(db_DB_DELIVERY_NOTE, value);

                _DeliveryNoteRepository.Update(db_DB_DELIVERY_NOTE);
                _DeliveryNoteRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Delivery Note Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel CancelById(DeliveryNoteViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_DELIVERY_NOTE dbValue = _DeliveryNoteRepository.Get(p => p.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.StatusId = (int)EnumDeliveryNote.InvoiceGenerated;
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _DeliveryNoteRepository.Update(dbValue);
                _DeliveryNoteRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<DeliveryNoteViewModel> GetOpenDeliveryNote(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<DeliveryNoteViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.CustomerName :
                iSortCol == 2 ? t.DeliveryDate.ToShortDateString() :
                iSortCol == 3 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<DeliveryNoteViewModel> data = _DeliveryNoteRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumDeliveryNote.Open).Select(p => new DeliveryNoteViewModel()
            {
                Id = p.Id,

                Name = p.DB_LOCATION.Name,
                CustomerName = p.DB_CUSTOMER.Name,
                orderdatestring = p.DeliveryDate.Year + @"/" + p.DeliveryDate.Month + @"/" + p.DeliveryDate.Day,

            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<DeliveryNoteViewModel> dataObject = new PaginatedRecordsModel<DeliveryNoteViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }

    }
}
