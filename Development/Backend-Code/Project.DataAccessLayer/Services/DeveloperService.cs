﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Project.DataAccessLayer.Services
{
    public class DeveloperService
    {
        private LocationTypeRepository _LocationTypeRepository = new LocationTypeRepository();
        private ChartOfAccountTypeRepository _ChartOfAccountTypeRepository = new ChartOfAccountTypeRepository();
        private ProductTypeRepository _ProductTypeRepository = new ProductTypeRepository();
        private ProductPriceTypeRepository _ProductPriceTypeRepository = new ProductPriceTypeRepository();
        private PurchaseTypeRepository _PurchaseTypeRepository = new PurchaseTypeRepository();
        private FiscalYearRepository _FiscalYearRepository = new FiscalYearRepository();
        private GLAccountIntegrationFieldRepository _GLAccountIntegrationField = new GLAccountIntegrationFieldRepository();


        //Get Active Location Types
        public ResponseViewModel GetAllLocationTypes()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<LocationTypeViewModel> LocationType = _LocationTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new LocationTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = LocationType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetAllActiveProductType()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductTypeViewModel> ProductType = _ProductTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ProductTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductType;

                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetAllActiveProductPriceType()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ProductPriceTypeViewModel> ProductPriceType = _ProductPriceTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ProductPriceTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductPriceType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetAllChartOfAccountypes()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<ChartOfAccountTypeViewModel> ProductType = _ChartOfAccountTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new ChartOfAccountTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductType;

                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetAllActivePurchaseType()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<PurchaseTypeViewModel> ProductPriceType = _PurchaseTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new PurchaseTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = ProductPriceType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetAllActiveFiscalYear()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<FiscalYearViewModel> fiscalYear = _FiscalYearRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new FiscalYearViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = fiscalYear;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }

        public ResponseViewModel GetAllActiveGLIntegrationField()
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<GLIntegrationFieldViewModel> GLIntegration = _GLAccountIntegrationField.GetMany(p => p.ValidFlag == true)
                 .Select(p => new GLIntegrationFieldViewModel()
                 {
                     Id = p.Id,
                     Code = p.Code,
                     TabName = p.TabName,
                     FieldName = p.FieldName
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = GLIntegration;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }
    }
}