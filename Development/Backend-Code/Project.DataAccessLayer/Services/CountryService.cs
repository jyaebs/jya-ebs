﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Project.DataAccessLayer.Services
{
    public class CountryService : IBaseService<CountryViewModel>
    {
        private CountryRepository _CountryDetailsRepository = new CountryRepository();

        public ResponseViewModel Activate(CountryViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_COUNTRY _dbvalue = _CountryDetailsRepository.Get(p => p.Id == value.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = true;
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;

                _CountryDetailsRepository.Update(_dbvalue);
                _CountryDetailsRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(CountryViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_COUNTRY _dbcountry = _CountryDetailsRepository.Get(x => x.DialingCode == value.DialingCode);
                if (_dbcountry != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Dialing Code Already Exist";
                    response.Message = "Dialing Code Already Exist";
                    return response;
                }
                DB_COUNTRY db_country = _CountryDetailsRepository.Get(x=>x.Code == value.Code);
                if(db_country != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                db_country = new DB_COUNTRY();

                db_country.Code = value.Code;
                db_country.DialingCode = value.DialingCode;
                db_country.Name = value.Name;

                db_country.ValidFlag = true;

                db_country.CreatedOn = value.ChangedOn;
                db_country.CreatedBy = value.ChangedBy;


                _CountryDetailsRepository.Add(db_country);
                _CountryDetailsRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Country Created";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(CountryViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_COUNTRY _dbvalue = _CountryDetailsRepository.Get(p => p.Id == value.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = false;
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;

                _CountryDetailsRepository.Update(_dbvalue);
                _CountryDetailsRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_COUNTRY _dbvalue = _CountryDetailsRepository.Get(p => p.Id == id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                CountryViewModel country = new CountryViewModel();
                country.Id = _dbvalue.Id;
                country.Code = _dbvalue.Code;
                country.DialingCode = _dbvalue.DialingCode;
                country.Name = _dbvalue.Name;

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = country;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<CountryViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            //Get the Sorting column
            Func<CountryViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.DialingCode :
                iSortCol == 4 ? t.Id.ToString() :
                t.Id.ToString()
            );

            IEnumerable<CountryViewModel> data = _CountryDetailsRepository.GetAll().Select(p => new CountryViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                DialingCode = p.DialingCode,
                ValidFlag = p.ValidFlag == null ? true : (bool)p.ValidFlag
            });
            
            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {
                //if(model.sorting == 0)
                //{
                //    data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper()));
                //}
                //if (model.sorting == 1)
                //{
                //    data = data.Where(x => x.Name.ToUpper().Contains(model.Search.ToUpper()));
                //}
                //if (model.sorting == 2)
                //{
                //    data = data.Where(x => x.DialingCode.ToUpper().Contains(model.Search.ToUpper()));
                //}
                //// In case of by default
                //if(model.sorting == 4)
                //{
                    data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper()) 
                    || x.Name.ToUpper().Contains(model.Search.ToUpper())
                    || x.DialingCode.ToUpper().Contains(model.Search.ToUpper()));
               // }
            }
            
            recordsFiltered = data.Count();
            if(model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<CountryViewModel> dataObject = new PaginatedRecordsModel<CountryViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(CountryViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_COUNTRY _dbvalue = _CountryDetailsRepository.Get(p => p.Id == value.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                DB_COUNTRY _dbexist = _CountryDetailsRepository.Get(x => x.DialingCode == value.DialingCode && x.Id != value.Id);
                if (_dbexist != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Dialing Code Already Exist";
                    response.Message = "Dialing Code Already Exist";
                    return response;
                }
                _dbvalue.DialingCode = value.DialingCode;
                _dbvalue.Name = value.Name;

                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;


                _CountryDetailsRepository.Update(_dbvalue);
                _CountryDetailsRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Country Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        //Get Active Countries
        public ResponseViewModel ActiveCountries()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<CountryViewModel> country = _CountryDetailsRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new CountryViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = country;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

    }
}
