﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Project.DataAccessLayer.Services
{
    public class RoleService : IBaseService<RolesViewModel>
    {
        private AspNetRoleRepository _RoleRepository = new AspNetRoleRepository();
        private AspNetUserRepository _UserRepository = new AspNetUserRepository();
        private PrivilegesRepository _PrivelageRepository = new PrivilegesRepository();
        private RolePrivilgesRepository _RolePrivelageRepository = new RolePrivilgesRepository();

        public ResponseViewModel Activate(RolesViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(RolesViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                string message = "";
                if (!value.Id.Equals(""))
                {
                    AspNetRole role = _RoleRepository.GetMany(a => a.Id == value.Id).FirstOrDefault();
                    List<DB_ROLE_PRIVILEGES> priviliges = _RolePrivelageRepository.GetMany(p => p.ROLE_ID == role.Id).ToList();

                    foreach (var privilege in priviliges)
                    {
                        _RolePrivelageRepository.Delete(privilege);
                    }

                    _RolePrivelageRepository.Commit();

                    foreach (var privilege in role.DB_ROLE_PRIVILEGES.ToList())
                    {
                        _RolePrivelageRepository.Delete(privilege);
                    }


                    foreach (var privilege in value.allPrivelages)
                    {
                        long id = 0;
                        long.TryParse(privilege.Value, out id);
                        DB_DEV_PRIVILEGES a = _PrivelageRepository.GetById(id);
                        DB_ROLE_PRIVILEGES temp = new DB_ROLE_PRIVILEGES();
                        temp.PRIVILEGE_ID = a.Id;
                        temp.ROLE_ID = role.Id;
                        _RolePrivelageRepository.Add(temp);
                    }
                    message = "Role Updated Successfully.";
                    _RolePrivelageRepository.Commit();


                }
                else if (value.Id.Equals(""))
                {
                    AspNetRole role = new AspNetRole();
                    role.Name = value.RoleName;
                    role.Id = Guid.NewGuid().ToString();
                    _RoleRepository.Add(role);
                    _RoleRepository.Commit();

                    foreach (var privilege in value.allPrivelages)
                    {
                        long id = 0;
                        long.TryParse(privilege.Value, out id);
                        DB_DEV_PRIVILEGES a = _PrivelageRepository.GetById(id);
                        DB_ROLE_PRIVILEGES temp = new DB_ROLE_PRIVILEGES();
                        temp.PRIVILEGE_ID = a.Id;
                        temp.ROLE_ID = role.Id;
                        _RolePrivelageRepository.Add(temp);
                    }
                    message = "Role Added Successfully.";

                    _RolePrivelageRepository.Commit();
                }

                response.Status = HttpStatusCode.OK;
                response.Message = message;
                response.LogLevel = EnumLogLevel.Information;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                if (((ex.InnerException).InnerException).Message.Contains("Cannot insert duplicate key"))
                {
                    response.Status = HttpStatusCode.NotFound;
                    response.Message = "Role Name Already Exists";

                    return response;
                }
                response.Status = HttpStatusCode.NotFound;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get Active Roles
        public ResponseViewModel GetAllRoles()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<RolesViewModel> roles = _RoleRepository.GetAll()
                 .Select(p => new RolesViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.OK;
                response.obj = roles;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get Distributor Id Value
        public ResponseViewModel GetDistributorValue()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                AspNetRole roles = _RoleRepository.Get(x => x.Name == "Distributor");

                RolesViewModel role = new RolesViewModel();

                role.Id = roles.Id;


                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.OK;
                response.obj = role;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Deactivate(RolesViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Delete(String id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                AspNetRole role = _RoleRepository.GetMany(a => a.Id.Equals(id)).FirstOrDefault();
                if (role.AspNetUsers.Count > 0)
                {
                    response.Status = HttpStatusCode.NotFound;
                    response.Message = "First delete the user associtated to Role.";
                    response.LogLevel = EnumLogLevel.Error;
                }


                else
                {
                    foreach (var privilege in role.DB_ROLE_PRIVILEGES.ToList())
                        _RolePrivelageRepository.Delete(privilege);
                    _RolePrivelageRepository.Commit();
                    _RoleRepository.Delete(role);
                    _RoleRepository.Commit();

                    response.Status = HttpStatusCode.OK;
                    response.Message = "Role Deleted Successfully.";
                    response.LogLevel = EnumLogLevel.Information;

                }

                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.NotFound;
                response.Message = ex.Message;
                return response;
            }
        }
        public ResponseViewModel Get(String roleorPrivilege)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                IList<RolesViewModel> items = new List<RolesViewModel>();
                RolesViewModel roleObj = new RolesViewModel();
                if (roleorPrivilege.Equals("privilege"))
                {
                    IEnumerable<DB_DEV_PRIVILEGES> allPrivelages = _PrivelageRepository.GetAll();
                    roleObj.allPrivelages = new List<System.Web.Mvc.SelectListItem>();
                    foreach (var elem in allPrivelages)
                    {
                        var privilege = new System.Web.Mvc.SelectListItem();
                        privilege.Text = elem.Privilege;
                        privilege.Value = elem.Id.ToString();
                        roleObj.allPrivelages.Add(privilege);
                    }
                    items.Add(roleObj);

                }
                if (roleorPrivilege.Equals("role"))
                {
                    IEnumerable<RolesViewModel> item = _RoleRepository.GetAll().Select(role => new RolesViewModel()
                    {

                        Id = role.Id,
                        RoleName = role.Name,
                        Name = role.Name


                    });

                    items = item.OrderBy(a => a.RoleName).ToList();
                }
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = items;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.NotFound;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;

            }


        }
        public ResponseViewModel Get(String roleorPrivilege, Guid id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<RolesViewModel> items = _RoleRepository.GetMany(a => a.Id.Equals(id)).FirstOrDefault().AspNetUsers.Select(user => new RolesViewModel()
                {
                    Id = user.Id,
                    RoleName = user.UserName,

                });
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = items;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.NotFound;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;

            }

        }
        public ResponseViewModel Get(String id, string roleOrUser)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
              
                    RolesViewModel roleObj = new RolesViewModel();

                    if (roleOrUser.Equals("role"))
                    {

                        AspNetRole item = _RoleRepository.GetMany(a => a.Id == id).FirstOrDefault();
                        ICollection<DB_ROLE_PRIVILEGES> selectedPrivelages = item.DB_ROLE_PRIVILEGES;
                        roleObj.RoleName = item.Name;
                        roleObj.Id = item.Id;
                        roleObj.hasChildren = true;
                        IEnumerable<DB_DEV_PRIVILEGES> allPrivelages = _PrivelageRepository.GetAll();
                        roleObj.allPrivelages = new List<System.Web.Mvc.SelectListItem>();
                        foreach (var elem in allPrivelages)
                        {
                            var privilege = new System.Web.Mvc.SelectListItem();
                            privilege.Text = elem.Privilege;
                            privilege.Value = elem.Id.ToString();
                            foreach (var selected in selectedPrivelages)
                            {
                                privilege.Selected = false;
                                if (elem.Id == selected.PRIVILEGE_ID)
                                {
                                    privilege.Selected = true;
                                    break;
                                }
                            }
                            roleObj.allPrivelages.Add(privilege);
                        }

                    }

                    if (roleOrUser.Equals("user"))
                    {

                        AspNetUser item = _UserRepository.GetMany(a => a.Id == id, "AspNetRoles").FirstOrDefault();
                        roleObj.RoleName = item.UserName;
                        roleObj.Id = item.Id;
                        roleObj.password = item.PasswordHash;
                        roleObj.roleOfuser = new List<string>();
                        foreach (var elem in item.AspNetRoles)
                        {

                            roleObj.roleOfuser.Add(elem.Id.ToString());
                        }

                    }

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = roleObj;
                return response;
                
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.NotFound;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }



        }

        public ResponseViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public PaginatedRecordsModel<RolesViewModel> GetPaginatedRecords(PaginationSearchModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Update(RolesViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                AspNetRole _dbvalue = _RoleRepository.Get(p => p.Id == value.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                _dbvalue.Name = value.RoleName;

                _RoleRepository.Update(_dbvalue);
                _RoleRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                if (((ex.InnerException).InnerException).Message.Contains("Cannot insert duplicate key"))
                {
                    response.Status = HttpStatusCode.NotFound;
                    response.Message = "Role Name Already Exists";

                    return response;
                }
                response.Status = HttpStatusCode.NotFound;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get All Role and Privilege
        public ResponseViewModel GetAllPrivilegeAndRole()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                AssignPrivilegesViewModel model = new AssignPrivilegesViewModel();
                IEnumerable<RolesModel> Roles = _RoleRepository.GetAll().Select(p => new RolesModel()
                {
                    RoleId = p.Id,
                    Name = p.Name
                });
                IEnumerable<PrivilegesModel> Privileges = _PrivelageRepository.GetAll().Select(p => new PrivilegesModel()
                {
                    PrivilegeId = p.Id,
                    Name = p.Privilege,
                    Category = p.Category,
                    Portal = p.Portal
                });


                model.Roles = Roles;
                model.Privileges = Privileges;

                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.OK;
                response.obj = model;
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get All Assign Privileges
        public ResponseViewModel GetAssignPrivilegeByRoleId(string id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<AssignPrivilegesViewModel> _data = _RolePrivelageRepository.GetMany(p => p.ROLE_ID == id).Select(p => new AssignPrivilegesViewModel()
                {
                    RoleId = p.ROLE_ID,
                    PrivilegeId = p.PRIVILEGE_ID,
                    Category = p.DB_DEV_PRIVILEGES.Category,
                    Portal = p.DB_DEV_PRIVILEGES.Portal
                });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = _data;
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel AssignViewsToRole(AssignPrivilegesViewModel[] value)
        {
            ResponseViewModel response = new ResponseViewModel();

            try
            {
                string RoleId = value[0].RoleId;
                List<DB_ROLE_PRIVILEGES> _data = _RolePrivelageRepository.GetMany(p => p.ROLE_ID == RoleId).ToList();
                if (_data.Count() > 0)
                {
                    foreach (var data in _data)
                    {
                        _RolePrivelageRepository.Delete(data);
                    }
                    _RolePrivelageRepository.Commit();
                }

                for (int i = 0; i < value.Length; i++)
                {
                    DB_ROLE_PRIVILEGES db_role_privileges = new DB_ROLE_PRIVILEGES();

                    db_role_privileges.ROLE_ID = value[i].RoleId;
                    db_role_privileges.PRIVILEGE_ID = value[i].PrivilegeId;

                    _RolePrivelageRepository.Add(db_role_privileges);
                    _RolePrivelageRepository.Commit();
                }
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.Created;
                response.Message = "Privileges Assign Successfully!";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }
    }
}
