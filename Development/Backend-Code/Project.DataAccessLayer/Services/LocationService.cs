﻿using Project.DataAccessLayer.BaseService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.Utilities;
using Project.Models.Enums;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class LocationService : IBaseService<LocationViewModel>
    {
        private LocationRepository _LocationRepository = new LocationRepository();

        public ResponseViewModel Activate(LocationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_LOCATION _dbvalue = _LocationRepository.Get(p => p.Id == value.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = true;
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;

                _LocationRepository.Update(_dbvalue);
                _LocationRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(LocationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_LOCATION db_location = _LocationRepository.Get(x => x.Code == value.Code && x.ParentId == value.ParentId);

                if (db_location != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                if (value.TypeId != value.ParentAccessId + 1)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "You cannot add Location under this";
                    response.Message = "You cannot add Location under this";
                    return response;
                }

                db_location = LocationMapper.DBMapper(db_location, value);



                _LocationRepository.Add(db_location);
                _LocationRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Location Created";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(LocationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_LOCATION _dbvalue = _LocationRepository.Get(p => p.Id == value.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = false;
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;

                _LocationRepository.Update(_dbvalue);
                _LocationRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_LOCATION _dbvalue = _LocationRepository.Get(p => p.Id == id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                LocationViewModel loc = LocationMapper.ValueMapper(_dbvalue);
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = loc;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<LocationViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Update(LocationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_LOCATION _dbvalue = _LocationRepository.Get(p => p.Id == value.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                if (_dbvalue.Type == (int)EnumLocationType.Company && _dbvalue.Name != value.Name)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "You can't Change Company Name";
                    response.Message = "You can't Change Company Name";
                    return response;
                }

                _dbvalue = LocationMapper.DBMapper(_dbvalue, value);


                _LocationRepository.Update(_dbvalue);
                _LocationRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Location Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        //Get Child Locations
        public ResponseViewModel GetLocationTree(bool isRootNode, int Id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<LocationTreeViewModel> location;
                if (isRootNode)
                {
                    location = _LocationRepository.GetMany(p => p.Id == Id && p.ValidFlag == true)
                .Select(p => new LocationTreeViewModel()
                {
                    id = p.Id,
                    text = p.Name,
                    children = p.DB_LOCATION1.Count > 0
                });
                }
                else
                {
                    location = _LocationRepository.GetMany(p => p.ParentId == Id && p.ValidFlag == true)
                .Select(p => new LocationTreeViewModel()
                {
                    id = p.Id,
                    text = p.Name,
                    children = p.DB_LOCATION1.Count > 0
                });
                }


                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = location;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetCompleteLocationTree(bool isRootNode, int Id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<LocationTreeViewModel> location;
                if (isRootNode)
                {
                    location = _LocationRepository.GetMany(p => p.Id == Id)
                .Select(p => new LocationTreeViewModel()
                {
                    id = p.Id,
                    text = p.Name,
                    children = p.DB_LOCATION1.Count > 0
                });
                }
                else
                {
                    location = _LocationRepository.GetMany(p => p.ParentId == Id)
                .Select(p => new LocationTreeViewModel()
                {
                    id = p.Id,
                    text = p.Name,
                    children = p.DB_LOCATION1.Count > 0
                });
                }


                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = location;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get All Location Type
        public ResponseViewModel GetAllLocationTypes(string User)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                LocationTypeRepository _LocationTypeRepository = new LocationTypeRepository();

                int Id = CommonUtility.GetLocationIdbyUser(User);

                IEnumerable<LocationTypeViewModel> locationType = _LocationTypeRepository.GetMany(p => p.ValidFlag == true && p.Id >= Id)
                .Select(p => new LocationTypeViewModel()
                {
                    Id = p.Id,
                    Name = p.Name
                });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = locationType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get All Location by Location Type
        public ResponseViewModel GetAllLocationsByLocationType(int Id, string User)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                List<int> LocationIds = CommonUtility.GetChildLocationsbyUser(User).ToList();
                IEnumerable<LocationTypeViewModel> locationType = _LocationRepository.GetMany(p => p.ValidFlag == true && p.Type == Id && LocationIds.Contains(p.Id))
                .Select(p => new LocationTypeViewModel()
                {
                    Id = p.Id,
                    Name = p.Name
                });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = locationType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        //Get Active CompanyWarehouse
        public ResponseViewModel GetActiveCompanyWarehouse(string User)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                List<int> LocationIds = CommonUtility.GetChildLocationsbyUser(User).ToList();
                IEnumerable<LocationViewModel> locations = _LocationRepository.GetMany(p => p.ValidFlag == true && p.IsWarehouse == true && p.IsCompanyWarehouse == true && LocationIds.Contains(p.Id))
                .Select(p => new LocationViewModel()
                {
                    Id = p.Id,
                    Name = p.Name
                });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = locations;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
