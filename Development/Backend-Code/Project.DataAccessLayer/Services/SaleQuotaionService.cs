﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using System.Net;
using Project.ViewModels.Enums;
using Project.Mapper.Mappers;
using Project.Models.Enums;

namespace Project.DataAccessLayer.Services
{
    public class SaleQuotaionService : IBaseService<SaleQuotationViewModel>
    {
        private SaleQuotationRepository _SaleQuotationRepository = new SaleQuotationRepository();
        public ResponseViewModel Activate(SaleQuotationViewModel value)
        {
            throw new NotImplementedException();
        }

        public ResponseViewModel Add(SaleQuotationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
              DB_SALE_QUOTATION  dbValue = SaleQuotationMapper.DBMapper(null, value);

                _SaleQuotationRepository.Add(dbValue);
                _SaleQuotationRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = " Sale Quotaion Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }

        }

        public ResponseViewModel Deactivate(SaleQuotationViewModel value)
        {
            throw new NotImplementedException();
        }
        public ResponseViewModel CancelById(SaleQuotationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SALE_QUOTATION dbValue = _SaleQuotationRepository.Get(p => p.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.StatusId = (int)EnumSaleQuotation.Cancelled;
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _SaleQuotationRepository.Update(dbValue);
                _SaleQuotationRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
             ResponseViewModel response = new ResponseViewModel();
            try
            {
                SaleQuotationViewModel model = new SaleQuotationViewModel();
                model.SaleQuotationDetail = new List<SaleQuotationDetailViewModel>();

                DB_SALE_QUOTATION dbValue = _SaleQuotationRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                SaleQuotationViewModel SaleQuotation = SaleQuotationMapper.ValueMapper(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = SaleQuotation;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<SaleQuotationViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {


            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<SaleQuotationViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.CustomerName :
                iSortCol == 2 ? t.ReferenceNumber :
                iSortCol == 3 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<SaleQuotationViewModel> data = _SaleQuotationRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumSaleQuotation.Open).Select(p => new SaleQuotationViewModel()
            {
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                CustomerName = p.DB_CUSTOMER.Name,
                ReferenceNumber = p.ReferenceNumber,
                ValidFlag = p.ValidFlag,
                StatusDescription = p.DB_DEV_SALE_QUOTATION_STATUS.Name,

            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<SaleQuotationViewModel> dataObject = new PaginatedRecordsModel<SaleQuotationViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }

        public ResponseViewModel Update(SaleQuotationViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_SALE_QUOTATION db_DB_SALE_QUOTATION = _SaleQuotationRepository.Get(p => p.Id == value.Id);
                if (db_DB_SALE_QUOTATION == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                else
                {
                    db_DB_SALE_QUOTATION.ValidFlag = false;
                    _SaleQuotationRepository.Update(db_DB_SALE_QUOTATION);
                   
                }
                DB_SALE_QUOTATION dbValue = SaleQuotationMapper.DBMapper(null, value);

                _SaleQuotationRepository.Add(dbValue);
                _SaleQuotationRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Sale Order Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public PaginatedRecordsModel<SaleQuotationViewModel> GetOpenSaleQuotation(PaginationSearchModel model)
        {


            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<SaleQuotationViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.CustomerName :
                iSortCol == 2 ? t.ReferenceNumber :
                t.Id.ToString()
            );
            IEnumerable<SaleQuotationViewModel> data = _SaleQuotationRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumSaleQuotation.Open).Select(p => new SaleQuotationViewModel()
            {
                Id = p.DB_SALE_QUOTATION_DETAIL.FirstOrDefault().SaleQuotationId,
                Name = p.DB_LOCATION.Name,
                CustomerName = p.DB_CUSTOMER.Name,
                ReferenceNumber = p.ReferenceNumber,
             LocationId=p.LocationId,
             

            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<SaleQuotationViewModel> dataObject = new PaginatedRecordsModel<SaleQuotationViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }
    }
    
}
