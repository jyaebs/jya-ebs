﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Mapper.Mappers;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;

namespace Project.DataAccessLayer.Services
{
    public class UserService : IBaseService<UserViewModel>
    {
        private UserDetailRepository _UserDetailRepository = new UserDetailRepository();

       

        public ResponseViewModel Activate(UserViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_USER_PROFILE _dbvalue = _UserDetailRepository.Get(p => p.Id == value.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Error;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = true;
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;

                _UserDetailRepository.Update(_dbvalue);
                _UserDetailRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(UserViewModel user)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_USER_PROFILE db_user = UserMapper.DBMapper(null, user);

                _UserDetailRepository.Add(db_user);
                _UserDetailRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "User Created";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(UserViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_USER_PROFILE _dbvalue = _UserDetailRepository.Get(p => p.Id == value.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Error;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = false;
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;

                _UserDetailRepository.Update(_dbvalue);
                _UserDetailRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_USER_PROFILE _dbvalue = _UserDetailRepository.Get(p => p.Id == id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Error;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                UserViewModel user = new UserViewModel();
                user.FirstName = _dbvalue.FirstName;
                user.LastName = _dbvalue.LastName;
                user.CNIC = _dbvalue.CNIC;
                user.Designation = _dbvalue.Designation;
                user.Email = _dbvalue.AspNetUser.Email;
                user.ContactNumber = _dbvalue.AspNetUser.PhoneNumber;
                user.Username = _dbvalue.UserName;
                user.LocationTypeId = _dbvalue.LocationTypeId;
                user.LocationType = _dbvalue.DB_DEV_LOCATION_TYPE.Name;
                user.Role = _dbvalue.AspNetUser.AspNetRoles.Count != 0 ? _dbvalue.AspNetUser.AspNetRoles.FirstOrDefault().Id : "";
                user.LocationId = _dbvalue.LocationId;
                user.Image = _dbvalue.Image;

                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.OK;
                response.obj = user;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Update(UserViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_USER_PROFILE _dbvalue = _UserDetailRepository.Get(p => p.Id == value.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Error;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                _dbvalue = UserMapper.DBMapper(_dbvalue, value);

                _UserDetailRepository.Update(_dbvalue);
                _UserDetailRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "User Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public PaginatedRecordsModel<UserViewModel> GetPaginatedRecords(PaginationSearchModel value)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = value.PageStart / value.PageSize;

            //Get the Sorting column
            Func<UserViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Username :
                iSortCol == 1 ? t.FirstName :
                iSortCol == 2 ? t.LastName :
                iSortCol == 3 ? t.Role :
                t.Id.ToString()
            );

            List<int> LocationIds = CommonUtility.GetChildLocationsByLocation(value.LocationId).ToList();

            IEnumerable<UserViewModel> data = _UserDetailRepository.GetMany(t => LocationIds.Contains(t.LocationId)).Select(p => new UserViewModel()
            {
                Id = p.Id,
                Username = p.UserName,
                FirstName = p.FirstName,
                LastName = p.LastName,
                ContactNumber = p.AspNetUser.PhoneNumber,
                Role = p.AspNetUser.AspNetRoles.FirstOrDefault().Name,
                ValidFlag = p.ValidFlag

            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(value.Search))
            {
             
                    data = data.Where(x => x.Username.ToUpper().Contains(value.Search.ToUpper())
                       || x.FirstName.ToUpper().Contains(value.Search.ToUpper())
                          || x.LastName.ToUpper().Contains(value.Search.ToUpper())
                            || x.Role.ToUpper().Contains(value.Search.ToUpper())
                    );
            
            }

            recordsFiltered = data.Count();
            if (value.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, value.sorting))
                .Skip(pageNo * value.PageSize)
                .Take(value.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, value.sorting))
                .Skip(pageNo * value.PageSize)
                .Take(value.PageSize);
            }


            PaginatedRecordsModel<UserViewModel> dataObject = new PaginatedRecordsModel<UserViewModel>();
            dataObject.draw = value.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }


        public ResponseViewModel GetOrderBookerByLocation(int LocationId)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                List<int> LocationIds = CommonUtility.GetChildLocationsByLocation(LocationId).ToList();
                IEnumerable<UserViewModel> user = _UserDetailRepository.GetMany(x => LocationIds.Contains(x.LocationId) && x.ValidFlag == true).Select(p => new UserViewModel()
                {
                    Id = p.Id,
                    Name = p.UserName + " - " + p.FirstName + " " + p.LastName,
                });
                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = user;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.NotFound;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }

        }
        
      
    }
}
