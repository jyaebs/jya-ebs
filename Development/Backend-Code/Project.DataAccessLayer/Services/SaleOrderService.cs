﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.ViewModels.Enums;
using System.Net;
using Project.Models.Enums;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
  
    public class SaleOrderService : IBaseService<SaleOrderViewModel>
    {
       
        private SaleOrderRepository _SaleOrderRepository = new SaleOrderRepository();
       
        public ResponseViewModel Activate(SaleOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SALE_ORDER dbValue = _SaleOrderRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _SaleOrderRepository.Update(dbValue);
                _SaleOrderRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(SaleOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SALE_ORDER dbValue = _SaleOrderRepository.Get(x => x.VoucherNo == value.VoucherNo);

                

                dbValue = SaleOrderMapper.DBMapper(dbValue, value);
                _SaleOrderRepository.Add(dbValue);
                _SaleOrderRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = " Sale Order Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }

        }

        public ResponseViewModel Deactivate(SaleOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_SALE_ORDER dbValue = _SaleOrderRepository.Get(p => p.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _SaleOrderRepository.Update(dbValue);
                _SaleOrderRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;
            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int Id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                SaleOrderViewModel model = new SaleOrderViewModel();
                model.SaleOrderDetail = new List<SaleOrderDetailViewModel>();

                DB_SALE_ORDER dbValue = _SaleOrderRepository.Get(p => p.Id == Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                SaleOrderViewModel SaleOrder = SaleOrderMapper.ValueMapper(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = SaleOrder;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<SaleOrderViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {

            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<SaleOrderViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.CustomerName :
                iSortCol == 2 ? t.VoucherNo :
                iSortCol == 3 ? t.OrderDate.ToShortDateString() :
                iSortCol == 4 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<SaleOrderViewModel> data = _SaleOrderRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumSaleOrder.Open).Select(p => new SaleOrderViewModel()
            {
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                VoucherNo = p.VoucherNo,
                CustomerName = p.DB_CUSTOMER.Name,
                orderdatestring = p.OrderDate.Year + @"/" + p.OrderDate.Month + @"/" + p.OrderDate.Day,
                ValidFlag = p.ValidFlag,
                StatusDescription = p.DB_DEV_SALE_ORDER_STATUS.Name,

            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<SaleOrderViewModel> dataObject = new PaginatedRecordsModel<SaleOrderViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }
    

        public ResponseViewModel Update(SaleOrderViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_SALE_ORDER db_DB_SALE_ORDER = _SaleOrderRepository.Get(p => p.Id == value.Id);
                if (db_DB_SALE_ORDER != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_DB_SALE_ORDER = SaleOrderMapper.DBMapper(db_DB_SALE_ORDER, value);

                _SaleOrderRepository.Update(db_DB_SALE_ORDER);
                _SaleOrderRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Sale Order Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel CancelById(SaleOrderViewModel value)
        {
           
                ResponseViewModel response = new ResponseViewModel();
                try
                {

                    DB_SALE_ORDER dbValue = _SaleOrderRepository.Get(p => p.Id == value.Id);
                    if (dbValue == null)
                    {
                        response.LogLevel = EnumLogLevel.Warning;
                        response.Status = HttpStatusCode.BadRequest;
                        response.obj = "Not Exist";
                        response.Message = "Not Exist";
                        return response;
                    }
                    dbValue.StatusId = (int)EnumSaleOrder.Cancelled;
                    dbValue.ValidFlag = false;
                    dbValue.UpdatedBy = value.ChangedBy;
                    dbValue.UpdatedOn = value.ChangedOn;

                   _SaleOrderRepository.Update(dbValue);
                   _SaleOrderRepository.Commit();

                    response.LogLevel = EnumLogLevel.Information;
                    response.Status = HttpStatusCode.OK;
                    response.obj = "Updated";
                    return response;
                }
                catch (Exception ex)
                {
                    response.LogLevel = EnumLogLevel.Error;
                    response.Status = HttpStatusCode.InternalServerError;
                    response.obj = ex.Message;
                    response.Message = ex.Message;
                    return response;
                }
        }

        public PaginatedRecordsModel<SaleOrderViewModel> GetOpenSaleOrder(PaginationSearchModel model)
        {


            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<SaleOrderViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.LocationId.ToString() :
                iSortCol == 1 ? t.CustomerName :
                iSortCol == 2 ? t.ReferenceNumber :
                 t.Id.ToString()
            );
            IEnumerable<SaleOrderViewModel> data = _SaleOrderRepository.GetMany(x => x.ValidFlag == true &&
            x.StatusId == (int)EnumSaleOrder.Open).Select(p => new SaleOrderViewModel()
            {
                Id = p.Id,
                Name = p.DB_LOCATION.Name,
                CustomerName = p.DB_CUSTOMER.Name,
                ReferenceNumber = p.ReferenceNumber,
                LocationId = p.LocationId,


            });

            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<SaleOrderViewModel> dataObject = new PaginatedRecordsModel<SaleOrderViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;

        }

    }
}
