﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
   
    public class PrimaryUOMService : IBaseService<PrimaryUOMViewModel>
    {
        private PrimaryUOMRepository _PrimaryUOMRepository = new PrimaryUOMRepository();
        public ResponseViewModel Activate(PrimaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRIMARY_UOM dbValue = _PrimaryUOMRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _PrimaryUOMRepository.Update(dbValue);
                _PrimaryUOMRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(PrimaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRIMARY_UOM dbValue = _PrimaryUOMRepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }
                dbValue = PrimaryUOMMapper.DBMapper(dbValue, value);


                _PrimaryUOMRepository.Add(dbValue);
                _PrimaryUOMRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Primary UOM Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(PrimaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRIMARY_UOM dbValue = _PrimaryUOMRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _PrimaryUOMRepository.Update(dbValue);
                _PrimaryUOMRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_PRIMARY_UOM dbValue = _PrimaryUOMRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                PrimaryUOMViewModel Warehouse = PrimaryUOMMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Warehouse;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<PrimaryUOMViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<PrimaryUOMViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<PrimaryUOMViewModel> data = _PrimaryUOMRepository.GetAll().Select(p => new PrimaryUOMViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<PrimaryUOMViewModel> dataObject = new PaginatedRecordsModel<PrimaryUOMViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(PrimaryUOMViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_PRIMARY_UOM db_PRIMARY_UOM = _PrimaryUOMRepository.Get(p => p.Id == value.Id);
                if (db_PRIMARY_UOM == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_PRIMARY_UOM = PrimaryUOMMapper.DBMapper(db_PRIMARY_UOM, value);

                _PrimaryUOMRepository.Update(db_PRIMARY_UOM);
                _PrimaryUOMRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "PrimaryUOM Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel GetAllActivePrimaryUOM()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<PrimaryUOMViewModel> PrimaryUOM = _PrimaryUOMRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new PrimaryUOMViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = PrimaryUOM;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}  