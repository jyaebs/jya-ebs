﻿using Project.DataAccessLayer.BaseService;
using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;
using Project.Utilities;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Project.DataAccessLayer.Services
{
    public class CityService : IBaseService<CityViewModel>
    {
        private CityRepository _CityDetailsRepository = new CityRepository();

        public ResponseViewModel Activate(CityViewModel City)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CITY _dbvalue = _CityDetailsRepository.Get(p => p.Id == City.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = true;
                _dbvalue.UpdatedBy = City.ChangedBy;
                _dbvalue.UpdatedOn = City.ChangedOn;

                _CityDetailsRepository.Update(_dbvalue);
                _CityDetailsRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(CityViewModel City)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CITY _dBCITY = _CityDetailsRepository.Get(x => x.DialingCode == City.DialingCode);
                if (_dBCITY != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Dialing Code Already Exist";
                    response.Message = "Dialing Code Already Exist";
                    return response;
                }
                DB_CITY dB_CITY = _CityDetailsRepository.Get(x => x.Code == City.Code);
                if (dB_CITY != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }
                dB_CITY = new DB_CITY();

                dB_CITY.CountryId = City.CountryId;
                dB_CITY.Code = City.Code;
                dB_CITY.Name = City.Name;
                dB_CITY.DialingCode = City.DialingCode;

                dB_CITY.ValidFlag = true;

                dB_CITY.CreatedOn = City.ChangedOn;
                dB_CITY.CreatedBy = City.ChangedBy;


                _CityDetailsRepository.Add(dB_CITY);
                _CityDetailsRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "City Created";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(CityViewModel City)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CITY _dbvalue = _CityDetailsRepository.Get(p => p.Id == City.Id);

                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.ValidFlag = false;
                _dbvalue.UpdatedBy = City.ChangedBy;
                _dbvalue.UpdatedOn = City.ChangedOn;

                _CityDetailsRepository.Update(_dbvalue);
                _CityDetailsRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_CITY _dbvalue = _CityDetailsRepository.Get(p => p.Id == id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                CityViewModel city = new CityViewModel();
                city.Id = _dbvalue.Id;
                city.CountryId =_dbvalue.CountryId;
                city.Code = _dbvalue.Code;
                city.Name = _dbvalue.Name;
                city.DialingCode = _dbvalue.DialingCode;


                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = city;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<CityViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            //Get the Sorting column
            Func<CityViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.DialingCode :
                iSortCol == 3 ? t.CountryName :
                iSortCol == 5 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<CityViewModel> data = _CityDetailsRepository.GetAll().Select(p => new CityViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                DialingCode = p.DialingCode,
                CountryName = p.DB_COUNTRY.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {
               
                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                || x.DialingCode.ToUpper().Contains(model.Search.ToUpper())
                || x.CountryName.ToUpper().Contains(model.Search.ToUpper())

                );
                
            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<CityViewModel> dataObject = new PaginatedRecordsModel<CityViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(CityViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CITY dB_CITY = _CityDetailsRepository.Get(x => x.DialingCode == value.DialingCode && x.Id != value.Id);
                if (dB_CITY != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Dialing Code Already Exist";
                    response.Message = "Dialing Code Already Exist";
                    return response;
                }
                DB_CITY _dbvalue = _CityDetailsRepository.Get(p => p.Id == value.Id);
                if (_dbvalue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                _dbvalue.Id = value.Id;
                _dbvalue.CountryId = value.CountryId;
                _dbvalue.Code = value.Code;
                _dbvalue.Name = value.Name;
                _dbvalue.DialingCode = value.DialingCode;
                
                _dbvalue.UpdatedBy = value.ChangedBy;
                _dbvalue.UpdatedOn = value.ChangedOn;


                _CityDetailsRepository.Update(_dbvalue);
                _CityDetailsRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "City Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
        //Get Active City
        public ResponseViewModel GetAllCities(int CountryId)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<CityViewModel> city = _CityDetailsRepository.GetMany(p => p.ValidFlag == true && p.CountryId==CountryId)
                 .Select(p => new CityViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = city;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
        
    }
}
