﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.ViewModels.Enums;
using System.Net;
using Project.DatabaseModels;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class CustomerTypeService : IBaseService<CustomerTypeViewModel>
    {
        private CustomerTypeRepository _CustomerTypeRepository = new CustomerTypeRepository();
        public ResponseViewModel Activate(CustomerTypeViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_CUSTOMER_TYPE dbValue = _CustomerTypeRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _CustomerTypeRepository.Update(dbValue);
                _CustomerTypeRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(CustomerTypeViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER_TYPE dbValue = _CustomerTypeRepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }

                dbValue = CustomerTypeMapper.DBMapper(dbValue,value);

                _CustomerTypeRepository.Add(dbValue);
                _CustomerTypeRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Customer Type Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(CustomerTypeViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER_TYPE dbValue = _CustomerTypeRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _CustomerTypeRepository.Update(dbValue);
                _CustomerTypeRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_CUSTOMER_TYPE dbValue = _CustomerTypeRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                CustomerTypeViewModel CustomerType = CustomerTypeMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = CustomerType;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<CustomerTypeViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<CustomerTypeViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<CustomerTypeViewModel> data = _CustomerTypeRepository.GetAll().Select(p => new CustomerTypeViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<CustomerTypeViewModel> dataObject = new PaginatedRecordsModel<CustomerTypeViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(CustomerTypeViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_CUSTOMER_TYPE db_CUSTOMER_TYPE = _CustomerTypeRepository.Get(p => p.Id == value.Id);
                if (db_CUSTOMER_TYPE == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_CUSTOMER_TYPE = CustomerTypeMapper.DBMapper(db_CUSTOMER_TYPE, value);

                _CustomerTypeRepository.Update(db_CUSTOMER_TYPE);
                _CustomerTypeRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Customer Type Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }


        //Get Active Customer Type
        public ResponseViewModel GetAllCustomerType()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<CustomerTypeViewModel> country = _CustomerTypeRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new CustomerTypeViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = country;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}

