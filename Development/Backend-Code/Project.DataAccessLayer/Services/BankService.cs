﻿using Project.DataAccessLayer.BaseService;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Models.ViewModels.Common;
using Project.DatabaseModels.Repository;
using Project.DatabaseModels;
using Project.ViewModels.Enums;
using System.Net;
using Project.Mapper.Mappers;

namespace Project.DataAccessLayer.Services
{
    public class BankService : IBaseService<BankViewModel>
    {
        private BankRepository _BankRepository = new BankRepository();
        public ResponseViewModel Activate(BankViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_BANK dbValue = _BankRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = true;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _BankRepository.Update(dbValue);
                _BankRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel Add(BankViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_BANK dbValue = _BankRepository.Get(x => x.Code == value.Code);
                if (dbValue != null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Code Already Exist";
                    response.Message = "Code Already Exist";
                    return response;
                }
                dbValue = BankMapper.DBMapper(dbValue, value);


                _BankRepository.Add(dbValue);
                _BankRepository.Commit();

                response.Status = HttpStatusCode.Created;
                response.Message = "Bank Added Successfully ";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }

        public ResponseViewModel Deactivate(BankViewModel value)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_BANK dbValue = _BankRepository.Get(x => x.Id == value.Id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }
                dbValue.ValidFlag = false;
                dbValue.UpdatedBy = value.ChangedBy;
                dbValue.UpdatedOn = value.ChangedOn;

                _BankRepository.Update(dbValue);
                _BankRepository.Commit();

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = "Updated";
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public ResponseViewModel GetById(int id)
        {

            ResponseViewModel response = new ResponseViewModel();
            try
            {

                DB_BANK dbValue = _BankRepository.Get(p => p.Id == id);
                if (dbValue == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }

                BankViewModel Bank = BankMapper.DBValue(dbValue);

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Bank;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }

        public PaginatedRecordsModel<BankViewModel> GetPaginatedRecords(PaginationSearchModel model)
        {
            int recordsFiltered;
            int totalRecords;
            int pageNo = model.PageStart / model.PageSize;

            // Get the Sorting column
            Func<BankViewModel, Int32, string> getColName = (
                (t, iSortCol) =>
                iSortCol == 0 ? t.Code :
                iSortCol == 1 ? t.Name :
                iSortCol == 2 ? t.Id.ToString() :
                t.Id.ToString()
            );
            IEnumerable<BankViewModel> data = _BankRepository.GetAll().Select(p => new BankViewModel()
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                ValidFlag = p.ValidFlag,
            });


            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(model.Search))
            {

                data = data.Where(x => x.Code.ToUpper().Contains(model.Search.ToUpper())
                || x.Name.ToUpper().Contains(model.Search.ToUpper())
                );

            }

            recordsFiltered = data.Count();
            if (model.direction.Contains("asc"))
            {
                data = data
                .OrderBy(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }
            else
            {
                data = data
                .OrderByDescending(x => getColName(x, model.sorting))
                .Skip(pageNo * model.PageSize)
                .Take(model.PageSize);
            }

            PaginatedRecordsModel<BankViewModel> dataObject = new PaginatedRecordsModel<BankViewModel>();
            dataObject.draw = model.Draw;
            dataObject.data = data;
            dataObject.recordsTotal = totalRecords;
            dataObject.recordsFiltered = recordsFiltered;
            return dataObject;
        }

        public ResponseViewModel Update(BankViewModel value)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                DB_BANK db_BANK = _BankRepository.Get(p => p.Id == value.Id);
                if (db_BANK == null)
                {
                    response.LogLevel = EnumLogLevel.Warning;
                    response.Status = HttpStatusCode.BadRequest;
                    response.obj = "Not Exist";
                    response.Message = "Not Exist";
                    return response;
                }


                db_BANK = BankMapper.DBMapper(db_BANK, value);

                _BankRepository.Update(db_BANK);
                _BankRepository.Commit();

                response.Status = HttpStatusCode.OK;
                response.Message = "Bank Updated";
                response.LogLevel = EnumLogLevel.Information;

                return response;

            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.LogLevel = EnumLogLevel.Error;
                return response;
            }
        }
        public ResponseViewModel GetAllActiveBank()
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                IEnumerable<BankViewModel> Bank = _BankRepository.GetMany(p => p.ValidFlag == true)
                 .Select(p => new BankViewModel()
                 {
                     Id = p.Id,
                     Name = p.Name
                 });

                response.LogLevel = EnumLogLevel.Information;
                response.Status = HttpStatusCode.OK;
                response.obj = Bank;
                return response;

            }
            catch (Exception ex)
            {
                response.LogLevel = EnumLogLevel.Error;
                response.Status = HttpStatusCode.InternalServerError;
                response.obj = ex.Message;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
