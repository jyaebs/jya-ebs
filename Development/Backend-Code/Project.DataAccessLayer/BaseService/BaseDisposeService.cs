﻿
using Project.DatabaseModels;
using System.Web;

namespace Project.DataAccessLayer.BaseService
{
    public class BaseDisposeService
    {
        public void disposeEntityContext()
        {
            var entityContext = HttpContext.Current.Items["DbEntities"] as DbEntities;
            if (entityContext != null)
                entityContext.Dispose();
        }
    }
}
