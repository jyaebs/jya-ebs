﻿using Project.Models.ViewModels;
using Project.Models.ViewModels.Common;

namespace Project.DataAccessLayer.BaseService
{
    public interface IBaseService<T>
    {
        ResponseViewModel Add(T value);
        ResponseViewModel Update(T value);
        ResponseViewModel GetById(int id);
        ResponseViewModel Activate(T value);
        ResponseViewModel Deactivate(T value);
        PaginatedRecordsModel<T> GetPaginatedRecords(PaginationSearchModel model);
    }

   
}
