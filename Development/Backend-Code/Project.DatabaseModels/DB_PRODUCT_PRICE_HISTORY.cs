//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project.DatabaseModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class DB_PRODUCT_PRICE_HISTORY
    {
        public int Id { get; set; }
        public int ProductPriceTypeId { get; set; }
        public Nullable<int> LocationId { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
    
        public virtual DB_DEV_PRODUCT_PRICE_TYPE DB_DEV_PRODUCT_PRICE_TYPE { get; set; }
        public virtual DB_LOCATION DB_LOCATION { get; set; }
        public virtual DB_PRODUCT DB_PRODUCT { get; set; }
    }
}
