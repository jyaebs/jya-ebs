﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace Project.DatabaseModels.DBImplementations
{
    public class ReportDAO
    {
        private DatabaseFactory dbf = new DatabaseFactory();

        public DataTable ExecuteReport(String reportQuery)
        {
            try
            {
                SqlConnection myReadOnlyConnection = dbf.getReadOnlyConnection();
                myReadOnlyConnection.Open();

                SqlCommand myCommand = new SqlCommand(reportQuery, myReadOnlyConnection);
                myCommand.CommandTimeout = 0;
                DataTable reportDataTable = new DataTable();
                reportDataTable.Load(myCommand.ExecuteReader());
                myReadOnlyConnection.Close();

                return reportDataTable;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public DataTable ExecuteNonQueryReport(String reportQuery)
        {
            try
            {
                SqlConnection myReadOnlyConnection = dbf.getReadOnlyConnection();
                myReadOnlyConnection.Open();

                SqlCommand myCommand = new SqlCommand(reportQuery, myReadOnlyConnection);

                DataTable reportDataTable = new DataTable();
                myCommand.ExecuteNonQuery();
                //reportDataTable.Load();
                myReadOnlyConnection.Close();

                return reportDataTable;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
