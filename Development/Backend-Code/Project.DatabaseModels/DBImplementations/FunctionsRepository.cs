﻿using Project.DatabaseModels.DBContracts;
using System;
using System.Collections.Generic;

namespace Project.DatabaseModels.DBImplementations
{
    public class FunctionsRepository
    {
        private DbEntities dataContext;
        private IDatabaseFactory DatabaseFactory { get; set; }

        public FunctionsRepository()
        : this(new DatabaseFactory())
        {
        }

        protected FunctionsRepository(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            DataContext.Configuration.LazyLoadingEnabled = true;
            DataContext.Configuration.ProxyCreationEnabled = true;
            DataContext.Database.CommandTimeout = 180;
        }
        
        public void Dispose()
        {
            DatabaseFactory.Dispose();
        }

        protected DbEntities DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        public IEnumerable<fn_GetAllChildLocations_Result> GetAllChildLocations(int id)
        {
            IEnumerable<fn_GetAllChildLocations_Result> branches = DataContext.fn_GetAllChildLocations(id);
            return branches;
        }
        public IEnumerable<fn_GetAllParentLocations_Result> GetAllParentLocations(int id)
        {
            IEnumerable<fn_GetAllParentLocations_Result> branches = DataContext.fn_GetAllParentLocations(id);
            return branches;
        }
       
    }
}
