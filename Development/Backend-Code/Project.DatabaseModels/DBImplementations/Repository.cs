﻿


using Project.DatabaseModels.DBContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace Project.DatabaseModels.DBImplementations
{
    public abstract class Repository<T> : IRepository<T> ,IDisposable where T : class
    {
        private DbEntities dataContext;
        private  IDbSet<T> dbset;
        private DateTime currentTimeStamp;

        public Repository()
        : this(new DatabaseFactory())
        {
        }

        protected Repository(IDatabaseFactory databaseFactory)
        {

            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
            DataContext.Configuration.LazyLoadingEnabled = true;
            DataContext.Configuration.ProxyCreationEnabled = true;
            currentTimeStamp = DateTime.Now;
        }
        
        public void Dispose()
        {
            DatabaseFactory.Dispose();
        }

        protected IDatabaseFactory DatabaseFactory
        {
            get;
            set;
        }

        protected DbEntities DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        public virtual T Add(T entity)
        {
            //using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, new object[] { entity.ToString() }))
            //{
                return dbset.Add(entity);
            //}
        }

        public virtual void Update(T entity)
        {
            //String xml = LoggingUtility.ser(entity);
           /* using (new FunctionTracer(this.GetType().FullName, MethodBase.GetCurrentMethod().Name))
            {
         //       dbset.Attach(entity);
                dataContext.Entry(entity).State = EntityState.Modified;
           }*/



        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbset.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbset.Remove(obj);
        }

        public virtual T GetById(long id)
        {
            try
            {
                return dbset.Find(id);
            }catch(Exception)
            {

                dataContext = new DbEntities();
                dbset = DataContext.Set<T>();
                return dbset.Find(id);
            }
        }

        public virtual T GetById(string id)
        {
            return dbset.Find(id);
        }

        public virtual IEnumerable<T> GetAll(params string[] paths)
        {
            IQueryable<T> obj = dataContext.Set<T>();
            obj = Includes(obj, paths);
            return dbset.ToList();
        }

        public virtual IEnumerable<T>
            GetMany(Expression<Func<T, bool>> where, params string[] paths)
        {
            IQueryable<T> obj = dbset.Where(where);
            obj = Includes(obj, paths);
            return obj;
        }

        public T Get(Expression<Func<T, bool>> where, params string[] paths)
        {
            IQueryable<T> obj = dbset.Where(where);
            obj = Includes(obj, paths);
            return obj.FirstOrDefault<T>();
        }

        private IQueryable<T> Includes(IQueryable<T> obj, params string[] paths)
        {
            try
            {
                foreach (var path in paths)
                    obj = obj.Include(path);

                obj.Load();
                return obj;
            }
            catch (Exception ex)
            {
 
            }
            return obj;
        }
       
        public virtual void Commit()
        {
            try
            {
                SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    //foreach (var validationError in validationErrors.ValidationErrors)
                    //{
                    //    Logger.Instance.Write(EnumLogLevel.Error, "Class: " + validationErrors.Entry.Entity.GetType().FullName +
                    //        ", Property: " + validationError.PropertyName + ", Error: " + validationError.ErrorMessage);
                    //}
                }
                throw;
            }
            catch(Exception ex)
            {
               // Logger.Instance.Write(EnumLogLevel.Error, ex.InnerException.Message, ex);
                throw;
            }

        }

        private int SaveChanges()
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            foreach (var ent in dataContext.ChangeTracker.Entries().Where(p => p.State == System.Data.Entity.EntityState.Added || p.State == System.Data.Entity.EntityState.Deleted || p.State == System.Data.Entity.EntityState.Modified))
            {
                // For each changed record, get the audit record entries and add them
                foreach (AuditLog audit in GetAuditRecordsForChange(ent))
                {
                    dataContext.AuditLogs.Add(audit);
                }
            }

            //Log newly added many-to-many relations
            foreach (AuditLog relationshipAuditLog in GetAddedRelationshipAuditLogs())
            {
                dataContext.AuditLogs.Add(relationshipAuditLog);
            }

            //Log deleted many-to-many relations
            foreach (AuditLog relationshipAuditLog in GetDeletedRelationshipAuditLogs())
            {
                dataContext.AuditLogs.Add(relationshipAuditLog);
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return dataContext.SaveChanges();
        }

        private string GetTableName(DbEntityEntry ent)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)this.dataContext).ObjectContext;
            Type entityType = ent.Entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                entityType = entityType.BaseType;

            string entityTypeName = entityType.Name;

            EntityContainer container =
                objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName, DataSpace.CSpace);
            string entitySetName = (from meta in container.BaseEntitySets
                                    where meta.ElementType.Name == entityTypeName
                                    select meta.Name).First();
            return entitySetName;
        }

        private List<AuditLog> GetAuditRecordsForChange(DbEntityEntry dbEntry)
        {
            List<AuditLog> result = new List<AuditLog>();

     
            string tableName = GetTableName(dbEntry);

            ObjectStateEntry objectStateEntry = ((IObjectContextAdapter)this.dataContext).ObjectContext.ObjectStateManager.GetObjectStateEntry(dbEntry.Entity);
            string entityId = objectStateEntry.EntityKey.EntityKeyValues != null ? objectStateEntry.EntityKey.EntityKeyValues[0].Value.ToString() : "";

            string createdBy = "";
            try
            {
                createdBy = System.Web.HttpContext.Current.User.Identity.Name;
            }
            catch(Exception ex)
            { }
            if (dbEntry.State == System.Data.Entity.EntityState.Added)
            {
                result.Add(new AuditLog()
                {
                    Id = Guid.NewGuid(),
                    CreatedBy = createdBy,
                    CreatedOn = currentTimeStamp,
                    EventType = "A", // Added
                    TableName = tableName,
                    EntityId = entityId,
                    ColumnName = "*ALL",
                    NewValue = dbEntry.CurrentValues.ToObject().ToString()
                }
                    );
            }
            else if (dbEntry.State == System.Data.Entity.EntityState.Deleted)
            {
                result.Add(new AuditLog()
                {
                    Id = Guid.NewGuid(),
                    CreatedBy = createdBy,
                    CreatedOn = currentTimeStamp,
                    EventType = "D", // Deleted
                    TableName = tableName,
                    EntityId = entityId,
                    ColumnName = "*ALL",
                    OriginalValue = dbEntry.OriginalValues.ToObject().ToString()
                }
                    );
            }
            else if (dbEntry.State == System.Data.Entity.EntityState.Modified)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    // For updates, we only want to capture the columns that actually changed
                    if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                    {
                        result.Add(new AuditLog()
                        {
                            Id = Guid.NewGuid(),
                            CreatedBy = createdBy,
                            CreatedOn = currentTimeStamp,
                            EventType = "M",    // Modified
                            TableName = tableName,
                            EntityId = entityId,
                            ColumnName = propertyName,
                            OriginalValue = dbEntry.OriginalValues.GetValue<object>(propertyName) == null ? null : dbEntry.OriginalValues.GetValue<object>(propertyName).ToString(),
                            NewValue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString()
                        }
                            );
                    }
                }
            }

            return result;
        }

        public IEnumerable<AuditLog> GetAddedRelationshipAuditLogs()
        {
            dataContext.ChangeTracker.DetectChanges();
            var objectContext = ((IObjectContextAdapter)dataContext).ObjectContext;

            IEnumerable<AuditLog> audits = objectContext
                .ObjectStateManager
                .GetObjectStateEntries(EntityState.Added)
                .Where(e => e.IsRelationship)
                .Select(
                    e => new AuditLog()
                    {
                        Id = Guid.NewGuid(),
                        CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                        CreatedOn = currentTimeStamp,
                        EventType = "A",
                        TableName = e.EntitySet.Name,
                        EntityId = "",
                        ColumnName = "",
                        NewValue = "",
                        RelationalColumnName = ((EntityKey)e.CurrentValues[1]).EntityKeyValues[0].Key,
                        RelationalColumnValue = ((EntityKey)e.CurrentValues[1]).EntityKeyValues[0].Value.ToString()
                    });

            return audits;
        }

        public IEnumerable<AuditLog> GetDeletedRelationshipAuditLogs()
        {
            dataContext.ChangeTracker.DetectChanges();
            var objectContext = ((IObjectContextAdapter)dataContext).ObjectContext;

            IEnumerable<AuditLog> audits = objectContext
                .ObjectStateManager
                .GetObjectStateEntries(EntityState.Added)
                .Where(e => e.IsRelationship)
                .Select(
                    e => new AuditLog()
                    {
                        Id = Guid.NewGuid(),
                        CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                        CreatedOn = currentTimeStamp,
                        EventType = "D",
                        TableName = e.EntitySet.Name,
                        EntityId = "",
                        ColumnName = "",
                        OriginalValue = "",
                        RelationalColumnName = ((EntityKey)e.CurrentValues[1]).EntityKeyValues[0].Key,
                        RelationalColumnValue = ((EntityKey)e.CurrentValues[1]).EntityKeyValues[0].Value.ToString()
                    });

            return audits;
        }
    }    
}
