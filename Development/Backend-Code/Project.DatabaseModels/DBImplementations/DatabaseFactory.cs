﻿

using Project.DatabaseModels.DBContracts;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
namespace Project.DatabaseModels.DBImplementations
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private DbEntities dataContext;
        public DbEntities Get()
        {
            if (HttpContext.Current == null)
            {
                return new DbEntities();
            }

            if (HttpContext.Current.Items["DbEntities"] == null)
            {
                HttpContext.Current.Items["DbEntities"] = new DbEntities();
            }

            return (DbEntities)HttpContext.Current.Items["DbEntities"];
            
            
          //  return dataContext ?? (dataContext = new Entities());
        }

        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }

        public SqlConnection getReadOnlyConnection()
        {
            if (ConfigurationManager.ConnectionStrings != null)
            {
                if (ConfigurationManager.ConnectionStrings["ReadOnlyConnection"] != null)
                {
                    String myConStr = ConfigurationManager.ConnectionStrings["ReadOnlyConnection"].ConnectionString;
                    SqlConnection myConnection = new SqlConnection(myConStr);
                    return myConnection;
                }
                else
                {
                    return null;
                }

            }
            return null;
        }

        public string getDatabaseName()
        {
            string databaseName = Get().Database.Connection.Database;
            return databaseName;
        }
    }
}
