//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project.DatabaseModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class DB_CHART_OF_ACCOUNT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DB_CHART_OF_ACCOUNT()
        {
            this.DB_BANK_BRANCH = new HashSet<DB_BANK_BRANCH>();
            this.DB_CHART_OF_ACCOUNT1 = new HashSet<DB_CHART_OF_ACCOUNT>();
            this.DB_CUSTOMER = new HashSet<DB_CUSTOMER>();
            this.DB_CUSTOMER1 = new HashSet<DB_CUSTOMER>();
            this.DB_GL_ACCOUNT_INTEGRATION = new HashSet<DB_GL_ACCOUNT_INTEGRATION>();
            this.DB_GST = new HashSet<DB_GST>();
            this.DB_PRODUCT = new HashSet<DB_PRODUCT>();
            this.DB_PRODUCT1 = new HashSet<DB_PRODUCT>();
            this.DB_PRODUCT2 = new HashSet<DB_PRODUCT>();
            this.DB_VENDOR = new HashSet<DB_VENDOR>();
            this.DB_VENDOR1 = new HashSet<DB_VENDOR>();
            this.DB_WITH_HOLDING_TAX = new HashSet<DB_WITH_HOLDING_TAX>();
            this.DB_GENERAL_LEDGER_DETAIL = new HashSet<DB_GENERAL_LEDGER_DETAIL>();
            this.DB_PRODUCT11 = new HashSet<DB_PRODUCT>();
        }
    
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ParentCode { get; set; }
        public int LevelID { get; set; }
        public int AccountTypeId { get; set; }
        public bool ValidFlag { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_BANK_BRANCH> DB_BANK_BRANCH { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_CHART_OF_ACCOUNT> DB_CHART_OF_ACCOUNT1 { get; set; }
        public virtual DB_CHART_OF_ACCOUNT DB_CHART_OF_ACCOUNT2 { get; set; }
        public virtual DB_DEV_CHART_OF_ACCOUNT_TYPE DB_DEV_CHART_OF_ACCOUNT_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_CUSTOMER> DB_CUSTOMER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_CUSTOMER> DB_CUSTOMER1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_GL_ACCOUNT_INTEGRATION> DB_GL_ACCOUNT_INTEGRATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_GST> DB_GST { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PRODUCT> DB_PRODUCT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PRODUCT> DB_PRODUCT1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PRODUCT> DB_PRODUCT2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_VENDOR> DB_VENDOR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_VENDOR> DB_VENDOR1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_WITH_HOLDING_TAX> DB_WITH_HOLDING_TAX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_GENERAL_LEDGER_DETAIL> DB_GENERAL_LEDGER_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PRODUCT> DB_PRODUCT11 { get; set; }
    }
}
