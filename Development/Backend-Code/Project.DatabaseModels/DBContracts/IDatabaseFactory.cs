﻿
using System;
using System.Data.SqlClient;

namespace Project.DatabaseModels.DBContracts
{
    public interface IDatabaseFactory : IDisposable
    {
        DbEntities Get();
        SqlConnection getReadOnlyConnection();
        string getDatabaseName();
    }
}
