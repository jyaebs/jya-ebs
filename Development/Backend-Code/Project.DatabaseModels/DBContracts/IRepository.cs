﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Project.DatabaseModels.DBContracts
{
    public interface IRepository<T> where T : class
    {
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        T GetById(long Id);
        T GetById(string Id);
        T Get(Expression<Func<T, bool>> where,params string [] paths);
        IEnumerable<T> GetAll(params string [] paths);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where, params string[] paths);
        void Commit();
    }
    public interface ICache
    {
        bool TryGetEntryByKey(string key, out object value);
        void Add(string key, object value,
                 IEnumerable<string> dependentEntitySets);
        void Invalidate(IEnumerable<string> entitySets);
    }

}
