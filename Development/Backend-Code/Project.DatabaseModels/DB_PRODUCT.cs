//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project.DatabaseModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class DB_PRODUCT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DB_PRODUCT()
        {
            this.DB_PRODUCT_PRICE = new HashSet<DB_PRODUCT_PRICE>();
            this.DB_PRODUCT_PRICE_HISTORY = new HashSet<DB_PRODUCT_PRICE_HISTORY>();
            this.DB_PURCHASE_ORDER_DETAIL = new HashSet<DB_PURCHASE_ORDER_DETAIL>();
            this.DB_PURCHASE_REQUISITION_DETAIL = new HashSet<DB_PURCHASE_REQUISITION_DETAIL>();
            this.DB_SALE_QUOTATION_DETAIL = new HashSet<DB_SALE_QUOTATION_DETAIL>();
            this.DB_PURCHASE_QUOTATION_DETAIL = new HashSet<DB_PURCHASE_QUOTATION_DETAIL>();
            this.DB_DELIVERY_NOTE_DETAIL = new HashSet<DB_DELIVERY_NOTE_DETAIL>();
            this.DB_INWARD_GATE_PASS_DETAIL = new HashSet<DB_INWARD_GATE_PASS_DETAIL>();
            this.DB_STOCK = new HashSet<DB_STOCK>();
            this.DB_STOCK_LEDGER = new HashSet<DB_STOCK_LEDGER>();
            this.DB_SALE_ORDER_DETAIL = new HashSet<DB_SALE_ORDER_DETAIL>();
            this.DB_INSPECTION_NOTE_DETAIL = new HashSet<DB_INSPECTION_NOTE_DETAIL>();
            this.DB_SALE_INVOICE_DETAIL = new HashSet<DB_SALE_INVOICE_DETAIL>();
        }
    
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string BarCode { get; set; }
        public int ProductTypeId { get; set; }
        public int ProductBrandId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductSubCategoryId { get; set; }
        public int PrimaryUomId { get; set; }
        public Nullable<int> SecondaryUomId { get; set; }
        public Nullable<decimal> ConverstionFactor { get; set; }
        public string Image { get; set; }
        public bool IsTrendyItem { get; set; }
        public string DepreciationMethod { get; set; }
        public string DepreciationType { get; set; }
        public Nullable<decimal> DepreciationRate { get; set; }
        public Nullable<int> UsageLife { get; set; }
        public Nullable<decimal> ScrapeValue { get; set; }
        public string InventoryAccount { get; set; }
        public string COGSAccount { get; set; }
        public string DepreciationAccount { get; set; }
        public bool ValidFlag { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        public virtual DB_CHART_OF_ACCOUNT DB_CHART_OF_ACCOUNT { get; set; }
        public virtual DB_CHART_OF_ACCOUNT DB_CHART_OF_ACCOUNT1 { get; set; }
        public virtual DB_CHART_OF_ACCOUNT DB_CHART_OF_ACCOUNT2 { get; set; }
        public virtual DB_DEV_PRODUCT_TYPE DB_DEV_PRODUCT_TYPE { get; set; }
        public virtual DB_PRIMARY_UOM DB_PRIMARY_UOM { get; set; }
        public virtual DB_PRODUCT_BRAND DB_PRODUCT_BRAND { get; set; }
        public virtual DB_PRODUCT_CATEGORY DB_PRODUCT_CATEGORY { get; set; }
        public virtual DB_PRODUCT_SUB_CATEGORY DB_PRODUCT_SUB_CATEGORY { get; set; }
        public virtual DB_SECONDARY_UOM DB_SECONDARY_UOM { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PRODUCT_PRICE> DB_PRODUCT_PRICE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PRODUCT_PRICE_HISTORY> DB_PRODUCT_PRICE_HISTORY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PURCHASE_ORDER_DETAIL> DB_PURCHASE_ORDER_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PURCHASE_REQUISITION_DETAIL> DB_PURCHASE_REQUISITION_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_SALE_QUOTATION_DETAIL> DB_SALE_QUOTATION_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_PURCHASE_QUOTATION_DETAIL> DB_PURCHASE_QUOTATION_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_DELIVERY_NOTE_DETAIL> DB_DELIVERY_NOTE_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_INWARD_GATE_PASS_DETAIL> DB_INWARD_GATE_PASS_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_STOCK> DB_STOCK { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_STOCK_LEDGER> DB_STOCK_LEDGER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_SALE_ORDER_DETAIL> DB_SALE_ORDER_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_INSPECTION_NOTE_DETAIL> DB_INSPECTION_NOTE_DETAIL { get; set; }
        public virtual DB_CHART_OF_ACCOUNT DB_CHART_OF_ACCOUNT11 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DB_SALE_INVOICE_DETAIL> DB_SALE_INVOICE_DETAIL { get; set; }
    }
}
