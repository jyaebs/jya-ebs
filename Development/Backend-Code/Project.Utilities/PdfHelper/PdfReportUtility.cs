﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Project.DatabaseModels.DBImplementations;
using Project.Models.ViewModels;
using Project.Utilities.PdfHelper.PdfReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.PdfHelper
{
    public class PdfReportUtility: BaseReportPdfUtility
    {
        public MemoryStream GenerateOrderBookerReport(DataTable Report, DataTable FooterReport, string locationName,DateTime date)
        {

            //Create Document
            Document document = new Document(PageSize.A4.Rotate(), 5f, 5f, 10f, 10f); // top margin is added so that the header works fine and left - right margins are added to expand the content to the page

            MemoryStream outputStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, outputStream);
            document.Open();
            try
            {
                //Report Header
                PdfPTable Titile = new PdfPTable(1);

                PdfPCell TitileCell = GetCell("OrderBooker KPI Report", PdfUtilityFont_Report.TitleFont(),Element.ALIGN_CENTER);
                TitileCell.Border = 0;
                Titile.AddCell(TitileCell);
                document.Add(Titile);

                PdfPTable ReportHeading = new PdfPTable(7);
                ReportHeading.WidthPercentage = 95;
                ReportHeading.SpacingBefore = 10f;

                PdfPCell CellHeading1 = GetCell("CreatedOn:  "+DateTimeUtility.DateTimeNowInPKTimeZone(), PdfUtilityFont_Report.TableHeaderFont(), Element.ALIGN_LEFT);
                CellHeading1.Border = 0;
                CellHeading1.Colspan = 3;
                ReportHeading.AddCell(CellHeading1);

                PdfPCell CellHeading2 = GetCell("Location Name:  "+ locationName ,PdfUtilityFont_Report.TableHeaderFont(), Element.ALIGN_LEFT);
                CellHeading2.Border = 0;
                CellHeading2.Colspan = 3;
                ReportHeading.AddCell(CellHeading2);
                
                PdfPCell CellHeading3 = GetCell("Date:  " + date.ToString("dd/MM/yyyy"), PdfUtilityFont_Report.TableHeaderFont(), Element.ALIGN_RIGHT);
                CellHeading3.Border = 0;
                ReportHeading.AddCell(CellHeading3);

                document.Add(ReportHeading);

                //ReportTable
                document = ReportTable(Report, document);
                document = ReportTable(FooterReport, document);

                writer.CloseStream = false;
                document.Close();
                return outputStream;
            }
            catch (Exception ex)
            {
                return null;
            }
        }   
    }
}
