﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.PdfHelper.PdfReport
{
    public class PdfUtilityFont_Report
    {
        public static Font TitleFont()
        {
            return new Font(Font.FontFamily.HELVETICA, 14f, Font.BOLD, BaseColor.BLACK);
        }
        public static Font TableHeaderFont()
        {
            return new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD, BaseColor.BLACK);
        }
        public static Font NormalFont()
        {
            return new Font(Font.FontFamily.HELVETICA, 6f, Font.NORMAL, BaseColor.BLACK);
        }
    }
}
