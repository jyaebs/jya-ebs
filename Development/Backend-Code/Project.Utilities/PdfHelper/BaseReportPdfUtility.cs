﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Project.Utilities.PdfHelper.PdfReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.PdfHelper
{
    public abstract class BaseReportPdfUtility
    {
        public virtual Document ReportTable(DataTable Report,Document document)
        {
                PdfPTable ContentTable = new PdfPTable(Report.Columns.Count);
                ContentTable.WidthPercentage = 95;
                ContentTable.SpacingBefore = 5f;
                for (var i = 0; i < Report.Columns.Count; i++)
                {

                    PdfPCell TableCell = GetCell(Report.Columns[i].ToString(), PdfUtilityFont_Report.TableHeaderFont(), Element.ALIGN_CENTER);
                    ContentTable.AddCell(TableCell);

                }
                document.Add(ContentTable);
                foreach (DataRow v in Report.Rows)
                {

                    ContentTable = new PdfPTable(Report.Columns.Count);
                    ContentTable.WidthPercentage = 95;
                    for (var i = 0; i < v.ItemArray.Count(); i++)
                    {

                        PdfPCell TableCell = GetCell(v.ItemArray[i].ToString(), PdfUtilityFont_Report.NormalFont(), Element.ALIGN_CENTER);
                        ContentTable.AddCell(TableCell);
                    }

                    document.Add(ContentTable);
                }

                return document;

        }
        public virtual PdfPCell GetCell(string col, Font font, int Alignment)
        {
            //SerialNo Cell
            PdfPCell Cell = new PdfPCell();
            Cell.AddElement(AddCellParagraph(col, font, Alignment));
            return Cell;
        }
        public virtual Paragraph AddCellParagraph(string value, Font font, int Alignment)
        {
            Phrase Phrase = new Phrase(new Chunk(value, font));
            Paragraph Paragraph = new Paragraph(Phrase);
            Paragraph.Alignment = Alignment;
            return Paragraph;


        }
    }
}
