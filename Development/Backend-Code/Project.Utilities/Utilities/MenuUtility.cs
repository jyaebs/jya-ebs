﻿using Project.Models.SiteMap;
using Project.Utilities.SiteMapUtility;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities
{
    public class MenuUtility
    {
    
        public static String getPortalMenu(string userName)
        {
            List<SiteMapItem> portalList = SiteMapList.getSiteMapList(userName);
            String menuhtml = String.Empty;
            foreach (var item in portalList)
            {
                menuhtml = menuhtml + gethtmlPortalItem(item);
                if (item.listSiteMap != null && item.listSiteMap.Count > 0)
                {
                    foreach (var cat in item.listSiteMap)
                    {
                        menuhtml = menuhtml + gethtmlCategoryItem(cat);
                    }
                }
            }

            return menuhtml;
        }

        private static string gethtmlPortalItem(SiteMapItem value)
        {
            return "<li class=\"menu-title\" key=\"t-menu\">" +
                         "<span class=\"nav-text\">" + value.ItemName + "</span>" +
                   "</li>";

        }
        
        private static string gethtmlCategoryItem(SiteMapItem value)
        {
            String html = String.Empty;
            if (value.listSiteMap == null)
            {
                html = html + "<li ><a href = \"" + value.URL+"\" ><i class=\""+value.Icon+"\"></i><span class=\"item-name\">"+value.ItemName+"</span></a></li>";
                return html;
            }
            if (value.listSiteMap != null && value.listSiteMap.Count > 0)
            {
                html = html+ "<li  class=\"nav-item dropdown-sidemenu\">" +
                    "<a href=\"#\"><i class=\"nav-icon "+value.Icon+"\"></i><span class=\"item-name\">"+value.ItemName+
                    "</span><i class=\"dd-arrow i-Arrow-Down\"></i></a>"+
                    "<ul class=\"submenu\">";
                foreach (var item in value.listSiteMap)
                {
                    html = html + "<li><a href=\""+item.URL+"\">"+item.ItemName+"</a></li>";
                }
            }
            html = html+"</ul></li>";

            return html;
        }
    }
}
