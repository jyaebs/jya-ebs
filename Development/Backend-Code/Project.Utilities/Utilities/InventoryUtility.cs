﻿using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.Utilities
{
   public class InventoryUtility
    {
      
        public static bool CheckInventory(DeliveryNoteViewModel DeliveryNote,out string message)
        {
            foreach (var value in DeliveryNote.DeliveryNoteDetail)
            {
               string ProductName ="";
               bool IsInventoryAvailable= CheckInventoryFromDb(value.ProductId,value.WarehouseId,value.DeliverQuantity, out ProductName);
                if (!IsInventoryAvailable)
                {
                    message = "Insufficient Stock For " + ProductName;
                    return false;
                }

            }
            message = "added!";
            return true;
        }

        private static bool CheckInventoryFromDb(int ProductId,int WarehouseId,int Quantity,out string Name)
        {
               ProductRepository _ProductRepository = new ProductRepository();
               StockRepository _StokeRepository = new StockRepository();
               IEnumerable<DB_STOCK> db_stock_list = _StokeRepository.GetMany(x => x.ProductId == 
               ProductId && x.WarehouseId == WarehouseId  && x.ValidFlag == true && x.Quantity > 0).ToList();
             DB_PRODUCT db_product= _ProductRepository.Get(x => x.Id == ProductId && x.ValidFlag == true);
            if (db_stock_list!=null && db_stock_list.Count() > 0) 
            {
               
                Name = db_product.Name;
                if (db_stock_list.Sum(x => x.Quantity) < Quantity)
                {
                    
                    return false;
                }
                return true;
            }
            Name = db_product.Name;
            return false;
        }

    }
}
