﻿
using Project.DatabaseModels;
using Project.DatabaseModels.DBImplementations;
using Project.DatabaseModels.Repository;
using Project.Mapper.Mappers;
using Project.Models.Enums;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;

namespace Project.Utilities
{
    public class CommonUtility
    {

        public static int GetIdbyUser(string user)
        {
            int Id = 1;
            UserDetailRepository _UserDetailRepository = new UserDetailRepository();
            DB_USER_PROFILE value = _UserDetailRepository.Get(p => p.UserName.Equals(user));
            if (value != null)
            {
                Id = value.Id;
            }

            return Id;
        }
        public static int GetLocationIdbyUser(string user)
        {
            int Id = 1;
            UserDetailRepository _UserDetailRepository = new UserDetailRepository();
            DB_USER_PROFILE value = _UserDetailRepository.Get(p => p.UserName.Equals(user));
            if (value != null)
            {
                Id = value.LocationId;
            }

            return Id;
        }
        public static int GetLocationIdbyUser(int id)
        {
            int Id = 1;
            UserDetailRepository _UserDetailRepository = new UserDetailRepository();
            DB_USER_PROFILE value = _UserDetailRepository.Get(p => p.Id==id);
            if (value != null)
            {
                Id = value.LocationId;
            }

            return Id;
        }

        public static IEnumerable<int> GetChildLocationsbyUser(string user)
        {
            int id = 1;
            UserDetailRepository _UserDetailsRepository = new UserDetailRepository();
            DB_USER_PROFILE value = _UserDetailsRepository.Get(p => p.UserName.Equals(user));
            if (value != null)
            {
                id = value.LocationId;
            }
            IEnumerable<int> locations = new List<int>();
            locations = new FunctionsRepository().GetAllChildLocations(id).Select(p => p.Id != null ? (int)p.Id : 0);

            return locations;
        }
        public static IEnumerable<int> GetChildLocationsByLocation(int Id)
        {
            IEnumerable<int> locations = new List<int>();
            locations = new FunctionsRepository().GetAllChildLocations(Id).Select(p => p.Id != null ? (int)p.Id : 0);

            return locations;
        }
        public static IEnumerable<int> GetParentLocationsByLocation(int Id)
        {
            IEnumerable<int> locations = new List<int>();
            locations = new FunctionsRepository().GetAllParentLocations(Id).Select(p => p.Id != null ? (int)p.Id : 0);

            return locations;
        }
        public static LocationViewModel GetCompanyOfDistributor(int Id)
        {
            int CompanyId = (int)EnumLocationType.Company;

            fn_GetAllParentLocations_Result _fn_GetAllParentLocations_Result = new FunctionsRepository().GetAllParentLocations(Id).Where(p => p.Type == CompanyId).FirstOrDefault();
            LocationViewModel _LocationViewModel = new LocationViewModel();
            if (_fn_GetAllParentLocations_Result != null)
            {
                _LocationViewModel.Code = _fn_GetAllParentLocations_Result.Code;
                _LocationViewModel.Name = _fn_GetAllParentLocations_Result.Name;
            }

            return _LocationViewModel;
        }
        public static int GetFiscalYearIdbyDate(DateTime Date)
        {
            FiscalYearRepository _FiscalYearRepository = new FiscalYearRepository();
            DB_DEV_FISCAL_YEAR value = _FiscalYearRepository.Get(p =>   Date.Date >= p.StartDate  &&    Date.Date <= p.EndDate);
            int Id = 0;
            if (value != null)
            {
                Id = value.Id;
            }
            return Id;
        }

        //public static LocationViewModel GetRegionOfDistributor(int Id)
        //{
        //    int RegionId = (int)EnumLocationType.Region;

        //    fn_GetAllParentLocations_Result _fn_GetAllParentLocations_Result = new FunctionsRepository().GetAllParentLocations(Id).Where(p => p.Type == RegionId).FirstOrDefault();
        //    LocationViewModel _LocationViewModel = new LocationViewModel();
        //    if (_fn_GetAllParentLocations_Result != null)
        //    {
        //        _LocationViewModel.Code = _fn_GetAllParentLocations_Result.Code;
        //        _LocationViewModel.Name = _fn_GetAllParentLocations_Result.Name;
        //    }

        //    return _LocationViewModel;
        //}
        //public static LocationViewModel GetZoneOfDistributor(int Id)
        //{
        //    int ZoneId = (int)EnumLocationType.Zone;

        //    fn_GetAllParentLocations_Result _fn_GetAllParentLocations_Result = new FunctionsRepository().GetAllParentLocations(Id).Where(p => p.Type == ZoneId).FirstOrDefault();
        //    LocationViewModel _LocationViewModel = new LocationViewModel();
        //    if (_fn_GetAllParentLocations_Result != null)
        //    {
        //        _LocationViewModel.Code = _fn_GetAllParentLocations_Result.Code;
        //        _LocationViewModel.Name = _fn_GetAllParentLocations_Result.Name;
        //    }

        //    return _LocationViewModel;
        //}
        //public static LocationViewModel GetAreaOfDistributor(int Id)
        //{
        //    int AreaId = (int)EnumLocationType.Area;

        //    fn_GetAllParentLocations_Result _fn_GetAllParentLocations_Result = new FunctionsRepository().GetAllParentLocations(Id).Where(p => p.Type == AreaId).FirstOrDefault();
        //    LocationViewModel _LocationViewModel = new LocationViewModel();
        //    if (_fn_GetAllParentLocations_Result != null)
        //    {
        //        _LocationViewModel.Code = _fn_GetAllParentLocations_Result.Code;
        //        _LocationViewModel.Name = _fn_GetAllParentLocations_Result.Name;
        //    }

        //    return _LocationViewModel;
        //}
        //public static LocationViewModel GetTerritoryOfDistributor(int Id)
        //{
        //    int TerritoryId = (int)EnumLocationType.Territory;

        //    fn_GetAllParentLocations_Result _fn_GetAllParentLocations_Result = new FunctionsRepository().GetAllParentLocations(Id).Where(p => p.Type == TerritoryId).FirstOrDefault();
        //    LocationViewModel _LocationViewModel = new LocationViewModel();
        //    if (_fn_GetAllParentLocations_Result != null)
        //    {
        //        _LocationViewModel.Code = _fn_GetAllParentLocations_Result.Code;
        //        _LocationViewModel.Name = _fn_GetAllParentLocations_Result.Name;
        //    }

        //    return _LocationViewModel;
        //}
    }
}