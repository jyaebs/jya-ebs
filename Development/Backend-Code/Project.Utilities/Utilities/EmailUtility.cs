﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities
{
    public class EmailUtility
    {
        private static EmailUtility instance = new EmailUtility();
        public static EmailUtility getInstance()
        {
            if (instance == null)
                instance = new EmailUtility();
            return instance;
        }


        //public static Attachment GetPDFAttachmentFromMS(MemoryStream ms)
        //{
        //    Document document = new Document(PageSize.A4, 80, 50, 30, 65);
        //    PdfWriter writer = PdfWriter.GetInstance(document, ms);
        //    try
        //    {
        //        MemoryStream pdfstream = new MemoryStream(ms.ToArray());
        //        //create attachment
        //        Attachment attachment = new Attachment(pdfstream, "Attachment.pdf");

        //        return attachment;
        //    }
        //    catch (Exception exc)
        //    {
        //        Console.Error.WriteLine(exc.Message);
        //    }
        //    return null;
        //}



        public bool SendEmail(string emailId, string body, string subject, MemoryStream memoryStream = null)
        {
            try
            {
                MailMessage mail = new MailMessage();

                /******* uncomment this codefor attachment **********/
                //System.Net.Mail.Attachment attachment;
                //attachment = GetPDFAttachmentFromMS(memoryStream);
                //mail.Attachments.Add(attachment);

                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("jya.notification@gmail.com");
                mail.To.Add(emailId);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;
                //SmtpServer.Port = 465;
                SmtpServer.Credentials = new System.Net.NetworkCredential("jya.notification@gmail.com", "Admin@1234");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                return true;
            }

            catch (SmtpException ex)
            {
                //throw new ApplicationException
                //  ("SmtpException has occured: " + ex.Message);

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
