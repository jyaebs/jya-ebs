﻿using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.Utilities
{
    public class ReferenceNumberUtility
    {
        public static string GetPurchaseRequisitionNumber(int LocationId)
        {
            PurchaseRequisitionRepository _PurchaseRequisitionRepository = new PurchaseRequisitionRepository();

            DB_PURCHASE_REQUISITION _db_Value = _PurchaseRequisitionRepository.GetMany(p => p.LocationId == LocationId && p.ReferenceNumber.Contains("REQ")).OrderByDescending(trans => trans.ReferenceNumber).FirstOrDefault();
            if (_db_Value != null)
            {
                string LastTransaction = _db_Value.ReferenceNumber.Split('-')[2];
                int counter;
                int.TryParse(LastTransaction, out counter);
                counter++;
                return "REQ" + "-" + LocationId + "-" + counter.ToString().PadLeft(6, '0');
            }
            else
            {
                return "REQ" + "-" + LocationId + "-000001";
            }

        }

        public static string GetPurchaseQuotationNumber(int LocationId)
        {
            PurchaseQuotationRepository _PurchaseQuotationRepository = new PurchaseQuotationRepository();

            DB_PURCHASE_QUOTATION _db_Value = _PurchaseQuotationRepository.GetMany(p => p.LocationId == LocationId && p.ReferenceNumber.Contains("QUO")).OrderByDescending(trans => trans.ReferenceNumber).FirstOrDefault();
            if (_db_Value != null)
            {
                string LastTransaction = _db_Value.ReferenceNumber.Split('-')[2];
                int counter;
                int.TryParse(LastTransaction, out counter);
                counter++;
                return "QUO" + "-" + LocationId + "-" + counter.ToString().PadLeft(6, '0');
            }
            else
            {
                return "QUO" + "-" + LocationId + "-000001";
            }

        }

        public static string GetPurchaseOrderNumber(int LocationId)
        {
            PurchaseOrderRepository _PurchaseOrderRepository = new PurchaseOrderRepository();

            DB_PURCHASE_ORDER _db_Value = _PurchaseOrderRepository.GetMany(p => p.LocationId == LocationId && p.ReferenceNumber.Contains("ODR")).OrderByDescending(trans => trans.ReferenceNumber).FirstOrDefault();
            if (_db_Value != null)
            {
                string LastTransaction = _db_Value.ReferenceNumber.Split('-')[2];
                int counter;
                int.TryParse(LastTransaction, out counter);
                counter++;
                return "ODR" + "-" + LocationId + "-" + counter.ToString().PadLeft(6, '0');
            }
            else
            {
                return "ODR" + "-" + LocationId + "-000001";
            }

        }

        public static string GetInwardGatePassNumber(int LocationId)
        {
            InwardGatePassRepository _InwardGatePassRepository = new InwardGatePassRepository();

            DB_INWARD_GATE_PASS _db_Value = _InwardGatePassRepository.GetMany(p => p.LocationId == LocationId && p.ReferenceNumber.Contains("IGP")).OrderByDescending(trans => trans.ReferenceNumber).FirstOrDefault();
            if (_db_Value != null)
            {
                string LastTransaction = _db_Value.ReferenceNumber.Split('-')[2];
                int counter;
                int.TryParse(LastTransaction, out counter);
                counter++;
                return "IGP" + "-" + LocationId + "-" + counter.ToString().PadLeft(6, '0');
            }
            else
            {
                return "IGP" + "-" + LocationId + "-000001";
            }

        }
    }
}
