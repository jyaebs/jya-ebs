﻿
using Project.DatabaseModels;
using Project.DatabaseModels.DBImplementations;
using Project.DatabaseModels.Repository;
using Project.Models.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;

namespace Project.Utilities
{
    public class WebVersionsUtility
    {
        public static string GetWebVersionNumber()
        {
            return "0.0.0.1";
        }

    }
}