﻿using Project.DatabaseModels;
using Project.DatabaseModels.Repository;
using Project.Models.ViewModels;
using Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Project.Utilities
{
    public class AuthorizationUtility
    {

       
        public static bool userHasPrivilege(string userName, string privilege)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                AspNetUserRepository UserRepository = new AspNetUserRepository();
                RolePrivilgesRepository RolePrivilegesRepository = new RolePrivilgesRepository();
                PrivilegesRepository PrivilegeRepository = new PrivilegesRepository();

                AspNetUser user = UserRepository.Get(AspNetUser => AspNetUser.UserName == userName);
                DB_DEV_PRIVILEGES privilegeDb = PrivilegeRepository.Get(a => a.Privilege == privilege);
                foreach (var aspnet_Role in user.AspNetRoles)
                {
                    foreach (DB_ROLE_PRIVILEGES emp_privilege in aspnet_Role.DB_ROLE_PRIVILEGES)
                    {
                        if (emp_privilege.PRIVILEGE_ID.Equals(privilegeDb.Id))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        public static bool userHasPrivilege(List<AssignPrivilegesViewModel> privileges, string privilege)
        {
            bool hasPrivilege = false;
            if (privileges.Count > 0)
            {
                foreach (var prev in privileges)
                {
                    if (prev.Name == privilege)
                    {
                        hasPrivilege = true;
                        break;
                    }
                }
            }
            return hasPrivilege;
        }

        public static bool userHasPrivilegeCategory(List<AssignPrivilegesViewModel> privileges, string category)
        {
            bool hasCategory = false;
            if (privileges.Count > 0)
            {
                foreach (var prev in privileges)
                {
                    if (prev.Category == category)
                    {
                        hasCategory = true;
                        break;
                    }
                }
            }
            return hasCategory;
        }

        public static bool userHasPrivilegePortal(List<AssignPrivilegesViewModel> privileges, string portal)
        {
            bool hasPortal = false;
            if (privileges.Count > 0)
            {
                foreach (var prev in privileges)
                {
                    if (prev.Portal == portal)
                    {
                        hasPortal = true;
                        break;
                    }
                }
            }
            return hasPortal;
        }
        public static IEnumerable<AssignPrivilegesViewModel> Getuserivilege(string userName)
        {
            RolePrivilgesRepository _RolePrivelageRepository = new RolePrivilgesRepository();
            AspNetUserRepository _UserRepository = new AspNetUserRepository();
            string RoleId = _UserRepository.Get(x => x.UserName == userName).AspNetRoles.FirstOrDefault().Id;
            IEnumerable<AssignPrivilegesViewModel> _data = _RolePrivelageRepository.GetMany(p => p.ROLE_ID == RoleId).Select(p => new AssignPrivilegesViewModel()
            {
                RoleId = p.ROLE_ID,
                Name = p.DB_DEV_PRIVILEGES.Privilege,
                PrivilegeId = p.PRIVILEGE_ID,
                Category = p.DB_DEV_PRIVILEGES.Category,
                Portal = p.DB_DEV_PRIVILEGES.Portal
            });
            return _data;
        }

        
        public static String getUserRoleId(string userName)
        {
            AspNetUserRepository UserRepository = new AspNetUserRepository();
            if (!String.IsNullOrEmpty(userName))
            {
                AspNetUser user = UserRepository.Get(AspNetUser => AspNetUser.UserName == userName);
                if (user.AspNetRoles.Count > 0)
                    return user.AspNetRoles.FirstOrDefault().Id;
                else
                    return null;
            }
            return null;
        }

      
        public static String getUserRoleNamebyRoleId(string roleId)
        {
            AspNetRoleRepository RoleRepository = new AspNetRoleRepository();
            if (!String.IsNullOrEmpty(roleId))
            {
                AspNetRole role = RoleRepository.Get(p => p.Id == roleId);
                if(role!=null)
                    return role.Name;
                else
                    return null;
            }
            return null;
        }

        public static Credentials GetUserCredentialsFromAuthorizationHeader(HttpRequestMessage request)
        {
            try
                {
                    if (request.Headers.Authorization != null)
                    {
                        Credentials credentials = new Credentials();
                        string decodedCredentials = Encoding.ASCII.GetString(Convert.FromBase64String(request.Headers.Authorization.Parameter));

                        string[] credentialsArray = decodedCredentials.Split(':');

                        if (!(credentialsArray.Length < 2 || string.IsNullOrEmpty(credentialsArray[0]) || string.IsNullOrEmpty(credentialsArray[1])))
                        {
                            credentials = new Credentials(credentialsArray[0], credentialsArray[1]);
                        }
                        return credentials;
                    }
                    else
                    {
                        return null;
                    }
                }

                catch (Exception ex)
                {
                  
                    return null;
                }
            
        } 

        public static bool isActiveUser(string username)
        {
            AspNetUserRepository UserRepository = new AspNetUserRepository();
            AspNetUser AspUser = UserRepository.Get(s => s.UserName == username);

            if (AspUser != null && AspUser.DB_USER_PROFILE.Count > 0 && !AspUser.DB_USER_PROFILE.FirstOrDefault().ValidFlag)
            {
                return false;
            }
            return true;
        }
    }
}
