﻿using Project.Models.SiteMap;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.SiteMapUtility
{
    class SiteMap_ProductMaster
    {

        public static List<SiteMapItem> SiteMap(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();

            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.PRODUCT_BRAND_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Product Brand", "#", "bx bx-cog", "", ProductBrandManagement_Child(privileges)));
            }
            
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.PRODUCT_CATEGORY_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Product Category", "#", "bx bx-cog", "", ProductCategoryManagement_Child(privileges)));
            }

            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.PRODUCT_SUB_CATEGORY_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Product Sub Category", "#", "bx bx-cog", "", ProductSubCategoryManagement_Child(privileges)));
            }
            
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.PRODUCT_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Product", "#", "bx bxl-product-hunt", "", ProductManagement_Child(privileges)));
            }

            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.PRODUCT_PRICE_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Product Price", "#", "bx bx-dollar-circle", "", ProductPriceManagement_Child(privileges)));
            }
            
            return list;

        }
      
        
        private static List<SiteMapItem> ProductCategoryManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_PRODUCT_CATEGORY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_PRODUCT_CATEGORY, "../ProductCategoryManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_PRODUCT_CATEGORY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_PRODUCT_CATEGORY, "../ProductCategoryManagement/Manage"));
            }


            return list;
        }
        private static List<SiteMapItem> ProductSubCategoryManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_PRODUCT_SUB_CATEGORY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_PRODUCT_SUB_CATEGORY, "../ProductSubCategoryManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_PRODUCT_SUB_CATEGORY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_PRODUCT_SUB_CATEGORY, "../ProductSubCategoryManagement/Manage"));
            }


            return list;
        }
       
        private static List<SiteMapItem> ProductBrandManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_PRODUCT_BRAND))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_PRODUCT_BRAND, "../ProductBrandManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_PRODUCT_BRAND))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_PRODUCT_BRAND, "../ProductBrandManagement/Manage"));
            }


            return list;
        }



        private static List<SiteMapItem> ProductManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_PRODUCT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_PRODUCT, "../ProductManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_PRODUCT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_PRODUCT, "../ProductManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> ProductPriceManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_PRODUCT_PRICE))
            {
                list.Add(new SiteMapItem("Add Price", "../ProductPriceManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_PRODUCT_PRICE))
            {
                list.Add(new SiteMapItem("View Price", "../ProductPriceManagement/Manage"));
            }


            return list;
        }
    }
}
