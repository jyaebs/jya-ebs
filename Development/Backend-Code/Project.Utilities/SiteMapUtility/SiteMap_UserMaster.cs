﻿using Project.Models.SiteMap;
using Project.ViewModels.Enums;
using Project.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.SiteMapUtility
{
    public class SiteMap_UserMaster
    {
        public static List<SiteMapItem> SiteMap(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();

            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.USER_MANAGEMENT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesCategory.USER_MANAGEMENT, "#", "bx bx-user-circle", "", UserManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.ROLE_MANAGEMENT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesCategory.ROLE_MANAGEMENT, "../RoleManagement/Index", "bx bx-toggle-right", "", null));
            }
            return list;

        }

        private static List<SiteMapItem> UserManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_USER))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_USER, "../UserManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_USER))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_USER, "../UserManagement/Manage"));
            }


            return list;
        }
          }
}
