﻿using Project.Models.SiteMap;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.SiteMapUtility
{
    public class SiteMap_SalesMaster
    {
        public static List<SiteMapItem> SiteMap(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();

            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.CUSTOMER_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Customer", "#", "bx bx-bus", "", CustomerManagement_Child(privileges)));
            }

            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.VEHICLE_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Vehicle Management", "#", "bx bx-bus", "", VehicleManagement_Child(privileges)));
            }
            
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.DRIVER_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Driver Management", "#", "bx bx-user", "", DriverManagement_Child(privileges)));
            }
            
            return list;
        }
        

        private static List<SiteMapItem> DriverManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_DRIVER))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_DRIVER, "../DriverManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_DRIVER))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_DRIVER, "../DriverManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> VehicleManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_VEHICLE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_VEHICLE, "../VehicleManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_VEHICLE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_VEHICLE, "../VehicleManagement/Manage"));
            }
            
            return list;
        }

        private static List<SiteMapItem> CustomerManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_CUSTOMER))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_CUSTOMER, "../CustomerManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_CUSTOMER))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_CUSTOMER, "../CustomerManagement/Manage"));
            }


            return list;
        }
    }
}
