﻿using Project.Models.SiteMap;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.SiteMapUtility
{
    class SiteMap_Setup
    {

        public static List<SiteMapItem> SiteMap(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
          
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.PRIMARY_UOM_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Primary UOM", "#", "bx bx-cog", "", PrimaryUOMManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.SECONDARY_UOM_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Secondary UOM", "#", "bx bx-cog", "", SecondaryUOMManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.GST_MANAGEMENT))
            {
                list.Add(new SiteMapItem("GST Management", "#", "bx bx-money", "", GSTManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.WHT_MANAGEMENT))
            {
                list.Add(new SiteMapItem("WHT Management", "#", "bx bx-money", "", WHTManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.VEHICLE_TYPE_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Vehicle Type", "#", "bx bx-bus", "", VehicleTypeManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.WAREHOUSE_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Warehouse", "#", "bx bx-bus", "", WarehouseManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.BANK_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Bank", "#", "bx bx-bus", "", BankManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.BANK_BRANCH_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Bank Branch", "#", "bx bx-bus", "", BankBranchManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.CUSTOMER_TYPE_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Customer Type", "#", "bx bx-bus", "", CustomerTypeManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.VENDOR_TYPE_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Vendor Type", "#", "bx bx-bus", "", VendorTypeManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.VENDOR_MANAGEMENT))
            {
                list.Add(new SiteMapItem("Vendor", "#", "bx bx-bus", "", VendorManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.COUNTRY_MANAGEMENT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesCategory.COUNTRY_MANAGEMENT, "#", "bx bx-world", "", CountryManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.CITY_MANAGEMENT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesCategory.CITY_MANAGEMENT, "#", "bx bx-world", "", CityManagement_Child(privileges)));
            }
            if (AuthorizationUtility.userHasPrivilegeCategory(privileges, EnumPrivilegesCategory.LOCATION_MANAGEMENT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesCategory.LOCATION_MANAGEMENT, "#", "bx bxs-bar-chart-alt-2", "", LocationManagement_Child(privileges)));
            }
            
            return list;

        }

        private static List<SiteMapItem> WarehouseManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_WAREHOUSE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_WAREHOUSE, "../WarehouseManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_WAREHOUSE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_WAREHOUSE, "../WarehouseManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> BankManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_BANK))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_BANK, "../BankManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_BANK))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_BANK, "../BankManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> BankBranchManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_BANK_BRANCH))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_BANK_BRANCH, "../BankBranchManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_BANK_BRANCH))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_BANK_BRANCH, "../BankBranchManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> VendorTypeManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_VENDOR_TYPE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_VENDOR_TYPE, "../VendorTypeManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_VENDOR_TYPE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_VENDOR_TYPE, "../VendorTypeManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> VendorManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_VENDOR))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_VENDOR, "../VendorManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_VENDOR))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_VENDOR, "../VendorManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> CustomerTypeManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_CUSTOMER_TYPE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_CUSTOMER_TYPE, "../CustomerTypeManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_CUSTOMER_TYPE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_CUSTOMER_TYPE, "../CustomerTypeManagement/Manage"));
            }


            return list;
        }

        private static List<SiteMapItem> PrimaryUOMManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_PRIMARYUOM))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_PRIMARYUOM, "../PrimaryUOMManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_PRIMARYUOM))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_PRIMARYUOM, "../PrimaryUOMManagement/Manage"));
            }


            return list;
        }
        private static List<SiteMapItem> SecondaryUOMManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_SECONDARYUOM))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_SECONDARYUOM, "../SecondaryUOMManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_SECONDARYUOM))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_SECONDARYUOM, "../SecondaryUOMManagement/Manage"));
            }


            return list;
        }
        private static List<SiteMapItem> GSTManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_GST))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_GST, "../GSTManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_GST))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_GST, "../GSTManagement/Manage"));
            }
            
            return list;
        }
        private static List<SiteMapItem> WHTManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_WHT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_WHT, "../WHTManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_WHT))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_WHT, "../WHTManagement/Manage"));
            }

            return list;
        }
        private static List<SiteMapItem> VehicleTypeManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_VEHICLE_TYPE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_VEHICLE_TYPE, "../VehicleTypeManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_VEHICLE_TYPE))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_VEHICLE_TYPE, "../VehicleTypeManagement/Manage"));
            }


            return list;
        }


        private static List<SiteMapItem> CountryManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_COUNTRY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_COUNTRY, "../CountryManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_COUNTRY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_COUNTRY, "../CountryManagement/Manage"));
            }


            return list;
        }
        private static List<SiteMapItem> CityManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_CITY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_CITY, "../CityManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_CITY))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_CITY, "../CityManagement/Manage"));
            }


            return list;
        }
        private static List<SiteMapItem> LocationManagement_Child(List<AssignPrivilegesViewModel> privileges)
        {
            List<SiteMapItem> list = new List<SiteMapItem>();
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.ADD_LOCATION))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.ADD_LOCATION, "../LocationManagement/Add"));
            }
            if (AuthorizationUtility.userHasPrivilege(privileges, EnumPrivilegesName.VIEW_LOCATION))
            {
                list.Add(new SiteMapItem(EnumPrivilegesName.VIEW_LOCATION, "../LocationManagement/Index"));
            }
            return list;
        }
    }
}
