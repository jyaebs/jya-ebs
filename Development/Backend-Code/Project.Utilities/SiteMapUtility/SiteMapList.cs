﻿using Project.Models.SiteMap;
using Project.Models.ViewModels;
using Project.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Utilities.SiteMapUtility
{
    class SiteMapList
    {

        public static List<SiteMapItem> getSiteMapList(String username)
        {
            List<AssignPrivilegesViewModel> privileges = AuthorizationUtility.Getuserivilege(username).ToList();

            List<SiteMapItem> SiteMapList = new List<SiteMapItem>();
            
            if (AuthorizationUtility.userHasPrivilegePortal(privileges, EnumPrivilegesPortal.DATA_DICIONARY))
            {
                List<SiteMapItem> setup = SiteMap_Setup.SiteMap(privileges);
                SiteMapList.Add(new SiteMapItem(EnumPrivilegesPortal.DATA_DICIONARY.ToString(), "#", "i-Data-Settings", EnumPrivilegesPortal.DATA_DICIONARY.ToString().Trim(), setup));
            }

            if (AuthorizationUtility.userHasPrivilegePortal(privileges, EnumPrivilegesPortal.USER_MASTER))
            {
                List<SiteMapItem> UserMaster = SiteMap_UserMaster.SiteMap(privileges);
                SiteMapList.Add(new SiteMapItem(EnumPrivilegesPortal.USER_MASTER.ToString(), "#", "i-Business-Mens", EnumPrivilegesPortal.USER_MASTER.ToString().Trim(), UserMaster));
            }

            
            if (AuthorizationUtility.userHasPrivilegePortal(privileges, EnumPrivilegesPortal.PRODUCT_MASTER))
            {
                List<SiteMapItem> ProductMaster = SiteMap_ProductMaster.SiteMap(privileges);
                SiteMapList.Add(new SiteMapItem(EnumPrivilegesPortal.PRODUCT_MASTER.ToString(), "#", "i-Android-Store", EnumPrivilegesPortal.PRODUCT_MASTER.ToString().Trim(), ProductMaster));
            }
            
            if (AuthorizationUtility.userHasPrivilegePortal(privileges, EnumPrivilegesPortal.SALES_MASTER))
            {
                List<SiteMapItem> SalesMaster = SiteMap_SalesMaster.SiteMap(privileges);
                SiteMapList.Add(new SiteMapItem(EnumPrivilegesPortal.SALES_MASTER.ToString(), "#", "i-Shopping-Cart", EnumPrivilegesPortal.SALES_MASTER.ToString().Trim(), SalesMaster));
            }
            return SiteMapList;
        }    
     
    }
}
